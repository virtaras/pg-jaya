module lobby {
	import Point = createjs.Point;

	export class CommonRefs {
		public layouts:Object;
		public config:any;
		public canvasHolderName:string;
		public artWidth:number;
		public artHeight:number;
		public currentGame:any;
		public key:string;
		public login:string;
		public password:string;

		constructor() {
		}
	}
}