module lobby {
	import LoadQueue = createjs.LoadQueue;
	import LayoutCreator = layout.LayoutCreator;
	import Layout = layout.Layout;

	export class LoaderController extends BaseController {
		private common:CommonRefs;

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
			this.loadConfig();
		}

		private loadConfig():void {
			var configLoader:LoadQueue = new LoadQueue(false);
			var configUrl:string = LobbyConst.MAIN_RES_FOLDER + LobbyConst.LOBBY_RES_FOLDER + LobbyConst.GAME_CONFIG_NAME + FileConst.JSON_EXTENSION;
			configLoader.on("complete", () => {
				this.common.config = configLoader.getResult(configUrl);
				this.loadFonts();
			});
			configLoader.loadFile(configUrl);
		}

		private loadFonts():void {
			var fonts:Array<any> = this.common.config.fonts;
			if (fonts != null && fonts.length > 0) {
				var style:HTMLElement = document.createElement('style');
				var manifest:Array<any> = [];

				for (var i:number = 0; i < fonts.length; i++) {
					var fontName:string = fonts[i].name;
					var fontURL:string = LobbyConst.MAIN_RES_FOLDER + LobbyConst.LOBBY_RES_FOLDER + LobbyConst.FONTS_FOLDER + fonts[i].fileName + FileConst.WOFF_EXTENSION;
					style.appendChild(document.createTextNode("@font-face {font-family: '" + fontName + "'; src: url('" + fontURL + "');}"));
					document.head.appendChild(style);
					manifest.push({"id": fontName, "src": fontURL});
				}

				var configLoader:LoadQueue = new LoadQueue(false);
				configLoader.on("complete", () => {
					this.loadLayouts();
				});
				configLoader.loadManifest(manifest);
			}
			else {
				this.loadLayouts();
			}
		}

		private loadLayouts():void {
			var layouts:Object = {};
			var layoutNames:Array<string> = <Array<string>>this.common.config.layouts;
			for (var i:number = 0; i < layoutNames.length; i++) {
				var layoutName:string = layoutNames[i];
				var LayoutClass:any = lobby[layoutName];
				layouts[layoutName] = LayoutClass ? new LayoutClass() : new LayoutCreator();
			}

			var needLoad:number = layoutNames.length;
			var leftLoad:number = needLoad;
			for (var i:number = 0; i < layoutNames.length; i++) {
				var layoutName:string = layoutNames[i];
				var layoutObj:Layout = layouts[layoutName];
				layoutObj.on(LayoutCreator.EVENT_LOADED, ()=> {
					leftLoad -= 1;
					var progress:number = (needLoad - leftLoad) / needLoad;
					console.log("Loading progress: " + progress);
					if (leftLoad == 0) {
						super.send(NotificationList.RES_LOADED);
					}
					else {
						// TODO: update progress bar
					}
				});
				layoutObj.load(LobbyConst.MAIN_RES_FOLDER + LobbyConst.LOBBY_RES_FOLDER + "/" + LobbyConst.LAYOUT_FOLDER + layoutName + FileConst.LAYOUT_EXTENSION);
			}
			this.common.layouts = layouts;
		}
	}
}