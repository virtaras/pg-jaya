module lobby {

	export class ServerController extends BaseController {
		private authorizationUrl:string = "onlinecasino/common.asmx/Authorization";
		private closeSessionUrl:string = "onlinecasino/common.asmx/Close";
		private getBalanceUrl:string = "onlinecasino/common.asmx/GetBalance";

		private common:CommonRefs;

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.RES_LOADED);
			notifications.push(NotificationList.AUTHORIZATION);
			notifications.push(NotificationList.LOGOUT);
			notifications.push(NotificationList.DISCONNECT);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.RES_LOADED:
				{
					var baseServerUrl:string = this.common.config.serverUrl;
					this.authorizationUrl = (baseServerUrl + this.authorizationUrl).toLowerCase();
					this.getBalanceUrl = (baseServerUrl + this.getBalanceUrl).toLowerCase();
					this.closeSessionUrl = (baseServerUrl + this.closeSessionUrl).toLowerCase();
					break;
				}
				case NotificationList.AUTHORIZATION:
				{
					this.sendLoginRequest(this.common.login, this.common.password, ()=> {
						this.send(NotificationList.AUTHORIZED);
					});
					break;
				}
				case NotificationList.LOGOUT:
				case NotificationList.DISCONNECT:
				{
					this.sendDisconnectRequest();
					break;
				}
			}
		}

		private sendLoginRequest(login:string, password:string, onComplete:Function):void {
			var passwordMD5:string = CryptoJS.MD5(password).toString();
			var request:AjaxRequest = new AjaxRequest(this.authorizationUrl, true, {"login": login, "password": passwordMD5});
			request.get((responseData)=> {
				var result:any = responseData.documentElement.textContent;
				switch (result) {
					case "1001":
					{
						console.log("ERROR: User already authorized!");
						super.send(NotificationList.LOGOUT);
						return;
					}
				}
				if (result == null || result.length == 0) {
					super.send(NotificationList.LOGOUT);
					return;
				}
				this.common.key = result;
				console.log("Key: " + result);
				onComplete();
			});
		}

		private sendDisconnectRequest():void {
			if (this.common.key != null) {
				var request:AjaxRequest = new AjaxRequest(this.closeSessionUrl, false, {key: this.common.key});
				request.get();
			}
		}
	}
}