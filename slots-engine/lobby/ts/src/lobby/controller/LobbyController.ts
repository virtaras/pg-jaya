module lobby {
	import Layout = layout.Layout;
	import Stage = createjs.Stage;
	import Ticker = createjs.Ticker;
	import Container = createjs.Container;

	export class LobbyController extends BaseController {
		private common:CommonRefs;
		private lobbyContainer:Container;
		private gameContainer:Container;
		private stage:Stage;

		constructor(artWidth:number, artHeight:number, canvasHolderName:string) {
			super(new ControllerManager());
			this.common = new CommonRefs();
			this.common.canvasHolderName = canvasHolderName;
			this.common.artWidth = artWidth;
			this.common.artHeight = artHeight;
		}

		public start():void {
			super.init();

			var canvas:HTMLCanvasElement = document.createElement("canvas");
			// if the method is not supported, i.e canvas is not supported
			if (!canvas.getContext) {
				document.write("Your browser does not support the HTML5 canvas element.");
				return;
			}
			canvas.style.position = "absolute";

			var canvasHolder:HTMLElement = document.getElementById(this.common.canvasHolderName);
			canvasHolder.appendChild(canvas);

			this.stage = new Stage(canvas);
			this.stage.enableMouseOver(LobbyConst.FPS);

			this.lobbyContainer = new Container();
			this.stage.addChild(this.lobbyContainer);
			this.gameContainer = new Container();
			this.stage.addChild(this.gameContainer);

			screen.fontSmoothingEnabled = true;

			Ticker.setFPS(LobbyConst.FPS);
			Ticker.addEventListener("tick", ()=> {
				this.manager.onEnterFrame();
			});

			this.onResize();
			window.onresize = ()=> {
				this.onResize();
			};

			this.startLoading();
		}

		public onEnterFrame():void {
			this.stage.update();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.RES_LOADED);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.RES_LOADED:
				{
					this.onLoaded();
					break;
				}
			}
		}

		public dispose():void {
			this.send(NotificationList.DISCONNECT);
		}

		private startLoading():void {
			var serverController:ServerController = new ServerController(this.manager, this.common);
			serverController.init();

			var loaderController:LoaderController = new LoaderController(this.manager, this.common);
			loaderController.init();
		}

		private onLoaded():void {
			var loginController:LoginController = new LoginController(this.manager, this.common, this.lobbyContainer);
			loginController.init();

			var selectGameController:SelectGameController = new SelectGameController(this.manager, this.common, this.lobbyContainer);
			selectGameController.init();

			var gameController:GameController = new GameController(this.manager, this.common, this.gameContainer);
			gameController.init();
		}

		private onResize():void {
			var maxWidth:number = window.innerWidth;
			var maxHeight:number = window.innerHeight;

			// keep aspect ratio
			var scaleLobby = Math.min(maxWidth / this.common.artWidth, maxHeight / this.common.artHeight);
			this.lobbyContainer.scaleX = scaleLobby;
			this.lobbyContainer.scaleY = scaleLobby;
			this.lobbyContainer.x = (maxWidth - this.common.artWidth * scaleLobby) / 2;
			this.lobbyContainer.y = (maxHeight - this.common.artHeight * scaleLobby) / 2;

			if (this.common.currentGame != null) {
				var scaleGame = Math.min(maxWidth / this.common.currentGame.artWidth, maxHeight / this.common.currentGame.artHeight);
				this.gameContainer.scaleX = scaleGame;
				this.gameContainer.scaleY = scaleGame;
				this.gameContainer.x = (maxWidth - this.common.currentGame.artWidth * scaleGame) / 2;
				this.gameContainer.y = (maxHeight - this.common.currentGame.artHeight * scaleGame) / 2;
			}

			// adjust canvas size
			var canvas:HTMLCanvasElement = this.stage.canvas;
			canvas.width = maxWidth;
			canvas.height = maxHeight;
		}
	}
}