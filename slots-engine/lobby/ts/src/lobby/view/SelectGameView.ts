module lobby {
	import Layout = layout.Layout;
	import Button = layout.Button;
	import Container = createjs.Container;
	import Sprite = createjs.Sprite;
	import Shape = createjs.Shape;
	import Rectangle = createjs.Rectangle;
	import Text = createjs.Text;
	import Ease = createjs.Ease;
	import Tween = createjs.Tween;
	import Bitmap = createjs.Bitmap;
	import LoadQueue = createjs.LoadQueue;

	export class SelectGameView extends Layout {
		public static LAYOUT_NAME:string = "SelectGameView";

		private static CHANGE_PAGE_TIME:number = 0.3;

		public static PREV_PAGE_BTN:string = "prevPageBtn";
		public static NEXT_PAGE_BTN:string = "nextPageBtn";
		public static BACK_BTN:string = "backBtn";
		public static GAME_ICONS_CONTENT:string = "content";
		public static GAME_ICON_PREFIX:string = "icon_";
		public static GAME_ICON_LABEL:string = "label";
		public static GAME_ICON_NAME:string = "icon";

		private gameIcons:Array<Container>;
		private content:Container;
		private prevPageBtn:Button;
		private nextPageBtn:Button;
		private backBtn:Button;
		private iconsDiv:number;

		constructor() {
			super();
			this.gameIcons = [];
		}

		public onInit():void {
			this.content = <Container>this.getChildByName(SelectGameView.GAME_ICONS_CONTENT);
			this.createMask();
			this.initIcons();
			this.initButtons();
		}

		private createMask():void {
			var bounds:Rectangle = this.content.getBounds();
			var mask:Shape = new Shape();
			// TODO: hack bounds.height * 2
			mask.graphics.beginFill("0").drawRect(this.content.x + bounds.x, this.content.y + bounds.y, bounds.width, bounds.height * 2);
			this.content.mask = mask;
		}

		private initButtons():void {
			this.prevPageBtn = <Button>this.getChildByName(SelectGameView.PREV_PAGE_BTN);
			this.nextPageBtn = <Button>this.getChildByName(SelectGameView.NEXT_PAGE_BTN);
			this.backBtn = <Button>this.getChildByName(SelectGameView.BACK_BTN);
		}

		private initIcons():void {
			var gameId:number = 1;
			var gameIcon:Container;
			while ((gameIcon = <Container>this.content.getChildByName(SelectGameView.GAME_ICON_PREFIX + gameId)) != null) {
				this.gameIcons.push(gameIcon);
				gameId++;
			}
			this.iconsDiv = this.gameIcons[1].x - this.gameIcons[0].x - this.gameIcons[0].getBounds().width;
		}

		public tweenPage(isNextPage:boolean):void {
			var cloneContent = this.content.clone(true);
			cloneContent.x = this.content.x;
			cloneContent.y = this.content.y;
			cloneContent.mask = this.content.mask;
			this.addChild(cloneContent);

			var div:number = (this.content.getBounds().width + this.iconsDiv) * (isNextPage ? -1 : 1);
			this.content.x -= div;
			Tween.get(this.content, {useTicks: true})
				.to({x: this.content.x + div}, SelectGameView.CHANGE_PAGE_TIME * LobbyConst.FPS, Ease.backOut);
			Tween.get(cloneContent, {useTicks: true})
				.to({x: cloneContent.x + div}, SelectGameView.CHANGE_PAGE_TIME * LobbyConst.FPS, Ease.backOut)
				.call(()=> {
					this.removeChild(cloneContent);
				}
			);
		}

		public updatePage(gamesParams:Array<any>, pageId:number):void {
			var gameIcons:Array<Container> = this.gameIcons;
			var gamesCount:number = gamesParams.length;
			var gameIconsCount:number = this.gameIcons.length;
			var manifest:Array<any> = [];
			var j:number = pageId * gameIconsCount;
			for (var i:number = 0; i < gameIconsCount; i++) {
				var gameId:number = j + i;
				var gameIcon:Container = gameIcons[i];
				if (gameId < gamesCount) {
					var gameParams:any = gamesParams[gameId];
					var label:Text = <Text>gameIcon.getChildByName(SelectGameView.GAME_ICON_LABEL);
					label.text = gameParams.label;

					// remove old icon
					var icon:Bitmap = <Bitmap>gameIcon.getChildByName(SelectGameView.GAME_ICON_NAME);
					if (icon != null) {
						gameIcon.removeChild(icon);
					}
					var iconUrl:string = LobbyConst.MAIN_RES_FOLDER + LobbyConst.LOBBY_RES_FOLDER + LobbyConst.ICONS_FOLDER + gameParams.iconUrl;
					manifest.push({id: gameIcon.name, src: iconUrl});

					gameIcon.alpha = 1;
				}
				else {
					gameIcon.alpha = 0;
				}
			}
			// start load new icon
			var queue:LoadQueue = new LoadQueue(true);
			queue.on("fileload", (eventObj:any) => {
				var iconName:string = eventObj.item.id;
				var result:any = eventObj.result;
				var icon:Bitmap = new Bitmap(result);
				var gameIcon:Container = <Container>this.content.getChildByName(iconName);
				var bounds:Rectangle = icon.getBounds();
				icon.regX = bounds.width >> 1;
				icon.regY = bounds.height >> 1;
				gameIcon.addChild(icon);
			});
			queue.loadManifest(manifest);

			this.nextPageBtn.setEnable(gamesCount > (pageId + 1) * gameIconsCount);
			this.prevPageBtn.setEnable(pageId > 0);
		}

		public getIcons():Array<Container> {
			return this.gameIcons;
		}

		public getPrevPageBtn():Button {
			return this.prevPageBtn;
		}

		public getNextPageBtn():Button {
			return this.nextPageBtn;
		}

		public getBackBtn():Button {
			return this.backBtn;
		}

		public dispose():void {
			this.parent.removeChild(this);
		}
	}
}