module lobby {
	import Layout = layout.Layout;
	import Container = createjs.Container;
	import Text = createjs.Text;

	export class LoginView extends Layout {
		public static LAYOUT_NAME:string = "LoginView";

		private static KEYBOARD:string = "keyboard";
		private static INPUT_TEXT:string = "inputTf";

		private static ZERO_BTN:string = "zeroBtn";
		private static ONE_BTN:string = "oneBtn";
		private static TWO_BTN:string = "twoBtn";
		private static THREE_BTN:string = "threeBtn";
		private static FOUR_BTN:string = "fourBtn";
		private static FIVE_BTN:string = "fiveBtn";
		private static SIX_BTN:string = "sixBtn";
		private static SEVEN_BTN:string = "sevenBtn";
		private static EIGHT_BTN:string = "eightBtn";
		private static NINE_BTN:string = "nineBtn";

		public static NUM_BUTTONS:Array<string> = [
			LoginView.ZERO_BTN,
			LoginView.ONE_BTN,
			LoginView.TWO_BTN,
			LoginView.THREE_BTN,
			LoginView.FOUR_BTN,
			LoginView.FIVE_BTN,
			LoginView.SIX_BTN,
			LoginView.SEVEN_BTN,
			LoginView.EIGHT_BTN,
			LoginView.NINE_BTN,
		];

		public static OK_BTN:string = "okBtn";
		public static CANCEL_BTN:string = "cancelBtn";

		private keyboard:Container;
		private inputTf:Text;

		constructor() {
			super();
		}

		public onInit():void {
			this.keyboard = <Container>this.getChildByName(LoginView.KEYBOARD);
			this.inputTf = <Text>this.keyboard.getChildByName(LoginView.INPUT_TEXT);
		}

		public getKeyboard():Container {
			return this.keyboard;
		}

		public getInputTf():Text {
			return this.inputTf;
		}

		public dispose():void {
			this.parent.removeChild(this);
		}
	}
}