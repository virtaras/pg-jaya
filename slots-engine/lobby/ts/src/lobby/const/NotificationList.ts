module lobby {
	export class NotificationList {
		public static RES_LOADED:string = "res_loaded";
		public static AUTHORIZATION:string = "authorization";
		public static DISCONNECT:string = "disconnect";
		public static AUTHORIZED:string = "authorized";
		public static LOGOUT:string = "logout";
		public static START_GAME:string = "start_game";
	}
}
