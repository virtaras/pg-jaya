module lobby {
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;

	export class Utils {
		public static getClassName(obj:any) {
			var funcNameRegex = /function (.{1,})\(/;
			var results = (funcNameRegex).exec(obj["constructor"].toString());
			return (results && results.length > 1) ? results[1] : "";
		}
	}
}