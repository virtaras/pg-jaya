///<reference path="easeljs.d.ts" />
///<reference path="createjs.d.ts" />
///<reference path="preloadjs.d.ts" />

declare module layout {
	import Container = createjs.Container;
	import LoadQueue = createjs.LoadQueue;
	import EventDispatcher = createjs.EventDispatcher;
	import SpriteSheet = createjs.SpriteSheet;
	import Bitmap = createjs.Bitmap;

	class LayoutCreator {
		static EVENT_LOADED:string;

		constructor();

		load(url:string):void;

		create(container:Container):void;
	}

	class Layout extends Container {
		constructor();

		load(url?:string, isAuto?:boolean):void;

		create():void;

		onInit():void;
	}

	class TextVO extends BaseVO {
		static CLASS_NAME:string;
		text:string;
		width:number;
		height:number;
		font:string;
		color:number;
		size:number;
		align:string;
		bold:boolean;
		italic:boolean;
		filters:Array<BaseFilterVO>;

		constructor();
	}

	class SpriteVO extends BaseVO {
		static CLASS_NAME:string;
		displays:Array<BaseVO>;

		constructor();

		read(data:any):void;
	}

	class SimpleMovieVO extends BaseVO {
		static CLASS_NAME:string;
		fileName:string;

		constructor();

		read(data:any):void;
	}

	class ImageVO extends BaseVO {
		static CLASS_NAME:string;
		fileName:string;

		constructor();

		read(data:any):void;
	}

	class BaseVO {
		className:string;
		name:string;
		x:number;
		y:number;

		constructor();

		parse(data:any):BaseVO;

		read(data:any):void;
	}

	class BaseFilterVO {
		constructor();

		parse(data:any):BaseFilterVO;

		read(data:any):void;
	}

	class BlurFilterVO extends BaseFilterVO {
		static CLASS_NAME:string;
		blurX:number;
		blurY:number;
		quality:number;

		constructor();

		read(data:any):void;
	}

	class DropShadowFilterVO extends GlowFilterVO {
		static CLASS_NAME:string;
		distance:number;
		angle:number;
		hideObject:boolean;

		constructor();

		read(data:any):void;
	}

	class GlowFilterVO extends BlurFilterVO {
		static CLASS_NAME:string;

		color:number;
		alpha:number;
		strength:number;
		inner:boolean;
		knockout:boolean;

		constructor();

		read(data:any):void;
	}

	class ButtonStateVO {
		imageVO:ImageVO;
		name:string;

		constructor();

		read(data:any):void;
	}

	class ButtonVO extends BaseVO {
		static CLASS_NAME:string;

		states:Array<ButtonStateVO>;

		constructor();

		read(data:any):void;
	}

	class AssetFinder {
		constructor();

		find(vo:BaseVO):void;

		getAssets():Array<AssetVO>;
	}

	class StarlingSpriteSheet {
		static create(xml:Document, image:any):SpriteSheet;
	}

	class URLUtils {
		static getFolderUrl(url:string):string;
	}

	class Button extends Bitmap {
		constructor(textures:any);

		getEnable():boolean;

		setEnable(value:boolean):void;
	}

	class LayoutLoader extends EventDispatcher {
		static EVENT_ON_LOADED:string;
		static LAYOUT_ASSET_ID:string;

		constructor(url:string);

		load():void;

		getVO():BaseVO;

		getAssets():LoadQueue;
	}

	class AssetVO {
		id:string;
		src:string;
		type:string;

		constructor(id:string, src:string, type:string);
	}
}