var engine;
(function (engine) {
    var BonusTypes = (function () {
        function BonusTypes() {
        }
        BonusTypes.GAMBLE = "gamble";
        BonusTypes.FREE_SPINS = "free_spins";
        BonusTypes.MINI_GAME = "mini_game";
        return BonusTypes;
    })();
    engine.BonusTypes = BonusTypes;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var GameConst = (function () {
        function GameConst() {
        }
        GameConst.DEMO_AUTHORIZATION_URL = "onlinecasino/common.asmx/DemoSession";
        GameConst.AUTHORIZATION_URL = "onlinecasino/common.asmx/Authorization";
        GameConst.AUTHORIZATION_ROOM = "onlinecasino/common.asmx/authorizationUIS";
        GameConst.CLOSE_SESSION_URL = "onlinecasino/common.asmx/Close";
        GameConst.GET_BALANCE_URL = "onlinecasino/common.asmx/GetBalance";
        GameConst.GET_BETS_URL = "onlinecasino/games/%s.asmx/GetBets";
        GameConst.SPIN_URL = "onlinecasino/games/%s.asmx/Spin";
        GameConst.BONUS_URL = "onlinecasino/games/%s.asmx/BonusGame";
        GameConst.SET_WHEEL_URL = "onlinecasino/games/%s.asmx/SetWheel";
        GameConst.MAIN_RES_FOLDER = "assets/";
        GameConst.LAYOUT_FOLDER = "layout/";
        GameConst.FONTS_FOLDER = "fonts/";
        GameConst.SOUNDS_FOLDER = "sounds/";
        GameConst.GAME_CONFIG_NAME = "config";
        GameConst.GAME_FPS = 30;
        //JayaSettings
        GameConst.ID_JAYA = 4;
        GameConst.SERVER_URL_JAYA = 'http://54.229.136.253:3001';
        return GameConst;
    })();
    engine.GameConst = GameConst;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var NotificationList = (function () {
        function NotificationList() {
        }
        NotificationList.BACK_TO_LOBBY = "back_to_lobby";
        NotificationList.HIDE_PRELOADER = "hide_preloader";
        NotificationList.SERVER_INIT = "server_init";
        NotificationList.SERVER_SEND_SPIN = "send_spin";
        NotificationList.SERVER_GOT_SPIN = "got_spin";
        NotificationList.SERVER_SEND_BONUS = "send_bonus";
        NotificationList.SERVER_GOT_BONUS = "got_bonus";
        NotificationList.SERVER_SET_WHEEL = "set_wheel";
        NotificationList.SERVER_DISCONNECT = "disconnect";
        NotificationList.RES_LOADED = "res_loaded";
        NotificationList.LOAD_SOUNDS = "load_sounds";
        NotificationList.SOUNDS_LOADED = "sounds_loaded";
        NotificationList.KEYBOARD_CLICK = "keyboard_click";
        NotificationList.ON_SCREEN_CLICK = "on_screen_click";
        NotificationList.TRY_START_AUTO_PLAY = "try_start_auto_play";
        NotificationList.END_AUTO_PLAY = "end_auto_play";
        NotificationList.TRY_START_SPIN = "try_start_spin";
        NotificationList.WIN_POPUP_SHOWED = "ready_to_start_spin";
        NotificationList.START_SPIN = "start_spin";
        NotificationList.EXPRESS_STOP = "express_stop";
        NotificationList.STOPPED_REEL_ID = "stopped_reel_id";
        NotificationList.STOPPED_REEL_ID_PREPARE = "stopped_reel_id_prepare";
        NotificationList.STOPPED_ALL_REELS = "stopped_all_reels";
        NotificationList.CHANGED_LINE_COUNT = "changed_line_count";
        NotificationList.OPEN_PAY_TABLE = "open_pay_table";
        NotificationList.CLOSE_PAY_TABLE = "close_pay_table";
        NotificationList.OPEN_AUTO_SPIN_MENU = "open_auto_spin_menu";
        NotificationList.TOGGLE_SETTINGS_MENU = "toggle_settings_menu";
        NotificationList.CLOSE_SETTINGS_MENU = "close_settings_menu";
        NotificationList.SOUND_TOGGLE = "sound_toggle";
        NotificationList.SHOW_WIN_LINES = "show_win_lines";
        NotificationList.WIN_LINES_SHOWED = "win_lines_showed";
        NotificationList.REMOVE_WIN_LINES = "remove_vin_lines";
        NotificationList.SHOW_LINES = "show_line";
        NotificationList.HIDE_LINES = "hide_lines";
        NotificationList.SHOW_HEADER = "show_header";
        NotificationList.REMOVE_HEADER = "remove_header";
        NotificationList.UPDATE_BALANCE_TF = "update_balance";
        NotificationList.SHOW_WIN_TF = "show_win";
        NotificationList.COLLECT_WIN_TF = "collect_balance";
        NotificationList.CREATE_BONUS = "create_bonus";
        NotificationList.START_BONUS = "start_bonus";
        NotificationList.END_BONUS = "end_bonus";
        NotificationList.SHOW_ALL_SYMBOLS = "show_all_symbols";
        NotificationList.HIDE_SYMBOLS = "hide_symbols";
        NotificationList.GET_ALL_IDS = "get_all_ids";
        NotificationList.LAZY_LOAD = "lazy_load";
        NotificationList.LAZY_LOAD_COMP = "lazy_load_compl";
        NotificationList.CHANGE_BET = "change_bet";
        NotificationList.CHANGE_DEVICE_ORIENTATION = "change device orientation";
        NotificationList.OPEN_SELECT_BET_TABLE = 'open_select_bet_table';
        NotificationList.CLOSE_SELECT_BET_TABLE = 'close_select_bet_table';
        NotificationList.SET_BET = 'set_bet';
        NotificationList.OPEN_OPTIONS_MENU = 'open_optoions_menu';
        NotificationList.INIT_OPTIONS_STATE = 'init_opt_state';
        NotificationList.EVENT_All_BONUSES_SHOWED = 'all_scatters_showed';
        NotificationList.END_FREE_SPINS = 'end_free_spins';
        NotificationList.SHOW_START_BONUS_MODAL = 'show_start_bonus_modal';
        NotificationList.START_WEEL_ROTATE = 'start_weel_rotate';
        NotificationList.STOP_WEEL_ROTATE = 'stop_weel_rotate';
        NotificationList.SCATTER_WIN = 'scatter_win';
        NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED = 'win_lines_and_win_popup_showed';
        NotificationList.PLAY_WIN_SOUND = 'play_win_sound';
        NotificationList.SHOW_WILD_REEL_FADEIN = "show_wild_reel_fadein";
        NotificationList.SHOW_WILD_REEL = "show_wild_reel";
        NotificationList.FREESPINS_SHOW_LEFT = "freespins_show_left";
        NotificationList.FREESPINS_SHOW_LEFT_FIELDS = "freespins_show_left_fields";
        NotificationList.SHOW_ERRORS = "show_errors";
        NotificationList.OK_BTN_ERROR_CLICKED = "ok_btn_error_clicked";
        return NotificationList;
    })();
    engine.NotificationList = NotificationList;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var SoundsList = (function () {
        function SoundsList() {
        }
        //-------------SOUND NAMES---------------------------------
        SoundsList.REGULAR_CLICK_BTN = "regular_click_btn";
        SoundsList.SPIN_CLICK_BTN = "spin_click_btn";
        SoundsList.REGULAR_SPIN_BG = "regular_spin_bg";
        SoundsList.AUTO_SPIN_BG = "auto_spin_bg";
        SoundsList.FREE_SPIN_BG = "free_spin_bg";
        SoundsList.PAY_TABLE_BG = "pay_table_bg";
        SoundsList.COLLECT = "collect";
        SoundsList.REEL_STOP = "reel_stop";
        SoundsList.FREE_SPIN_COLLECT = "free_spin_collect";
        SoundsList.SMALL_WIN_COUNTER = "small_win_counter";
        SoundsList.NORMAL_WIN_COUNTER = "normal_win_counter";
        SoundsList.MEDIUM_WIN_COUNTER = "medium_win_counter";
        SoundsList.BIG_WIN_COUNTER = "big_win_counter";
        SoundsList.COLOSSAL_WIN_COUNTER = "colossal_win_counter";
        SoundsList.AUTO_SPIN_WIN = "auto_spin_win";
        SoundsList.LINE_WIN_ENUM = "line_win_enum";
        SoundsList.WEEL_ROTATION = "weel_rotation";
        SoundsList.SECTOR_ANIMATION = "sector_animation";
        SoundsList.GLASS_ROTATE = "glass_sound";
        SoundsList.SCATER_START = "scatter_start";
        return SoundsList;
    })();
    engine.SoundsList = SoundsList;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinAniVO = (function () {
        function WinAniVO() {
        }
        return WinAniVO;
    })();
    engine.WinAniVO = WinAniVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BonusVO = (function () {
        function BonusVO() {
        }
        return BonusVO;
    })();
    engine.BonusVO = BonusVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinLineVO = (function () {
        function WinLineVO() {
        }
        return WinLineVO;
    })();
    engine.WinLineVO = WinLineVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ServerData = (function () {
        function ServerData() {
            this.maxLineCount = 40;
            this.lineCount = 40;
            this.winLines = [];
            this.win = 0;
        }
        ServerData.prototype.setBalance = function (value) {
            this.balance = value;
        };
        ServerData.prototype.getBalance = function () {
            return this.balance;
        };
        ServerData.prototype.setMaxMultiply = function () {
            this.multiply = this.availableMultiples[this.availableMultiples.length - 1];
        };
        ServerData.prototype.setMaxLines = function () {
            this.lineCount = this.maxLineCount;
        };
        ServerData.prototype.setNextMultiply = function () {
            var index = this.availableMultiples.indexOf(this.multiply);
            var nextIndex = (index + 1) % this.availableMultiples.length;
            this.multiply = this.availableMultiples[nextIndex];
        };
        ServerData.prototype.setNextBet = function (isCircle) {
            var index = this.availableBets.indexOf(this.bet);
            if (++index == this.availableBets.length) {
                index = isCircle ? 0 : this.availableBets.length - 1;
            }
            this.bet = this.availableBets[index];
        };
        ServerData.prototype.setBet = function (data) {
            var index = this.availableBets.indexOf(data);
            console.log(data, index, 'availableBets');
            this.bet = this.availableBets[index];
        };
        ServerData.prototype.isHasNextBet = function () {
            return this.availableBets.indexOf(this.bet) != this.availableBets.length - 1;
        };
        ServerData.prototype.setPrevBet = function (isCircle) {
            var index = this.availableBets.indexOf(this.bet);
            if (--index == -1) {
                index = isCircle ? this.availableBets.length - 1 : 0;
            }
            this.bet = this.availableBets[index];
        };
        ServerData.prototype.isHasPrevBet = function () {
            return this.availableBets.indexOf(this.bet) != 0;
        };
        ServerData.prototype.setNextLineCount = function (isCircle) {
            var lines = this.lineCount + 1;
            if (lines > this.maxLineCount) {
                lines = isCircle ? 1 : this.maxLineCount;
            }
            this.lineCount = lines;
        };
        ServerData.prototype.isHasNextLine = function () {
            return this.lineCount != this.maxLineCount;
        };
        ServerData.prototype.setPrevLineCount = function (isCircle) {
            var lines = this.lineCount - 1;
            if (lines < 1) {
                lines = isCircle ? this.maxLineCount : 1;
            }
            this.lineCount = lines;
        };
        ServerData.prototype.isHasPrevLine = function () {
            return this.lineCount != 1;
        };
        ServerData.prototype.getTotalBet = function () {
            return this.lineCount * this.multiply * this.bet;
        };
        ServerData.prototype.getBetPerLine = function () {
            return this.multiply * this.bet;
        };
        return ServerData;
    })();
    engine.ServerData = ServerData;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var CommonRefs = (function () {
        function CommonRefs() {
            this.is_win_popup_showed = true;
            this.is_win_lines_showed = true;
            this.is_lazy_load_compl = false;
            this.is_layout_load_compl = false;
            this.is_server_init = false;
        }
        return CommonRefs;
    })();
    engine.CommonRefs = CommonRefs;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Utils = (function () {
        function Utils() {
        }
        Utils.float2int = function (value) {
            return value | 0;
        };
        Utils.toIntArray = function (value) {
            var returnVal = new Array(value.length);
            for (var i = 0; i < value.length; i++) {
                returnVal[i] = parseInt(value[i]);
            }
            return returnVal;
        };
        Utils.getClassName = function (obj) {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(obj["constructor"].toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        Utils.fillDisplayObject = function (container, prefix) {
            var i = 1;
            var item;
            var result = [];
            while ((item = container.getChildByName(prefix + i)) != null) {
                result.push(item);
                i++;
            }
            return result;
        };
        return Utils;
    })();
    engine.Utils = Utils;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Keyboard = (function () {
        function Keyboard() {
        }
        Keyboard.SPACE = 32;
        Keyboard.BACK_QUOTE = 192;
        return Keyboard;
    })();
    engine.Keyboard = Keyboard;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var XMLUtils = (function () {
        function XMLUtils() {
        }
        XMLUtils.getElements = function (xml, tagName) {
            var result = [];
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                if (node.nodeName == tagName) {
                    result.push(node);
                }
            }
            return result;
        };
        XMLUtils.getElement = function (xml, tagName) {
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                if (node.nodeName == tagName) {
                    return node;
                }
            }
            return null;
        };
        XMLUtils.getSessionRoom = function (xml, tagName) {
            var childNodes = xml.getElementsByTagName(tagName)[0];
            if (childNodes != null) {
                return childNodes.textContent;
            }
            else
                return null;
        };
        XMLUtils.getAttribute = function (xml, attributeName) {
            var attributes = xml.attributes;
            if (attributes.hasOwnProperty(attributeName)) {
                return xml.attributes[attributeName].value;
            }
        };
        XMLUtils.getAttributeInt = function (xml, attributeName) {
            var attribute = XMLUtils.getAttribute(xml, attributeName);
            if (attribute != null) {
                return parseInt(attribute);
            }
            return null;
        };
        XMLUtils.getAttributeFloat = function (xml, attributeName) {
            var attribute = XMLUtils.getAttribute(xml, attributeName);
            if (attribute != null) {
                return parseFloat(attribute);
            }
            return null;
        };
        return XMLUtils;
    })();
    engine.XMLUtils = XMLUtils;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var MoneyFormatter = (function () {
        function MoneyFormatter() {
        }
        MoneyFormatter.format = function (value, isTrimZeros, isShowZero, symbol) {
            if (isShowZero === void 0) { isShowZero = true; }
            if (symbol === void 0) { symbol = "$"; }
            if (value == 0) {
                return isShowZero ? symbol + "0" : "";
            }
            var digits = value.toFixed(2).split(".");
            digits[0] = digits[0].split("").reverse().join("").replace(/(\d{3})(?=\d)/g, "$1,").split("").reverse().join("");
            if (isTrimZeros) {
                value = parseInt(digits[1]);
                if (value > 0) {
                    digits[1] += '';
                }
                else {
                    return symbol + digits[0];
                }
            }
            return symbol + digits.join(".");
        };
        return MoneyFormatter;
    })();
    engine.MoneyFormatter = MoneyFormatter;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var FullScreen = (function () {
        function FullScreen() {
        }
        FullScreen.init = function () {
            var element = document.documentElement;
            if (element.requestFullscreen) {
                FullScreen.fullscreenRequest = "requestFullscreen";
            }
            else if (element.mozRequestFullScreen) {
                FullScreen.fullscreenRequest = "mozRequestFullScreen";
            }
            else if (element.webkitRequestFullscreen) {
                FullScreen.fullscreenRequest = "webkitRequestFullscreen";
            }
            else if (element.msRequestFullscreen) {
                FullScreen.fullscreenRequest = "msRequestFullscreen";
            }
            if (document.exitFullscreen) {
                FullScreen.cancelFullScreen = "exitFullscreen";
            }
            else if (document.mozCancelFullScreen) {
                FullScreen.cancelFullScreen = "mozCancelFullScreen";
            }
            else if (document.webkitExitFullscreen) {
                FullScreen.cancelFullScreen = "webkitExitFullscreen";
            }
            else if (document.msExitFullscreen) {
                FullScreen.cancelFullScreen = "msExitFullscreen";
            }
        };
        FullScreen.toggleFullScreen = function () {
            if (FullScreen.isFullScreen()) {
                FullScreen.fullScreen();
            }
            else {
                FullScreen.closeFullScreen();
            }
        };
        FullScreen.isFullScreen = function () {
            return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement;
        };
        FullScreen.fullScreen = function () {
            var element = document.documentElement;
            if (FullScreen.fullscreenRequest != undefined) {
                element[FullScreen.fullscreenRequest]();
            }
        };
        FullScreen.closeFullScreen = function () {
            if (FullScreen.cancelFullScreen != undefined) {
                document[FullScreen.cancelFullScreen]();
            }
        };
        return FullScreen;
    })();
    engine.FullScreen = FullScreen;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AssetVO = (function () {
        function AssetVO(id, src, type) {
            if (type === void 0) { type = ""; }
            this.id = id;
            this.src = src;
            this.type = type;
        }
        return AssetVO;
    })();
    engine.AssetVO = AssetVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var FileConst = (function () {
        function FileConst() {
        }
        FileConst.LAYOUT_EXTENSION = ".layout";
        FileConst.JSON_EXTENSION = ".json";
        FileConst.WOFF_EXTENSION = ".woff";
        return FileConst;
    })();
    engine.FileConst = FileConst;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BaseEasing = (function () {
        function BaseEasing() {
        }
        /**
         * Установка параметров
         * @param distance - рассояние
         * @param updateCount - за сколько кадров нужно прйти от distance
         */
        BaseEasing.prototype.setParameters = function (distance, updateCount) {
            this.distance = distance;
            this.dt = 1 / updateCount;
            this.reset();
        };
        BaseEasing.prototype.getPosition = function () {
            this.currentTime += this.dt;
            if (this.currentTime + BaseEasing.EPSILON > 1) {
                this.currentTime = 1;
            }
            var currentPos = this.getRatio(this.currentTime) * this.distance;
            this.speed = currentPos - this.position;
            this.position = currentPos;
            return currentPos;
        };
        BaseEasing.prototype.getTime = function () {
            return this.currentTime;
        };
        BaseEasing.prototype.reset = function () {
            this.currentTime = 0;
            this.position = 0;
            this.speed = 0;
        };
        BaseEasing.prototype.getRatio = function (time) {
            throw new Error("ERROR: Need override method");
        };
        BaseEasing.EPSILON = 0.000000001;
        return BaseEasing;
    })();
    engine.BaseEasing = BaseEasing;
})(engine || (engine = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var engine;
(function (engine) {
    var LinearEasing = (function (_super) {
        __extends(LinearEasing, _super);
        function LinearEasing() {
            _super.call(this);
        }
        LinearEasing.prototype.getRatio = function (time) {
            return time;
        };
        return LinearEasing;
    })(engine.BaseEasing);
    engine.LinearEasing = LinearEasing;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BezierEasing = (function (_super) {
        __extends(BezierEasing, _super);
        function BezierEasing(a, b, c, d, e) {
            _super.call(this);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        BezierEasing.prototype.getRatio = function (time) {
            var ts = time * time;
            var tc = ts * time;
            return this.a * tc * ts + this.b * ts * ts + this.c * tc + this.d * ts + this.e * time;
        };
        return BezierEasing;
    })(engine.BaseEasing);
    engine.BezierEasing = BezierEasing;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AjaxRequest = (function () {
        function AjaxRequest(type, url, isAsynchronous, params) {
            this.type = type;
            this.url = url + AjaxRequest.paramsToString(params);
            this.isAsynchronous = isAsynchronous;
        }
        AjaxRequest.paramsToString = function (params) {
            var paramsStr = "";
            for (var key in params) {
                paramsStr += paramsStr.length == 0 ? "?" : "&";
                paramsStr += key + "=" + params[key];
            }
            return paramsStr;
        };
        AjaxRequest.getXmlHttp = function () {
            if (window.hasOwnProperty("XMLHttpRequest")) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                return new XMLHttpRequest();
            }
            else if (window.hasOwnProperty("ActiveXObject")) {
                // code for IE6, IE5
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        };
        /**
         * Send GET request
         */
        AjaxRequest.prototype.get = function (handler) {
            var _this = this;
            if (handler === void 0) { handler = null; }
            this.handler = handler;
            this.xmlHttp = AjaxRequest.getXmlHttp();
            this.xmlHttp.onreadystatechange = function () {
                _this.onComplete();
            };
            this.xmlHttp.open("GET", this.url, this.isAsynchronous);
            this.xmlHttp.send(null);
        };
        /**
         * Request is ready
         */
        AjaxRequest.prototype.onComplete = function () {
            if (this.xmlHttp.readyState == XMLHttpRequest.DONE) {
                // success
                if (this.xmlHttp.status == 200) {
                    // Registration : in the registration process xmlRootNode return current status that user registered or not.
                    var responseData;
                    switch (this.type) {
                        case AjaxRequest.XML:
                            {
                                responseData = this.xmlHttp.responseXML;
                                break;
                            }
                        case AjaxRequest.JSON:
                            {
                                responseData = JSON.parse(this.xmlHttp.response);
                                break;
                            }
                    }
                    // TODO: need remove on server empty response
                    /*if (responseData == null) {
                        throw new Error("ERROR: Server response null");
                    }*/
                    if (this.handler != null) {
                        this.handler(responseData);
                    }
                }
                else {
                    throw new Error("ERROR: There was a problem retrieving the data: " + this.xmlHttp.statusText);
                }
            }
        };
        AjaxRequest.JSON = "json";
        AjaxRequest.XML = "xml";
        return AjaxRequest;
    })();
    engine.AjaxRequest = AjaxRequest;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ItemVO = (function () {
        function ItemVO(id) {
            this.id = id;
            this.isSelected = false;
            this.winValue = -1;
        }
        return ItemVO;
    })();
    engine.ItemVO = ItemVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AnimationTextField = (function () {
        function AnimationTextField(textField) {
            this.textField = textField;
            this.easing = new engine.LinearEasing();
            this.isUpdate = false;
        }
        AnimationTextField.prototype.setValue = function (value, updateTime) {
            if (updateTime === void 0) { updateTime = 0; }
            //console.log("value="+value+" time="+updateTime,"setValue");
            if (updateTime == 0 || value == 0) {
                this.value = value;
                this.setText(value);
                this.isUpdate = false;
            }
            else if (this.value != value) {
                this.isUpdate = true;
                this.easing.setParameters(value - this.value, updateTime);
            }
        };
        AnimationTextField.prototype.update = function () {
            if (this.isUpdate) {
                var pos = this.easing.getPosition();
                this.setText(this.value + pos);
                if (this.easing.getTime() >= 1) {
                    this.isUpdate = false;
                    this.value += pos;
                    this.easing.reset();
                }
            }
        };
        AnimationTextField.prototype.setText = function (value) {
            this.textField.text = engine.MoneyFormatter.format(value, false);
            if (value == 0)
                this.textField.text = "";
        };
        return AnimationTextField;
    })();
    engine.AnimationTextField = AnimationTextField;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var TouchMenu = (function () {
        function TouchMenu(view) {
            this.view = view;
        }
        TouchMenu.prototype.init = function (currentIdx, allValues, onUpdate) {
            var _this = this;
            this.currentIdx = currentIdx;
            this.allValues = allValues;
            this.onUpdate = onUpdate;
            this.initTextFields();
            this.updateValue();
            this.step = this.view.getBounds().width / this.texts.length / 2;
            this.view.on("mousedown", function (eventDownObj) {
                var div;
                var startIdx = _this.currentIdx;
                _this.view.on("pressmove", function (eventMoveObj) {
                    div = engine.Utils.float2int((eventDownObj.stageX - eventMoveObj.stageX) / _this.step);
                    var currentIdx = startIdx + div;
                    if (currentIdx != _this.currentIdx && currentIdx >= 0 && currentIdx < _this.allValues.length) {
                        _this.changeIdx(currentIdx);
                    }
                });
                _this.view.on("pressup", function () {
                    _this.view.removeAllEventListeners("pressmove");
                    _this.view.removeAllEventListeners("pressup");
                });
            });
        };
        TouchMenu.prototype.changeIdx = function (idx) {
            this.currentIdx = idx;
            this.updateValue();
            this.onUpdate(this.currentIdx);
        };
        TouchMenu.prototype.updateValue = function () {
            var idx;
            var div = (this.texts.length - 1) / 2;
            for (var i = 0; i < this.texts.length; i++) {
                idx = this.currentIdx + i - div;
                if (idx >= 0 && idx < this.allValues.length) {
                    this.texts[i].text = this.allValues[idx].toString();
                }
                else {
                    this.texts[i].text = "";
                }
            }
        };
        TouchMenu.prototype.initTextFields = function () {
            var textField;
            var i = 1;
            this.texts = [];
            while ((textField = this.view.getChildByName(TouchMenu.TEXT_PREFIX + i)) != null) {
                this.texts.push(textField);
                i++;
            }
        };
        TouchMenu.TEXT_PREFIX = "text";
        return TouchMenu;
    })();
    engine.TouchMenu = TouchMenu;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Sprite = createjs.Sprite;
    var WinSymbolView = (function (_super) {
        __extends(WinSymbolView, _super);
        function WinSymbolView() {
            _super.call(this);
            this._is_wild = false;
        }
        WinSymbolView.prototype.create = function (winSymbolCreator, highlightCreator) {
            if (winSymbolCreator != null) {
                var winSymbolContainer = new Container();
                winSymbolCreator.create(winSymbolContainer);
                this.winSymbol = winSymbolContainer.getChildAt(0);
                this.addChild(winSymbolContainer);
            }
            if (highlightCreator != null) {
                var highlightContainer = new Container();
                highlightCreator.create(highlightContainer);
                this.highlight = highlightContainer.getChildAt(0);
                this.addChild(highlightContainer);
            }
        };
        WinSymbolView.prototype.play = function () {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = this.winSymbol;
                sprite.gotoAndPlay(0);
            }
            if (this.highlight != null) {
                this.highlight.gotoAndPlay(0);
            }
        };
        WinSymbolView.prototype.setY = function (value) {
            this.y = value;
        };
        WinSymbolView.prototype.setX = function (value) {
            this.x = value;
        };
        return WinSymbolView;
    })(Container);
    engine.WinSymbolView = WinSymbolView;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.05.2015.
 */
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Sprite = createjs.Sprite;
    var Tween = createjs.Tween;
    var WildSymbolView = (function (_super) {
        __extends(WildSymbolView, _super);
        function WildSymbolView() {
            _super.call(this);
            this._is_wild_started = false;
            this._is_wild_to_show = false;
        }
        WildSymbolView.prototype.create = function (winSymbolCreator, highlightCreator, wildSymbolCreator, isMobile) {
            if (winSymbolCreator != null) {
                var winSymbolContainer = new Container();
                winSymbolCreator.create(winSymbolContainer);
                this.winSymbol = winSymbolContainer.getChildAt(0);
                this.addChild(winSymbolContainer);
            }
            if (highlightCreator != null) {
                var highlightContainer = new Container();
                highlightCreator.create(highlightContainer);
                this.highlight = highlightContainer.getChildAt(0);
                if (isMobile) {
                    this.highlight.scaleX *= 2;
                    this.highlight.scaleY *= 2;
                }
                this.addChild(highlightContainer);
            }
            var wildSymbolLoop = new Container();
            wildSymbolCreator.create(wildSymbolLoop);
            this.wildLoop = wildSymbolLoop.getChildAt(0);
            this.wildLoop.visible = false;
            this.addChild(wildSymbolLoop);
        };
        WildSymbolView.prototype.play = function () {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = this.winSymbol;
                this.wildLoop.visible = false;
                var wildLoop = this.wildLoop;
                var frameCount = sprite.spriteSheet.getNumFrames(null);
                sprite.addEventListener("tick", function (eventObj) {
                    var currentAnimation = eventObj.currentTarget;
                    currentAnimation.visible = true;
                    if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
                        currentAnimation.stop();
                        currentAnimation.visible = false;
                        currentAnimation.removeAllEventListeners("tick");
                        //sprite.removeAllEventListeners("tick");
                        wildLoop.visible = true;
                        wildLoop.gotoAndPlay(0);
                    }
                });
                if (!this._is_wild_started) {
                    this._is_wild_started = true;
                    sprite.gotoAndPlay(0);
                }
            }
        };
        WildSymbolView.prototype.playFadeIn = function () {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = this.winSymbol;
                sprite.visible = false;
                var wildLoop = this.wildLoop;
                wildLoop.visible = true;
                wildLoop.alpha = 0;
                wildLoop.gotoAndPlay(0);
                this._is_wild_started = true;
                Tween.get(wildLoop).to({ alpha: 1 }, 900);
            }
        };
        WildSymbolView.prototype.setY = function (value) {
            this.y = value;
        };
        WildSymbolView.prototype.setX = function (value) {
            this.x = value;
        };
        return WildSymbolView;
    })(Container);
    engine.WildSymbolView = WildSymbolView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var BigWinPopupView = (function (_super) {
        __extends(BigWinPopupView, _super);
        function BigWinPopupView() {
            _super.call(this);
            this.textsPos = false;
        }
        BigWinPopupView.prototype.onInit = function () {
            BigWinPopupView.INVISIBLE_TIME_UP = engine.Utils.float2int(BigWinPopupView.INVISIBLE_TIME * Ticker.getFPS());
            this.setTextPositions();
            this.winTfPlain = this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
            this.winTfPlain.shadow = new createjs.Shadow("#000000", 4, 4, 4);
            this.winTf = new engine.AnimationTextField(this.winTfPlain);
            this.bg = this.getChildByName(BigWinPopupView.BG_COUNT);
            this.bg.alpha = BigWinPopupView.BG_COUNT_ALPHA;
            //this.alpha = 0;
        };
        BigWinPopupView.prototype.update = function () {
            if (this.winTf != null) {
                this.winTf.update();
            }
        };
        BigWinPopupView.prototype.setTextPositions = function () {
            if (!this.textsPos) {
                var title = this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
                if (title != null) {
                    title.y += title.lineHeight;
                }
            }
            this.textsPos = true;
        };
        BigWinPopupView.prototype.setWinTf = function (value, updateTime) {
            if (updateTime === void 0) { updateTime = 0; }
            //var tf:Text = <Text>this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
            if (this.winTf != null) {
                this.winTf.setValue(value, updateTime);
            }
        };
        BigWinPopupView.prototype.show = function (callback, time) {
            if (time === void 0) { time = 0; }
            time = engine.Utils.float2int(time * Ticker.getFPS());
            var tween = Tween.get(this, { useTicks: true, override: true });
            tween.to({ alpha: 1 }, time);
            tween.call(callback);
        };
        BigWinPopupView.prototype.hide = function (callback, time) {
            if (time === void 0) { time = 0; }
            time = engine.Utils.float2int(time * Ticker.getFPS());
            var tween = Tween.get(this, { useTicks: true, override: true });
            tween.wait(time).to({ alpha: 0 }, BigWinPopupView.INVISIBLE_TIME_UP);
            tween.call(callback);
        };
        BigWinPopupView.LAYOUT_NAME = "BigWinPopupView";
        BigWinPopupView.FREE_SPINS_COUNT = "winTf";
        BigWinPopupView.BG_COUNT = "bg";
        BigWinPopupView.BG_COUNT_ALPHA = 0.7;
        BigWinPopupView.INVISIBLE_TIME = 1.5;
        return BigWinPopupView;
    })(Layout);
    engine.BigWinPopupView = BigWinPopupView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var ColossalWinPopupView = (function (_super) {
        __extends(ColossalWinPopupView, _super);
        function ColossalWinPopupView() {
            _super.call(this);
            this.textsPos = false;
        }
        ColossalWinPopupView.prototype.onInit = function () {
            ColossalWinPopupView.INVISIBLE_TIME_UP = engine.Utils.float2int(ColossalWinPopupView.INVISIBLE_TIME * Ticker.getFPS());
            this.setTextPositions();
            this.winTf = new engine.AnimationTextField(this.getChildByName(ColossalWinPopupView.FREE_SPINS_COUNT));
            this.bg = this.getChildByName(ColossalWinPopupView.BG_COUNT);
            this.bg.alpha = ColossalWinPopupView.BG_COUNT_ALPHA;
        };
        ColossalWinPopupView.prototype.update = function () {
            if (this.winTf != null) {
                this.winTf.update();
            }
        };
        ColossalWinPopupView.prototype.setTextPositions = function () {
            if (!this.textsPos) {
                var title = this.getChildByName(ColossalWinPopupView.FREE_SPINS_COUNT);
                if (title != null) {
                    title.y += title.lineHeight;
                }
            }
            this.textsPos = true;
        };
        ColossalWinPopupView.prototype.setWinTf = function (value, updateTime) {
            if (updateTime === void 0) { updateTime = 0; }
            //var tf:Text = <Text>this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
            if (this.winTf != null) {
                this.winTf.setValue(value, updateTime);
            }
        };
        ColossalWinPopupView.prototype.hide = function (callback, time) {
            if (time === void 0) { time = 0; }
            time = engine.Utils.float2int(time * Ticker.getFPS());
            var tween = Tween.get(this, { useTicks: true, override: true });
            tween.wait(time).to({ alpha: 0 }, ColossalWinPopupView.INVISIBLE_TIME_UP);
            tween.call(callback);
        };
        ColossalWinPopupView.LAYOUT_NAME = "ColossalWinPopupView";
        ColossalWinPopupView.FREE_SPINS_COUNT = "winTf";
        ColossalWinPopupView.BG_COUNT = "bg";
        ColossalWinPopupView.BG_COUNT_ALPHA = 0.7;
        ColossalWinPopupView.INVISIBLE_TIME = 0.05;
        return ColossalWinPopupView;
    })(Layout);
    engine.ColossalWinPopupView = ColossalWinPopupView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ReelView = (function () {
        function ReelView(view, reelId) {
            this.view = view;
            this.reelId = reelId;
        }
        ReelView.prototype.init = function () {
            this.symbols = [];
            var symbolId = 1;
            var symbol;
            while ((symbol = this.view.getChildByName(ReelView.SYMBOL_PREFIX + symbolId)) != null) {
                this.symbols.push(symbol);
                symbolId++;
            }
        };
        ReelView.prototype.showAllSymbols = function () {
            for (var i = 0; i < this.symbols.length; i++) {
                this.symbols[i].visible = true;
            }
        };
        /////////////////////////////////////////
        //	GETTERS
        /////////////////////////////////////////
        ReelView.prototype.getReelId = function () {
            return this.reelId;
        };
        ReelView.prototype.getSymbol = function (id) {
            return this.symbols[id];
        };
        ReelView.prototype.getSymbolFrame = function (id) {
            return this.symbols[id].currentFrame;
        };
        ReelView.prototype.getSymbolCount = function () {
            return this.symbols.length - 1;
        };
        ReelView.prototype.getX = function () {
            return this.view.x;
        };
        ReelView.prototype.getY = function () {
            return this.view.y;
        };
        /////////////////////////////////////////
        //	SETTERS
        /////////////////////////////////////////
        ReelView.prototype.setX = function (value) {
            this.view.x = value;
        };
        ReelView.prototype.setY = function (value) {
            this.view.y = value;
        };
        ReelView.prototype.setSymbolFrame = function (symbolId, frameId) {
            return this.symbols[symbolId].gotoAndStop(frameId);
        };
        ReelView.prototype.getView = function () {
            return this.view;
        };
        ReelView.SYMBOL_PREFIX = "symbol_";
        return ReelView;
    })();
    engine.ReelView = ReelView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Shape = createjs.Shape;
    var Layout = layout.Layout;
    var ReelsView = (function (_super) {
        __extends(ReelsView, _super);
        function ReelsView() {
            _super.call(this);
            this.isMobile = false;
        }
        ReelsView.prototype.onInit = function () {
            this.createReelsView();
            this.createReelsMask();
        };
        ReelsView.prototype.setMobileTrue = function (isMobile) {
            this.isMobile = isMobile;
            this.createReelsMask();
        };
        ReelsView.prototype.createReelsView = function () {
            this.reels = [];
            var view;
            var reelId = 1;
            while ((view = this.getChildByName(ReelsView.REEL_PREFIX + reelId)) != null) {
                var reelView = new engine.ReelView(view, reelId - 1);
                reelView.init();
                this.reels.push(reelView);
                reelId++;
            }
        };
        ReelsView.prototype.createReelsMask = function () {
            var firstReel = this.reels[0];
            var symbolBounds = firstReel.getSymbol(0).getBounds();
            var lastReel = this.reels[this.reels.length - 1];
            var mask = new Shape();
            mask.graphics.beginFill("0").drawRect(firstReel.getX(), firstReel.getY(), lastReel.getX() + symbolBounds.width, this.getHeight() - symbolBounds.height);
            this.mask = mask;
            this.hitArea = mask;
            ReelsView.PLAY_FIELD_MASK = mask;
        };
        ReelsView.prototype.showAllSymbols = function () {
            for (var i = 0; i < this.reels.length; i++) {
                this.reels[i].showAllSymbols();
            }
        };
        ReelsView.prototype.hideSymbols = function (positions) {
            for (var i = 0; i < positions.length; i++) {
                var pos = positions[i];
                this.reels[pos.x].getSymbol(pos.y + 1).visible = false;
            }
        };
        ReelsView.prototype.dispose = function () {
            this.parent.removeChild(this);
            this.reels = null;
        };
        /////////////////////////////////////////
        //	GETTERS
        /////////////////////////////////////////
        ReelsView.prototype.getReel = function (id) {
            return this.reels[id];
        };
        ReelsView.prototype.getReelCount = function () {
            return this.reels.length;
        };
        ReelsView.prototype.getHeight = function () {
            return this.getBounds().height;
        };
        ReelsView.LAYOUT_NAME = "ReelsView";
        ReelsView.REEL_PREFIX = "reel_";
        return ReelsView;
    })(Layout);
    engine.ReelsView = ReelsView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var PayTableHudView = (function (_super) {
        __extends(PayTableHudView, _super);
        function PayTableHudView() {
            _super.call(this);
            this.buttonsNames = [
                PayTableHudView.BACK_TO_GAME_BTN,
                PayTableHudView.PREV_PAGE_BTN,
                PayTableHudView.NEXT_PAGE_BTN
            ];
            this.rightObj = [
                PayTableHudView.BACK_TO_GAME_BTN,
                PayTableHudView.PREV_PAGE_BTN,
                PayTableHudView.NEXT_PAGE_BTN
            ];
        }
        PayTableHudView.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        PayTableHudView.prototype.resizeForMobile = function () {
            var stage = this.getStage();
            if (stage == null) {
                return;
            }
            var i;
            var displayObject;
            var canvas = stage.canvas;
            var rightX = (canvas.width - stage.x) / stage.scaleX;
            for (i = 0; i < this.rightObj.length; i++) {
                displayObject = this.getChildByName(this.rightObj[i]);
                if (displayObject != null) {
                    displayObject.x = rightX - displayObject.getBounds().width;
                }
            }
        };
        PayTableHudView.LAYOUT_NAME = "PayTableHudView";
        PayTableHudView.BACK_TO_GAME_BTN = "backToGameBtn";
        PayTableHudView.PREV_PAGE_BTN = "prevPageBtn";
        PayTableHudView.NEXT_PAGE_BTN = "nextPageBtn";
        return PayTableHudView;
    })(Layout);
    engine.PayTableHudView = PayTableHudView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Shape = createjs.Shape;
    var Layout = layout.Layout;
    var HudView = (function (_super) {
        __extends(HudView, _super);
        function HudView() {
            _super.call(this);
            this.hudTitles = [
                HudView.AUTO_SPIN_TEXT,
                HudView.TOTAL_BET_TITLE,
                HudView.WIN_TITLE,
                HudView.CASH_TEXT
            ];
            this.buttonsNames = [
                HudView.SETTINGS_BTN,
                HudView.BACK_TO_LOBBY_BTN,
                HudView.STOP_AUTO_PLAY_BTN,
                HudView.GAMBLE_BTN,
                HudView.INC_SPIN_COUNT_BTN,
                HudView.DEC_SPIN_COUNT_BTN,
                HudView.INC_LINE_COUNT_BTN,
                HudView.DEC_LINE_COUNT_BTN,
                HudView.NEXT_BET_BTN,
                HudView.PREV_BET_BTN,
                HudView.MAX_BET_BTN,
                HudView.MAX_LINES_BTN,
                HudView.START_SPIN_BTN,
                HudView.START_BONUS_BTN,
                HudView.STOP_SPIN_BTN,
                HudView.PAY_TABLE_BTN,
                HudView.BET_BTN,
                HudView.LINE_COUNT_BTN,
                HudView.MULTIPLY_BTN,
                HudView.OPEN_AUTO_SPIN_TABLE_BTN,
                HudView.TOTAL_BET_BTN
            ];
            this.spinButtons = [
                HudView.START_SPIN_BTN,
                HudView.STOP_SPIN_BTN
            ];
            this.panels = [
                HudView.AUTO_SPIN_TABLE_NEW,
                HudView.SELECT_BET_TABLE,
                HudView.AUTO_SPIN_TABLE_MASK,
                HudView.SELECT_BET_TABLE_MASK
            ];
            this.viewInitialised = false;
            this.linesText = [];
        }
        HudView.prototype.onInit = function () {
            //var textField:Text;
            //var i:number = 1;
            //while ((textField = <Text>this.getChildByName(HudView.LINES_COUNT_TEXT + i)) != null) {
            //	this.linesText.push(textField);
            //	i++;
            //}
            this.initSetOrigin();
            this.scaleItems(0.58, this.panels);
            //this.scaleItems(0.7, this.hudTitles);
            //this.prepareHudTitles();
        };
        HudView.prototype.prepareHudTitles = function () {
            var fontSize = 18;
            this.setText(HudView.AUTO_SPINS_COUNT_TEXT, "");
            var item = this.getChildByName(HudView.AUTO_SPIN_TEXT);
            if (item != null) {
                item.font = " " + fontSize + "px Bebas";
                //item.y += 25;
                item.y += 27;
            }
            item = this.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT);
            if (item != null) {
                item.font = " " + fontSize + "px Bebas";
                //item.y += fontSize;
                item.y += fontSize;
            }
            item = this.getChildByName(HudView.TOTAL_BET_TITLE);
            if (item != null) {
                item.font = " " + fontSize + "px Bebas";
                item.y += fontSize;
            }
            item = this.getChildByName(HudView.WIN_TITLE);
            if (item != null) {
                item.font = " " + fontSize + "px Bebas";
                item.y += fontSize;
            }
            item = this.getChildByName(HudView.CASH_TEXT);
            if (item != null) {
                item.font = " " + fontSize + "px Bebas";
                item.y += fontSize;
            }
            return;
        };
        HudView.prototype.initSetOrigin = function () {
            var autoSpinTableView = this.getChildByName(HudView.AUTO_SPIN_TABLE_NEW);
            this.autoSpinOrigin = autoSpinTableView.y;
            var totalBetTableView = this.getChildByName(HudView.SELECT_BET_TABLE);
            this.totalBetOrigin = totalBetTableView.y;
        };
        HudView.prototype.setUpElements = function (common) {
            var stage = this.getStage();
            if (stage == null || this.textPositions) {
                return;
            }
            var winValue = this.getChildByName(HudView.WIN_TEXT);
            winValue.y = winValue.y + winValue.lineHeight;
            var totalBetValue = this.getChildByName(HudView.TOTAL_BET_TEXT);
            totalBetValue.y = totalBetValue.y + totalBetValue.getBounds().height;
            var balanceValue = this.getChildByName(HudView.BALANCE_TEXT);
            balanceValue.y = balanceValue.y + balanceValue.getBounds().height;
            var bar = this.getChildByName(HudView.HUD_BAR);
            var arrowsA = this.getChildByName(HudView.OPEN_AUTO_SPIN_TABLE_BTN);
            var arrowsT = this.getChildByName(HudView.TOTAL_BET_BTN);
            this.setHitArea(arrowsA, bar);
            this.setHitArea(arrowsT, bar);
            this.textPositions = true;
        };
        HudView.prototype.scaleItems = function (scale, arr) {
            for (var i = 0; i < arr.length; i++) {
                var item = this.getChildByName(arr[i]);
                if (item != null) {
                    item.scaleX = item.scaleY = item.scaleX * scale;
                }
            }
        };
        HudView.prototype.resizeForMobile = function (common) {
            var stage = this.getStage();
            if (stage == null || this.textPositions) {
                return;
            }
            //var winValue = <Text>this.getChildByName(HudView.WIN_TEXT);
            //winValue.y = winValue.y + winValue.lineHeight;
            //
            //var totalBetValue = <Text>this.getChildByName(HudView.TOTAL_BET_TEXT);
            //totalBetValue.y = totalBetValue.y + totalBetValue.getBounds().height;
            //
            //var balanceValue = <Text>this.getChildByName(HudView.BALANCE_TEXT);
            //balanceValue.y = balanceValue.y + balanceValue.getBounds().height;
            var bar = this.getChildByName(HudView.HUD_BAR);
            var arrowsA = this.getChildByName(HudView.OPEN_AUTO_SPIN_TABLE_BTN);
            var arrowsT = this.getChildByName(HudView.TOTAL_BET_BTN);
            this.setHitArea(arrowsA, bar);
            this.setHitArea(arrowsT, bar);
            this.textPositions = true;
        };
        HudView.prototype.setWin = function (winVal) {
            var winValue = new engine.AnimationTextField(this.getText(HudView.WIN_TEXT));
            winValue.setValue(winVal);
        };
        HudView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        HudView.prototype.setLineCountText = function (value) {
            for (var i = 0; i < this.linesText.length; i++) {
                this.linesText[i].text = value;
            }
        };
        HudView.prototype.setHitArea = function (button, topBar) {
            var bounds = button.getBounds();
            var hitArea = new Shape();
            hitArea.graphics.beginFill("0").drawRect(-2 * bounds.width, 0, bounds.width * 5, bounds.height * 5);
            button.hitArea = hitArea;
        };
        HudView.prototype.setOrigin = function () {
            var autoSpinTableView = this.getChildByName(HudView.AUTO_SPIN_TABLE_NEW);
            autoSpinTableView.y = this.autoSpinOrigin;
            var totalBetTableView = this.getChildByName(HudView.SELECT_BET_TABLE);
            totalBetTableView.y = this.totalBetOrigin;
        };
        HudView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        /////////////////////////////////////////
        //	GETTERS
        /////////////////////////////////////////
        HudView.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        HudView.prototype.getText = function (textFieldName) {
            return this.getChildByName(textFieldName);
        };
        HudView.prototype.setText = function (field, value) {
            var tf = this.getChildByName(field);
            tf.text = value;
            return;
        };
        HudView.LAYOUT_NAME = "HudView";
        //================================================================
        //------------------MENU------------------------------------------
        //================================================================
        //autospin table
        HudView.AUTO_SPIN_TABLE_NEW = "autoPlayPanel";
        HudView.AUTO_SPIN_TABLE_MASK = "autoSpinTableMask";
        HudView.OPEN_AUTO_SPIN_TABLE_BTN = "autoSpinStartBtn";
        HudView.TOTAL_BET_BTN = "totalBetBtn";
        HudView.SELECT_BET_TABLE = "TotalBetPanel";
        HudView.SELECT_BET_TABLE_MASK = "selectBetTableMask";
        HudView.SETTINGS_MENU = "settingsMenu";
        HudView.AUTO_SPIN_TEXT = "autoPlayText";
        HudView.TOTAL_BET_TITLE = "totalBet";
        HudView.WIN_TITLE = "winTitle";
        HudView.CASH_TEXT = "cashTitle";
        HudView.AUTO_SPIN_TEXT_START = "AUTOPLAY";
        HudView.AUTO_SPIN_TEXT_STOP = "STOP";
        HudView.AUTO_SPIN_TEXT_FREESPINS = "FREESPINS";
        HudView.TITLES_MARGIN = 10;
        //================================================================
        //------------------BUTTONS NAMES---------------------------------
        //================================================================
        HudView.BACK_TO_LOBBY_BTN = "lobbyBtn";
        HudView.SETTINGS_BTN = "settingsBtn";
        HudView.FULL_SCREEN_BTN = "fullScreenBtn";
        //Запускает автоспины
        //public static START_AUTO_PLAY_BTN:string = "autoSpinStartBtn";
        //Начинает спин
        HudView.START_SPIN_BTN = "spinBtn";
        //Останавлевает автоспины
        HudView.STOP_AUTO_PLAY_BTN = "autoStopBtn";
        //Ускоряет остановку барабанов
        HudView.STOP_SPIN_BTN = "stopBtn";
        //Запускает рисковую
        HudView.GAMBLE_BTN = "gambleBtn";
        //Запускает бонус
        HudView.START_BONUS_BTN = "bonusStartBtn";
        //Открывает HELP
        HudView.PAY_TABLE_BTN = "payTableBtn";
        //Устанавлевает следующее значение фриспинов из конфига autoPlayCount
        HudView.INC_SPIN_COUNT_BTN = "incSpinsBtn";
        //Устанавлевает предыдущее значение фриспинов из конфига autoPlayCount
        HudView.DEC_SPIN_COUNT_BTN = "decSpinsBtn";
        //Увеличевает количество линий на 1 до максимального
        HudView.INC_LINE_COUNT_BTN = "incLinesBtn";
        //Уменьшает количество линий на 1 до 1
        HudView.DEC_LINE_COUNT_BTN = "decLinesBtn";
        //Меняет количество линий по кругу
        HudView.LINE_COUNT_BTN = "linesBtn";
        //Устанавлевает максимальное количество линий и начинает спин
        HudView.MAX_LINES_BTN = "maxLinesBtn";
        //Устанавлевает следующую ставку до максимальной
        HudView.NEXT_BET_BTN = "nextBetBtn";
        //Устанавлевает предыдущую ставку до первой
        HudView.PREV_BET_BTN = "prevBetBtn";
        //Устанавлевает следующую ставку по кругу
        HudView.BET_BTN = "betBtn";
        //Устанавлевает максимальный мультиплекатор и начинает спин
        HudView.MAX_BET_BTN = "maxBetBtn";
        //Устанавлевает следующий мультиплекатор по кругу
        HudView.MULTIPLY_BTN = "multiplyBtn";
        //================================================================
        //------------------TEXT FIELDS NAMES-----------------------------
        //================================================================
        HudView.WIN_TEXT = "winTf";
        HudView.BET_TEXT = "betTf";
        HudView.TOTAL_BET_TEXT = "totalBetTf";
        HudView.PREV_BET_PER_LINE_TEXT = "prevLineBetTf";
        HudView.BET_PER_LINE_TEXT = "lineBetTf";
        HudView.NEXT_BET_PER_LINE_TEXT = "nextLineBetTf";
        HudView.PREV_LINES_COUNT_TEXT = "prevLinesCountTf";
        HudView.LINES_COUNT_TEXT = "linesCountTf";
        HudView.NEXT_LINES_COUNT_TEXT = "nextLinesCountTf";
        HudView.PREV_AUTO_SPINS_COUNT_TEXT = "prevSpinCountTf";
        HudView.AUTO_SPINS_COUNT_TEXT = "spinCountTf";
        HudView.NEXT_AUTO_SPINS_COUNT_TEXT = "nextSpinCountTf";
        HudView.BALANCE_TEXT = "balanceTf";
        HudView.HUD_BAR = "instance221";
        return HudView;
    })(Layout);
    engine.HudView = HudView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Shape = createjs.Shape;
    var HudViewP = (function (_super) {
        __extends(HudViewP, _super);
        function HudViewP() {
            _super.call(this);
            this.hudTitles = [
                engine.HudView.AUTO_SPIN_TEXT,
                engine.HudView.TOTAL_BET_TITLE,
                engine.HudView.WIN_TITLE,
                engine.HudView.CASH_TEXT
            ];
            this.viewInitialised = false;
            this.buttonsNames = [
                HudViewP.TOTAL_BET_BTNP,
                engine.HudView.SETTINGS_BTN,
                engine.HudView.START_SPIN_BTN,
                engine.HudView.STOP_SPIN_BTN,
                engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN
            ];
            this.panels = [
                engine.HudView.SELECT_BET_TABLE,
                engine.HudView.SELECT_BET_TABLE_MASK
            ];
            this.spinButtons = [
                engine.HudView.START_SPIN_BTN,
                engine.HudView.STOP_SPIN_BTN
            ];
            this.linesText = [];
        }
        HudViewP.prototype.onInit = function () {
            var textField;
            var i = 1;
            while ((textField = this.getChildByName(engine.HudView.LINES_COUNT_TEXT + i)) != null) {
                this.linesText.push(textField);
                i++;
            }
            this.initSetOrigin();
        };
        HudViewP.prototype.prepareHudTitles = function () {
            this.setText(engine.HudView.AUTO_SPINS_COUNT_TEXT, "");
            var item = this.getChildByName(engine.HudView.AUTO_SPIN_TEXT);
            var itemTf = item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
                item.y = item.y + itemTf.lineHeight;
            }
            item = this.getChildByName(engine.HudView.AUTO_SPINS_COUNT_TEXT);
            itemTf = item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
                item.y = item.y + itemTf.lineHeight;
            }
            item = this.getChildByName(engine.HudView.TOTAL_BET_TITLE);
            //itemTf = <Text>item;
            if (item != null) {
                //item.scaleY = item.scaleY * 0.5;
                //item.scaleX = item.scaleX * 0.7;
                item.y = item.y + itemTf.lineHeight;
                item.scaleX = item.scaleY = item.scaleX * 0.8;
            }
            item = this.getChildByName(engine.HudView.WIN_TITLE);
            //itemTf = <Text>item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
            }
            item = this.getChildByName(engine.HudView.CASH_TEXT);
            //itemTf = <Text>item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
            }
        };
        HudViewP.prototype.scaleItems = function (scale, arr) {
            for (var i = 0; i < arr.length; i++) {
                var item = this.getChildByName(arr[i]);
                if (item != null) {
                    item.scaleX = item.scaleY = item.scaleX * scale;
                }
            }
        };
        HudViewP.prototype.initSetOrigin = function () {
            var autoSpinTableView = this.getChildByName(HudViewP.AUTO_SPIN_TABLE_NEW);
            this.autoSpinOrigin = autoSpinTableView.y;
            var totalBetTableView = this.getChildByName(HudViewP.SELECT_BET_TABLE);
            this.totalBetOrigin = totalBetTableView.y;
            this.scaleItems(0.75, this.panels);
            //this.scaleItems(0.9,this.hudTitles);
            //this.prepareHudTitles();
        };
        HudViewP.prototype.resizeForMobile = function (common) {
            console.log("resizeForMobile HudViewPPPPPPPPPPPPPP", "bottom=" + bottom);
            var stage = this.getStage();
            if (stage == null) {
                return;
            }
            var canvas = stage.canvas;
            var bottom = (canvas.height - stage.y) / this.parent.scaleY;
            var topBar = this.getChildByName(HudViewP.HUD_TOP_BAR);
            var topBounds = topBar.getBounds();
            var bottomBar = this.getChildByName(HudViewP.HUD_BOTTOM_BAR);
            var bottBounds = bottomBar.getBounds();
            bottomBar.y = bottom - bottBounds.height;
            //var totalBetValue = <Text>this.getChildByName(HudView.TOTAL_BET_TEXT);
            //totalBetValue.y = totalBetValue.lineHeight + HudView.TITLES_MARGIN;
            //totalBetValue.scaleX = totalBetValue.scaleY = 0.8;
            var winTitle = this.getChildByName(HudViewP.WIN_TITLE);
            winTitle.y = bottom - engine.HudView.TITLES_MARGIN;
            var winValue = this.getChildByName(engine.HudView.WIN_TEXT);
            winValue.y = winTitle.y - winTitle.lineHeight;
            var cashTitle = this.getChildByName(engine.HudView.CASH_TEXT);
            var cashValue = this.getChildByName(engine.HudView.BALANCE_TEXT);
            cashValue.y = winValue.y;
            cashTitle.y = winTitle.y;
            var arrowsA = this.getChildByName(HudViewP.START_AUTO_PLAY_BTN);
            var arrowsT = this.getChildByName(HudViewP.TOTAL_BET_BTNP);
            arrowsA.rotation = arrowsT.rotation = 180;
            this.setHitAreaP(arrowsA, topBar);
            this.setHitAreaP(arrowsT, topBar);
            var spin;
            for (var i = 0; i < this.spinButtons.length; i++) {
                spin = this.getBtn(this.spinButtons[i]);
                spin.y = bottomBar.y - spin.getBounds().height;
            }
            this.textPositions = true;
        };
        HudViewP.prototype.setWin = function (winVal) {
            var winText = this.getChildByName(engine.HudView.WIN_TEXT);
            var winValue = new engine.AnimationTextField(winText);
            winValue.setValue(winVal);
        };
        HudViewP.prototype.setHitAreaP = function (button, topBar) {
            var bounds = button.getBounds();
            var hitArea = new Shape();
            hitArea.graphics.beginFill("0").drawRect(bounds.x - bounds.width * 2, topBar.y, bounds.width * 5, bounds.height * 5);
            button.hitArea = hitArea;
        };
        HudViewP.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        HudViewP.prototype.setLineCountText = function (value) {
            for (var i = 0; i < this.linesText.length; i++) {
                this.linesText[i].text = value;
            }
        };
        HudViewP.prototype.setOrigin = function () {
            var autoSpinTableView = this.getChildByName(HudViewP.AUTO_SPIN_TABLE_NEW);
            autoSpinTableView.y = this.autoSpinOrigin;
            var totalBetTableView = this.getChildByName(HudViewP.SELECT_BET_TABLE);
            totalBetTableView.y = this.totalBetOrigin;
        };
        HudViewP.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        HudViewP.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        HudViewP.prototype.getText = function (textFieldName) {
            return this.getChildByName(textFieldName);
        };
        HudViewP.prototype.setText = function (field, value) {
            var tf = this.getChildByName(field);
            tf.text = value;
            return;
        };
        HudViewP.LAYOUT_NAME = "HudView";
        HudViewP.WIN_TEXT = "winTf";
        HudViewP.WIN_TITLE = "winTitle";
        //buttons
        HudViewP.START_AUTO_PLAY_BTN = "autoSpinStartBtn";
        HudViewP.TOTAL_BET_BTNP = "totalBetBtn";
        HudViewP.AUTO_SPIN_BUTTONP = "autoSpinStartBtn";
        HudViewP.START_SPIN_BTN = "spinBtn";
        //autospin table
        HudViewP.AUTO_SPIN_TABLE_NEW = "autoPlayPanel";
        HudViewP.SELECT_BET_TABLE = "TotalBetPanel";
        HudViewP.AUTO_SPIN_TABLE_MASK = "autoSpinTableMask";
        HudViewP.SELECT_BET_TABLE_MASK = "selectBetTableMask";
        //select bets table
        HudViewP.BALANCE_TEXT = "balanceTf";
        //bars
        HudViewP.HUD_TOP_BAR = "topBar";
        HudViewP.HUD_BOTTOM_BAR = "bottomBar";
        return HudViewP;
    })(engine.HudView);
    engine.HudViewP = HudViewP;
})(engine || (engine = {}));
/**
 * Created by Taras on 02.02.2015.
 */
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var BorderView = (function (_super) {
        __extends(BorderView, _super);
        //		private static LINE_PREFIX:string = "line_";
        //		private lines:Array<DisplayObject>;
        function BorderView() {
            _super.call(this);
        }
        BorderView.prototype.onInit = function () {
            //			var lineId = 1;
            //			var line:DisplayObject;
            //			this.lines = [];
            //			while ((line = this.getChildByName(LinesView.LINE_PREFIX + lineId)) != null) {
            //				this.lines.push(line);
            //				lineId++;
            //			}
        };
        BorderView.LAYOUT_NAME = "BorderView";
        return BorderView;
    })(Layout);
    engine.BorderView = BorderView;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.12.2014.
 */
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var DrawView = (function (_super) {
        __extends(DrawView, _super);
        function DrawView() {
            _super.call(this);
        }
        DrawView.LAYOUT_NAME = "DrawView";
        return DrawView;
    })(Layout);
    engine.DrawView = DrawView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Shape = createjs.Shape;
    var Layout = layout.Layout;
    var LoaderView = (function (_super) {
        __extends(LoaderView, _super);
        function LoaderView() {
            _super.call(this);
            this.buttonsNames = [
                LoaderView.BUTTON_YES,
                LoaderView.BUTTON_NO
            ];
        }
        LoaderView.prototype.onInit = function () {
            this.initAnimation();
            this.initProgressBar();
            this.showPreloaderBg(1, 0);
        };
        LoaderView.prototype.initAnimation = function () {
            var animation = this.getChildByName(LoaderView.ANIMATION_CONTAINER_NAME);
            if (animation != null) {
                animation.play();
            }
        };
        LoaderView.prototype.initProgressBar = function () {
            this.progressBar = this.getChildByName(LoaderView.PROGRESS_BAR);
            this.progressBar.visible = false;
            this.progress = this.progressBar.getChildByName(LoaderView.PROGRESS);
            var bounds = this.progress.getBounds();
            var mask = new Shape();
            mask.graphics.beginFill("0").drawRect(this.progress.x, this.progress.y, bounds.width, bounds.height);
            this.progress.x -= bounds.width;
            this.progress.mask = mask;
            this.startPos = this.progress.x;
        };
        LoaderView.prototype.showPreloaderBg = function (bg_sounds_alpha, bg_loader_alpha) {
            this.getChildByName(LoaderView.BG_SOUNDS).alpha = bg_sounds_alpha;
            this.getChildByName(LoaderView.BG_LOADER).alpha = bg_loader_alpha;
            return;
        };
        LoaderView.prototype.hideButtons = function () {
            for (var i = 0; i < this.buttonsNames.length; i++) {
                var buttonName = this.buttonsNames[i];
                var button = this.getChildByName(buttonName);
                button.visible = false;
            }
        };
        LoaderView.prototype.showProgressBar = function () {
            this.progressBar.visible = true;
        };
        /**
         * @param value - loading progress [0;1]
         */
        LoaderView.prototype.setProgress = function (value) {
            if (value > 1 || value < 0) {
                throw new Error("loading progress value is not correct");
            }
            this.progress.x = this.startPos + this.progress.getBounds().width * value;
            //console.log("Loading progress: " + value);
        };
        LoaderView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        LoaderView.LAYOUT_NAME = "LoaderView";
        LoaderView.ANIMATION_CONTAINER_NAME = "animation";
        LoaderView.PROGRESS_BAR = "progressBar";
        LoaderView.PROGRESS = "progress";
        LoaderView.BG_SOUNDS = "bg_sounds";
        LoaderView.BG_LOADER = "bg_loader";
        LoaderView.BUTTON_YES = "yesBtn";
        LoaderView.BUTTON_NO = "noBtn";
        return LoaderView;
    })(Layout);
    engine.LoaderView = LoaderView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var GameFonView = (function (_super) {
        __extends(GameFonView, _super);
        function GameFonView() {
            _super.call(this);
        }
        GameFonView.prototype.onInit = function () {
        };
        GameFonView.LAYOUT_NAME = "GameFonView";
        return GameFonView;
    })(Layout);
    engine.GameFonView = GameFonView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var LinesView = (function (_super) {
        __extends(LinesView, _super);
        function LinesView() {
            _super.call(this);
        }
        LinesView.prototype.onInit = function () {
            var lineId = 1;
            var line;
            this.lines = [];
            while ((line = this.getChildByName(LinesView.LINE_PREFIX + lineId)) != null) {
                this.lines.push(line);
                lineId++;
            }
        };
        LinesView.prototype.showLine = function (id) {
            this.lines[id - 1].visible = true;
        };
        LinesView.prototype.hideLine = function (id) {
            this.lines[id - 1].visible = false;
        };
        LinesView.prototype.showLinesFromTo = function (fromId, toId) {
            for (var i = fromId - 1; i < toId; i++) {
                this.lines[i].visible = true;
            }
        };
        LinesView.prototype.showLines = function (linesIds) {
            for (var i = 0; i < linesIds.length; i++) {
                this.lines[linesIds[i] - 1].visible = true;
            }
        };
        LinesView.prototype.hideAllLines = function () {
            for (var i = 0; i < this.lines.length; i++) {
                this.lines[i].visible = false;
            }
        };
        LinesView.LAYOUT_NAME = "LinesView";
        LinesView.LINE_PREFIX = "line_";
        return LinesView;
    })(Layout);
    engine.LinesView = LinesView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Shape = createjs.Shape;
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ease = createjs.Ease;
    var Ticker = createjs.Ticker;
    var WeelView = (function (_super) {
        __extends(WeelView, _super);
        function WeelView() {
            _super.call(this);
            this.initAnimFrame = 0;
            this.bonusGameElements = [
                WeelView.ROTATOR,
                WeelView.BUTTON_START,
                WeelView.BUTTON_BG,
                WeelView.SECTOR_CONT
            ];
        }
        WeelView.prototype.setBonus = function (sector) {
            this.bonusSector = sector;
        };
        WeelView.prototype.onInit = function () {
            this.bonusAnimationContainer = new Container();
            this.addChild(this.bonusAnimationContainer);
            WeelView.WEEL_ROTATION_TIME = engine.Utils.float2int(WeelView.WEEL_ROTATION_TIME * Ticker.getFPS());
            WeelView.WEEL_ROTATION_STOP_TIME = engine.Utils.float2int(WeelView.WEEL_ROTATION_STOP_TIME * Ticker.getFPS());
        };
        WeelView.prototype.resetWeel = function () {
            this.triangles = [];
            for (var a = 0; a < this.bonusGameElements.length; a++) {
                var el = this.getChildByName(this.bonusGameElements[a]);
                if (el != null)
                    el.alpha = 0;
            }
            this.initStart = false;
            this.rotator = this.getChildByName(this.bonusGameElements[0]);
            this.originY = this.rotator.y;
            this.originX = this.rotator.x;
            this.rotator.rotation = -90;
            this.rotator.y = -this.rotator.getBounds().height / 2;
            this.rotator.x = -this.rotator.getBounds().width / 2;
            var ring = this.rotator['children'][0];
            ring['rotation'] = 0;
            ring['x'] = ring['y'] = ring['regX'] = ring['regY'] = 0;
            if (this.sectorCont)
                this.sectorCont['x'] = this.sectorCont['y'] = this.sectorCont['regX'] = this.sectorCont['regY'] = 0;
        };
        WeelView.prototype.showSectorAnimation = function () {
            this.dispatchEvent(engine.NotificationList.STOP_WEEL_ROTATE);
            var sectorTriangle = this.sectorCont;
            var cont = this.rotator;
            cont.addChild(sectorTriangle);
            var thisCont = sectorTriangle;
            var sector = thisCont.getChildByName(WeelView.SECTOR_CONT);
            var sectorTwo = sector.getChildByName(WeelView.SECTORS_TWO);
            var triangle1 = sectorTwo.getChildByName(WeelView.SECTORS_PREFIX + '1');
            var triangle2 = sectorTwo.getChildByName(WeelView.SECTORS_PREFIX + '2');
            triangle2.alpha = triangle1.alpha = 0;
            this.triangles.push(triangle1);
            this.triangles.push(triangle2);
            this.dispatchEvent(engine.NotificationList.EVENT_All_BONUSES_SHOWED);
        };
        WeelView.prototype.animationParser = function (keyframes) {
            this.initSector = true;
            var animation1 = keyframes['anim_1'];
            var animation2 = keyframes['anim_2'];
            this.createTwin(animation1, this.triangles[0]);
            var triangle = this.triangles[1];
            var self = this;
            Tween.get(animation2, { useTicks: true }).to({ alpha: 0 }).wait(WeelView.SECOND_ANIMATION_DELAY).call(function () {
                self.createTwin(animation2, triangle);
            });
        };
        WeelView.prototype.onEnterFrame = function () {
            if (this.initSector) {
                this.initAnimFrame++;
                if (this.initAnimFrame > WeelView.ANIMTAION_SECTOR_FRAMES / 2) {
                    this.initSector = false;
                    this.initAnimFrame = 0;
                    this.bonusAnimEnd();
                }
            }
        };
        WeelView.prototype.bonusAnimEnd = function () {
            var tween = Tween.get(this.sectorCont, { useTicks: true });
            var self = this;
            tween.to({ alpha: 0 }, WeelView.START_ROTATION_TIME).call(function () {
                self.dispatchEvent(engine.NotificationList.SHOW_START_BONUS_MODAL);
            });
        };
        WeelView.prototype.createTwin = function (keyframes, elem) {
            var tween = Tween.get(elem, { useTicks: true });
            var previos = 1;
            elem.x = elem.getBounds().width / 2;
            elem.regX = elem.getBounds().width / 2;
            //elem.y = elem.getBounds().height / 2;
            elem.regY = elem.getBounds().height / 2;
            for (var t = 1; t <= WeelView.ANIMTAION_SECTOR_FRAMES; t++) {
                var name = WeelView.FRAME_PREFIX + t;
                if (keyframes[name]) {
                    var params = keyframes[name];
                    var duration = Math.floor(t - previos);
                    tween.to(params, duration);
                    previos = t;
                }
            }
        };
        WeelView.prototype.weelStartAnimation = function () {
            var _this = this;
            if (this.initStart)
                return;
            this.initStart = true;
            var button = this.getChildByName(this.bonusGameElements[1]);
            button.visible = false;
            for (var a = 0; a < this.bonusGameElements.length - 1; a++) {
                var el = this.getChildByName(this.bonusGameElements[a]);
                el.alpha = 1;
            }
            button.on("click", function (eventObj) {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    button.removeAllEventListeners("click");
                    button.visible = false;
                    _this.bonusAnimationContainer.removeAllChildren();
                    _this.prepareRotation();
                }
            });
            Tween.get(this.rotator, { useTicks: true }).to({ rotation: 0, y: this.originY, x: this.originX }, WeelView.START_ROTATION_TIME).wait(WeelView.DELAY_TIME).call(function () {
                button.visible = true;
                button.cursor = 'pointer';
            });
        };
        WeelView.prototype.prepareRotation = function () {
            var ring = this.rotator['children'][0];
            var sectorCont = new Container();
            sectorCont.rotation = 0;
            var sectorShape = new Shape();
            if (!this.rotateCenter)
                this.rotateCenter = ring['_rectangle']['width'];
            var width = this.rotateCenter;
            sectorShape.graphics.beginFill("0").drawRect(ring['x'], ring['y'], width, width);
            sectorShape.alpha = 0;
            sectorCont.addChild(sectorShape);
            var clone = this.getChildByName(WeelView.SECTOR_CONT);
            var sectorTriangle = clone.clone(true);
            sectorTriangle.y = width / 2 * 1.03;
            sectorTriangle.x = width / 2 - sectorTriangle.getBounds().width / 2;
            sectorCont.addChild(sectorTriangle);
            sectorCont.getChildByName(WeelView.SECTOR_CONT).alpha = 1;
            ring['x'] = ring['y'] = ring['regX'] = ring['regY'] = width / 2;
            sectorCont['x'] = sectorCont['y'] = sectorCont['regX'] = sectorCont['regY'] = width / 2;
            this.sectorCont = sectorCont;
            this.rotationLeft = 0;
            this.dispatchEvent(engine.NotificationList.START_WEEL_ROTATE);
            this.launchWeel(ring);
        };
        WeelView.prototype.rotateWeel = function (ring) {
            var _this = this;
            ring.rotation = 0;
            var degrees = WeelView.DEGRESS_IN_A_FULL_ROTATION * WeelView.ROTATION_TIMES;
            Tween.get(ring, { useTicks: true }).to({ "rotation": degrees }, WeelView.DELAY_TIME / 2, Ease.linear).call(function () {
                _this.stopRotate(ring);
            });
        };
        WeelView.prototype.stopRotate = function (ring) {
            var _this = this;
            ring.rotation = 0;
            var sectorDegr = WeelView.DEGRESS_IN_A_FULL_ROTATION / 5;
            var degrees = WeelView.DEGRESS_IN_A_FULL_ROTATION + (sectorDegr * this.bonusSector) + 360;
            Tween.get(ring, { useTicks: true }).to({ "rotation": degrees }, WeelView.WEEL_ROTATION_STOP_TIME, Ease.getBackOut(1)).call(function () {
                _this.showSectorAnimation();
            });
        };
        WeelView.prototype.launchWeel = function (ring) {
            var _this = this;
            Tween.get(ring, { useTicks: true }).to({ "rotation": WeelView.DEGRESS_IN_A_FULL_ROTATION }, WeelView.WEEL_ROTATION_TIME, Ease.getBackIn(1)).call(function () {
                _this.rotateWeel(ring);
            });
        };
        WeelView.prototype.addBonusAnimation = function (winVOs, isMobile) {
            var _this = this;
            this.resetWeel();
            this.winVOs = winVOs;
            for (var i = 0; i < this.winVOs.length; i++) {
                var vo = this.winVOs[i];
                var winSymbolView = vo.winSymbolView;
                var spriteContainer = winSymbolView.getChildAt(0);
                var sprite = spriteContainer.getChildAt(0);
                var frameCount = sprite.spriteSheet.getNumFrames(null);
                sprite.addEventListener("tick", function (eventObj) {
                    var currentAnimation = eventObj.currentTarget;
                    if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
                        currentAnimation.stop();
                        currentAnimation.removeAllEventListeners("tick");
                        if (isMobile) {
                            var target = eventObj.currentTarget;
                            Tween.get(target, { useTicks: true }).to({ "alpha": 0.9 }, WeelView.WEEL_ROTATION_TIME / 2).call(function () {
                                eventObj.currentTarget.alpha = 0;
                                _this.weelStartAnimation();
                            });
                        }
                        else {
                            eventObj.currentTarget.alpha = 0;
                            _this.weelStartAnimation();
                        }
                    }
                });
                winSymbolView.x = vo.rect.x;
                winSymbolView.y = vo.rect.y;
                winSymbolView.play();
                this.bonusAnimationContainer.addChild(winSymbolView);
            }
        };
        WeelView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        WeelView.LAYOUT_NAME = "WeelView";
        WeelView.WEEL = "weel";
        WeelView.ROTATOR = "rotator";
        WeelView.BUTTON_START = "startWeel";
        WeelView.BUTTON_BG = "bg";
        WeelView.START_ROTATION_TIME = 30;
        WeelView.DELAY_TIME = 2;
        WeelView.WEEL_ROTATION_TIME = 3;
        WeelView.WEEL_ROTATION_STOP_TIME = 10;
        WeelView.ROTATION_TIMES = 1; //4
        WeelView.DEGRESS_IN_A_FULL_ROTATION = 360;
        //sectorn name space
        WeelView.SECTOR_CONT = "sector_container";
        WeelView.SECTORS_TWO = "sector_2";
        WeelView.SECTORS_PREFIX = "triangle_";
        WeelView.FRAME_PREFIX = "f_";
        WeelView.ANIMTAION_SECTOR_FRAMES = 90;
        WeelView.SECOND_ANIMATION_DELAY = 22;
        return WeelView;
    })(Layout);
    engine.WeelView = WeelView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var RotateScreenView = (function (_super) {
        __extends(RotateScreenView, _super);
        function RotateScreenView() {
            _super.call(this);
            RotateScreenView.ROTATION_TIME = engine.Utils.float2int(RotateScreenView.ROTATION_TIME * Ticker.getFPS());
            RotateScreenView.DELAY_TIME = engine.Utils.float2int(RotateScreenView.DELAY_TIME * Ticker.getFPS());
        }
        RotateScreenView.prototype.onInit = function () {
            this.art = this.getChildAt(0);
            var bounds = this.art.getBounds();
            this.art.regX = bounds.width / 2;
            this.art.regY = bounds.height / 2;
            this.art.x += this.art.regX;
            this.art.y += this.art.regY;
        };
        RotateScreenView.prototype.play = function () {
            if (this.art != undefined) {
                var _art = this.art;
                tween();
                function tween() {
                    _art.rotation = 0;
                    Tween.get(_art, { useTicks: true }).to({ rotation: -90 }, RotateScreenView.ROTATION_TIME).wait(RotateScreenView.DELAY_TIME).call(tween);
                }
            }
        };
        RotateScreenView.prototype.stop = function () {
            if (this.art != undefined) {
                Tween.removeTweens(this.art);
            }
        };
        RotateScreenView.LAYOUT_NAME = "RotateScreenView";
        RotateScreenView.ROTATION_TIME = 0.7;
        RotateScreenView.DELAY_TIME = 0.7;
        return RotateScreenView;
    })(Layout);
    engine.RotateScreenView = RotateScreenView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var SettingMobileView = (function (_super) {
        __extends(SettingMobileView, _super);
        function SettingMobileView() {
            _super.call(this);
            this.isPortrait = false;
            this.buttonsNames = [
                SettingMobileView.CLOSE_BTN,
                SettingMobileView.PAY_BTN,
                SettingMobileView.OPTIONS_BTN
            ];
            this.payViews = [
                "PayView_1",
                "PayView_2",
                "PayView_3",
                "PayView_4",
                "PayView_5"
            ];
        }
        SettingMobileView.prototype.indicatorClick = function (ind) {
            var indicators = this.getChildByName(SettingMobileView.PAGES_INDICATORS);
            var indicator = indicators.getChildByName(SettingMobileView.INDICATOR_PREFIX + (ind + 1));
            indicator.gotoAndStop(1);
        };
        SettingMobileView.prototype.updateMove = function (id) {
            var indicator;
            var indicators = this.getChildByName(SettingMobileView.PAGES_INDICATORS);
            for (var a = 0; a < indicators.getNumChildren(); a++) {
                indicator = indicators.getChildByName(SettingMobileView.INDICATOR_PREFIX + (a + 1));
                indicator.gotoAndStop(0);
            }
            indicator = indicators.getChildByName(SettingMobileView.INDICATOR_PREFIX + id);
            indicator.gotoAndStop(1);
        };
        SettingMobileView.prototype.resizeForMobile = function () {
            this.toggleIndicator(false);
        };
        SettingMobileView.prototype.toggleIndicator = function (status) {
            var indicators = this.getChildByName(SettingMobileView.PAGES_INDICATORS);
            indicators.visible = status;
        };
        SettingMobileView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        SettingMobileView.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        SettingMobileView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        SettingMobileView.prototype.getSoundTempl = function () {
            return SettingMobileView.SOUND_SETTING_TEMPLATE;
        };
        SettingMobileView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        SettingMobileView.LAYOUT_NAME = "SettingMobileView";
        SettingMobileView.BG_NAME = "setting_bg_hor";
        SettingMobileView.PAGES_INDICATORS = "indicators";
        SettingMobileView.INDICATOR_PREFIX = 'indicator';
        SettingMobileView.SOUND_SETTING_TEMPLATE = "SettingSoundView";
        SettingMobileView.CLOSE_BTN = "CloseBtn";
        SettingMobileView.PAY_BTN = "payBtn";
        SettingMobileView.OPTIONS_BTN = "settinBtn";
        return SettingMobileView;
    })(Layout);
    engine.SettingMobileView = SettingMobileView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var SettingMobileViewP = (function (_super) {
        __extends(SettingMobileViewP, _super);
        function SettingMobileViewP() {
            _super.call(this);
            this.isPortrait = true;
            this.buttonsNames = [
                SettingMobileViewP.CLOSE_BTN
            ];
            this.payViews = [
                "PayView_1P",
                "PayView_2P",
                "PayView_3P",
                "PayView_4P",
                "PayView_5P"
            ];
        }
        SettingMobileViewP.prototype.updateMove = function (percent) {
            var dragSlider = this.getChildByName(SettingMobileViewP.SLIDER_DRAG);
            dragSlider.y = percent;
        };
        SettingMobileViewP.prototype.resizeForMobile = function () {
        };
        SettingMobileViewP.prototype.getSoundTempl = function () {
            return SettingMobileViewP.SOUND_SETTING_TEMPLATE;
        };
        SettingMobileViewP.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        SettingMobileViewP.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        SettingMobileViewP.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        SettingMobileViewP.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        SettingMobileViewP.LAYOUT_NAME = "SettingMobileViewP";
        SettingMobileViewP.SOUND_SETTING_TEMPLATE = "SettingSoundViewP";
        SettingMobileViewP.SLIDER_DRAG = "sliderDrug";
        SettingMobileViewP.CLOSE_BTN = "CloseBtn";
        return SettingMobileViewP;
    })(Layout);
    engine.SettingMobileViewP = SettingMobileViewP;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var DOMElement = createjs.DOMElement;
    var Container = createjs.Container;
    var SetWheelView = (function (_super) {
        __extends(SetWheelView, _super);
        function SetWheelView(width, height) {
            _super.call(this);
            this.width = width;
            this.height = height;
        }
        SetWheelView.prototype.init = function () {
            var canvasHolder = document.getElementById("canvasHolder");
            this.div = document.createElement("div");
            this.div.style.background = "#666";
            canvasHolder.appendChild(this.div);
            var table = document.createElement("table");
            this.div.appendChild(table);
            this.sendBtn = document.createElement("button");
            this.sendBtn.innerHTML = "SET";
            this.div.appendChild(this.sendBtn);
            var caption = document.createElement("caption");
            caption.innerHTML = "SET WHEEL:";
            table.appendChild(caption);
            var tr = document.createElement("tr");
            table.appendChild(tr);
            this.textFields = new Array(this.width);
            for (var i = 0; i < this.width; i++) {
                var td = document.createElement("td");
                tr.appendChild(td);
                var column = new Array(this.height);
                for (var j = 0; j < this.height; j++) {
                    var input = document.createElement("input");
                    input.type = "text";
                    input.value = "-1";
                    input.style.width = "40px";
                    td.appendChild(input);
                    column[j] = input;
                    var br = document.createElement("br");
                    td.appendChild(br);
                }
                this.textFields[i] = column;
            }
            var element = new DOMElement(this.div);
            this.addChild(element);
            this.setVisible(false);
        };
        SetWheelView.prototype.getSendBtn = function () {
            return this.sendBtn;
        };
        SetWheelView.prototype.setVisible = function (value) {
            if (value) {
                this.div.style.display = "";
            }
            else {
                this.div.style.display = "none";
            }
        };
        SetWheelView.prototype.getVisible = function () {
            return this.div.style.display != "none";
        };
        SetWheelView.prototype.getTextValue = function () {
            var result = new Array(this.textFields.length);
            for (var i = 0; i < this.textFields.length; i++) {
                var columnText = this.textFields[i];
                var column = new Array(columnText.length);
                for (var j = 0; j < columnText.length; j++) {
                    column[j] = parseInt(columnText[j].value);
                    if (isNaN(column[j])) {
                        column[j] = -1;
                    }
                }
                result[i] = column;
            }
            return result;
        };
        return SetWheelView;
    })(Container);
    engine.SetWheelView = SetWheelView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var AutoPlayView = (function (_super) {
        __extends(AutoPlayView, _super);
        function AutoPlayView() {
            _super.call(this);
            this.buttonsNames = [
                AutoPlayView.YES_BTN,
                AutoPlayView.NO_BTN
            ];
        }
        AutoPlayView.prototype.onInit = function () {
            var lines = this.getChildByName(AutoPlayView.AUTO_COUNT);
            if (lines) {
                lines.y = lines.y + lines.getBounds().height / 1.5;
            }
        };
        AutoPlayView.prototype.setText = function (count) {
            var lines = this.getChildByName(AutoPlayView.AUTO_COUNT);
            if (lines) {
                lines.text = count.toString();
            }
        };
        AutoPlayView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        AutoPlayView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        AutoPlayView.LAYOUT_NAME = "AutoPlayView";
        AutoPlayView.YES_BTN = "yesBtn";
        AutoPlayView.NO_BTN = "noBtn";
        AutoPlayView.AUTO_COUNT = "counter";
        return AutoPlayView;
    })(Layout);
    engine.AutoPlayView = AutoPlayView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var ConnectionView = (function (_super) {
        __extends(ConnectionView, _super);
        function ConnectionView() {
            _super.call(this);
            this.buttonsNames = [
                ConnectionView.OK_BTN
            ];
        }
        ConnectionView.prototype.onInit = function () {
        };
        ConnectionView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        ConnectionView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        ConnectionView.prototype.setText = function (txt) {
            return;
        };
        ConnectionView.LAYOUT_NAME = "ConnectionView";
        ConnectionView.OK_BTN = "okBtn";
        return ConnectionView;
    })(Layout);
    engine.ConnectionView = ConnectionView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var ErrorView = (function (_super) {
        __extends(ErrorView, _super);
        function ErrorView() {
            _super.call(this);
            this.buttonsNames = [
                ErrorView.OK_BTN
            ];
        }
        ErrorView.prototype.onInit = function () {
        };
        ErrorView.prototype.setText = function (txt) {
            var tf = this.getChildByName(ErrorView.ERROR_MESSAGE);
            if (tf) {
                tf.text = txt.join("\n");
            }
        };
        ErrorView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        ErrorView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        ErrorView.LAYOUT_NAME = "ErrorView";
        ErrorView.OK_BTN = "okBtnError";
        ErrorView.ERROR_MESSAGE = "errorMessage";
        return ErrorView;
    })(Layout);
    engine.ErrorView = ErrorView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var SelectItemView = (function (_super) {
        __extends(SelectItemView, _super);
        function SelectItemView() {
            _super.call(this);
            SelectItemView.DELAY_FOR_NEXT_STEP = engine.Utils.float2int(SelectItemView.DELAY_FOR_NEXT_STEP * Ticker.getFPS());
            SelectItemView.HIDE_POPUP_TIME = engine.Utils.float2int(SelectItemView.HIDE_POPUP_TIME * Ticker.getFPS());
        }
        SelectItemView.prototype.onInit = function () {
            this.bonus = this.getChildByName(SelectItemView.BONUS);
            this.winPopup = this.getChildByName(SelectItemView.WIN_POPUP);
            this.items = engine.Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_BTN);
            this.itemsCount = this.items.length;
            this.texts = engine.Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_TEXT);
            this.animations = engine.Utils.fillDisplayObject(this.bonus, SelectItemView.WIN_ANIMATION);
        };
        SelectItemView.prototype.showWinAnimation = function (vo, callback) {
            this.items[vo.id].setEnable(false);
            var animation = this.animations[vo.id];
            var frameCount = animation.spriteSheet.getNumFrames(null);
            animation.visible = true;
            animation.addEventListener("tick", function (eventObj) {
                var currentAnimation = eventObj.currentTarget;
                if (currentAnimation.currentFrame == frameCount - 1) {
                    currentAnimation.stop();
                }
            });
            animation.gotoAndPlay(0);
            var itemText = this.texts[vo.id];
            itemText.text = engine.MoneyFormatter.format(vo.winValue, true);
            itemText.visible = true;
            itemText.alpha = 0;
            Tween.get(itemText, { useTicks: true }).to({ alpha: 1 }, frameCount / 2).wait(SelectItemView.DELAY_FOR_NEXT_STEP).call(callback);
        };
        SelectItemView.prototype.showWinPopup = function () {
            this.winPopup.visible = true;
            this.winPopup.alpha = 1;
            this.bonus.visible = false;
        };
        SelectItemView.prototype.showBonus = function () {
            this.bonus.visible = true;
            this.bonus.alpha = 1;
            this.winPopup.visible = false;
        };
        SelectItemView.prototype.hideAllWinAnimation = function () {
            for (var i = 0; i < this.animations.length; i++) {
                this.animations[i].visible = false;
                this.texts[i].visible = false;
            }
        };
        SelectItemView.prototype.hideWinPopup = function (callback) {
            Tween.get(this.winPopup, { useTicks: true }).to({ alpha: 0 }, SelectItemView.HIDE_POPUP_TIME).call(callback);
        };
        SelectItemView.prototype.getCollectBtn = function () {
            return this.winPopup.getChildByName(SelectItemView.COLLECT_BTN);
        };
        SelectItemView.prototype.getItems = function () {
            return this.items;
        };
        SelectItemView.prototype.getItemCount = function () {
            return this.itemsCount;
        };
        SelectItemView.prototype.setTotalWin = function (value) {
            var totalWin = this.winPopup.getChildByName(SelectItemView.TOTAL_WIN_TEXT);
            totalWin.text = engine.MoneyFormatter.format(value, true);
        };
        SelectItemView.getItemIdByName = function (itemName) {
            return parseInt(itemName.substr(SelectItemView.ITEM_TEXT.length + 1));
        };
        SelectItemView.LAYOUT_NAME = "SelectItemView";
        SelectItemView.WIN_POPUP = "winPopup";
        SelectItemView.BONUS = "bonus";
        SelectItemView.WIN_ANIMATION = "winAnimation_";
        SelectItemView.ITEM_TEXT = "itemTf_";
        SelectItemView.ITEM_BTN = "itemBtn_";
        SelectItemView.TOTAL_WIN_TEXT = "totalWin";
        SelectItemView.COLLECT_BTN = "collectBtn";
        SelectItemView.DELAY_FOR_NEXT_STEP = 2;
        SelectItemView.HIDE_POPUP_TIME = 0.5;
        return SelectItemView;
    })(Layout);
    engine.SelectItemView = SelectItemView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var GambleView = (function (_super) {
        __extends(GambleView, _super);
        function GambleView() {
            _super.call(this);
            this.buttonsNames = [
                GambleView.COLLECT_BTN,
                GambleView.RED_BTN,
                GambleView.BLACK_BTN
            ];
            GambleView.INVISIBLE_TIME = engine.Utils.float2int(GambleView.INVISIBLE_TIME * Ticker.getFPS());
            GambleView.DELAY_REMOVE = engine.Utils.float2int(GambleView.DELAY_REMOVE * Ticker.getFPS());
        }
        GambleView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        GambleView.prototype.setBankValue = function (value) {
            var tf = this.getChildByName(GambleView.BANK_TEXT);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView.prototype.setDoubleValue = function (value) {
            var tf = this.getChildByName(GambleView.DOUBLE_TEXT);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView.prototype.setMessageText = function (text) {
            var tf = this.getChildByName(GambleView.MESSAGE_TEXT);
            tf.text = text;
        };
        GambleView.prototype.showCard = function (cardId) {
            var back = this.getChildByName(GambleView.BACK);
            back.visible = false;
            var card = this.getChildByName(GambleView.CARD);
            card.gotoAndStop(cardId);
            card.visible = true;
        };
        GambleView.prototype.hideCard = function () {
            var back = this.getChildByName(GambleView.BACK);
            back.visible = true;
            var card = this.getChildByName(GambleView.CARD);
            card.visible = false;
        };
        GambleView.prototype.showHistory = function (historyData) {
            var i = 1;
            var cardContainer;
            while ((cardContainer = this.getChildByName(GambleView.HISTORY_CARD_PREFIX + i)) != null) {
                var back = cardContainer.getChildByName(GambleView.BACK);
                var cards = cardContainer.getChildByName(GambleView.CARD);
                if (i <= historyData.length) {
                    cards.gotoAndStop(historyData[i - 1]);
                    cards.visible = true;
                    back.visible = false;
                }
                else {
                    cards.visible = false;
                    back.visible = true;
                }
                i++;
            }
        };
        GambleView.prototype.hideBonus = function (isDelay, callback) {
            var tween = Tween.get(this, { useTicks: true });
            if (isDelay) {
                tween.wait(GambleView.DELAY_REMOVE);
            }
            tween.to({ alpha: 0 }, GambleView.INVISIBLE_TIME);
            tween.call(callback);
        };
        GambleView.LAYOUT_NAME = "GambleView";
        GambleView.INVISIBLE_TIME = 0.5;
        GambleView.DELAY_REMOVE = 3;
        GambleView.MESSAGE_TEXT = "massageTf";
        GambleView.DOUBLE_TEXT = "doubleTf";
        GambleView.BANK_TEXT = "bankTf";
        GambleView.BACK = "back";
        GambleView.CARD = "card";
        GambleView.HISTORY_CARD_PREFIX = "history_";
        GambleView.COLLECT_BTN = "collectBtn";
        GambleView.RED_BTN = "redBtn";
        GambleView.BLACK_BTN = "blackBtn";
        return GambleView;
    })(Layout);
    engine.GambleView = GambleView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var GambleView5 = (function (_super) {
        __extends(GambleView5, _super);
        function GambleView5() {
            _super.call(this);
            this.buttonsNames = [
                GambleView5.COLLECT_BTN,
                GambleView5.HALF_BTN,
                GambleView5.DOUBLE_BTN
            ];
            GambleView5.INVISIBLE_TIME = engine.Utils.float2int(GambleView5.INVISIBLE_TIME * Ticker.getFPS());
            GambleView5.DELAY_REMOVE = engine.Utils.float2int(GambleView5.DELAY_REMOVE * Ticker.getFPS());
        }
        GambleView5.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        GambleView5.prototype.setBankValue = function (value) {
            console.log('setBankValue');
            var tf = this.getChildByName(GambleView5.BANK_TEXT);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView5.prototype.setHalfDoubleValue = function (value) {
            console.log('setHalfValue');
            var tf = this.getChildByName(GambleView5.HALF_TEXT);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView5.prototype.setDoubleValue = function (value) {
            console.log('setDoubleValue');
            var tf = this.getChildByName(GambleView5.DOUBLE_TEXT);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView5.prototype.setMessageText = function (text) {
            var tf = this.getChildByName(GambleView5.MESSAGE_TEXT);
            tf.text = text;
        };
        GambleView5.prototype.showCard = function (cardId) {
            console.log(cardId);
            var cardName = 'card_1';
            var back = this.getChildByName(cardName);
            back.visible = false;
            var card = this.getChildByName(cardName);
            card.gotoAndStop(cardId);
            card.visible = true;
        };
        GambleView5.prototype.hideCard = function () {
            console.log('hide all');
            for (var i = 1; i < 6; i++) {
                var cardName = 'card_' + i;
                var card = this.getChildByName(cardName);
                this.resetLot(card);
            }
        };
        GambleView5.prototype.resetLot = function (card) {
            var tween = Tween.get(card, { useTicks: true });
            tween.wait(GambleView5.DELAY_REMOVE);
            tween.to({ alpha: 0 }, GambleView5.INVISIBLE_TIME);
            tween.call(function () {
                card.gotoAndStop(0);
                card.alpha = 1;
            });
        };
        GambleView5.prototype.showHistory = function (historyData) {
            for (var i = 2; i < 6; i++) {
                var cardName = 'card_' + i;
                var back = this.getChildByName(cardName);
                back.visible = false;
                var card = this.getChildByName(cardName);
                card.gotoAndStop(historyData[i - 2]);
                card.visible = true;
            }
        };
        GambleView5.prototype.hideBonus = function (isDelay, callback) {
            var tween = Tween.get(this, { useTicks: true });
            if (isDelay) {
                tween.wait(GambleView5.DELAY_REMOVE);
            }
            tween.to({ alpha: 0 }, GambleView5.INVISIBLE_TIME);
            tween.call(callback);
        };
        GambleView5.LAYOUT_NAME = "GambleView5";
        GambleView5.INVISIBLE_TIME = 0.5;
        GambleView5.DELAY_REMOVE = 3;
        GambleView5.MESSAGE_TEXT = "massageTf";
        GambleView5.DOUBLE_TEXT = "doubleTf";
        GambleView5.BANK_TEXT = "bankTf";
        GambleView5.HALF_TEXT = "halfTf";
        GambleView5.BACK = "back";
        GambleView5.CARD = "card";
        GambleView5.HISTORY_CARD_PREFIX = "history_";
        GambleView5.COLLECT_BTN = "collectBtn";
        //public static RED_BTN:string = "redBtn";
        //public static BLACK_BTN:string = "blackBtn";
        GambleView5.HALF_BTN = "halfBtn";
        GambleView5.DOUBLE_BTN = "doubleBtn";
        return GambleView5;
    })(Layout);
    engine.GambleView5 = GambleView5;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var FreeSpinsView = (function (_super) {
        __extends(FreeSpinsView, _super);
        function FreeSpinsView() {
            _super.call(this);
        }
        FreeSpinsView.prototype.onInit = function () {
            this.win = new engine.AnimationTextField(this.getChildByName(FreeSpinsView.WIN_TEXT));
        };
        FreeSpinsView.prototype.setLeftSpins = function (value) {
            var tf = this.getChildByName(FreeSpinsView.LEFT_SPINS_COUNT_TEXT);
            if (tf != null) {
                tf.text = value.toString();
            }
        };
        FreeSpinsView.prototype.setMultiplicator = function (value) {
            var tf = this.getChildByName(FreeSpinsView.MULTIPLICATOR_TEXT);
            if (tf != null) {
                tf.text = "x" + value.toString();
            }
        };
        FreeSpinsView.prototype.setWin = function (value, updateTime) {
            if (updateTime === void 0) { updateTime = 0; }
            this.win.setValue(value, updateTime);
        };
        FreeSpinsView.prototype.update = function () {
            if (this.win != null) {
                this.win.update();
            }
        };
        FreeSpinsView.LAYOUT_NAME = "FreeSpinsView";
        FreeSpinsView.LEFT_SPINS_COUNT_TEXT = "leftSpinsTf";
        FreeSpinsView.MULTIPLICATOR_TEXT = "multiplicatorTf";
        FreeSpinsView.WIN_TEXT = "winTf";
        return FreeSpinsView;
    })(Layout);
    engine.FreeSpinsView = FreeSpinsView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var FreeSpinsStartView = (function (_super) {
        __extends(FreeSpinsStartView, _super);
        function FreeSpinsStartView() {
            _super.call(this);
            this.buttonsNames = [
                FreeSpinsStartView.START_BTN
            ];
        }
        FreeSpinsStartView.prototype.onInit = function () {
            FreeSpinsStartView.INVISIBLE_TIME = engine.Utils.float2int(FreeSpinsStartView.INVISIBLE_TIME * Ticker.getFPS());
        };
        FreeSpinsStartView.prototype.getBtn = function (btnName) {
            var cont = this.getChildByName(FreeSpinsStartView.MODAL_CONTAINER);
            return cont.getChildByName(btnName);
        };
        FreeSpinsStartView.prototype.showBonusTitle = function (index) {
            var cont = this.getChildByName(FreeSpinsStartView.MODAL_CONTAINER);
            var title;
            var i = 1;
            while ((title = cont.getChildByName(FreeSpinsStartView.BONUS_TITLE_PREFIX + i)) != null) {
                title.alpha = 0;
                i++;
            }
            title = cont.getChildByName(FreeSpinsStartView.BONUS_TITLE_PREFIX + (index + 1));
            title.alpha = 1;
        };
        FreeSpinsStartView.prototype.hide = function (callback) {
            var tween = Tween.get(this, { useTicks: true });
            tween.to({ alpha: 0, override: true }, FreeSpinsStartView.INVISIBLE_TIME);
            tween.call(callback);
        };
        FreeSpinsStartView.LAYOUT_NAME = "FreeSpinsStartView";
        FreeSpinsStartView.START_BTN = "startFreeSpinBtn";
        FreeSpinsStartView.MODAL_CONTAINER = "startFreeSpins";
        FreeSpinsStartView.BONUS_TITLE_PREFIX = "bonus_";
        FreeSpinsStartView.INVISIBLE_TIME = 0.5;
        return FreeSpinsStartView;
    })(Layout);
    engine.FreeSpinsStartView = FreeSpinsStartView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var Shadow = createjs.Shadow;
    var FreeSpinsResultView = (function (_super) {
        __extends(FreeSpinsResultView, _super);
        //private static FEATURE_WIN:string = "featureWinTf";
        //private static TOTAL_WIN:string = "totalWinTf";
        function FreeSpinsResultView() {
            _super.call(this);
        }
        FreeSpinsResultView.prototype.onInit = function () {
            FreeSpinsResultView.INVISIBLE_TIME = engine.Utils.float2int(FreeSpinsResultView.INVISIBLE_TIME * Ticker.getFPS());
            this.resultFreeSpinModal = this.getChildByName(FreeSpinsResultView.MODAL_CONTAINER);
            this.setTextPositions();
        };
        FreeSpinsResultView.prototype.setTextPositions = function () {
            if (!this.textsPos) {
                var title = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_WIN);
                if (title != null) {
                    title.y += title.lineHeight;
                    title.color = '#fff';
                    title.shadow = new Shadow("#000", 0, 0, 10);
                    title.maxWidth = 450;
                }
                title = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_1);
                if (title != null) {
                    title.y += title.lineHeight;
                    title.shadow = new Shadow("#000", 0, 0, 10);
                    title.maxWidth = 450;
                }
                title = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_2);
                if (title != null) {
                    title.y += title.lineHeight;
                    title.shadow = new Shadow("#000", 0, 0, 10);
                    title.maxWidth = 450;
                }
            }
            this.textsPos = true;
        };
        FreeSpinsResultView.prototype.getCollectBtn = function () {
            return this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.COLLECT_BTN);
        };
        FreeSpinsResultView.prototype.setTotalWin = function (value) {
            var tf = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_WIN);
            if (tf != null) {
                tf.text = engine.MoneyFormatter.format(value, true);
            }
            tf = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_1);
            if (tf != null) {
                tf.text = FreeSpinsResultView.DEFAULT_GAME_TITLE_1;
            }
            tf = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_2);
            if (tf != null) {
                tf.text = FreeSpinsResultView.DEFAULT_GAME_TITLE_2;
            }
        };
        FreeSpinsResultView.prototype.setFreeSpinsMessage = function (title1, title2) {
            var tf = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_WIN);
            if (tf != null) {
                tf.text = "";
            }
            tf = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_1);
            if (tf != null) {
                tf.text = title1;
            }
            tf = this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_2);
            if (tf != null) {
                tf.text = title2;
            }
            //var collectBtn:Button = this.getCollectBtn(); TODO: change collect button text to continue
        };
        FreeSpinsResultView.prototype.setGameWin = function (value) {
            //var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.GAME_WIN);
            //if (tf != null) {
            //	tf.text = MoneyFormatter.format(value, true);
            //}
        };
        FreeSpinsResultView.prototype.setFeatureWin = function (value) {
            //var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.FEATURE_WIN);
            //if (tf != null) {
            //	tf.text = MoneyFormatter.format(value, true);
            //}
        };
        FreeSpinsResultView.prototype.hide = function (callback) {
            var tween = Tween.get(this, { useTicks: true });
            tween.to({ alpha: 0 }, FreeSpinsResultView.INVISIBLE_TIME);
            tween.call(callback);
        };
        FreeSpinsResultView.LAYOUT_NAME = "FreeSpinsResultView";
        FreeSpinsResultView.INVISIBLE_TIME = 0.5;
        //
        FreeSpinsResultView.COLLECT_BTN = "collectBtn";
        FreeSpinsResultView.GAME_WIN = "winTf";
        FreeSpinsResultView.GAME_TITLE_1 = "winTitle1";
        FreeSpinsResultView.GAME_TITLE_2 = "winTitle2";
        FreeSpinsResultView.DEFAULT_GAME_TITLE_1 = "Fantastic!";
        FreeSpinsResultView.DEFAULT_GAME_TITLE_2 = "You won";
        FreeSpinsResultView.MODAL_CONTAINER = "resultFreeSpinModal";
        return FreeSpinsResultView;
    })(Layout);
    engine.FreeSpinsResultView = FreeSpinsResultView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ControllerManager = (function () {
        function ControllerManager() {
            this.observerMap = {};
            this.controllers = [];
        }
        ControllerManager.prototype.register = function (controller) {
            var notificationsNames = controller.listNotification();
            for (var i = 0; i < notificationsNames.length; i++) {
                this.registerObserver(notificationsNames[i], controller);
            }
            this.controllers.push(controller);
        };
        ControllerManager.prototype.remove = function (controller) {
            var notificationsNames = controller.listNotification();
            for (var i = 0; i < notificationsNames.length; i++) {
                this.removeObserver(notificationsNames[i], controller);
            }
            var index = this.controllers.indexOf(controller);
            this.controllers.splice(index, 1);
        };
        ControllerManager.prototype.registerObserver = function (notificationName, controller) {
            if (this.observerMap[notificationName] != null) {
                this.observerMap[notificationName].push(controller);
            }
            else {
                this.observerMap[notificationName] = [controller];
            }
        };
        ControllerManager.prototype.removeObserver = function (notificationName, controller) {
            var controllers = this.observerMap[notificationName];
            for (var i = 0; i < controllers.length; i++) {
                if (controllers[i] == controller) {
                    controllers.splice(i, 1);
                }
            }
        };
        ControllerManager.prototype.send = function (notificationName, data) {
            console.log(notificationName, data, "send");
            var controllers = this.observerMap[notificationName];
            if (controllers != null) {
                for (var i = 0; i < controllers.length; i++) {
                    controllers[i].handleNotification(notificationName, data);
                }
            }
        };
        ControllerManager.prototype.onEnterFrame = function () {
            for (var i = 0; i < this.controllers.length; i++) {
                this.controllers[i].onEnterFrame();
            }
        };
        return ControllerManager;
    })();
    engine.ControllerManager = ControllerManager;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BaseController = (function () {
        function BaseController(manager) {
            this.manager = manager;
        }
        BaseController.prototype.init = function () {
            this.manager.register(this);
        };
        BaseController.prototype.listNotification = function () {
            return [];
        };
        BaseController.prototype.handleNotification = function (message, data) {
        };
        BaseController.prototype.send = function (message, data) {
            if (data === void 0) { data = null; }
            this.manager.send(message, data);
        };
        BaseController.prototype.dispose = function () {
            console.log("Dispose controller: " + engine.Utils.getClassName(this));
            this.manager.remove(this);
        };
        BaseController.prototype.onEnterFrame = function () {
        };
        return BaseController;
    })();
    engine.BaseController = BaseController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AutoSpinState = (function () {
        function AutoSpinState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.setView(controller.view);
        }
        AutoSpinState.prototype.start = function (data) {
            this.autoSpinCount = data;
            //var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
            //autoPlayText.visible = true;
            this.afterRotate();
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.controller.tryStartSpin();
            this.view.setText(engine.HudView.AUTO_SPIN_TEXT, engine.HudView.AUTO_SPIN_TEXT_STOP);
            this.view.getChildByName(engine.HudView.AUTO_SPINS_COUNT_TEXT).alpha = 1;
        };
        AutoSpinState.prototype.afterRotate = function () {
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, false);
            this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, false, false);
            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, true, true);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            console.log("afterRotate", "AutoSpinState");
            this.view.getText(engine.HudView.AUTO_SPIN_TEXT).text = engine.HudView.AUTO_SPIN_TEXT_STOP;
            this.updateAutoSpinCountText();
        };
        AutoSpinState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.WIN_POPUP_SHOWED:
                    {
                        break;
                    }
                case engine.NotificationList.TRY_START_SPIN:
                    {
                        if (this.common.is_win_popup_showed === true) {
                            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
                            this.autoSpinCount--;
                            this.updateAutoSpinCountText();
                            this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
                            this.controller.send(engine.NotificationList.START_SPIN);
                            this.controller.send(engine.NotificationList.SERVER_SEND_SPIN);
                            break;
                        }
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus == null || bonus.type == engine.BonusTypes.GAMBLE) {
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        }
                        else {
                            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
                            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
                            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        }
                        this.controller.send(engine.NotificationList.SHOW_WIN_TF);
                        this.controller.send(engine.NotificationList.UPDATE_BALANCE_TF);
                        break;
                    }
                case engine.NotificationList.HIDE_SYMBOLS:
                    {
                        this.controller.send(engine.NotificationList.HIDE_SYMBOLS, data);
                        break;
                    }
                case engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus != null && bonus.type != engine.BonusTypes.GAMBLE) {
                            this.controller.changeState(engine.BonusState.NAME);
                        }
                        else if (this.autoSpinCount == 0) {
                            this.controller.changeState(engine.DefaultState.NAME);
                            this.controller.send(engine.NotificationList.END_AUTO_PLAY);
                            return;
                        }
                        else {
                            if (this.common.is_win_popup_showed === true) {
                                this.controller.tryStartSpin();
                            }
                        }
                        break;
                    }
            }
        };
        AutoSpinState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN:
                case engine.HudViewP.AUTO_SPIN_BUTTONP:
                    {
                        this.autoSpinCount = 0;
                        //if(!this.common.isMobile){
                        //	var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
                        //	autoPlayText.visible = this.common.config.showAllTimeSpinCount;
                        //}
                        this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
                        //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
                        this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
                        this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, true, false);
                        break;
                    }
            }
        };
        AutoSpinState.prototype.updateAutoSpinCountText = function () {
            var autoPlayText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
            if (autoPlayText != null) {
                console.log("updateAutoSpinCountText");
                autoPlayText.text = this.autoSpinCount.toString();
            }
        };
        AutoSpinState.prototype.end = function () {
            this.view.setText(engine.HudView.AUTO_SPIN_TEXT, engine.HudView.AUTO_SPIN_TEXT_START);
            //this.view.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT).alpha = 0;
            this.view.setText(engine.HudView.AUTO_SPINS_COUNT_TEXT, "");
        };
        AutoSpinState.prototype.setView = function (view) {
            this.view = view;
        };
        AutoSpinState.NAME = "AutoSpinState";
        return AutoSpinState;
    })();
    engine.AutoSpinState = AutoSpinState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BonusState = (function () {
        function BonusState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.setView(controller.view);
        }
        BonusState.prototype.start = function (data) {
            this.afterRotate();
            this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.controller.send(engine.NotificationList.CREATE_BONUS);
        };
        BonusState.prototype.afterRotate = function () {
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, false, false);
            console.log("afterRotate", "BonusState");
            this.controller.send(engine.NotificationList.FREESPINS_SHOW_LEFT_FIELDS);
        };
        BonusState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.END_BONUS:
                    {
                        // remove bonus VO
                        this.common.server.bonus = null;
                        this.controller.changeState(engine.DefaultState.NAME);
                        break;
                    }
            }
        };
        BonusState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.START_SPIN_BTN:
                case engine.HudView.START_BONUS_BTN:
                    {
                        this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
                        this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
                        break;
                    }
                case engine.NotificationList.WIN_LINES_SHOWED:
                    {
                        console.log('WIN_LINES_SHOWED');
                        this.controller.send(engine.NotificationList.SHOW_WIN_TF);
                        this.controller.send(engine.NotificationList.UPDATE_BALANCE_TF);
                        break;
                    }
            }
        };
        BonusState.prototype.end = function () {
        };
        BonusState.prototype.setView = function (view) {
            this.view = view;
        };
        BonusState.NAME = "BonusState";
        return BonusState;
    })();
    engine.BonusState = BonusState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var RegularSpinState = (function () {
        function RegularSpinState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.setView(controller.view);
        }
        RegularSpinState.prototype.start = function (data) {
            this.afterRotate();
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
            this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.controller.send(engine.NotificationList.START_SPIN);
            this.controller.send(engine.NotificationList.SERVER_SEND_SPIN);
        };
        RegularSpinState.prototype.afterRotate = function () {
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, false);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, true, true);
            this.view.changeButtonState(engine.HudView.BACK_TO_LOBBY_BTN, true, false);
            console.log("afterRotate", "RegularSpinState");
        };
        RegularSpinState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.KEYBOARD_CLICK:
                    {
                        if (data == engine.Keyboard.SPACE) {
                            this.stopSpin();
                        }
                        break;
                    }
                case engine.NotificationList.ON_SCREEN_CLICK:
                    {
                        this.stopSpin();
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus == null || bonus.type == engine.BonusTypes.GAMBLE) {
                            this.controller.changeState(engine.DefaultState.NAME);
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, false);
                        }
                        else {
                            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
                            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        }
                        this.controller.send(engine.NotificationList.SHOW_WIN_TF);
                        this.controller.send(engine.NotificationList.UPDATE_BALANCE_TF);
                        break;
                    }
                case engine.NotificationList.WIN_LINES_SHOWED:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus != null && bonus.type != engine.BonusTypes.GAMBLE) {
                            this.controller.changeState(engine.BonusState.NAME);
                        }
                        this.view.setWin(this.common.server.win);
                        break;
                    }
                case engine.NotificationList.EXPRESS_STOP:
                    {
                        this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
                        break;
                    }
            }
        };
        RegularSpinState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.STOP_SPIN_BTN:
                    {
                        this.stopSpin();
                        break;
                    }
            }
        };
        RegularSpinState.prototype.stopSpin = function () {
            this.controller.send(engine.NotificationList.EXPRESS_STOP);
        };
        RegularSpinState.prototype.end = function () {
        };
        RegularSpinState.prototype.setView = function (view) {
            this.view = view;
        };
        RegularSpinState.NAME = "RegularSpinState";
        return RegularSpinState;
    })();
    engine.RegularSpinState = RegularSpinState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var PayTableState = (function () {
        function PayTableState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.setView(controller.view);
        }
        PayTableState.prototype.start = function (data) {
            // update buttons
            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, !this.common.isMobile, false);
            this.afterRotate();
            if (this.common.config.showAllTimeSpinCount) {
                this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT).visible = false;
            }
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
            this.controller.send(engine.NotificationList.OPEN_PAY_TABLE);
        };
        PayTableState.prototype.afterRotate = function () {
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
        };
        PayTableState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.controller.changeState(engine.DefaultState.NAME);
                        break;
                    }
            }
        };
        PayTableState.prototype.onBtnClick = function (buttonName) {
        };
        PayTableState.prototype.end = function () {
            if (this.common.config.showAllTimeSpinCount) {
                this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT).visible = true;
            }
        };
        PayTableState.prototype.setView = function (view) {
            this.view = view;
        };
        PayTableState.NAME = "PayTableState";
        return PayTableState;
    })();
    engine.PayTableState = PayTableState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var DefaultState = (function () {
        function DefaultState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.setView(controller.view);
        }
        DefaultState.prototype.start = function (data) {
            console.log("start DefaultState " + this.view);
            this.controller.send(engine.NotificationList.SHOW_HEADER);
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, true);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, true);
            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, true);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, true);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, true);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, true);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, true);
            this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.BACK_TO_LOBBY_BTN, true, true);
            this.controller.updateAutoPlayBtn();
            this.updateGambleBtn();
            this.controller.updateAutoSpinCountText();
            this.updateBetBtn();
            this.updateLineBtn();
        };
        DefaultState.prototype.afterRotate = function () {
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, true);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, true);
            //this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, true);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, true);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, true);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, true);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, true);
            this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.BACK_TO_LOBBY_BTN, true, true);
            this.controller.updateAutoPlayBtn();
            this.updateGambleBtn();
            this.controller.updateAutoSpinCountText();
            this.updateBetBtn();
            this.updateLineBtn();
            console.log("afterRotate", "DefaultState");
            this.view.setText(engine.HudView.AUTO_SPIN_TEXT, engine.HudView.AUTO_SPIN_TEXT_START);
            this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT).text = "";
        };
        DefaultState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.KEYBOARD_CLICK:
                    {
                        if (data == engine.Keyboard.SPACE) {
                            if (this.common.is_win_popup_showed) {
                                this.controller.tryStartSpin();
                                this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
                                this.common.config.statechanged = false;
                            }
                        }
                        break;
                    }
                case engine.NotificationList.ON_SCREEN_CLICK:
                    {
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.NotificationList.TRY_START_SPIN:
                    {
                        console.log('try TRY_START_SPIN', "DefaultState");
                        if (!this.common.config.statechanged) {
                            this.controller.changeState(engine.RegularSpinState.NAME);
                            this.common.config.statechanged = true;
                        }
                        break;
                    }
                case engine.NotificationList.TRY_START_AUTO_PLAY:
                    {
                        var balanceAfterSpin = this.common.server.getBalance() - this.common.server.getTotalBet();
                        if (balanceAfterSpin >= 0) {
                            this.controller.changeState(engine.AutoSpinState.NAME, data);
                        }
                        else {
                            this.controller.send(engine.NotificationList.SHOW_ERRORS, [engine.ErrorController.ERROR_NO_MONEY_STR]);
                        }
                        break;
                    }
                case engine.NotificationList.SET_BET:
                    {
                        this.common.server.setBet(data);
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        this.controller.updateBetOnServ();
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, true);
                        this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
                        this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, true, true);
                        this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, true);
                        this.view.changeButtonState(engine.HudView.BACK_TO_LOBBY_BTN, true, true);
                        break;
                    }
            }
        };
        DefaultState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.TOTAL_BET_BTN:
                    {
                        this.controller.send(engine.NotificationList.OPEN_SELECT_BET_TABLE);
                        break;
                    }
                case engine.HudView.START_SPIN_BTN:
                case engine.HudViewP.START_SPIN_BTN:
                    {
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.HudView.LINE_COUNT_BTN:
                    {
                        this.common.server.setNextLineCount(true);
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.updateLinesOnServ();
                        break;
                    }
                case engine.HudView.INC_LINE_COUNT_BTN:
                    {
                        this.common.server.setNextLineCount(false);
                        this.updateLineBtn();
                        this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, true);
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.updateLinesOnServ();
                        break;
                    }
                case engine.HudView.DEC_LINE_COUNT_BTN:
                    {
                        this.common.server.setPrevLineCount(false);
                        this.updateLineBtn();
                        this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, true);
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.updateLinesOnServ();
                        break;
                    }
                case engine.HudView.MULTIPLY_BTN:
                    {
                        this.common.server.setNextMultiply();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        break;
                    }
                case engine.HudView.BET_BTN:
                    {
                        this.common.server.setNextBet(true);
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        this.controller.updateBetOnServ();
                        break;
                    }
                case engine.HudView.NEXT_BET_BTN:
                    {
                        this.common.server.setNextBet(false);
                        this.updateBetBtn();
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        this.controller.updateBetOnServ();
                        break;
                    }
                case engine.HudView.PREV_BET_BTN:
                    {
                        this.common.server.setPrevBet(false);
                        this.updateBetBtn();
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        this.controller.updateBetOnServ();
                        break;
                    }
                case engine.HudView.MAX_BET_BTN:
                    {
                        this.common.server.setMaxMultiply();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        this.controller.tryStartSpin();
                        this.controller.updateBetOnServ();
                        break;
                    }
                case engine.HudView.MAX_LINES_BTN:
                    {
                        this.common.server.setMaxLines();
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.HudView.SETTINGS_BTN:
                    {
                        this.controller.send(engine.NotificationList.OPEN_OPTIONS_MENU, this.view);
                        this.view.changeButtonState(engine.HudView.START_SPIN_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.TOTAL_BET_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.SETTINGS_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.BACK_TO_LOBBY_BTN, false, false);
                        break;
                    }
                case engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN:
                    {
                        console.log(this.common.is_win_lines_showed + " " + this.common.is_win_popup_showed, "OPEN_AUTO_SPIN_TABLE_BTN");
                        if (this.common.is_win_popup_showed) {
                            this.controller.send(engine.NotificationList.OPEN_AUTO_SPIN_MENU);
                        }
                        break;
                    }
                case engine.HudView.INC_SPIN_COUNT_BTN:
                    {
                        this.controller.autoPlayCountIdx++;
                        this.controller.updateAutoPlayBtn();
                        this.controller.updateAutoSpinCountText();
                        break;
                    }
                case engine.HudView.DEC_SPIN_COUNT_BTN:
                    {
                        this.controller.autoPlayCountIdx--;
                        this.controller.updateAutoPlayBtn();
                        this.controller.updateAutoSpinCountText();
                        break;
                    }
            }
        };
        DefaultState.prototype.updateGambleBtn = function () {
            //console.log("Update gamble btn");
            var bonusVO = this.common.server.bonus;
            if (bonusVO != null && bonusVO.type == engine.BonusTypes.GAMBLE) {
                this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, true);
            }
        };
        DefaultState.prototype.updateBetBtn = function () {
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, this.common.server.isHasNextBet());
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, this.common.server.isHasPrevBet());
        };
        DefaultState.prototype.updateLineBtn = function () {
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, this.common.server.isHasNextLine());
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, this.common.server.isHasPrevLine());
        };
        DefaultState.prototype.end = function () {
        };
        DefaultState.prototype.setView = function (view) {
            console.log("DefaultState setView", view);
            this.view = view;
        };
        DefaultState.NAME = "DefaultState";
        return DefaultState;
    })();
    engine.DefaultState = DefaultState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var OptionsState = (function () {
        function OptionsState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.settingView;
        }
        OptionsState.prototype.start = function (data) {
            // update buttons
            this.view.changeButtonState(engine.SettingMobileView.CLOSE_BTN, true, true);
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
        };
        OptionsState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        break;
                    }
                case engine.NotificationList.OPEN_OPTIONS_MENU:
                    {
                        break;
                    }
            }
        };
        OptionsState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.SettingMobileView.CLOSE_BTN:
                    {
                        this.controller.settingView.visible = false;
                        console.log('close BTN opt');
                        break;
                    }
            }
        };
        OptionsState.prototype.end = function () {
        };
        OptionsState.prototype.setView = function (view) {
        };
        OptionsState.prototype.afterRotate = function () {
        };
        OptionsState.NAME = "OptionsState";
        return OptionsState;
    })();
    engine.OptionsState = OptionsState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var WinLine = (function (_super) {
        __extends(WinLine, _super);
        function WinLine(container, lineId, winVOs) {
            this.wildLineId = -1;
            this.container = container;
            this.lineId = lineId;
            this.winVOs = winVOs;
        }
        WinLine.prototype.create = function () {
            for (var i = 0; i < this.winVOs.length; i++) {
                var vo = this.winVOs[i];
                var winSymbolView = vo.winSymbolView;
                if (winSymbolView.parent != null) {
                    winSymbolView.parent.removeChild(winSymbolView);
                }
                winSymbolView.x = vo.rect.x;
                winSymbolView.y = vo.rect.y;
                if (!winSymbolView._is_wild && !engine.ReelWildController.wildSymbolsViews[i]._is_wild_to_show) {
                    if (!engine.ReelWildController.isWildReelStarted(i)) {
                        winSymbolView.play();
                        this.container.addChild(winSymbolView);
                    }
                }
                else {
                    this.wildLineId = i;
                    this.dispatchEvent(engine.BaseWinLines.SHOW_WILD_REEL);
                }
            }
        };
        WinLine.prototype.remove = function () {
            for (var i = 0; i < this.winVOs.length; i++) {
                var vo = this.winVOs[i];
                var winSymbolView = vo.winSymbolView;
                this.container.removeChild(winSymbolView);
            }
        };
        WinLine.prototype.getPositions = function () {
            var positions = new Array(this.winVOs.length);
            for (var i = 0; i < this.winVOs.length; i++) {
                positions[i] = this.winVOs[i].posIdx.clone();
            }
            return positions;
        };
        WinLine.prototype.getLineId = function () {
            return this.lineId;
        };
        WinLine.prototype.getWinVOs = function () {
            return this.winVOs;
        };
        WinLine.prototype.getWinSymbolViewByInd = function (ind) {
            return this.winVOs[ind].winSymbolView;
        };
        return WinLine;
    })(EventDispatcher);
    engine.WinLine = WinLine;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var BaseWinLines = (function (_super) {
        __extends(BaseWinLines, _super);
        function BaseWinLines(winLines, time) {
            _super.call(this);
            this.winLines = winLines;
            this.time = time;
            this.leftTime = time;
            this.isAllLineShowed = false;
        }
        BaseWinLines.prototype.onEnterFrame = function () {
            if (--this.leftTime == 0) {
                this.onEndTime();
            }
        };
        BaseWinLines.prototype.create = function () {
        };
        BaseWinLines.prototype.onEndTime = function () {
        };
        BaseWinLines.prototype.dispose = function () {
            for (var i = 0; i < this.winLines.length; i++) {
                this.winLines[i].remove();
            }
            this.winLines = null;
        };
        BaseWinLines.prototype.getWinLines = function () {
            return this.winLines;
        };
        BaseWinLines.EVENT_CREATE_LINE = "create_line";
        BaseWinLines.EVENT_All_LINES_SHOWED = "lines_showed";
        BaseWinLines.EVENT_LINE_SHOWED = "line_showed";
        BaseWinLines.SHOW_WILD_REEL = "shoow_wild_symbol";
        return BaseWinLines;
    })(EventDispatcher);
    engine.BaseWinLines = BaseWinLines;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinLines = (function (_super) {
        __extends(WinLines, _super);
        function WinLines(winLines, time) {
            _super.call(this, winLines, time);
        }
        WinLines.prototype.create = function () {
            this.index = -1;
            this.showNextLine();
        };
        WinLines.prototype.onEndTime = function () {
            if (!this.isAllLineShowed && this.index == this.winLines.length - 1) {
                this.dispatchEvent(engine.BaseWinLines.EVENT_All_LINES_SHOWED);
                this.isAllLineShowed = true;
            }
            if (this.winLines == null || this.winLines.length == 1) {
                return;
            }
            this.winLines[this.index].remove();
            this.showNextLine();
        };
        WinLines.prototype.showNextLine = function () {
            this.leftTime = this.time;
            this.index = (this.index + 1) % this.winLines.length;
            this.showLine();
            this.dispatchEvent(engine.BaseWinLines.EVENT_LINE_SHOWED, this.index);
        };
        WinLines.prototype.showLine = function () {
            var winLine = this.winLines[this.index];
            var winLinesIds = [];
            var lineId = winLine.getLineId();
            if (lineId > 0) {
                winLinesIds.push(lineId);
            }
            this.dispatchEvent(engine.BaseWinLines.EVENT_CREATE_LINE, [winLine.getPositions(), winLinesIds]);
            winLine.create();
        };
        return WinLines;
    })(engine.BaseWinLines);
    engine.WinLines = WinLines;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinLinesTogether = (function (_super) {
        __extends(WinLinesTogether, _super);
        function WinLinesTogether(winLines, time) {
            _super.call(this, winLines, time);
        }
        WinLinesTogether.prototype.create = function () {
            var positions = [];
            var winLinesId = [];
            for (var i = 0; i < this.winLines.length; i++) {
                var winLine = this.winLines[i];
                var lineId = winLine.getLineId();
                if (lineId > 0) {
                    winLinesId.push(lineId);
                }
                WinLinesTogether.pushUniquePos(positions, winLine.getPositions());
                winLine.create();
            }
            this.dispatchEvent(engine.BaseWinLines.EVENT_CREATE_LINE, [positions, winLinesId]);
        };
        WinLinesTogether.pushUniquePos = function (allPositions, positions) {
            for (var i = 0; i < positions.length; i++) {
                var pos = positions[i];
                var isFind = false;
                for (var j = 0; j < allPositions.length; j++) {
                    var pos2 = allPositions[j];
                    if (pos.x == pos2.x && pos.y == pos2.y) {
                        isFind = true;
                        break;
                    }
                }
                if (!isFind) {
                    allPositions.push(pos);
                }
            }
        };
        WinLinesTogether.prototype.onEndTime = function () {
            if (!this.isAllLineShowed) {
                this.dispatchEvent(engine.BaseWinLines.EVENT_All_LINES_SHOWED);
                this.isAllLineShowed = true;
            }
        };
        return WinLinesTogether;
    })(engine.BaseWinLines);
    engine.WinLinesTogether = WinLinesTogether;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BonusHolderController = (function (_super) {
        __extends(BonusHolderController, _super);
        function BonusHolderController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        BonusHolderController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        BonusHolderController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            return notifications;
        };
        BonusHolderController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CREATE_BONUS:
                    {
                        this.startBonus();
                        break;
                    }
                case engine.NotificationList.END_BONUS:
                    {
                        this.removeBonus();
                        break;
                    }
            }
        };
        BonusHolderController.prototype.startBonus = function () {
            var className = this.common.server.bonus.className;
            var BonusClass = engine[className];
            this.bonusController = new BonusClass(this.manager, this.common, this.container);
            this.bonusController.init();
            this.send(engine.NotificationList.SERVER_GOT_BONUS);
        };
        BonusHolderController.prototype.removeBonus = function () {
            this.bonusController.dispose();
            this.bonusController = null;
        };
        BonusHolderController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.removeBonus();
        };
        return BonusHolderController;
    })(engine.BaseController);
    engine.BonusHolderController = BonusHolderController;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.12.2014.
 */
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var Ticker = createjs.Ticker;
    var DrawController = (function (_super) {
        __extends(DrawController, _super);
        function DrawController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            DrawController.DELAY_REMOVE = engine.Utils.float2int(DrawController.DELAY_REMOVE * Ticker.getFPS());
            DrawController.INVISIBLE_TIME = engine.Utils.float2int(DrawController.INVISIBLE_TIME * Ticker.getFPS());
            this.drawLineClass = new engine.DrawLine(this.view, this.common);
        }
        DrawController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        DrawController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.REMOVE_WIN_LINES);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.GET_ALL_IDS);
            notifications.push(engine.NotificationList.SHOW_LINES);
            if (!this.common.isMobile) {
                notifications.push(engine.NotificationList.CHANGED_LINE_COUNT);
            }
            return notifications;
        };
        DrawController.prototype.drawLine = function () {
            var newLine;
            var lines = this.common.server.arrLinesIds;
            for (var n = 0; n < lines.length; n++) {
                newLine = this.drawLineClass.create(lines[n], 0);
                this.view.addChild(newLine);
            }
        };
        DrawController.prototype.showLinesFromTo = function (fromId, toId) {
            var newLine;
            for (var n = fromId; n <= toId; n++) {
                newLine = this.drawLineClass.create(n, 1);
                this.view.addChild(newLine);
            }
        };
        DrawController.prototype.remove = function () {
            this.drawLineClass.remove();
        };
        DrawController.prototype.createMask = function (positions) {
            var symbolsRect = this.common.symbolsRect;
            var mask = new Shape();
            mask.graphics.beginFill("0");
            for (var x = 0; x < symbolsRect.length; x++) {
                var reelSymbolsRect = symbolsRect[x];
                for (var y = 0; y < reelSymbolsRect.length; y++) {
                    if (!isExist(x, y, positions)) {
                        var rect = this.common.symbolsRect[x][y];
                        mask.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
                    }
                }
            }
            this.view.mask = mask;
            function isExist(x, y, positions) {
                for (var i = 0; i < positions.length; i++) {
                    var position = positions[i];
                    if (position.x == x && position.y == y) {
                        return true;
                    }
                }
                return false;
            }
        };
        DrawController.prototype.handleNotification = function (message, data) {
            var _this = this;
            switch (message) {
                case engine.NotificationList.GET_ALL_IDS:
                    {
                        this.drawLine();
                        this.drawLineClass.reset();
                        break;
                    }
                case engine.NotificationList.REMOVE_WIN_LINES:
                    {
                        this.remove();
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.visible = true;
                        break;
                    }
                case engine.NotificationList.SHOW_LINES:
                    {
                        this.createMask(data[0]);
                        this.drawLineClass.showNextLine();
                        break;
                    }
                case engine.NotificationList.CHANGED_LINE_COUNT:
                    {
                        Tween.removeTweens(this.view);
                        this.send(engine.NotificationList.REMOVE_WIN_LINES);
                        this.view.mask = null;
                        this.view.alpha = 1;
                        this.showLinesFromTo(1, this.common.server.lineCount);
                        Tween.get(this.view, { useTicks: true }).wait(DrawController.DELAY_REMOVE).to({ alpha: 0 }, DrawController.INVISIBLE_TIME).call(function () {
                            _this.remove();
                            _this.view.alpha = 1;
                        });
                        break;
                    }
            }
        };
        DrawController.DELAY_REMOVE = 2;
        DrawController.INVISIBLE_TIME = 1;
        return DrawController;
    })(engine.BaseController);
    engine.DrawController = DrawController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var Ticker = createjs.Ticker;
    var LinesController = (function (_super) {
        __extends(LinesController, _super);
        function LinesController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            LinesController.DELAY_REMOVE = engine.Utils.float2int(LinesController.DELAY_REMOVE * Ticker.getFPS());
            LinesController.INVISIBLE_TIME = engine.Utils.float2int(LinesController.INVISIBLE_TIME * Ticker.getFPS());
        }
        LinesController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.view.create();
            this.view.hideAllLines();
        };
        LinesController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_LINES);
            notifications.push(engine.NotificationList.HIDE_LINES);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            if (!this.common.isMobile) {
                notifications.push(engine.NotificationList.CHANGED_LINE_COUNT);
            }
            return notifications;
        };
        LinesController.prototype.createMask = function (positions) {
            var symbolsRect = this.common.symbolsRect;
            var mask = new Shape();
            mask.graphics.beginFill("0");
            for (var x = 0; x < symbolsRect.length; x++) {
                var reelSymbolsRect = symbolsRect[x];
                for (var y = 0; y < reelSymbolsRect.length; y++) {
                    if (!isExist(x, y, positions)) {
                        var rect = this.common.symbolsRect[x][y];
                        mask.graphics.drawRect(rect.x, rect.y, rect.width + 5, rect.height + 5);
                    }
                }
            }
            this.view.mask = mask;
            function isExist(x, y, positions) {
                for (var i = 0; i < positions.length; i++) {
                    var position = positions[i];
                    if (position.x == x && position.y == y) {
                        return true;
                    }
                }
                return false;
            }
        };
        LinesController.prototype.handleNotification = function (message, data) {
            var _this = this;
            switch (message) {
                case engine.NotificationList.SHOW_LINES:
                    {
                        this.createMask(data[0]);
                        this.view.showLines(data[1]);
                        break;
                    }
                case engine.NotificationList.START_SPIN:
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.HIDE_LINES:
                    {
                        this.view.mask = null;
                        this.view.hideAllLines();
                        break;
                    }
                case engine.NotificationList.CHANGED_LINE_COUNT:
                    {
                        Tween.removeTweens(this.view);
                        this.send(engine.NotificationList.REMOVE_WIN_LINES);
                        this.view.mask = null;
                        this.view.alpha = 1;
                        this.view.showLinesFromTo(1, this.common.server.lineCount);
                        Tween.get(this.view, { useTicks: true }).wait(LinesController.DELAY_REMOVE).to({ alpha: 0 }, LinesController.INVISIBLE_TIME).call(function () {
                            _this.view.hideAllLines();
                            _this.view.alpha = 1;
                        });
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.visible = true;
                        break;
                    }
            }
        };
        LinesController.DELAY_REMOVE = 2;
        LinesController.INVISIBLE_TIME = 1;
        return LinesController;
    })(engine.BaseController);
    engine.LinesController = LinesController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Sound = createjs.Sound;
    var Container = createjs.Container;
    var Shape = createjs.Shape;
    var SettingMobileController = (function (_super) {
        __extends(SettingMobileController, _super);
        function SettingMobileController(manager, common, view, name, hudContainer, stage) {
            _super.call(this, manager);
            this.dragFactor = 5;
            this.payViewsAdded = false;
            this.bgScaleY = 1;
            this.payViewsAll = [];
            this.common = common;
            this.settingView = this.common.layouts[name];
            this.gameContainer = view;
            this.topScroll = 0;
            this.viewsCount = this.common.config.viewsCount;
            this.hudContainer = hudContainer;
            this.stage = stage;
        }
        SettingMobileController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.allSettingsView = new Container();
            this.allSettingsView.visible = false;
            this.soundView = new Container();
            this.initContainers();
            this.settingView.create();
            this.soundViewName = this.settingView.getSoundTempl();
            this.gameContainer.addChild(this.allSettingsView);
            this.initButtons();
            this.settingViewHeight = this.settingViewHeight || this.settingView.getBounds().height;
        };
        SettingMobileController.prototype.initContainers = function () {
            for (var a = 0; a < this.viewsCount; a++) {
                this.payViewsAll.push(new Container());
            }
            this.payViews = new Container();
        };
        SettingMobileController.prototype.setupLandscapeMode = function () {
            this.scaleSettings(false);
            this.payViews.y = 0;
            var topStart = 0;
            this.gameContainer.x = this.controlContainer.parent.x;
            this.payViews.visible = false;
            var screenSize = this.headerHeight * 1.8;
            var maskHeight = (this.common.isMobile) ? window.innerHeight : this.settingViewHeight / this.bgScaleY;
            var mask = new Shape();
            mask.graphics.beginFill("0").drawRect(0, 0, this.allSettingsView.getBounds().width * this.allSettingsView.scaleX, maskHeight);
            this.allSettingsView.mask = mask;
            //this.allSettingsView.hitArea = mask;
            var contain = this;
            contain.settingView.updateMove(1);
            var diff;
            var lastStageY = null;
            var bottomPoint = this.allSettingsView.getBounds().height;
            var doDrag = function (event) {
                //console.log("contain.settingViewHeight= "+contain.settingViewHeight+" contain.settingView.scaleY="+contain.allSettingsView.scaleY+" contain.settingViewHeight="+contain.settingViewHeight+" contain.payViews.getBounds().height="+contain.payViews.getBounds().height+" this.settingView.getBounds().height="+contain.settingView.getBounds().height);
                if (contain.payViews.getBounds().height * contain.allSettingsView.scaleY > window.innerHeight) {
                    if (lastStageY == null) {
                        lastStageY = event.stageY;
                    }
                    else {
                        diff = event.stageY - lastStageY;
                        //console.log("diff=" + diff + " contain.payViews.y=" + contain.payViews.y + " bottomPoint=" + bottomPoint + " contain.payViews.getBounds().height=" + contain.payViews.getBounds().height);
                        if ((contain.payViews.y + diff) <= (topStart) && (contain.payViews.y + diff + 100) >= (bottomPoint - contain.payViews.getBounds().height)) {
                            contain.payViews.y += diff;
                        }
                        else {
                            if ((contain.payViews.y + diff) > (topStart)) {
                                contain.payViews.y = topStart;
                            }
                            else {
                                if ((contain.payViews.y + diff) < (bottomPoint - contain.payViews.getBounds().height))
                                    contain.payViews.y = bottomPoint - contain.payViews.getBounds().height - 100;
                            }
                        }
                        var indicator = Math.min(Math.abs(Math.floor(contain.payViews.y / screenSize)), 4);
                        //var indicator:number = Math.floor(contain.payViews.y / (contain.settingViewHeight - window.innerHeight) / 4) + 1;
                        contain.settingView.updateMove(indicator + 1);
                    }
                }
            };
            var onUp = function () {
                lastStageY = null;
            };
            this.payViews.removeAllEventListeners("pressmove");
            this.payViews.removeAllEventListeners("pressup");
            this.payViews.on("pressmove", doDrag);
            this.payViews.on("pressup", onUp);
            for (var n = 1; n < 5; n++) {
                this.payViewsAll[n].y = (n > 1) ? this.payViewsAll[n - 1].y + screenSize : screenSize;
            }
        };
        SettingMobileController.prototype.setupPortraitMode = function () {
            this.scaleSettings(true);
            var topStart = (this.headerHeight * 1.65);
            this.payViews.y = topStart;
            //var heightMask:number = this.allSettingsView.getBounds().height - topStart;
            var heightMask = window.innerHeight - topStart;
            var widthMask = this.allSettingsView.getBounds().width;
            var mask = new Shape();
            mask.graphics.beginFill("0").drawRect(0, topStart, widthMask, heightMask);
            this.payViews.mask = mask;
            this.payViews.hitArea = mask;
            var contain = this;
            var diff;
            var lastY;
            var lastStageY = null;
            //var bottomPoint:number = this.allSettingsView.getBounds().height - topStart;
            var bottomPoint = 900;
            var sliderPos = topStart;
            var payViewsToSliderHeight = 1 / 4;
            var newPayViewsPosY;
            var doDrag = function (event) {
                if (lastStageY == null) {
                    lastStageY = event.stageY;
                }
                else {
                    diff = event.stageY - lastStageY;
                    if ((contain.payViews.y + diff) <= (topStart) && (contain.payViews.y + diff) >= (bottomPoint - contain.payViews.getBounds().height)) {
                        newPayViewsPosY = contain.payViews.y + diff;
                        sliderPos -= diff * payViewsToSliderHeight;
                    }
                    else {
                        if ((contain.payViews.y + diff) > (topStart)) {
                            newPayViewsPosY = topStart;
                            sliderPos = topStart;
                        }
                        if ((contain.payViews.y + diff) < (bottomPoint - contain.payViews.getBounds().height)) {
                            newPayViewsPosY = bottomPoint - contain.payViews.getBounds().height;
                            sliderPos = bottomPoint + 300;
                        }
                    }
                    contain.payViews.y = newPayViewsPosY;
                    contain.settingView.updateMove(sliderPos);
                }
            };
            var onUp = function () {
                lastStageY = null;
            };
            this.payViews.removeAllEventListeners("pressmove");
            this.payViews.removeAllEventListeners("pressup");
            this.payViews.on("pressmove", doDrag);
            this.payViews.on("pressup", onUp);
            this.payViewsAll[1].y = this.headerHeight * 3;
            this.payViewsAll[2].y = this.payViewsAll[1].y + this.headerHeight * 4.3;
            this.payViewsAll[3].y = this.payViewsAll[2].y + this.headerHeight * 4;
            this.payViewsAll[4].y = this.payViewsAll[3].y + (this.headerHeight * 2.7);
        };
        SettingMobileController.prototype.initSoundToggle = function () {
            var _this = this;
            var soundToggle = this.soundView.getChildByName(SettingMobileController.SOUND_TOGGLE);
            if (soundToggle != null) {
                if (!this.common.isSoundOn)
                    soundToggle.gotoAndStop(1);
                else
                    soundToggle.gotoAndStop(0);
                soundToggle.removeAllEventListeners("click");
                soundToggle.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        if (!_this.common.isSoundLoaded) {
                            _this.send(engine.NotificationList.LOAD_SOUNDS);
                        }
                        if (_this.common.isSoundOn)
                            soundToggle.gotoAndStop(1);
                        else
                            soundToggle.gotoAndStop(0);
                        _this.send(engine.NotificationList.SOUND_TOGGLE);
                        Sound.play(engine.SoundsList.REGULAR_CLICK_BTN);
                    }
                });
            }
        };
        SettingMobileController.prototype.createPage = function () {
            console.log("createPage");
            this.createPayViews();
            this.clearSoundCont();
            var settingView = this.common.layouts[this.soundViewName];
            settingView.create(this.soundView);
            this.headerHeight = this.soundView.getBounds().height;
            this.soundView.getChildByName(SettingMobileController.SOUND_TOGGLE).scaleY = 1.1; // special for iphone5 and iPod, that resize toggle if scale=1
            this.allSettingsView.addChild(this.settingView);
            this.settingView.resizeForMobile();
            this.allSettingsView.addChild(this.payViews);
            this.allSettingsView.addChild(this.soundView);
            this.soundView.visible = true;
            if (this.settingView.isPortrait)
                this.setupPortraitMode();
            else
                this.setupLandscapeMode();
            this.updateOptions();
            var hitArea = new Shape();
            hitArea.graphics.beginFill("0").drawRect(this.payViews.x, this.payViews.y, this.payViews.getBounds().width, this.payViews.getBounds().height);
            this.payViews.hitArea = hitArea;
            this.totalHeight = this.payViewsAll[this.payViewsAll.length - 1].y;
            this.initSoundToggle();
        };
        SettingMobileController.prototype.scaleSettings = function (isPortait) {
            //this.settingViewHeight = this.settingViewHeight || this.settingView.getBounds().height;
            //var scaleSettingContainer:number = this.hudContainer.getBounds().width * this.hudContainer.scaleX / (this.settingView.getBounds().width);
            var scaleSettingContainer;
            if (this.common.isMobile) {
                if (window.innerWidth > window.innerHeight) {
                    scaleSettingContainer = this.hudContainer.scaleX * Math.min(this.common.artWidth / this.settingView.getBounds().width, this.common.artHeight / this.settingViewHeight);
                }
                else {
                    scaleSettingContainer = Math.min(window.innerWidth / this.settingView.getBounds().width, window.innerHeight / this.settingViewHeight);
                }
                this.bgScaleY = window.innerHeight / this.settingViewHeight / scaleSettingContainer;
            }
            else {
                scaleSettingContainer = Math.min(this.hudContainer.getBounds().width / this.settingView.getBounds().width, this.hudContainer.getBounds().height / this.settingViewHeight);
                this.bgScaleY = this.hudContainer.getBounds().height / this.settingViewHeight / scaleSettingContainer;
            }
            this.allSettingsView.scaleX = this.allSettingsView.scaleY = scaleSettingContainer;
            console.log("window.innerHeight=" + window.innerHeight + " this.settingView.getBounds().height=" + this.settingView.getBounds().height + " this.allSettingsView.scaleY=" + this.allSettingsView.scaleY + " this.bgScaleY=" + this.bgScaleY);
            this.settingView.getChildByName(SettingMobileController.BG_INFO).scaleY = this.bgScaleY;
            this.settingView.getChildByName(SettingMobileController.BG_SIDEBAR).scaleY = this.bgScaleY;
            var options_btn = this.settingView.getBtn(engine.SettingMobileView.OPTIONS_BTN);
            if (options_btn)
                options_btn.y = 0.2 * this.settingViewHeight * this.settingView.scaleX;
            var pay_btn = this.settingView.getBtn(engine.SettingMobileView.PAY_BTN);
            if (pay_btn)
                pay_btn.y = 0.7 * this.settingViewHeight * this.settingView.scaleY;
        };
        SettingMobileController.prototype.createPayViews = function () {
            var payViewName;
            this.totalHeight = 0;
            if (!this.payViewsAdded) {
                for (var a = 0; a < this.payViewsAll.length; a++) {
                    var viewCont = this.payViewsAll[a];
                    payViewName = this.settingView.payViews[a];
                    var payView = this.common.layouts[payViewName];
                    payView.create(viewCont);
                    this.payViews.addChild(viewCont);
                }
                this.payViewsAdded = true;
            }
        };
        SettingMobileController.prototype.updateOptions = function () {
            var bet = this.soundView.getChildByName(SettingMobileController.BET_STRING_NAME);
            bet.text = engine.MoneyFormatter.format(this.common.server.bet, true);
            var lines = this.soundView.getChildByName(SettingMobileController.LINES_COUNT_STRING_NAME);
            lines.text = this.common.server.lineCount.toString();
        };
        SettingMobileController.prototype.clearSoundCont = function () {
            while (this.soundView.getNumChildren() > 0) {
                this.soundView.removeChildAt(0);
            }
        };
        SettingMobileController.prototype.clearPageContainer = function () {
            this.gameContainer.removeAllChildren();
        };
        SettingMobileController.prototype.initOptState = function () {
            this.states = {};
            this.states[engine.OptionsState.NAME] = new engine.OptionsState(this);
            this.changeState(engine.OptionsState.NAME);
        };
        SettingMobileController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.OPEN_OPTIONS_MENU);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.INIT_OPTIONS_STATE);
            notifications.push(engine.NotificationList.TRY_START_SPIN);
            notifications.push(engine.NotificationList.CHANGE_DEVICE_ORIENTATION);
            return notifications;
        };
        SettingMobileController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.tryClosePayTables();
                        break;
                    }
                case engine.NotificationList.OPEN_OPTIONS_MENU:
                    {
                        this.controlContainer = data;
                        this.createPage();
                        this.allSettingsView.visible = true;
                        break;
                    }
                case engine.NotificationList.INIT_OPTIONS_STATE:
                    {
                        this.initOptState();
                        break;
                    }
                case engine.NotificationList.TRY_START_SPIN:
                    {
                        if (this.allSettingsView.visible) {
                            this.send(engine.NotificationList.CLOSE_PAY_TABLE);
                        }
                        break;
                    }
                case engine.NotificationList.CHANGE_DEVICE_ORIENTATION:
                    {
                        this.tryClosePayTables();
                        break;
                    }
            }
        };
        SettingMobileController.prototype.removeHandlers = function () {
            var buttonsNames = this.settingView.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                var button = this.settingView.getBtn(buttonsNames[i]);
                if (button && button != null)
                    button.removeAllEventListeners("click");
            }
        };
        SettingMobileController.prototype.initButtons = function () {
            var _this = this;
            var buttonsNames = this.settingView.buttonsNames;
            for (var i = 0; i < buttonsNames.length; i++) {
                var buttonName = buttonsNames[i];
                var button = this.settingView.getBtn(buttonName);
                if (button == null) {
                    continue;
                }
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        SettingMobileController.prototype.onBtnClick = function (buttonName) {
            console.log("onBtnClick " + buttonName);
            switch (buttonName) {
                case engine.SettingMobileView.CLOSE_BTN:
                    {
                        this.tryClosePayTables();
                        break;
                    }
                case engine.SettingMobileView.PAY_BTN:
                    {
                        this.soundView.visible = false;
                        this.payViews.visible = true;
                        this.settingView.toggleIndicator(true);
                        break;
                    }
                case engine.SettingMobileView.OPTIONS_BTN:
                    {
                        this.soundView.visible = true;
                        this.payViews.visible = false;
                        this.settingView.toggleIndicator(false);
                        break;
                    }
            }
        };
        SettingMobileController.prototype.changeState = function (name, data) {
            if (data === void 0) { data = null; }
            console.log('init state ' + name);
            if (this.currentState != null) {
                this.currentState.end();
            }
            this.currentState = this.states[name];
            this.currentState.start(data);
        };
        SettingMobileController.prototype.dispose = function () {
            this.removeHandlers();
            _super.prototype.dispose.call(this);
            this.common = null;
            this.clearPageContainer();
        };
        SettingMobileController.prototype.tryClosePayTables = function () {
            if (this.allSettingsView.visible) {
                this.allSettingsView.visible = false;
                this.send(engine.NotificationList.CLOSE_PAY_TABLE);
            }
        };
        SettingMobileController.BG_INFO = "bg_info";
        SettingMobileController.BG_SIDEBAR = "bg_sidebar";
        SettingMobileController.BET_STRING_NAME = "bet";
        SettingMobileController.LINES_COUNT_STRING_NAME = "betLines";
        SettingMobileController.SOUND_TOGGLE = "soundSwitcher";
        return SettingMobileController;
    })(engine.BaseController);
    engine.SettingMobileController = SettingMobileController;
})(engine || (engine = {}));
/**
 * Created by Taras on 02.02.2015.
 */
var engine;
(function (engine) {
    //	import Tween = createjs.Tween;
    //	import Point = createjs.Point;
    //	import Shape = createjs.Shape;
    //	import Ticker = createjs.Ticker;
    //	import Graphics = createjs.Graphics;
    //	import Rectangle = createjs.Rectangle;
    var BorderController = (function (_super) {
        __extends(BorderController, _super);
        function BorderController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            //this.view.visible = false;
            //			LinesController.DELAY_REMOVE = Utils.float2int(LinesController.DELAY_REMOVE * Ticker.getFPS());
            //			LinesController.INVISIBLE_TIME = Utils.float2int(LinesController.INVISIBLE_TIME * Ticker.getFPS());
        }
        BorderController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.view.create();
            //			this.view.hideAllLines();
        };
        return BorderController;
    })(engine.BaseController);
    engine.BorderController = BorderController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var BackgroundController = (function (_super) {
        __extends(BackgroundController, _super);
        function BackgroundController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        BackgroundController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.changeType();
        };
        BackgroundController.prototype.createView = function (type) {
            if (window.innerHeight > window.innerWidth && this.common.isMobile)
                type += 'P';
            var creator = this.common.layouts[type];
            if (creator != null) {
                if (this.view != null) {
                    this.container.removeChild(this.view);
                }
                this.view = new Container();
                creator.create(this.view);
                this.view.y -= 150;
                this.container.addChild(this.view);
            }
        };
        BackgroundController.prototype.changeType = function () {
            var bonus = this.common.server.bonus;
            if (bonus != null && bonus.type == engine.BonusTypes.FREE_SPINS) {
                //this.createView(BackgroundController.FREE_SPINS_LAYOUT_NAME);
                this.createView(BackgroundController.REGULAR_LAYOUT_NAME);
            }
            else {
                this.createView(BackgroundController.REGULAR_LAYOUT_NAME);
            }
        };
        BackgroundController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            return notifications;
        };
        BackgroundController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.END_BONUS:
                    {
                        this.changeType();
                        break;
                    }
            }
        };
        BackgroundController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        BackgroundController.REGULAR_LAYOUT_NAME = "BackgroundView";
        BackgroundController.FREE_SPINS_LAYOUT_NAME = "FreeSpinsBackgroundView";
        return BackgroundController;
    })(engine.BaseController);
    engine.BackgroundController = BackgroundController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Container = createjs.Container;
    var HeaderController = (function (_super) {
        __extends(HeaderController, _super);
        function HeaderController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            HeaderController.DELAY = engine.Utils.float2int(HeaderController.DELAY * Ticker.getFPS());
        }
        HeaderController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        HeaderController.prototype.createView = function (type) {
            var creator = this.common.layouts[type];
            if (creator != null) {
                this.view = new Container();
                creator.create(this.view);
                this.delay = 0;
                this.animation = this.view.getChildByName(HeaderController.ANIMATION_CONTAINER);
                if (this.animation != null) {
                    this.animationFrames = this.animation.spriteSheet.getNumFrames(null) - 1;
                }
            }
        };
        HeaderController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_HEADER);
            notifications.push(engine.NotificationList.REMOVE_HEADER);
            return notifications;
        };
        HeaderController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_HEADER:
                    {
                        if (this.view == null) {
                            this.createView(HeaderController.REGULAR_LAYOUT_NAME);
                        }
                        if (!this.container.contains(this.view)) {
                            this.container.addChild(this.view);
                        }
                        break;
                    }
                case engine.NotificationList.REMOVE_HEADER:
                    {
                        if (this.view != null && this.container.contains(this.view)) {
                            this.container.removeChild(this.view);
                        }
                        break;
                    }
            }
        };
        HeaderController.prototype.onEnterFrame = function () {
            if (this.animation != null) {
                if (this.animation.paused) {
                    if (this.delay == 0) {
                        this.animation.gotoAndPlay(0);
                    }
                    this.delay -= 1;
                }
                else {
                    if (this.animation.currentFrame == this.animationFrames) {
                        this.animation.stop();
                        this.delay = HeaderController.DELAY;
                    }
                }
            }
        };
        HeaderController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        HeaderController.REGULAR_LAYOUT_NAME = "HeaderView";
        HeaderController.FREE_SPINS_LAYOUT_NAME = "FreeSpinsHeaderView";
        HeaderController.ANIMATION_CONTAINER = "animation";
        HeaderController.DELAY = 2;
        return HeaderController;
    })(engine.BaseController);
    engine.HeaderController = HeaderController;
})(engine || (engine = {}));
//new controller
var engine;
(function (engine) {
    var Socket = io;
    var ServerController = (function (_super) {
        __extends(ServerController, _super);
        function ServerController(manager, common) {
            _super.call(this, manager);
            this.startingBonus = false;
            this.actions = {
                'test': { 'action': 'play', id: engine.GameConst.ID_JAYA },
                'bonus': { 'action': 'play', id: engine.GameConst.ID_JAYA, 'context': [5, 14, 4, 17, 1] }
            };
            this.common = common;
        }
        ServerController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.startGame = false;
            this.common.jayaServer = true;
        };
        ServerController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.RES_LOADED);
            notifications.push(engine.NotificationList.SERVER_SEND_SPIN);
            notifications.push(engine.NotificationList.SERVER_SEND_BONUS);
            notifications.push(engine.NotificationList.SERVER_SET_WHEEL);
            notifications.push(engine.NotificationList.SERVER_DISCONNECT);
            notifications.push(engine.NotificationList.CHANGED_LINE_COUNT);
            notifications.push(engine.NotificationList.CHANGE_BET);
            return notifications;
        };
        ServerController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.RES_LOADED:
                    {
                        this.common.server = new engine.ServerData();
                        this.sendJayaRequest();
                    }
                    break;
                case engine.NotificationList.SERVER_SEND_SPIN:
                    {
                        this.send(engine.NotificationList.SHOW_ALL_SYMBOLS);
                        this.playAction();
                    }
                    break;
                case engine.NotificationList.SERVER_SEND_BONUS:
                    {
                        this.send(engine.NotificationList.SHOW_ALL_SYMBOLS);
                        this.playAction();
                    }
                    break;
                case engine.NotificationList.SERVER_SET_WHEEL:
                    {
                    }
                    break;
                case engine.NotificationList.SERVER_DISCONNECT:
                    {
                    }
                    break;
                case engine.NotificationList.CHANGE_BET:
                case engine.NotificationList.CHANGED_LINE_COUNT:
                    {
                        this.updateBets();
                    }
                    break;
            }
        };
        ServerController.prototype.updateBets = function () {
            this.socketIo.emit('action', { 'action': 'bet', 'context': [this.common.server.lineCount, this.common.server.bet], id: engine.GameConst.ID_JAYA });
        };
        ServerController.prototype.sendJayaRequest = function () {
            var self = this;
            this.socketIo = Socket.connect(engine.GameConst.SERVER_URL_JAYA);
            this.socketIo.emit('join', { 'id': engine.GameConst.ID_JAYA });
            this.socketIo.on('join', function (data) {
                self.sendConfig();
            });
        };
        ServerController.prototype.sendConfig = function () {
            this.common.server.bonus = null;
            var self = this;
            this.socketIo.emit('action', { 'action': 'config', id: engine.GameConst.ID_JAYA });
            this.socketIo.on('action', function (data) {
                switch (data.action) {
                    case 'bet':
                        if (!this.startGame) {
                            this.startGame = true;
                            self.onAuthorized();
                        }
                        break;
                    case 'config':
                        self.setActions(data.events[0]);
                        break;
                    case 'play':
                        self.parseServResult(data);
                        break;
                }
            });
            this.socketIo.on('balance', function (data) {
                self.common.server.setBalance(parseFloat(data));
            });
        };
        ServerController.prototype.playAction = function () {
            var params = this.actions[this.common.login];
            if (this.common.login == 'bonus')
                this.common.login = 'test';
            console.log(params, "params");
            this.socketIo.emit('action', params);
        };
        ServerController.prototype.parseServResult = function (data) {
            console.log(data, "parseServResult");
            this.common.server.win = 0;
            this.common.server.winLines = [];
            this.common.server.arrLinesIds = [];
            this.startingBonus = false;
            var events = data.events;
            var winlines = [];
            var win = false;
            for (var n = 0; n < events.length; n++) {
                if (events[n].event == 'spinWin') {
                    win = true;
                    var context = events[n].context;
                    winlines.push(context);
                    var mode = context['mode'];
                    if (mode == 'scatter' && context['what'] == "bonus") {
                        this.common.server.scatters = context["context"];
                        this.startingBonus = true;
                    }
                }
                if (events[n].event == 'playedSpin') {
                    this.setResultWeels(events[n].context);
                }
                if (events[n].event == 'enterBonus') {
                    this.setBonus(events[n].context);
                }
                if (events[n].event == 'playedBonusSpin') {
                    this.processBonusStep(events[n].context);
                }
                if (events[n].event == 'gameEnd') {
                    var winValue = events[n].context['win'];
                    //this.common.server.win = winValue;
                    this.send(engine.NotificationList.END_FREE_SPINS, winValue);
                }
            }
            if (win && !this.startingBonus) {
                for (var i = 0; i < winlines.length; i++) {
                    var winContext = winlines[i].context;
                    this.parseLine(winContext, winlines[i].occurs, winlines[i].pay, winlines[i]);
                }
            }
            //console.log(this.common.server.winLines,"this.common.server.winLines parseServResult");
            this.send(engine.NotificationList.SERVER_GOT_SPIN);
        };
        ServerController.prototype.setResultWeels = function (reels) {
            var resultArray = [];
            for (var i = 0; i < reels.length; i++) {
                var reel = reels[i];
                for (var s = 0; s < reel.length; s++) {
                    var symbolIndex = this.parseSymbol(reel[s]);
                    resultArray.push(symbolIndex + 1);
                    if ((symbolIndex + 1) == this.common.config.wildSymbolId && typeof (engine.ReelWildController.wildSymbolsViews[i]) !== "undefined")
                        engine.ReelWildController.wildSymbolsViews[i]._is_wild_to_show = true;
                }
            }
            this.common.server.wheel = resultArray;
        };
        ServerController.prototype.parseSymbol = function (symbol) {
            var targetNames = ['wild', 'bonus', 'h1', 'h2', 'h3', 'h4', 'h5', 'l1', 'l2', 'l3', 'l4'];
            return targetNames.indexOf(symbol);
        };
        ServerController.prototype.getBonusIndex = function (name) {
            var bonusNames = [
                'freespins_high_icon',
                'freespins_reel_3_wild',
                'freespins_all_scatter',
                'freespins_reel_2_4_wild',
                'freespins_reel_1_3_5_wild'
            ];
            return bonusNames.indexOf(name);
        };
        ServerController.prototype.processBonusStep = function (context) {
            this.common.server.bonus.step = context['left'];
            return;
        };
        ServerController.prototype.setBonus = function (context) {
            var triggering = context['triggering'];
            var bonusVO = new engine.BonusVO();
            bonusVO.id = 2;
            bonusVO.key = 'key';
            bonusVO.step = context['left'];
            bonusVO.type = triggering.bonus;
            console.log(bonusVO.type, 'BONUS TYPE');
            bonusVO.className = "FreeSpinsController";
            bonusVO.data = { 'sector': this.getBonusIndex(triggering.bonus) };
            this.common.server.bonus = bonusVO;
        };
        ServerController.prototype.parseLine = function (context, count, pay, context_full) {
            var lineId = context['paylineId'];
            if (lineId >= 0 && lineId != null && context_full['mode'] != 'scatter') {
                var winLineVO = new engine.WinLineVO();
                winLineVO.lineId = lineId;
                winLineVO.win = pay;
                winLineVO.winPos = [];
                var line = '';
                var positions = context['payline'];
                for (var n = 0; n < positions.length; n++) {
                    var current = positions[n];
                    line += current;
                    if (n < count)
                        winLineVO.winPos.push(current);
                }
                this.common.server.winLines.push(winLineVO);
                this.common.server.arrLinesIds.push(winLineVO.lineId);
            }
            else if (context_full['what'] != "bonus" && context_full['mode'] == 'scatter') {
                var winLineVO = new engine.WinLineVO();
                winLineVO.win = pay;
                winLineVO.winPos = [];
                for (var i = 0; i < engine.ReelWildController.reelsCnt; ++i) {
                    winLineVO.winPos[i] = -1;
                }
                for (var i = 0; i < context.length; ++i) {
                    winLineVO.winPos[context[i]['reel']] = context[i]['row'];
                }
                this.common.server.winLines.push(winLineVO);
            }
        };
        ServerController.prototype.setLines = function (lines) {
            this.common.config.allLines = lines['availablePayLines'];
        };
        ServerController.prototype.setRegularSpins = function (reels) {
            var lines = reels['reels'];
            var initWeelArray = [];
            for (var a = 0; a < lines.length; a++) {
                var lineReel = lines[a];
                var newLineArray = [];
                for (var n = 0; n < lineReel.length; n++) {
                    var symbolName = lineReel[n];
                    var index = this.parseSymbol(symbolName);
                    //console.log(symbolName, index);
                    index++;
                    newLineArray.push(index);
                    if (n < 3)
                        initWeelArray.push(index);
                }
                this.common.config.reelStrips.regular[a] = newLineArray;
            }
            this.common.server.wheel = initWeelArray;
        };
        ServerController.prototype.setActions = function (config) {
            //console.log(config, "setActions");
            var context = config['context'];
            this.setLines(context);
            this.setRegularSpins(context);
            this.common.server.jackpot = parseFloat("0.00");
            this.common.server.multiply = 1;
            if (config['context']['availablePayLines']) {
                this.common.server.maxLineCount = config['context']['availablePayLines'].length;
                this.common.server.lineCount = this.common.server.maxLineCount;
            }
            this.common.server.availableMultiples = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            //this.common.server.availableBets = [0.02, 0.05, 0.1, 0.2, 0.25];
            this.common.server.availableBets = this.common.config.selectBetCount;
            this.common.server.bet = parseFloat(this.common.server.availableBets[0].toString());
            this.common.server.bonus = null;
            this.socketIo.emit('action', { 'action': 'bet', 'context': [this.common.server.lineCount, this.common.server.bet], id: engine.GameConst.ID_JAYA });
        };
        ServerController.prototype.onAuthorized = function () {
            this.send(engine.NotificationList.SERVER_INIT);
        };
        ServerController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
        };
        return ServerController;
    })(engine.BaseController);
    engine.ServerController = ServerController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Sound = createjs.Sound;
    var PayTableController = (function (_super) {
        __extends(PayTableController, _super);
        function PayTableController(manager, common, gameContainer) {
            _super.call(this, manager);
            this.common = common;
            this.gameContainer = gameContainer;
            this.pageIdx = 1;
        }
        PayTableController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.payTableContainer = new Container();
            this.pageContainer = new Container();
            this.payTableContainer.addChild(this.pageContainer);
            this.hudView = this.common.layouts[engine.PayTableHudView.LAYOUT_NAME];
            this.hudView.create();
            this.createPage(this.pageIdx);
            this.payTableContainer.addChild(this.hudView);
            this.initButtonsHandler();
        };
        PayTableController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            return notifications;
        };
        PayTableController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        break;
                    }
            }
        };
        PayTableController.prototype.initButtonsHandler = function () {
            var _this = this;
            var buttonsNames = this.hudView.buttonsNames;
            for (var i = 0; i < buttonsNames.length; i++) {
                this.hudView.getBtn(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        PayTableController.playBtnSound(eventObj.currentTarget.name);
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        PayTableController.playBtnSound = function (buttonName) {
            Sound.play(engine.SoundsList.REGULAR_CLICK_BTN);
        };
        PayTableController.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.PayTableHudView.PREV_PAGE_BTN:
                    {
                        this.createPage(--this.pageIdx);
                        break;
                    }
                case engine.PayTableHudView.NEXT_PAGE_BTN:
                    {
                        this.createPage(++this.pageIdx);
                        break;
                    }
                case engine.PayTableHudView.BACK_TO_GAME_BTN:
                    {
                        this.send(engine.NotificationList.CLOSE_PAY_TABLE);
                        break;
                    }
            }
        };
        PayTableController.prototype.createPage = function (pageIdx) {
            this.clearPageContainer();
            var prevCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + (pageIdx - 1)];
            this.hudView.getBtn(engine.PayTableHudView.PREV_PAGE_BTN).setEnable(prevCreator != null);
            var nextCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + (pageIdx + 1)];
            this.hudView.getBtn(engine.PayTableHudView.NEXT_PAGE_BTN).setEnable(nextCreator != null);
            var creator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + pageIdx];
            creator.create(this.pageContainer);
        };
        PayTableController.prototype.onClose = function () {
            this.gameContainer.removeChild(this.payTableContainer);
            this.gameContainer.removeChild(this.hudView);
        };
        PayTableController.prototype.onOpen = function () {
            this.gameContainer.addChild(this.payTableContainer);
            if (this.common.isMobile) {
                this.hudView = this.common.layouts[engine.PayTableHudView.LAYOUT_NAME];
                this.hudView.create();
                this.hudView.resizeForMobile();
                this.gameContainer.addChild(this.hudView);
            }
        };
        PayTableController.prototype.clearPageContainer = function () {
            while (this.pageContainer.getNumChildren() > 0) {
                this.pageContainer.removeChildAt(0);
            }
        };
        PayTableController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        PayTableController.PAY_TABLE_LAYOUT_PREFIX = "PayTable_";
        return PayTableController;
    })(engine.BaseController);
    engine.PayTableController = PayTableController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Sound = createjs.Sound;
    var SoundController = (function (_super) {
        __extends(SoundController, _super);
        function SoundController(manager, common) {
            _super.call(this, manager);
            this.regularSpinBg = null;
            this.autoSpinBg = null;
            this.freeSpinBg = null;
            this.rotateBg = null;
            this.rotateBgStart = null;
            this.payTable = null;
            this.winCounterSound = null;
            this.common = common;
        }
        SoundController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        SoundController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(engine.NotificationList.END_AUTO_PLAY);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.SHOW_WIN_LINES);
            notifications.push(engine.NotificationList.STOPPED_REEL_ID);
            notifications.push(engine.NotificationList.STOPPED_REEL_ID_PREPARE);
            notifications.push(engine.NotificationList.OPEN_OPTIONS_MENU);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.SHOW_LINES);
            notifications.push(engine.NotificationList.SOUND_TOGGLE);
            notifications.push(engine.NotificationList.START_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.START_WEEL_ROTATE);
            notifications.push(engine.NotificationList.STOP_WEEL_ROTATE);
            notifications.push(engine.NotificationList.SCATTER_WIN);
            notifications.push(engine.NotificationList.END_FREE_SPINS);
            notifications.push(engine.NotificationList.PLAY_WIN_SOUND);
            notifications.push(engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
            return notifications;
        };
        SoundController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SOUND_TOGGLE:
                    {
                        this.common.isSoundOn = !this.common.isSoundOn;
                        Sound.setMute(!this.common.isSoundOn);
                        break;
                    }
                case engine.NotificationList.TRY_START_AUTO_PLAY:
                    {
                        if (this.autoSpinBg == null) {
                            this.autoSpinBg = Sound.createInstance(engine.SoundsList.AUTO_SPIN_BG);
                            this.autoSpinBg.play({ loop: -1 });
                        }
                        break;
                    }
                case engine.NotificationList.END_AUTO_PLAY:
                    {
                        this.autoSpinBg.stop();
                        this.autoSpinBg = null;
                        break;
                    }
                case engine.NotificationList.START_SPIN:
                    {
                        //console.log();
                        if (this.common.server.win > 0) {
                            Sound.play(engine.SoundsList.COLLECT);
                        }
                        if (this.autoSpinBg == null && this.freeSpinBg == null) {
                            this.regularSpinBg = Sound.createInstance(engine.SoundsList.REGULAR_SPIN_BG);
                            this.regularSpinBg.play();
                        }
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        if (this.autoSpinBg == null) {
                            if (this.regularSpinBg != null)
                                this.regularSpinBg.stop();
                        }
                        break;
                    }
                case engine.NotificationList.SHOW_WIN_LINES:
                    {
                        this.isShowAllLines = data;
                        var winLinesVOs = this.common.server.winLines;
                        if (winLinesVOs.length > 0) {
                            if (this.autoSpinBg == null) {
                            }
                            else {
                                console.log('play free spin');
                                Sound.play(engine.SoundsList.AUTO_SPIN_WIN);
                            }
                        }
                        break;
                    }
                case engine.NotificationList.PLAY_WIN_SOUND:
                    {
                        console.log("play_win_sound soundcontroller");
                        if (this.winCounterSound != null) {
                            this.winCounterSound.stop();
                            this.winCounterSound = null;
                        }
                        this.winCounterSound = Sound.createInstance(engine.StatsController.WIN_COUNTER_SOUND);
                        this.winCounterSound.play();
                        break;
                    }
                case engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED:
                    {
                        if (this.winCounterSound != null) {
                            this.winCounterSound.stop();
                            this.winCounterSound = null;
                        }
                        break;
                    }
                case engine.NotificationList.STOPPED_REEL_ID_PREPARE:
                    {
                        if (!this.autoSpinBg) {
                            Sound.play(engine.SoundsList.REEL_STOP);
                        }
                        break;
                    }
                case engine.NotificationList.OPEN_OPTIONS_MENU:
                    {
                        this.payTable = Sound.createInstance(engine.SoundsList.PAY_TABLE_BG);
                        this.payTable.play({ loop: -1 });
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.payTable.stop();
                        break;
                    }
                case engine.NotificationList.SHOW_LINES:
                    {
                        if (!this.isShowAllLines) {
                            Sound.play(engine.SoundsList.LINE_WIN_ENUM);
                        }
                        break;
                    }
                case engine.NotificationList.START_BONUS:
                    {
                        this.freeSpinBg = Sound.createInstance(engine.SoundsList.FREE_SPIN_BG);
                        this.freeSpinBg.play({ loop: -1 });
                        break;
                    }
                case engine.NotificationList.END_BONUS:
                    {
                        if (this.freeSpinBg != undefined && this.freeSpinBg != null) {
                            this.freeSpinBg.stop();
                            this.freeSpinBg = null;
                        }
                        if (this.common.server.bonus && this.common.server.bonus.className == "FreeSpinsController") {
                            var bonusResult = Sound.createInstance(engine.SoundsList.FREE_SPIN_COLLECT);
                            bonusResult.play();
                        }
                        break;
                    }
                case engine.NotificationList.START_WEEL_ROTATE:
                    {
                        if (this.rotateBgStart == null) {
                            this.rotateBgStart = Sound.createInstance(engine.SoundsList.GLASS_ROTATE);
                            this.rotateBgStart.play({ loop: -1 });
                        }
                        break;
                    }
                case engine.NotificationList.SCATTER_WIN:
                    {
                        if (this.autoSpinBg != null) {
                            this.autoSpinBg.stop();
                            this.autoSpinBg = null;
                        }
                        if (this.rotateBg == null) {
                            this.rotateBg = Sound.createInstance(engine.SoundsList.WEEL_ROTATION);
                            this.rotateBg.play({ loop: -1 });
                        }
                        Sound.play(engine.SoundsList.SCATER_START);
                        break;
                    }
                case engine.NotificationList.STOP_WEEL_ROTATE:
                    {
                        if (this.rotateBgStart != null) {
                            this.rotateBgStart.stop();
                            this.rotateBgStart = null;
                        }
                        if (this.rotateBg != null) {
                            this.rotateBg.stop();
                            this.rotateBg = null;
                        }
                        Sound.play(engine.SoundsList.SECTOR_ANIMATION);
                    }
            }
        };
        SoundController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
        };
        return SoundController;
    })(engine.BaseController);
    engine.SoundController = SoundController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var AutoSpinTableController = (function (_super) {
        __extends(AutoSpinTableController, _super);
        function AutoSpinTableController(manager, common, view, maskView) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.maskView = maskView;
            AutoSpinTableController.OPEN_TIME = engine.Utils.float2int(0.3 * Ticker.getFPS());
            AutoSpinTableController.CLOSE_TIME = engine.Utils.float2int(0.3 * Ticker.getFPS());
            this.view.alpha = 0;
        }
        AutoSpinTableController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.setupMask();
            this.initItems();
            this.isOpen = true;
            this.close(false);
        };
        AutoSpinTableController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.OPEN_AUTO_SPIN_MENU);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(engine.NotificationList.TRY_START_SPIN);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.OPEN_SELECT_BET_TABLE);
            notifications.push(engine.NotificationList.CHANGE_DEVICE_ORIENTATION);
            return notifications;
        };
        AutoSpinTableController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.OPEN_AUTO_SPIN_MENU:
                    {
                        this.toggle();
                        break;
                    }
                case engine.NotificationList.TRY_START_AUTO_PLAY:
                case engine.NotificationList.TRY_START_SPIN:
                case engine.NotificationList.OPEN_SELECT_BET_TABLE:
                case engine.NotificationList.CHANGE_DEVICE_ORIENTATION:
                    {
                        if (this.isOpen)
                            this.close(true);
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.close(false);
                        break;
                    }
            }
        };
        AutoSpinTableController.prototype.toggle = function () {
            if (this.isOpen) {
                this.close(true);
            }
            else {
                this.open(true);
            }
        };
        AutoSpinTableController.prototype.initItems = function () {
            var _this = this;
            var i;
            var autoPlayCount = this.common.config.autoPlayCount;
            this.itemsBtn = new Array(autoPlayCount.length);
            this.itemsText = new Array(autoPlayCount.length);
            for (var i = 1; i <= this.itemsBtn.length; i++) {
                var button = this.view.getChildByName(AutoSpinTableController.ITEM_PREFIX + i + AutoSpinTableController.ITEM_POSTFIX);
                var bounds = button.getBounds();
                var hitArea = new Shape();
                hitArea.graphics.beginFill("0").drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
                button.hitArea = hitArea;
                button.removeAllEventListeners("click");
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        var target = eventObj.currentTarget;
                        var id = parseInt(target.name.substring(AutoSpinTableController.ITEM_PREFIX.length, target.name.length - AutoSpinTableController.ITEM_POSTFIX.length));
                        var count = parseInt(_this.itemsText[id - 1].text);
                        _this.send(engine.NotificationList.TRY_START_AUTO_PLAY, count);
                    }
                });
                var itemValue = this.view.getChildByName(AutoSpinTableController.ITEM_TEXT_PREFIX + i);
                itemValue.text = autoPlayCount[autoPlayCount.length - i].toString();
                itemValue.y = (this.isNotPortr) ? button.y + bounds.height / 1.5 : button.y + bounds.height / 1.5;
                //console.log("this.isNotPortr",this.isNotPortr);
                this.itemsBtn[i - 1] = button;
                this.itemsText[i - 1] = itemValue;
            }
        };
        AutoSpinTableController.prototype.setupMask = function () {
            //this.maskView.parent.removeChild(this.maskView);
            this.maskView.visible = false;
            var bounds = this.maskView.getBounds();
            var mode = this.common.rotateHudMode;
            this.isNotPortr = (mode == 'HudView' || !this.common.isMobile);
            var mask = new Shape();
            if (this.isNotPortr) {
                mask.graphics.beginFill("0").drawRect(this.maskView.x + bounds.x, this.maskView.y + bounds.y, bounds.width * 1.5, bounds.height * 0.5);
            }
            else {
                mask.graphics.beginFill("0").drawRect(this.view.x + bounds.x, this.view.y, bounds.width * 2.5, bounds.height * 2);
            }
            //mask.alpha = 0.4;
            //this.view.addChild(mask);
            this.view.mask = mask;
            this.startY = this.view.y;
        };
        AutoSpinTableController.prototype.close = function (animation) {
            var _this = this;
            var endPos = (this.isNotPortr) ? this.startY + this.view.getBounds().height : this.startY - (this.view.getBounds().height * 2);
            if (animation) {
                var tween = Tween.get(this.view, { useTicks: true });
                tween.to({ y: endPos }, AutoSpinTableController.CLOSE_TIME).call(function () {
                    _this.view.alpha = 0;
                });
            }
            else {
                this.view.y = endPos;
            }
            this.isOpen = false;
        };
        AutoSpinTableController.prototype.open = function (animation) {
            if (animation) {
                var tween = Tween.get(this.view, { useTicks: true });
                tween.to({ y: this.startY }, AutoSpinTableController.OPEN_TIME);
            }
            else {
                this.view.y = this.startY;
            }
            this.isOpen = true;
            this.view.alpha = 1;
        };
        AutoSpinTableController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        AutoSpinTableController.ITEM_PREFIX = "item";
        AutoSpinTableController.ITEM_POSTFIX = "Btn";
        AutoSpinTableController.ITEM_TEXT_PREFIX = "item_value_";
        return AutoSpinTableController;
    })(engine.BaseController);
    engine.AutoSpinTableController = AutoSpinTableController;
})(engine || (engine = {}));
/**
 * Created by Taras on 10.02.2015.
 */
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var SelectBetController = (function (_super) {
        __extends(SelectBetController, _super);
        function SelectBetController(manager, common, view, maskView) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.maskView = maskView;
            SelectBetController.OPEN_TIME = engine.Utils.float2int(0.3 * Ticker.getFPS());
            SelectBetController.CLOSE_TIME = engine.Utils.float2int(0.3 * Ticker.getFPS());
            this.view.alpha = 0;
        }
        SelectBetController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.setupMask();
            this.initItems();
            this.isOpen = true;
            this.close(false);
        };
        SelectBetController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.OPEN_SELECT_BET_TABLE);
            notifications.push(engine.NotificationList.SET_BET);
            notifications.push(engine.NotificationList.TRY_START_SPIN);
            notifications.push(engine.NotificationList.OPEN_AUTO_SPIN_MENU);
            notifications.push(engine.NotificationList.CHANGE_DEVICE_ORIENTATION);
            return notifications;
        };
        SelectBetController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.OPEN_SELECT_BET_TABLE:
                    {
                        this.toggle();
                        break;
                    }
                case engine.NotificationList.OPEN_AUTO_SPIN_MENU:
                case engine.NotificationList.TRY_START_SPIN:
                case engine.NotificationList.SET_BET:
                case engine.NotificationList.CHANGE_DEVICE_ORIENTATION:
                    {
                        this.close(true);
                        break;
                    }
            }
        };
        SelectBetController.prototype.toggle = function () {
            if (this.isOpen) {
                this.close(true);
            }
            else {
                this.open(true);
            }
        };
        SelectBetController.prototype.initItems = function () {
            var _this = this;
            var i;
            var selectBetCount = this.common.config.selectBetCount;
            this.itemsBtn = new Array(selectBetCount.length);
            this.itemsText = new Array(selectBetCount.length);
            for (var i = 1; i <= this.itemsBtn.length; i++) {
                var button = this.view.getChildByName(SelectBetController.ITEM_PREFIX + i + SelectBetController.ITEM_POSTFIX);
                var bounds = button.getBounds();
                var hitArea = new Shape();
                hitArea.graphics.beginFill("0").drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
                button.hitArea = hitArea;
                button.removeAllEventListeners("click");
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        var target = eventObj.currentTarget;
                        var id = parseInt(target.name.substring(SelectBetController.ITEM_PREFIX.length, target.name.length - SelectBetController.ITEM_POSTFIX.length));
                        var bet = parseFloat(_this.itemsText[id - 1].text);
                        _this.send(engine.NotificationList.SET_BET, bet);
                    }
                });
                var itemValue = this.view.getChildByName(SelectBetController.ITEM_TEXT_PREFIX + i);
                itemValue.text = selectBetCount[selectBetCount.length - i].toString();
                itemValue.y = button.y + bounds.height / 1.5;
                this.itemsBtn[i - 1] = button;
                this.itemsText[i - 1] = itemValue;
            }
        };
        SelectBetController.prototype.setupMask = function () {
            this.maskView.visible = false;
            //this.maskView.parent.removeChild(this.maskView);
            var bounds = this.maskView.getBounds();
            var mode = this.common.rotateHudMode;
            this.isNotPortr = (mode == 'HudView' || !this.common.isMobile);
            var mask = new Shape();
            if (this.isNotPortr) {
                mask.graphics.beginFill("0").drawRect(this.maskView.x + bounds.x, this.maskView.y, bounds.width * 1.5, bounds.height * 1.75);
            }
            else {
                mask.graphics.beginFill("0").drawRect(this.view.x, this.view.y + bounds.y, bounds.width * 2, bounds.height * 2);
            }
            this.view.mask = mask;
            this.startY = (this.isNotPortr) ? this.view.y * 1.20 : this.view.y;
        };
        SelectBetController.prototype.close = function (animation) {
            var _this = this;
            var endPos = (this.isNotPortr) ? this.startY + this.view.getBounds().height : this.startY - this.view.getBounds().height;
            if (animation) {
                var tween = Tween.get(this.view, { useTicks: true });
                tween.to({ y: endPos }, SelectBetController.CLOSE_TIME).call(function () {
                    _this.view.alpha = 0;
                });
            }
            else {
                this.view.y = endPos;
            }
            this.isOpen = false;
        };
        SelectBetController.prototype.open = function (animation) {
            if (animation) {
                var tween = Tween.get(this.view, { useTicks: true });
                tween.to({ y: this.startY }, SelectBetController.OPEN_TIME);
            }
            else {
                this.view.y = this.startY;
            }
            this.isOpen = true;
            this.view.alpha = 1;
        };
        SelectBetController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        SelectBetController.ITEM_PREFIX = "Bet";
        SelectBetController.ITEM_POSTFIX = "Btn";
        SelectBetController.ITEM_TEXT_PREFIX = "item_value_";
        return SelectBetController;
    })(engine.BaseController);
    engine.SelectBetController = SelectBetController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var StatsController = (function (_super) {
        __extends(StatsController, _super);
        function StatsController(manager, common, view, container) {
            _super.call(this, manager);
            this.currentWin = 0;
            this.common = common;
            this.view = view;
            this.container = container;
            //StatsController.SHOW_TIME = Utils.float2int(StatsController.SHOW_TIME * Ticker.getFPS());
            StatsController.HIDE_TIME = engine.Utils.float2int(StatsController.HIDE_TIME * Ticker.getFPS());
            var self = this;
            this.bigWinPopupView = this.common.layouts[engine.BigWinPopupView.LAYOUT_NAME];
            this.bigWinPopupView.create();
            this.bigWinPopupView.winTf.setValue(0);
            self.bigWinPopupView.removeAllEventListeners("click");
            this.bigWinPopupView.on('click', function (eventObj) {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    console.log("bigWinPopupView clicked");
                    self.bigWinPopupView.winTf.setValue(self.currentWin);
                    self.bigWinPopupView.hide(function () {
                        self.hideBigWinPopupView(self.view.getBtn(engine.HudView.START_SPIN_BTN).getEnable());
                    }, 0.1);
                }
            });
            this.colossalWinPopupView = this.common.layouts[engine.ColossalWinPopupView.LAYOUT_NAME];
            this.colossalWinPopupView.create();
            this.colossalWinPopupView.winTf.setValue(0);
            this.colossalWinPopupView.removeAllEventListeners("click");
            this.colossalWinPopupView.on('click', function (eventObj) {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    console.log("colossalWinPopupView clicked");
                    self.colossalWinPopupView.winTf.setValue(self.currentWin);
                    self.colossalWinPopupView.hide(function () {
                        self.hideColossalWinPopupView(self.view.getBtn(engine.HudView.START_SPIN_BTN).getEnable());
                    }, 1.5);
                }
            });
            this.bigWinPopupView.alpha = 0;
            this.colossalWinPopupView.alpha = 0;
            //this.container.addChild(this.bigWinPopupView);
            //this.container.addChild(this.colossalWinPopupView);
            console.log("StatsController constructor");
        }
        StatsController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.resetTf();
        };
        StatsController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_WIN_TF);
            notifications.push(engine.NotificationList.COLLECT_WIN_TF);
            notifications.push(engine.NotificationList.UPDATE_BALANCE_TF);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.WIN_POPUP_SHOWED);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.START_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.FREESPINS_SHOW_LEFT);
            return notifications;
        };
        StatsController.prototype.onEnterFrame = function () {
            this.winTf.update();
            this.balanceTf.update();
            this.bigWinPopupView.winTf.update();
            this.colossalWinPopupView.winTf.update();
        };
        StatsController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.FREESPINS_SHOW_LEFT:
                    {
                        this.view.setText(engine.HudView.AUTO_SPIN_TEXT, engine.HudView.AUTO_SPIN_TEXT_FREESPINS);
                        this.view.setText(engine.HudView.AUTO_SPINS_COUNT_TEXT, data);
                        break;
                    }
                case engine.NotificationList.START_BONUS:
                    {
                        this.view.setText(engine.HudView.AUTO_SPIN_TEXT, engine.HudView.AUTO_SPIN_TEXT_FREESPINS);
                        this.view.setText(engine.HudView.AUTO_SPINS_COUNT_TEXT, "");
                        break;
                    }
                case engine.NotificationList.END_BONUS:
                    {
                        this.view.setText(engine.HudView.AUTO_SPIN_TEXT, engine.HudView.AUTO_SPIN_TEXT_START);
                        //this.view.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT).alpha = 0;
                        this.view.setText(engine.HudView.AUTO_SPINS_COUNT_TEXT, "");
                        break;
                    }
                case engine.NotificationList.UPDATE_BALANCE_TF:
                    {
                        this.updateBalanceText();
                        break;
                    }
                case engine.NotificationList.SHOW_WIN_TF:
                    {
                        if (data != null)
                            this.winTf.setValue(data);
                        else
                            this.updateWinText();
                        break;
                    }
                case engine.NotificationList.COLLECT_WIN_TF:
                    {
                        this.collectWinText();
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                case engine.NotificationList.START_SPIN:
                    {
                        this.common.is_win_popup_showed = false;
                        this.common.is_win_lines_showed = false;
                        break;
                    }
                case engine.NotificationList.WIN_POPUP_SHOWED:
                    {
                        console.log("WIN_POPUP_SHOWED");
                        this.common.is_win_popup_showed = true;
                        this.tryProcessWinLinesAndPopupShown();
                        break;
                    }
                case engine.NotificationList.WIN_LINES_SHOWED:
                    {
                        this.common.is_win_lines_showed = true;
                        this.tryProcessWinLinesAndPopupShown();
                        break;
                    }
            }
        };
        StatsController.prototype.tryProcessWinLinesAndPopupShown = function () {
            console.log(this.common.is_win_lines_showed + " " + this.common.is_win_popup_showed, "tryProcessWinLinesAndPopupShown");
            if (this.common.is_win_lines_showed && this.common.is_win_popup_showed) {
                this.send(engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
            }
        };
        StatsController.prototype.collectWinText = function () {
            this.winTf.setValue(0, StatsController.HIDE_TIME);
        };
        StatsController.prototype.updateBalanceText = function () {
            this.balanceTf.setValue(this.common.server.getBalance(), engine.Utils.float2int(StatsController.SHOW_TIME * Ticker.getFPS()));
        };
        StatsController.prototype.updateWinText = function () {
            //this.common.is_win_popup_showed = true;
            this.bigWinPopupView.winTf.setValue(0);
            this.colossalWinPopupView.winTf.setValue(0);
            var win_to_bet = Math.abs(this.common.server.win / this.common.server.getTotalBet());
            console.log("bet=" + this.common.server.getTotalBet() + "; value=" + this.common.server.win + " time=" + StatsController.SHOW_TIME + "; win_to_bet=" + win_to_bet, "setValue");
            switch (true) {
                case (win_to_bet > StatsController.SMALL_WIN_COUNTER_FROM && win_to_bet < StatsController.SMALL_WIN_COUNTER_TO):
                    {
                        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.SMALL_WIN_COUNTER;
                        this.winTf.setValue(this.common.server.win, engine.Utils.float2int(StatsController.SMALL_WIN_COUNTER_TIME * Ticker.getFPS()));
                        this.send(engine.NotificationList.WIN_POPUP_SHOWED);
                        break;
                    }
                case (win_to_bet >= StatsController.NORMAL_WIN_COUNTER_FROM && win_to_bet < StatsController.NORMAL_WIN_COUNTER_TO):
                    {
                        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.NORMAL_WIN_COUNTER;
                        this.winTf.setValue(this.common.server.win, engine.Utils.float2int(StatsController.NORMAL_WIN_COUNTER_TIME * Ticker.getFPS()));
                        var self = this;
                        setTimeout(function () {
                            self.send(engine.NotificationList.WIN_POPUP_SHOWED);
                        }, (StatsController.NORMAL_WIN_COUNTER_TIME + 1) * 1000);
                        break;
                    }
                case (win_to_bet >= StatsController.MEDIUM_WIN_COUNTER_FROM && win_to_bet < StatsController.MEDIUM_WIN_COUNTER_TO):
                    {
                        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.MEDIUM_WIN_COUNTER;
                        this.winTf.setValue(this.common.server.win, engine.Utils.float2int(StatsController.MEDIUM_WIN_COUNTER_TIME * Ticker.getFPS()));
                        var self = this;
                        setTimeout(function () {
                            self.send(engine.NotificationList.WIN_POPUP_SHOWED);
                        }, (StatsController.MEDIUM_WIN_COUNTER_TIME + 1) * 1000);
                        break;
                    }
                case (win_to_bet >= StatsController.BIG_WIN_COUNTER_FROM && win_to_bet < StatsController.BIG_WIN_COUNTER_TO):
                    {
                        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.BIG_WIN_COUNTER;
                        //this.winTf.setValue(0);
                        this.winTf.setValue(this.common.server.win, engine.Utils.float2int(StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS()));
                        this.showBigWinPopupView(this.common.server.win, StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS());
                        break;
                    }
                case (win_to_bet >= StatsController.COLOSSAL_WIN_COUNTER_FROM):
                    {
                        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.COLOSSAL_WIN_COUNTER;
                        //this.winTf.setValue(0);
                        this.winTf.setValue(this.common.server.win, engine.Utils.float2int(StatsController.COLOSSAL_WIN_COUNTER_TIME * Ticker.getFPS()));
                        this.showColossalWinPopupView(this.common.server.win, StatsController.COLOSSAL_WIN_COUNTER_TIME * Ticker.getFPS());
                        break;
                    }
                default:
                    {
                        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.SMALL_WIN_COUNTER;
                        this.send(engine.NotificationList.WIN_POPUP_SHOWED);
                        break;
                    }
            }
            if (this.common.server.win)
                this.send(engine.NotificationList.PLAY_WIN_SOUND);
            //this.showColossalWinPopupView(7777, StatsController.COLOSSAL_WIN_COUNTER_TIME * Ticker.getFPS());
            //this.showBigWinPopupView(999, StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS());
            //this.winTf.setValue(0);
            //this.winTf.setValue(this.common.server.win, StatsController.SHOW_TIME);
        };
        StatsController.prototype.hideBigWinPopupView = function (enableSpinBtn) {
            if (enableSpinBtn === void 0) { enableSpinBtn = true; }
            enableSpinBtn = true;
            this.container.removeChild(this.bigWinPopupView);
            this.bigWinPopupView.alpha = 0;
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, this.view.getBtn(engine.HudView.START_SPIN_BTN).visible, enableSpinBtn);
            this.send(engine.NotificationList.WIN_POPUP_SHOWED);
        };
        StatsController.prototype.hideColossalWinPopupView = function (enableSpinBtn) {
            if (enableSpinBtn === void 0) { enableSpinBtn = true; }
            enableSpinBtn = true;
            this.container.removeChild(this.colossalWinPopupView);
            this.colossalWinPopupView.alpha = 0;
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, this.view.getBtn(engine.HudView.START_SPIN_BTN).visible, enableSpinBtn);
            this.send(engine.NotificationList.WIN_POPUP_SHOWED);
        };
        StatsController.prototype.showBigWinPopupView = function (value, updateTime) {
            var _this = this;
            if (updateTime === void 0) { updateTime = 0; }
            this.currentWin = value;
            var self = this;
            this.bigWinPopupView.alpha = 1;
            this.bigWinPopupView.winTf.setValue(value, updateTime);
            this.container.addChild(this.bigWinPopupView);
            this.bigWinPopupView.alpha = 1;
            //console.log("this.bigWinPopupView.alpha",this.bigWinPopupView.alpha);
            this.bigWinPopupView.hide(function () {
                _this.hideBigWinPopupView(_this.view.getBtn(engine.HudView.START_SPIN_BTN).getEnable());
            }, StatsController.BIG_WIN_POPUP_DELAY);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, this.view.getBtn(engine.HudView.START_SPIN_BTN).visible, false);
        };
        StatsController.prototype.showColossalWinPopupView = function (value, updateTime) {
            var _this = this;
            if (updateTime === void 0) { updateTime = 0; }
            this.currentWin = value;
            var self = this;
            this.colossalWinPopupView.alpha = 1;
            this.colossalWinPopupView.winTf.setValue(value, updateTime);
            this.container.addChild(this.colossalWinPopupView);
            this.colossalWinPopupView.alpha = 1;
            this.colossalWinPopupView.hide(function () {
                _this.hideColossalWinPopupView(_this.view.getBtn(engine.HudView.START_SPIN_BTN).getEnable());
            }, StatsController.COLOSSAL_WIN_POPUP_DELAY);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, this.view.getBtn(engine.HudView.START_SPIN_BTN).visible, false);
        };
        StatsController.prototype.setView = function (view) {
            this.view = view;
            this.resetTf();
        };
        StatsController.prototype.resetTf = function () {
            this.winTf = new engine.AnimationTextField(this.view.getText(engine.HudViewP.WIN_TEXT));
            this.winTf.setValue(0);
            this.balanceTf = new engine.AnimationTextField(this.view.getText(engine.HudViewP.BALANCE_TEXT));
            this.balanceTf.setValue(this.common.server.getBalance());
        };
        StatsController.WIN_COUNTER_SOUND = engine.SoundsList.SMALL_WIN_COUNTER;
        StatsController.WIN_TO_BET_POPUP_RELATION = 0;
        StatsController.BIG_WIN_POPUP_DELAY = 7.5;
        StatsController.COLOSSAL_WIN_POPUP_DELAY = 9.5;
        StatsController.SHOW_TIME = 1.5;
        StatsController.HIDE_TIME = 0.3;
        StatsController.SMALL_WIN_COUNTER_TIME = 1.5; // tr : 1.5
        StatsController.NORMAL_WIN_COUNTER_TIME = 3; // tr : 3
        StatsController.MEDIUM_WIN_COUNTER_TIME = 3; // tr : 3
        StatsController.BIG_WIN_COUNTER_TIME = 6;
        StatsController.COLOSSAL_WIN_COUNTER_TIME = 8;
        StatsController.SMALL_WIN_COUNTER_FROM = 0;
        StatsController.SMALL_WIN_COUNTER_TO = 1;
        StatsController.NORMAL_WIN_COUNTER_FROM = 1;
        StatsController.NORMAL_WIN_COUNTER_TO = 3;
        StatsController.MEDIUM_WIN_COUNTER_FROM = 3;
        StatsController.MEDIUM_WIN_COUNTER_TO = 6;
        StatsController.BIG_WIN_COUNTER_FROM = 6;
        StatsController.BIG_WIN_COUNTER_TO = 12;
        StatsController.COLOSSAL_WIN_COUNTER_FROM = 12;
        return StatsController;
    })(engine.BaseController);
    engine.StatsController = StatsController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var SettingsController = (function (_super) {
        __extends(SettingsController, _super);
        function SettingsController(manager, common, hudController, view) {
            _super.call(this, manager);
            this.common = common;
            this.hud = hudController;
            this.view = view;
            this.view.visible = false;
            this.autoPlayCountIdx = 0;
            SettingsController.PRELOADER_ROTATION_TIME = engine.Utils.float2int(SettingsController.PRELOADER_ROTATION_TIME * Ticker.getFPS());
        }
        SettingsController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.initLinesMenu();
            this.initBetMenu();
            this.initSpinsCountMenu();
            this.initSoundToggle();
            this.initPreloader();
        };
        SettingsController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.TOGGLE_SETTINGS_MENU);
            notifications.push(engine.NotificationList.CLOSE_SETTINGS_MENU);
            notifications.push(engine.NotificationList.LOAD_SOUNDS);
            notifications.push(engine.NotificationList.SOUNDS_LOADED);
            return notifications;
        };
        SettingsController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.TOGGLE_SETTINGS_MENU:
                    {
                        this.view.visible = !this.view.visible;
                        break;
                    }
                case engine.NotificationList.CLOSE_SETTINGS_MENU:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.LOAD_SOUNDS:
                    {
                        this.showPreloader();
                        break;
                    }
                case engine.NotificationList.SOUNDS_LOADED:
                    {
                        this.removePreloader();
                        break;
                    }
            }
        };
        SettingsController.prototype.initPreloader = function () {
            if (this.common.isSoundLoaded) {
                this.removePreloader();
            }
            else {
                var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
                var bounds = soundPreloader.getBounds();
                soundPreloader.regX = bounds.width / 2;
                soundPreloader.regY = bounds.height / 2;
                soundPreloader.x += bounds.width / 2;
                soundPreloader.y += bounds.height / 2;
                soundPreloader.visible = false;
            }
        };
        SettingsController.prototype.showPreloader = function () {
            var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
            this.rotatePreloader();
            soundPreloader.visible = true;
        };
        SettingsController.prototype.rotatePreloader = function () {
            var _this = this;
            var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
            soundPreloader.rotation = 0;
            Tween.get(soundPreloader, { useTicks: true }).to({ "rotation": 360 }, SettingsController.PRELOADER_ROTATION_TIME).call(function () {
                _this.rotatePreloader();
            });
        };
        SettingsController.prototype.removePreloader = function () {
            var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
            Tween.removeAllTweens();
            this.view.removeChild(soundPreloader);
        };
        SettingsController.prototype.initSoundToggle = function () {
            //var soundToggle:Toggle = <Toggle>this.view.getChildByName(SettingsController.SOUND_TOGGLE);
            //if (soundToggle != null) {
            //	soundToggle.setIsOn(this.common.isSoundLoaded);
            //	soundToggle.on("click", (eventObj:any)=> {
            //		if (eventObj.nativeEvent instanceof MouseEvent) {
            //			if (!this.common.isSoundLoaded) {
            //				this.send(NotificationList.LOAD_SOUNDS);
            //			}
            //			this.send(NotificationList.SOUND_TOGGLE);
            //			Sound.play(SoundsList.REGULAR_CLICK_BTN);
            //		}
            //	});
            //}
        };
        SettingsController.prototype.initLinesMenu = function () {
            var _this = this;
            var values = [];
            for (var i = 1; i <= this.common.server.maxLineCount; i++) {
                values.push(i);
            }
            var menu = new engine.TouchMenu(this.view.getChildByName(SettingsController.LINES_MENU));
            menu.init(this.common.server.lineCount - 1, values, function (value) {
                _this.common.server.lineCount = value + 1;
                _this.hud.updateLineCountText();
            });
        };
        SettingsController.prototype.initBetMenu = function () {
            var _this = this;
            var availableBets = this.common.server.availableBets;
            var idx = availableBets.indexOf(this.common.server.bet);
            var menu = new engine.TouchMenu(this.view.getChildByName(SettingsController.BET_MENU));
            menu.init(idx, availableBets, function (value) {
                _this.common.server.bet = availableBets[value];
                _this.hud.updateBetText();
                _this.hud.updateBetPerLineText();
                _this.hud.updateTotalBetText();
            });
        };
        SettingsController.prototype.initSpinsCountMenu = function () {
            var _this = this;
            var menu = new engine.TouchMenu(this.view.getChildByName(SettingsController.SPINS_COUNT_MENU));
            menu.init(0, this.common.config.autoPlayCount, function (value) {
                _this.hud.autoPlayCountIdx = value;
                _this.hud.updateAutoPlayBtn();
                _this.hud.updateAutoSpinCountText();
            });
        };
        SettingsController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.view = null;
            this.common = null;
        };
        SettingsController.PRELOADER_ROTATION_TIME = 1;
        SettingsController.BET_MENU = "betMenu";
        SettingsController.SOUND_TOGGLE = "soundToggle";
        SettingsController.SOUND_PRELOADER = "soundPreloader";
        SettingsController.LINES_MENU = "linesMenu";
        SettingsController.SPINS_COUNT_MENU = "spinsCountMenu";
        return SettingsController;
    })(engine.BaseController);
    engine.SettingsController = SettingsController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Sound = createjs.Sound;
    var HudController = (function (_super) {
        __extends(HudController, _super);
        function HudController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.autoPlayCount = this.common.config.autoPlayCount;
            this.autoPlayCountIdx = 0;
        }
        HudController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.initHudState();
            this.refreshView();
        };
        HudController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.KEYBOARD_CLICK);
            notifications.push(engine.NotificationList.ON_SCREEN_CLICK);
            notifications.push(engine.NotificationList.TRY_START_SPIN);
            notifications.push(engine.NotificationList.WIN_POPUP_SHOWED);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            notifications.push(engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.EXPRESS_STOP);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(engine.NotificationList.CHANGE_BET);
            notifications.push(engine.NotificationList.SET_BET);
            notifications.push(engine.NotificationList.COLLECT_WIN_TF);
            return notifications;
        };
        HudController.prototype.handleNotification = function (message, data) {
            this.currentState.handleNotification(message, data);
        };
        HudController.prototype.initHudState = function () {
            this.states = {};
            this.states[engine.DefaultState.NAME] = new engine.DefaultState(this);
            this.states[engine.PayTableState.NAME] = new engine.PayTableState(this);
            this.states[engine.RegularSpinState.NAME] = new engine.RegularSpinState(this);
            this.states[engine.AutoSpinState.NAME] = new engine.AutoSpinState(this);
            this.states[engine.BonusState.NAME] = new engine.BonusState(this);
            if (this.common.server.bonus == null) {
                this.changeState(engine.DefaultState.NAME);
            }
            else {
                this.changeState(engine.BonusState.NAME);
            }
        };
        HudController.prototype.setOrigin = function () {
            this.view.setOrigin();
            this.view.setWin(this.common.server.win);
        };
        HudController.prototype.initButtons = function () {
            var _this = this;
            console.log("initButtons");
            var buttonsNames = this.view.buttonsNames;
            for (var i = 0; i < buttonsNames.length; i++) {
                var buttonName = buttonsNames[i];
                var button = this.view.getBtn(buttonName);
                if (button == null) {
                    continue;
                }
                button.on("click", function (eventObj) {
                    console.log("button click " + eventObj.currentTarget.name);
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        HudController.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.BACK_TO_LOBBY_BTN:
                    {
                        this.send(engine.NotificationList.BACK_TO_LOBBY);
                        break;
                    }
                case engine.HudView.FULL_SCREEN_BTN:
                    {
                        engine.FullScreen.toggleFullScreen();
                        break;
                    }
            }
            HudController.playBtnSound(buttonName);
            this.currentState.onBtnClick(buttonName);
        };
        HudController.playBtnSound = function (buttonName) {
            if (buttonName == engine.HudView.START_SPIN_BTN) {
                Sound.play(engine.SoundsList.SPIN_CLICK_BTN);
            }
            else {
                Sound.play(engine.SoundsList.REGULAR_CLICK_BTN);
            }
        };
        HudController.prototype.changeState = function (name, data) {
            if (data === void 0) { data = null; }
            //console.log("Change state: " + name);
            if (this.currentState != null) {
                this.currentState.end();
            }
            if (name == 'OptionsState') {
                console.log('OptionsState');
                this.send(engine.NotificationList.INIT_OPTIONS_STATE);
            }
            else {
                this.currentState = this.states[name];
                this.currentState.start(data);
            }
        };
        HudController.prototype.getCommonRefs = function () {
            return this.common;
        };
        HudController.prototype.tryStartSpin = function () {
            this.send(engine.NotificationList.COLLECT_WIN_TF);
            this.common.config.statechanged = false;
            var balanceAfterSpin = this.common.server.getBalance() - this.common.server.getTotalBet();
            if (balanceAfterSpin >= 0) {
                var balance = this.common.server.getBalance() - this.common.server.getTotalBet();
                this.common.server.setBalance(balance);
                this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                this.send(engine.NotificationList.TRY_START_SPIN);
            }
            else {
                this.send(engine.NotificationList.SHOW_ERRORS, [engine.ErrorController.ERROR_NO_MONEY_STR]);
            }
        };
        HudController.prototype.updateTotalBetText = function () {
            var totalBetText = this.view.getText(engine.HudView.TOTAL_BET_TEXT);
            if (totalBetText != null) {
                totalBetText.text = engine.MoneyFormatter.format(this.common.server.getTotalBet(), true);
            }
        };
        HudController.prototype.updateBetOnServ = function () {
            this.send(engine.NotificationList.CHANGE_BET);
        };
        HudController.prototype.updateLinesOnServ = function () {
            this.send(engine.NotificationList.CHANGED_LINE_COUNT);
        };
        HudController.prototype.updateBetText = function () {
            var betText = this.view.getText(engine.HudView.BET_TEXT);
            if (betText != null) {
                betText.text = engine.MoneyFormatter.format(this.common.server.bet, true);
            }
        };
        HudController.prototype.updateBetPerLineText = function () {
            var betPerLineText = this.view.getText(engine.HudView.BET_PER_LINE_TEXT);
            if (betPerLineText != null) {
                betPerLineText.text = engine.MoneyFormatter.format(this.common.server.getBetPerLine(), true);
            }
        };
        HudController.prototype.updateLineCountText = function () {
            this.view.setLineCountText(this.common.server.lineCount.toString());
        };
        HudController.prototype.updateAutoPlayBtn = function () {
            //console.log("Update auto play btn");
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx > 0);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx < this.autoPlayCount.length - 1);
        };
        HudController.prototype.updateAutoSpinCountText = function () {
            //console.log("Update auto spin count text");
            if (!this.common.config.isMobile) {
                var autoSpinCountText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
                if (autoSpinCountText != null && this.autoPlayCountIdx > -1 && this.autoPlayCountIdx < this.autoPlayCount.length) {
                    var autoPlayCount = this.autoPlayCount[this.autoPlayCountIdx];
                    autoSpinCountText.text = autoPlayCount.toString();
                }
            }
        };
        HudController.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                var button = this.view.getBtn(buttonsNames[i]);
                if (button && button != null)
                    button.removeAllEventListeners("click");
            }
        };
        HudController.prototype.dispose = function () {
            this.removeHandlers();
            _super.prototype.dispose.call(this);
            this.view.dispose();
            this.view = null;
            this.common = null;
        };
        HudController.prototype.setView = function (view) {
            this.view = view;
            this.refreshStatesView();
        };
        HudController.prototype.refreshHud = function () {
            this.refreshView();
            this.currentState.afterRotate();
        };
        HudController.prototype.refreshView = function () {
            if (!this.view.viewInitialised) {
                this.view.viewInitialised = true;
                this.initButtons();
            }
            this.updateLineCountText();
            this.updateTotalBetText();
            this.updateBetText();
            this.updateBetPerLineText();
        };
        HudController.prototype.refreshStatesView = function () {
            for (var state in this.states) {
                this.states[state].setView(this.view);
            }
        };
        return HudController;
    })(engine.BaseController);
    engine.HudController = HudController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ReelController = (function (_super) {
        __extends(ReelController, _super);
        function ReelController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.movers = [];
            this.runnerReels = 0;
        }
        ReelController.prototype.init = function () {
            _super.prototype.init.call(this);
            //if(!this.common.config.isRotated){
            this.view.create();
            this.view.setMobileTrue(this.common.isMobile);
            this.setSymbolsPos();
            this.createMovers();
            //}
        };
        ReelController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.SERVER_GOT_SPIN);
            notifications.push(engine.NotificationList.EXPRESS_STOP);
            notifications.push(engine.NotificationList.SHOW_ALL_SYMBOLS);
            notifications.push(engine.NotificationList.HIDE_SYMBOLS);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.CHANGE_DEVICE_ORIENTATION);
            return notifications;
        };
        ReelController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.START_SPIN:
                    {
                        this.startSpin();
                        break;
                    }
                case engine.NotificationList.SERVER_GOT_SPIN:
                    {
                        this.gotSpin();
                        break;
                    }
                case engine.NotificationList.EXPRESS_STOP:
                    {
                        this.stopSpin();
                        break;
                    }
                case engine.NotificationList.SHOW_ALL_SYMBOLS:
                    {
                        this.showAllSymbols();
                        break;
                    }
                case engine.NotificationList.HIDE_SYMBOLS:
                    {
                        this.hideSymbols(data);
                        break;
                    }
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.END_BONUS:
                    {
                        this.updateSequenceType();
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.visible = true;
                        break;
                    }
                case engine.NotificationList.CHANGE_DEVICE_ORIENTATION:
                    {
                        break;
                    }
            }
        };
        ReelController.prototype.onEnterFrame = function () {
            for (var i = 0; i < this.movers.length; i++) {
                this.movers[i].onEnterFrame();
            }
        };
        ReelController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.view.dispose();
            this.view = null;
            this.common = null;
            this.movers = null;
        };
        ReelController.prototype.getSequenceType = function () {
            var bonus = this.common.server.bonus;
            if (bonus != null && bonus.type == engine.BonusTypes.FREE_SPINS) {
                return engine.ReelMover.TYPE_FREE_SPIN;
            }
            return engine.ReelMover.TYPE_REGULAR;
        };
        ReelController.prototype.updateSequenceType = function () {
            var type = this.getSequenceType();
            for (var i = 0; i < this.movers.length; i++) {
                this.movers[i].changeSequenceType(type);
            }
        };
        ReelController.prototype.showAllSymbols = function () {
            this.view.showAllSymbols();
        };
        ReelController.prototype.hideSymbols = function (positions) {
            this.view.hideSymbols(positions);
        };
        ReelController.prototype.setSymbolsPos = function () {
            var reelCount = this.view.getReelCount();
            var symbolsRect = new Array(reelCount);
            for (var reelId = 0; reelId < reelCount; reelId++) {
                var reelView = this.view.getReel(reelId);
                var symbolCount = reelView.getSymbolCount();
                var reelSymbolsRect = new Array(symbolCount);
                for (var symbolId = 0; symbolId < symbolCount; symbolId++) {
                    var symbol = reelView.getSymbol(symbolId);
                    var symbolPos = symbol.localToGlobal(0, 0);
                    var symbolRect = symbol.getBounds().clone();
                    symbolRect.x = symbolPos.x;
                    symbolRect.y = symbolPos.y;
                    symbolRect.width = parseFloat(symbolRect.width);
                    symbolRect.height = parseFloat(symbolRect.height);
                    reelSymbolsRect[symbolId] = symbolRect;
                }
                symbolsRect[reelId] = reelSymbolsRect;
            }
            this.common.symbolsRect = symbolsRect;
        };
        ReelController.prototype.createMovers = function () {
            var _this = this;
            var sequenceType = this.getSequenceType();
            for (var reelId = 0; reelId < this.view.getReelCount(); reelId++) {
                var reelView = this.view.getReel(reelId);
                var reelMover = new engine.ReelMover(this.common.config.reelStrips, reelView);
                reelMover.on(engine.ReelMover.EVENT_REEL_STOPPED, function (stoppedReelId) {
                    _this.send(engine.NotificationList.STOPPED_REEL_ID, _this.movers.length - _this.runnerReels);
                    if (--_this.runnerReels == 0) {
                        _this.send(engine.NotificationList.STOPPED_ALL_REELS);
                    }
                });
                reelMover.on(engine.ReelMover.EVENT_REEL_PREPARE_STOPPED, function (stoppedReelId) {
                    _this.send(engine.NotificationList.STOPPED_REEL_ID_PREPARE, _this.movers.length - _this.runnerReels);
                });
                reelMover.init(this.common.server.wheel, sequenceType, this.common);
                this.movers.push(reelMover);
            }
            if (this.common.config.spinOnReelsClick) {
                this.view.addEventListener("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.send(engine.NotificationList.ON_SCREEN_CLICK);
                    }
                });
            }
        };
        ReelController.prototype.updateDeviceOrient = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].resetStartPosition();
            }
        };
        ReelController.prototype.startSpin = function () {
            if (this.runnerReels > 0) {
                return;
            }
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.runnerReels++;
                this.movers[reelId].start();
            }
        };
        ReelController.prototype.gotSpin = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].onGetSpin(this.common.server.wheel);
            }
        };
        ReelController.prototype.stopSpin = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].expressStop();
            }
        };
        ReelController.prototype.setTempReels = function (id) {
            console.log(id);
        };
        return ReelController;
    })(engine.BaseController);
    engine.ReelController = ReelController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ReelWildController = (function (_super) {
        __extends(ReelWildController, _super);
        function ReelWildController(manager, common, reelsView, winView) {
            _super.call(this, manager);
            this.common = common;
            this.reelsView = reelsView;
            this.winView = winView;
            for (var i = 0; i < ReelWildController.reelsCnt; ++i) {
                ReelWildController.wildSymbolsViews[i] = new engine.WildSymbolView();
                ReelWildController.wildSymbolsViews[i].create(this.common.layouts[engine.WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId], null, this.common.layouts[engine.WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId + 'loop'], this.common.isMobile);
            }
        }
        ReelWildController.isWildReelStarted = function (i) {
            return ReelWildController.wildSymbolsViews[i]._is_wild_started;
        };
        ReelWildController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        ReelWildController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_WILD_REEL_FADEIN);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.SHOW_WILD_REEL);
            notifications.push(engine.NotificationList.REMOVE_WIN_LINES);
            notifications.push(engine.NotificationList.STOPPED_REEL_ID);
            return notifications;
        };
        ReelWildController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.END_BONUS:
                    {
                        break;
                    }
                case engine.NotificationList.SHOW_WILD_REEL_FADEIN:
                    {
                        this.showWildFadeIn(data);
                        break;
                    }
                case engine.NotificationList.SHOW_WILD_REEL:
                    {
                        this.showWild(data);
                        break;
                    }
                case engine.NotificationList.REMOVE_WIN_LINES:
                    {
                        this.setWildsToShow(false);
                        break;
                    }
                case engine.NotificationList.STOPPED_REEL_ID:
                    {
                        this.removeWild(data);
                        break;
                    }
            }
        };
        ReelWildController.prototype.showWild = function (reelId) {
            if (ReelWildController.wildSymbolsViews[reelId] && !ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
                ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x - 10;
                ReelWildController.wildSymbolsViews[reelId].y = 25;
                this.winView.addChild(ReelWildController.wildSymbolsViews[reelId]);
                ReelWildController.wildSymbolsViews[reelId].play();
            }
        };
        ReelWildController.prototype.showWildFadeIn = function (reelId) {
            if (ReelWildController.wildSymbolsViews[reelId] && !ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
                ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x - 10;
                ReelWildController.wildSymbolsViews[reelId].y = 25;
                this.winView.addChild(ReelWildController.wildSymbolsViews[reelId]);
                ReelWildController.wildSymbolsViews[reelId].playFadeIn();
            }
        };
        ReelWildController.prototype.removeWilds = function () {
            for (var reelId = 0; reelId < ReelWildController.reelsCnt; ++reelId) {
                this.removeWild(reelId);
            }
        };
        ReelWildController.prototype.removeWild = function (reelId) {
            if (ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
                this.winView.removeChild(ReelWildController.wildSymbolsViews[reelId]);
                ReelWildController.wildSymbolsViews[reelId]._is_wild_started = false;
            }
        };
        ReelWildController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.reelsView.dispose();
            this.reelsView = null;
            this.common = null;
        };
        ReelWildController.prototype.hideSymbols = function (positions) {
            this.reelsView.hideSymbols(positions);
        };
        ReelWildController.prototype.setWildsToShow = function (show) {
            for (var i = 0; i < ReelWildController.reelsCnt; ++i) {
                this.setWildToShow(i, show);
            }
        };
        ReelWildController.prototype.setWildToShow = function (wildId, show) {
            ReelWildController.wildSymbolsViews[wildId]._is_wild_to_show = show;
        };
        ReelWildController.wildSymbolsViews = [];
        ReelWildController.reelsCnt = 5;
        return ReelWildController;
    })(engine.BaseController);
    engine.ReelWildController = ReelWildController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var ReelMover = (function (_super) {
        __extends(ReelMover, _super);
        function ReelMover(reelStrips, reelView) {
            _super.call(this);
            this.reelStrips = reelStrips;
            this.reelView = reelView;
        }
        ReelMover.prototype.init = function (wheel, sequenceType, common) {
            this.state = ReelMover.STATE_NORMAL;
            this.dh = this.reelView.getSymbol(0).getBounds().height;
            this.startReelPos = this.reelView.getY();
            this.common = common;
            this.changeSequenceType(sequenceType);
            this.resetData();
            this.setServerResponse(wheel);
        };
        ReelMover.prototype.onEnterFrame = function () {
            if (this.state == ReelMover.STATE_NORMAL) {
                return;
            }
            if (this.delayStartReel > 0 && this.state == ReelMover.STATE_UP) {
                this.delayStartReel -= 1;
                return;
            }
            if (this.isStarted) {
                this.isStarted = false;
            }
            if (this.state == ReelMover.STATE_UP) {
                this.up();
            }
            if (this.state == ReelMover.STATE_MOVING) {
                this.move();
            }
            if (this.state == ReelMover.STATE_DOWN) {
                this.down();
            }
            this.updatePos(false);
        };
        ReelMover.prototype.start = function () {
            this.isCanStop = false;
            this.state = ReelMover.STATE_UP;
        };
        ReelMover.prototype.expressStop = function () {
            this.isCutSequence = false;
            this.isHardStop = true;
        };
        ReelMover.prototype.onGetSpin = function (wheel) {
            if (!this.isCanStop) {
                this.updatePos(false);
                this.isCanStop = true;
                this.setServerResponse(wheel);
            }
        };
        ReelMover.prototype.up = function () {
            if (this.upEasing.getTime() < 1) {
                this.pos = this.upEasing.getPosition();
            }
            else {
                this.posPrevState = this.pos;
                this.state = ReelMover.STATE_MOVING;
            }
        };
        ReelMover.prototype.move = function () {
            if (this.moveEasing.getTime() >= 1) {
                if (this.isCanStop) {
                    this.cutSequence();
                    if (this.currentIdx == this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS)) {
                        this.posPrevState = this.pos;
                        this.state = ReelMover.STATE_DOWN;
                        return;
                    }
                }
                this.moveEasing.reset();
                this.posPrevState = this.pos;
            }
            this.pos = this.posPrevState + this.moveEasing.getPosition();
        };
        ReelMover.prototype.down = function () {
            if (this.downEasing.getTime() < 1) {
                this.pos = this.posPrevState + this.downEasing.getPosition();
                if (this.downEasing.getTime() < ReelMover.PREPARE_REEL_SOUND_TIME) {
                    this.dispatchEvent(ReelMover.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
                }
            }
            else {
                this.stop();
            }
        };
        ReelMover.prototype.changeSequenceType = function (moverType) {
            if (this.moverType != moverType) {
                this.moverType = moverType;
                var reelStrips = this.reelStrips[moverType];
                var reelId = this.reelView.getReelId();
                this.reelSequence = reelStrips[reelId].reverse();
                this.sequenceLength = this.reelSequence.length;
                this.upEasing = new engine.BezierEasing(0, 0, 4, -3, 0);
                this.upEasing.setParameters(this.dh * ReelMover.UP_MOVE_SYMBOLS, 10);
                this.moveEasing = new engine.LinearEasing();
                this.moveEasing.setParameters(this.dh, ReelMover.MOVE_SPEED);
                this.downEasing = new engine.BezierEasing(0, -2, 10, -15, 8);
                this.downEasing.setParameters(this.dh * ReelMover.DOWN_MOVE_SYMBOLS, 15);
                this.resetForChangeSequence();
            }
        };
        ReelMover.prototype.stop = function () {
            this.state = ReelMover.STATE_NORMAL;
            this.resetData();
            this.dispatchEvent(ReelMover.EVENT_REEL_STOPPED, this.reelView.getReelId());
        };
        ReelMover.prototype.setServerResponse = function (wheel) {
            var symbolCount = this.reelView.getSymbolCount();
            var targetWheels = new Array(symbolCount);
            if (this.common.isMobile) {
                var id = this.reelView.getReelId();
                for (var i = 0; i < symbolCount; i++) {
                    targetWheels[i] = wheel[symbolCount - i - 1 + symbolCount * this.reelView.getReelId()];
                }
                this.common.config.tempReels[id] = targetWheels;
            }
            else {
                for (var i = 0; i < symbolCount; i++) {
                    targetWheels[i] = wheel[symbolCount - i - 1 + symbolCount * this.reelView.getReelId()];
                }
            }
            var idx = this.getSequenceIdx(targetWheels);
            //console.log(idx);
            if (idx == -1) {
                var reverseReelSequence = this.reelSequence.slice(0).reverse();
                var reverseTargetWheels = targetWheels.slice(0).reverse();
                //console.log("[ERROR]: reel id: " + this.reelView.getReelId() + " sequence type: " + this.moverType + " target wheels: " + reverseTargetWheels + " not found in sequence: " + reverseReelSequence);
                this.reelSequence = this.reelSequence.concat(targetWheels);
                this.sequenceLength = this.reelSequence.length;
                idx = this.getSequenceIdx(targetWheels);
            }
            //console.log(idx, 'setServerResponse idx');
            if (this.state == ReelMover.STATE_NORMAL) {
                this.startIdx = idx;
                this.updatePos(true);
            }
            else {
                this.targetIdx = idx;
            }
        };
        ReelMover.prototype.resetStartPosition = function () {
            this.isRotated = true;
            this.reelView.setY(this.startReelPos);
            var id = this.reelView.getReelId();
            var targetWheelsTemp = this.common.config.tempReels[id];
            if (targetWheelsTemp) {
                var idx = this.getSequenceIdx(targetWheelsTemp);
                if (idx == -1) {
                    this.reelSequence = this.reelSequence.concat(targetWheelsTemp);
                    idx = this.getSequenceIdx(targetWheelsTemp);
                }
            }
            var symbolCount = this.reelView.getSymbolCount();
            for (var i = 0; i < symbolCount; i++) {
                var symbolId = symbolCount - i - 1;
                var symbolIdx = this.correctIdx(idx + i);
                var frame = this.reelSequence[symbolIdx] - 1;
                this.reelView.setSymbolFrame(symbolId, frame);
            }
            this.currentIdx = idx;
        };
        ReelMover.prototype.updatePos = function (isFist) {
            var idx = this.getCurrentIdx();
            var dh = this.pos % this.dh - this.dh;
            this.reelView.setY(this.startReelPos + dh);
            //if (this.currentIdx != idx || isFist) {
            var symbolCount = this.reelView.getSymbolCount() + 1;
            for (var i = 0; i < symbolCount; i++) {
                var symbolId = symbolCount - i - 1;
                var symbolIdx = this.correctIdx(idx + i);
                //var frame:number = (symbolId == 0 || isFist) ? this.reelSequence[symbolIdx] - 1 : this.reelView.getSymbolFrame(symbolId - 1);
                var frame = this.reelSequence[symbolIdx] - 1;
                this.reelView.setSymbolFrame(symbolId, frame);
            }
            this.currentIdx = idx;
            //}
            //update WildPos
            //if(WinController.winSymbolsWild[this.reelView.getReelId()]!=null) {
            //	WinController.winSymbolsWild[this.reelView.getReelId()].setY(this.pos+25); //TODO: calculate 25
            //}
            if (typeof (engine.ReelWildController.wildSymbolsViews[this.reelView.getReelId()]) != "undefined" && (engine.ReelWildController.wildSymbolsViews[this.reelView.getReelId()]._is_wild_started && engine.ReelWildController.wildSymbolsViews[this.reelView.getReelId()].y < (this.pos + 25) || engine.ReelWildController.wildSymbolsViews[this.reelView.getReelId()]._is_wild_started && engine.ReelWildController.wildSymbolsViews[this.reelView.getReelId()].y - this.pos < 200)) {
                engine.ReelWildController.wildSymbolsViews[this.reelView.getReelId()].setY(this.pos + 25); //TODO: calculate 25
            }
        };
        ReelMover.prototype.getCurrentIdx = function () {
            return engine.Utils.float2int(this.correctIdx(this.startIdx + Math.abs(this.pos + ReelMover.EPSILON) / this.dh));
        };
        ReelMover.prototype.getSequenceIdx = function (value) {
            var i = 0;
            var valueLength = value.length;
            var j = 0;
            while (j < valueLength) {
                if (this.reelSequence.length == i) {
                    // for set wheel
                    return -1;
                }
                if (this.reelSequence[i] == value[j]) {
                    j++;
                    if (i == this.sequenceLength - 1) {
                        i = -1;
                    }
                }
                else if (j > 0) {
                    i = this.correctIdx(i - j);
                    j = 0;
                }
                i++;
            }
            var idx = i - valueLength;
            if (idx < 0) {
                idx = this.sequenceLength + idx;
            }
            //console.log(idx);
            //console.log(this.reelSequence, "getSequenceIdx idx="+idx);
            return idx;
        };
        ReelMover.prototype.correctIdx = function (value) {
            if (value >= 0 && value < this.sequenceLength) {
                return value;
            }
            return (value + this.sequenceLength) % this.sequenceLength;
        };
        ReelMover.prototype.cutSequence = function () {
            if (this.isCutSequence) {
                return false;
            }
            var maxDiv = this.isHardStop ? ReelMover.HARD_STOP_SYMBOLS : ReelMover.REGULAR_STOP_SYMBOLS;
            this.isHardStop = false;
            var realDivIdx = this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS - this.currentIdx);
            if (realDivIdx <= maxDiv) {
                return;
            }
            var symbolId = this.reelSequence[this.correctIdx(this.currentIdx + maxDiv)];
            if (this.isGroupSymbol(symbolId)) {
                return;
            }
            var div = realDivIdx - maxDiv;
            while (this.isGroupSymbol(this.reelSequence[this.correctIdx(this.currentIdx + div + maxDiv)])) {
                div--;
            }
            if (div <= 0) {
                return;
            }
            this.pos += div * this.dh;
            this.currentIdx = this.getCurrentIdx();
            //console.log(this.currentIdx);
            this.isCutSequence = true;
        };
        // TODO: need implemented
        ReelMover.prototype.isGroupSymbol = function (symbolId) {
            return false;
        };
        // Сброс данных после остановки барабана
        ReelMover.prototype.resetData = function () {
            this.delayStartReel = ReelMover.DELAY_BETWEEN_START_REEL * this.reelView.getReelId();
            this.startIdx = this.targetIdx;
            this.targetIdx = 0;
            this.isCutSequence = false;
            this.pos = 0;
            this.upEasing.reset();
            this.downEasing.reset();
            this.moveEasing.reset();
        };
        // Сброс данных после изменения ленты
        ReelMover.prototype.resetForChangeSequence = function () {
            this.currentIdx = 0;
            this.startIdx = 0;
        };
        ReelMover.prototype.getReelView = function () {
            return this.reelView;
        };
        // events
        ReelMover.EVENT_REEL_STOPPED = "event_reel_stopped";
        ReelMover.EVENT_REEL_PREPARE_STOPPED = "event_reel_prepare_stopped";
        // reel strips type
        ReelMover.TYPE_REGULAR = "regular";
        ReelMover.TYPE_FREE_SPIN = "freeSpins";
        // mover states
        ReelMover.STATE_NORMAL = "normal";
        ReelMover.STATE_UP = "up";
        ReelMover.STATE_MOVING = "moving";
        ReelMover.STATE_DOWN = "end";
        ReelMover.UP_MOVE_SYMBOLS = 1;
        ReelMover.DOWN_MOVE_SYMBOLS = 1;
        ReelMover.MOVE_SPEED = 2;
        ReelMover.DELAY_BETWEEN_START_REEL = 7;
        ReelMover.REGULAR_STOP_SYMBOLS = 4;
        ReelMover.HARD_STOP_SYMBOLS = 4;
        ReelMover.EPSILON = 0.001;
        ReelMover.PREPARE_REEL_SOUND_TIME = 0.09;
        return ReelMover;
    })(EventDispatcher);
    engine.ReelMover = ReelMover;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var LoadQueue = createjs.LoadQueue;
    var Sound = createjs.Sound;
    var WebAudioPlugin = createjs.WebAudioPlugin;
    var HTMLAudioPlugin = createjs.HTMLAudioPlugin;
    var LayoutCreator = layout.LayoutCreator;
    var LoaderController = (function (_super) {
        __extends(LoaderController, _super);
        function LoaderController(manager, common, view) {
            _super.call(this, manager);
            this.isSoundLoadStarted = false;
            this.common = common;
            this.view = view;
            this.isGameStarted = false;
        }
        LoaderController.prototype.init = function () {
            var _this = this;
            _super.prototype.init.call(this);
            this.common.isSoundLoaded = false;
            this.view.on(LayoutCreator.EVENT_LOADED, function () {
                _this.send(engine.NotificationList.HIDE_PRELOADER);
                _this.initLoader();
            });
            this.firstUp = 0;
            this.tempLayout = [];
            this.view.load(engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.LAYOUT_FOLDER + engine.LoaderView.LAYOUT_NAME + engine.FileConst.LAYOUT_EXTENSION, true);
        };
        LoaderController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SERVER_INIT);
            notifications.push(engine.NotificationList.LOAD_SOUNDS);
            notifications.push(engine.NotificationList.LAZY_LOAD);
            notifications.push(engine.NotificationList.LAZY_LOAD_COMP);
            return notifications;
        };
        LoaderController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SERVER_INIT:
                    {
                        break;
                    }
                case engine.NotificationList.LOAD_SOUNDS:
                    {
                        this.loadSounds();
                        break;
                    }
            }
        };
        LoaderController.prototype.initLoader = function () {
            var _this = this;
            for (var i = 0; i < this.view.buttonsNames.length; i++) {
                var buttonName = this.view.buttonsNames[i];
                var button = this.view.getChildByName(buttonName);
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onButtonClick(eventObj.currentTarget);
                    }
                });
            }
        };
        LoaderController.prototype.onButtonClick = function (button) {
            switch (button.name) {
                case engine.LoaderView.BUTTON_YES:
                    {
                        this.common.isSoundOn = true;
                        break;
                    }
                case engine.LoaderView.BUTTON_NO:
                    {
                        this.common.isSoundOn = false;
                        break;
                    }
            }
            if (this.common.isMobile) {
                engine.FullScreen.fullScreen();
            }
            this.loadConfig();
            this.view.showPreloaderBg(0, 1);
            this.view.hideButtons();
            this.view.showProgressBar();
        };
        LoaderController.prototype.loadSounds = function () {
            var _this = this;
            if (!this.isSoundLoadStarted) {
                this.isSoundLoadStarted = true;
                var totalFiles = this.common.config.sounds.length;
                var leftLoad = totalFiles;
                if (leftLoad > 0) {
                    Sound.alternateExtensions = ["mp3"];
                    Sound.registerPlugins([WebAudioPlugin, HTMLAudioPlugin]);
                    Sound.addEventListener("fileload", function (event) {
                        console.log("loaded sound id = " + event.id + " url = " + event.src);
                        leftLoad--;
                        if (_this.view != null) {
                            var progress = LoaderController.ASSETS_PERCENTAGE + (totalFiles - leftLoad) / totalFiles * LoaderController.SOUND_PERCENTAGE;
                            _this.view.setProgress(progress);
                        }
                        if (leftLoad == 0) {
                            _this.onSoundsLoaded();
                        }
                    });
                    Sound.registerManifest(this.common.config.sounds, engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.SOUNDS_FOLDER);
                }
                else if (leftLoad == 0) {
                    this.onSoundsLoaded();
                }
            }
        };
        LoaderController.prototype.onSoundsLoaded = function () {
            this.common.isSoundLoaded = true;
            this.onLoaded();
        };
        LoaderController.prototype.loadConfig = function () {
            var _this = this;
            var configUrl = engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.GAME_CONFIG_NAME + engine.FileConst.JSON_EXTENSION + "?" + this.common.key;
            var assetVO = new engine.AssetVO(engine.GameConst.GAME_CONFIG_NAME, configUrl, LoadQueue.JSON);
            createjs.LoadQueue.loadTimeout = 1000;
            var configLoader = new LoadQueue(false);
            configLoader.on("complete", function () {
                _this.common.config = configLoader.getResult(engine.GameConst.GAME_CONFIG_NAME);
                _this.loadFonts();
            });
            configLoader.setMaxConnections(10);
            configLoader.loadFile(assetVO);
        };
        LoaderController.prototype.loadFonts = function () {
            var _this = this;
            var fonts = this.common.config.fonts;
            if (fonts != null && fonts.length > 0) {
                var style = document.createElement('style');
                var manifest = [];
                for (var i = 0; i < fonts.length; i++) {
                    var fontName = fonts[i].name;
                    var fontURL = engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.FONTS_FOLDER + fonts[i].fileName + engine.FileConst.WOFF_EXTENSION;
                    style.appendChild(document.createTextNode("@font-face {font-family: '" + fontName + "'; src: url('" + fontURL + "');}"));
                    document.head.appendChild(style);
                    manifest.push({ "id": fontName, "src": fontURL });
                }
                var configLoader = new LoadQueue(false);
                configLoader.on("complete", function () {
                    _this.getForFirst();
                });
                configLoader.loadManifest(manifest);
            }
            else {
                this.getForFirst();
            }
        };
        LoaderController.prototype.getUrlDownl = function () {
            return engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.LAYOUT_FOLDER;
        };
        LoaderController.prototype.loadLayouts = function (from, to, onComplete, eachLoaded) {
            if (onComplete === void 0) { onComplete = null; }
            if (eachLoaded === void 0) { eachLoaded = null; }
            if (to > this.maxLeyers)
                return;
            var layoutsName = this.common.config.layouts;
            var layouts = this.tempLayout;
            for (var i = from; i < to; i++) {
                var layoutName = layoutsName[i];
                var LayoutClass = engine[layoutName];
                layouts[layoutName] = LayoutClass ? new LayoutClass() : new LayoutCreator();
            }
            var needLoad = to - from;
            var leftLoad = needLoad;
            var baseLayoutUrl = this.getUrlDownl();
            for (var i = from; i < to; i++) {
                //console.log(i);
                var layoutName = layoutsName[i];
                var layoutObj = layouts[layoutName];
                layoutObj.on(LayoutCreator.EVENT_LOADED, function () {
                    eachLoaded != null && eachLoaded();
                    leftLoad -= 1;
                    if (leftLoad == 0) {
                        onComplete != null && onComplete();
                    }
                });
                var url = baseLayoutUrl + layoutName + engine.FileConst.LAYOUT_EXTENSION + "?" + this.common.key;
                layoutObj.load(url);
            }
            this.common.layouts = layouts;
        };
        LoaderController.prototype.initGame = function () {
            var _this = this;
            if (this.view != null) {
                this.view.dispose();
                this.view = null;
            }
            this.isGameStarted = true;
            this.loadLayouts(this.firstUp, this.common.config.layouts.length, function () {
                _this.common.is_layout_load_compl = true;
                _this.send(engine.NotificationList.RES_LOADED);
                if (_this.common.isSoundOn) {
                    _this.send(engine.NotificationList.LOAD_SOUNDS);
                }
                else {
                    _this.onLoaded();
                }
            });
        };
        LoaderController.prototype.getForFirst = function () {
            var _this = this;
            this.breakPoint = this.common.config.priorityLevel;
            this.maxLeyers = this.common.config.layouts.length;
            this.loadLayouts(this.firstUp, this.breakPoint, function () {
                _this.firstUp = _this.breakPoint;
                _this.initGame();
            }, function () {
                ++_this.firstUp;
                _this.updateLoader();
            });
        };
        LoaderController.prototype.updateLoader = function () {
            var leftLoad = this.breakPoint - this.firstUp;
            var progress = (this.breakPoint - leftLoad) / this.breakPoint;
            if (this.common.isSoundOn) {
                progress *= LoaderController.ASSETS_PERCENTAGE;
            }
            this.view.setProgress(progress);
        };
        LoaderController.prototype.onLoaded = function () {
            this.send(engine.NotificationList.LAZY_LOAD_COMP);
            this.dispose();
        };
        LoaderController.prototype.dispose = function () {
            if (this.common.isSoundLoaded) {
                _super.prototype.dispose.call(this);
                this.common = null;
            }
        };
        LoaderController.SOUND_PERCENTAGE = 0.4;
        LoaderController.ASSETS_PERCENTAGE = 1 - LoaderController.SOUND_PERCENTAGE;
        return LoaderController;
    })(engine.BaseController);
    engine.LoaderController = LoaderController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Point = createjs.Point;
    var Ticker = createjs.Ticker;
    var Shape = createjs.Shape;
    var WinController = (function (_super) {
        __extends(WinController, _super);
        function WinController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            this.createWinMask();
        }
        WinController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.createWinMask();
        };
        WinController.prototype.createWinMask = function () {
            this.container.mask = engine.ReelsView.PLAY_FIELD_MASK;
            this.container.hitArea = engine.ReelsView.PLAY_FIELD_MASK;
            var mask = new Shape();
            if (this.common.isMobile) {
                mask.graphics.beginFill("0").drawRect(0, 25, 800, 460);
                this.container.mask = mask;
                this.container.hitArea = mask;
            }
        };
        WinController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_WIN_LINES);
            notifications.push(engine.NotificationList.REMOVE_WIN_LINES);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.HIDE_SYMBOLS);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.STOPPED_REEL_ID);
            return notifications;
        };
        WinController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_WIN_LINES:
                    {
                        this.create(data);
                        break;
                    }
                case engine.NotificationList.REMOVE_WIN_LINES:
                    {
                        this.remove();
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.container.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.container.visible = true;
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        break;
                    }
                case engine.NotificationList.STOPPED_REEL_ID:
                    {
                        this.removeWinSymbolWild(data);
                        break;
                    }
            }
        };
        WinController.prototype.onEnterFrame = function () {
            this.container.tickChildren = false;
            var k = (this.common.isMobile) ? 5 : 2;
            var speedControl = Ticker.getTicks() % 2;
            if (speedControl == 0) {
                this.container.tickChildren = true;
                if (this.winLines != null) {
                    this.winLines.onEnterFrame();
                }
            }
        };
        WinController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.remove();
            this.common = null;
            this.container = null;
        };
        WinController.prototype.create = function (isTogether) {
            var _this = this;
            var winLinesVOs = this.common.server.winLines;
            if (winLinesVOs.length == 0) {
                this.send(engine.NotificationList.WIN_LINES_SHOWED);
                return;
            }
            if (isTogether) {
                this.winLines = new engine.WinLinesTogether(this.createWinLines(), this.common.config.winAnimationTime);
            }
            else {
                this.winLines = new engine.WinLines(this.createWinLines(), this.common.config.winAnimationTime);
                this.send(engine.NotificationList.GET_ALL_IDS);
            }
            this.winLines.on(engine.BaseWinLines.EVENT_CREATE_LINE, function (eventObj) {
                var hydeSymbols = eventObj.target[0];
                _this.send(engine.NotificationList.HIDE_SYMBOLS, hydeSymbols);
                _this.send(engine.NotificationList.HIDE_LINES);
                _this.send(engine.NotificationList.SHOW_LINES, eventObj.target);
            });
            this.winLines.on(engine.BaseWinLines.EVENT_All_LINES_SHOWED, function () {
                //if(this.common.is_win_popup_showed) {
                _this.send(engine.NotificationList.WIN_LINES_SHOWED);
                //}
            });
            this.winLines.on(engine.BaseWinLines.EVENT_LINE_SHOWED, function (eventObj) {
                _this.send(engine.NotificationList.SHOW_ALL_SYMBOLS);
                var index = eventObj.currentTarget.index;
                var hydeSymbols = _this.symbolsToHide[index];
                _this.send(engine.NotificationList.HIDE_SYMBOLS, hydeSymbols);
            });
            for (var i = 0; i < this.winLines.winLines.length; ++i) {
                this.winLines.winLines[i].on(engine.BaseWinLines.SHOW_WILD_REEL, function (data) {
                    _this.send(engine.NotificationList.SHOW_WILD_REEL, data.currentTarget.wildLineId);
                });
            }
            this.winLines.create();
        };
        WinController.prototype.createWinLines = function () {
            var wheel = this.common.server.wheel;
            var symbolsRect = this.common.symbolsRect;
            var winLinesVOs = this.common.server.winLines;
            var symbolCount = symbolsRect[0].length;
            var allWinAniVOs = [];
            var winLines = [];
            var winValue = 0;
            this.symbolsToHide = [];
            for (var i = 0; i < winLinesVOs.length; i++) {
                var maskedSymbols = [];
                var winLinesVO = winLinesVOs[i];
                winValue += winLinesVO.win;
                var winPos = winLinesVO.winPos;
                var winAniVOs = [];
                for (var reelId = 0; reelId < winPos.length; reelId++) {
                    var symbolIdx = (this.common.jayaServer) ? winPos[reelId] : winPos[reelId] - 1;
                    if (symbolIdx >= 0) {
                        var rect = (wheel[symbolIdx + symbolCount * reelId] == 1) ? symbolsRect[reelId][0] : symbolsRect[reelId][symbolIdx];
                        var posIdx = new Point(reelId, symbolIdx);
                        var winAniVO = WinController.findWinVO(allWinAniVOs, posIdx);
                        var winSymbolView;
                        if (winAniVO == null) {
                            winSymbolView = this.createWinSymbol(wheel[symbolIdx + symbolCount * reelId]);
                        }
                        else {
                            winSymbolView = winAniVO.winSymbolView;
                        }
                        winAniVO = new engine.WinAniVO();
                        winAniVO.winSymbolView = winSymbolView;
                        winAniVO.rect = rect.clone();
                        winAniVO.rect.x -= 10;
                        winAniVO.rect.y -= 10;
                        winAniVO.posIdx = posIdx;
                        var hideSymbol = posIdx;
                        allWinAniVOs.push(winAniVO);
                        winAniVOs.push(winAniVO);
                        maskedSymbols.push(hideSymbol);
                    }
                }
                this.symbolsToHide.push(maskedSymbols);
                var winLine = new engine.WinLine(this.container, winLinesVO.lineId + 1, winAniVOs);
                winLines.push(winLine);
            }
            this.common.server.win = winValue;
            return winLines;
        };
        WinController.prototype.createWinSymbol = function (symbolId) {
            var regularAni = this.common.layouts[WinController.WIN_ANIMATION_PREFIX + symbolId];
            var highlightAni = this.common.layouts[WinController.WIN_ANIMATION_HIGHLIGHT_PREFIX];
            var winSymbolView = new engine.WinSymbolView();
            winSymbolView.create(regularAni, highlightAni);
            if (typeof (this.common.config.wildSymbolId) != "undefined" && symbolId == this.common.config.wildSymbolId) {
                winSymbolView._is_wild = true;
            }
            return winSymbolView;
        };
        WinController.findWinVO = function (winVOs, posIdx) {
            for (var i = 0; i < winVOs.length; i++) {
                var vo = winVOs[i];
                if (vo.posIdx.x == posIdx.x && vo.posIdx.y == posIdx.y) {
                    return vo;
                }
            }
            return null;
        };
        WinController.prototype.remove = function () {
            if (this.winLines != null) {
                this.winLines.dispose();
                this.winLines = null;
            }
        };
        WinController.prototype.removeWinSymbolsWild = function () {
            if (WinController.winSymbolsWild != null) {
                for (var i = 0; i < WinController.winSymbolsWild.length; ++i) {
                    this.removeWinSymbolWild(i);
                }
            }
        };
        WinController.prototype.removeWinSymbolWild = function (symbolid) {
            //console.log(symbolid, "removeWinSymbolWild");
            if (WinController.winSymbolsWild[symbolid] != null) {
                this.container.removeChild(WinController.winSymbolsWild[symbolid]);
                WinController.winSymbolsWild[symbolid] = null;
            }
        };
        WinController.winSymbolsWild = new Array();
        WinController.WIN_ANIMATION_PREFIX = "WinAnimation_";
        WinController.WIN_ANIMATION_HIGHLIGHT_PREFIX = "SymbolHighlight";
        return WinController;
    })(engine.BaseController);
    engine.WinController = WinController;
})(engine || (engine = {}));
/**
 * Created by Taras on 24.03.2015.
 */
var engine;
(function (engine) {
    var WeelController = (function (_super) {
        __extends(WeelController, _super);
        function WeelController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
        }
        WeelController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.create();
        };
        WeelController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            //notifications.push(NotificationList.SHOW_WIN_LINES);
            //notifications.push(NotificationList.REMOVE_WIN_LINES);
            //notifications.push(NotificationList.OPEN_PAY_TABLE);
            //notifications.push(NotificationList.CLOSE_PAY_TABLE);
            //notifications.push(NotificationList.HIDE_SYMBOLS);
            return notifications;
        };
        WeelController.prototype.create = function () {
            this.view.create();
            //mask.alpha = 0.3;
            //this.view.addChild(mask);
        };
        WeelController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_WIN_LINES:
                    {
                        break;
                    }
            }
        };
        WeelController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
        };
        return WeelController;
    })(engine.BaseController);
    engine.WeelController = WeelController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Touch = createjs.Touch;
    var Container = createjs.Container;
    var GameController = (function (_super) {
        __extends(GameController, _super);
        function GameController() {
            _super.call(this, new engine.ControllerManager());
            this.statsController = null;
            this.hudController = null;
            this.settingMobileController = null;
        }
        /*public start(gameName:string, key:string, container:Container):void {
            super.init();

            this.container = container;
            this.common = new CommonRefs();
            this.common.gameName = gameName;
            this.common.key = key;
            this.common.isMobile = Device.isMobile();

            this.initBaseControllers();
        }*/
        GameController.prototype.startSingle = function (stage, artWidth, artHeight, gameName, mobile, isDemo, login, password, isDebug, room) {
            var _this = this;
            _super.prototype.init.call(this);
            this.stage = stage;
            this.common = new engine.CommonRefs();
            this.common.isMobile = mobile;
            this.common.gameName = this.common.isMobile ? gameName + "_mobile" : gameName;
            this.common.login = login;
            this.common.password = password;
            this.common.room = room;
            this.common.isDemo = isDemo;
            this.common.isDebug = isDebug;
            this.common.artWidth = artWidth;
            this.common.artHeight = artHeight;
            this.tempLayout = [];
            Ticker.setFPS(engine.GameConst.GAME_FPS);
            this.container = new Container();
            this.backgroundContainer = new Container();
            this.hudContainer = new Container();
            this.settingContainer = new Container();
            if (!this.common.isMobile) {
                this.stage.enableMouseOver(Ticker.getFPS());
            }
            this.stage.mouseMoveOutside = true;
            if (Touch.isSupported()) {
                Touch.enable(this.stage, true, true);
            }
            this.stage.addChild(this.backgroundContainer);
            this.stage.addChild(this.container);
            this.stage.addChild(this.hudContainer);
            this.stage.addChild(this.settingContainer);
            Ticker.addEventListener("tick", function () {
                _this.gameUpdate();
            });
            this.onResize();
            engine.FullScreen.init();
            this.initBaseControllers();
        };
        GameController.prototype.gameUpdate = function () {
            this.manager.onEnterFrame();
        };
        GameController.prototype.onEnterFrame = function () {
            if (this.stage != null) {
                this.stage.update();
            }
        };
        GameController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SERVER_INIT);
            notifications.push(engine.NotificationList.BACK_TO_LOBBY);
            notifications.push(engine.NotificationList.HIDE_PRELOADER);
            notifications.push(engine.NotificationList.LAZY_LOAD_COMP);
            return notifications;
        };
        GameController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SERVER_INIT:
                    {
                        this.initGameControllers();
                        this.initKeyboardHandler();
                        break;
                    }
                case engine.NotificationList.BACK_TO_LOBBY:
                    {
                        this.container.dispatchEvent(GameController.EVENT_BACK_TO_LOBBY);
                        break;
                    }
                case engine.NotificationList.HIDE_PRELOADER:
                    {
                        this.stage.dispatchEvent(GameController.EVENT_HIDE_PRELOADER);
                        break;
                    }
                case engine.NotificationList.LAZY_LOAD_COMP:
                    {
                        this.common.is_lazy_load_compl = true;
                        break;
                    }
            }
        };
        GameController.prototype.tryInitGame = function () {
            if (this.common.is_server_init && this.common.is_layout_load_compl) {
                this.initGameControllers();
                this.initKeyboardHandler();
            }
        };
        GameController.prototype.initBaseControllers = function () {
            var baseController;
            baseController = new engine.ServerController(this.manager, this.common);
            baseController.init();
            var loaderView = new engine.LoaderView();
            baseController = new engine.LoaderController(this.manager, this.common, loaderView);
            baseController.init();
            this.backgroundContainer.addChild(loaderView);
        };
        GameController.prototype.refreshSomeView = function () {
            var baseController;
            var hudView = this.getHud();
            console.log("refreshSomeView", hudView);
            this.statsController.setView(hudView);
            this.hudController.setView(hudView);
            this.hudController.refreshHud();
            this.hudController.setOrigin();
            this.createHudPanels(hudView);
            hudView.resizeForMobile(this.common);
        };
        GameController.prototype.initGameControllers = function () {
            var baseController;
            this.crateHuds();
            var hudView = this.getHud();
            // create background
            var backgroundContainer = new Container();
            baseController = new engine.BackgroundController(this.manager, this.common, backgroundContainer);
            baseController.init();
            this.tempLayout.push(baseController);
            // create header
            var headerContainer = new Container();
            baseController = new engine.HeaderController(this.manager, this.common, headerContainer);
            baseController.init();
            var reelsView = this.common.layouts[engine.ReelsView.LAYOUT_NAME];
            baseController = new engine.ReelController(this.manager, this.common, reelsView);
            baseController.init();
            // create win animation
            var winView = new Container();
            baseController = new engine.WinController(this.manager, this.common, winView);
            baseController.init();
            //create Wild Views
            baseController = new engine.ReelWildController(this.manager, this.common, reelsView, winView);
            baseController.init();
            // create lines
            var linesView = this.common.layouts[engine.LinesView.LAYOUT_NAME];
            baseController = new engine.LinesController(this.manager, this.common, linesView);
            baseController.init();
            // create bonus
            var bonusContainer = new Container();
            baseController = new engine.BonusHolderController(this.manager, this.common, bonusContainer);
            baseController.init();
            var modalContainer = new Container();
            baseController = new engine.ModalWindowController(this.manager, this.common, modalContainer);
            baseController.init();
            // error controller
            baseController = new engine.ErrorController(this.manager, this.common, modalContainer);
            baseController.init();
            this.statsController = new engine.StatsController(this.manager, this.common, hudView, this.container);
            this.statsController.init();
            this.hudController = new engine.HudController(this.manager, this.common, hudView);
            this.hudController.init();
            var borderView = this.common.layouts[engine.BorderView.LAYOUT_NAME];
            baseController = new engine.BorderController(this.manager, this.common, borderView);
            baseController.init();
            var nameTmpl = engine.SettingMobileView.LAYOUT_NAME;
            this.settingMobileController = new engine.SettingMobileController(this.manager, this.common, this.settingContainer, nameTmpl, this.hudContainer, this.stage);
            this.settingMobileController.init();
            this.createHudPanels(hudView);
            baseController = new engine.SoundController(this.manager, this.common);
            baseController.init();
            var fon = this.common.layouts[engine.GameFonView.LAYOUT_NAME];
            fon.create();
            this.backgroundContainer.addChild(backgroundContainer);
            this.container.addChild(fon);
            this.container.addChild(reelsView);
            this.container.addChild(borderView);
            this.container.addChild(linesView);
            this.container.addChild(winView);
            //fon.scaleX = fon.scaleY = reelsView.scaleX = reelsView.scaleY = borderView.scaleX = borderView.scaleY = linesView.scaleX = linesView.scaleY = winView.scaleX = winView.scaleY = hudView.getBounds().width / borderView.getBounds().width;
            this.hudContainer.addChild(hudView);
            this.artLoaded = true;
            if (this.common.isMobile) {
            }
            else {
                hudView.setUpElements(this.common);
            }
            this.onResize();
            this.container.addChild(bonusContainer);
            this.container.addChild(headerContainer);
            this.container.addChild(modalContainer);
        };
        GameController.prototype.createHudPanels = function (hudView) {
            var baseController;
            var autoSpinTableView = hudView.getChildByName(engine.HudView.AUTO_SPIN_TABLE_NEW);
            var autoSpinTableMask = hudView.getChildByName(engine.HudView.AUTO_SPIN_TABLE_MASK);
            if (autoSpinTableView != null) {
                baseController = new engine.AutoSpinTableController(this.manager, this.common, autoSpinTableView, autoSpinTableMask);
                baseController.init();
            }
            var selectBetTableView = hudView.getChildByName(engine.HudView.SELECT_BET_TABLE);
            var selectBetTableMask = hudView.getChildByName(engine.HudView.SELECT_BET_TABLE_MASK);
            if (selectBetTableView != null) {
                baseController = new engine.SelectBetController(this.manager, this.common, selectBetTableView, selectBetTableMask);
                baseController.init();
            }
        };
        GameController.prototype.createReels = function () {
            //var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
            //var index:number = this.container.getChildIndex(reelsView);
            //this.container.removeChild(reelsView);
            //reelsView.onInit();
            //this.container.addChildAt(reelsView, index);
            //var weelView:WeelView = this.common.layouts[WeelView.LAYOUT_NAME];
            //var index:number = this.container.getChildIndex(weelView);
            //this.container.removeChild(weelView);
            //weelView.onInit();
            //this.container.addChildAt(weelView, index);
            this.refreshSomeView();
        };
        GameController.prototype.initKeyboardHandler = function () {
            var _this = this;
            document.onkeydown = function (event) {
                _this.send(engine.NotificationList.KEYBOARD_CLICK, event.keyCode);
            };
        };
        GameController.prototype.crateHuds = function () {
            if (this.common.layouts != null) {
                var hudView = this.common.layouts['HudView'];
                hudView.create();
                if (this.common.isMobile) {
                    var hudView2 = this.common.layouts['HudViewP'];
                    hudView2.create();
                }
            }
        };
        GameController.prototype.getHud = function () {
            var hudView;
            if (this.orientationView != undefined) {
                while (this.hudContainer.getNumChildren() > 0) {
                    this.hudContainer.removeChildAt(0);
                }
            }
            if (this.common.layouts != null) {
                var name;
                name = (window.innerHeight > window.innerWidth && this.common.isMobile) ? 'HudViewP' : 'HudView';
                hudView = this.common.layouts[name];
                this.orientationView = hudView;
                this.hudContainer.addChild(this.orientationView);
                this.common.rotateHudMode = name;
            }
            return hudView;
        };
        GameController.prototype.onResize = function () {
            console.log("onResize");
            this.onResizeWindow(); //TODO: fix that function is called 2 times when going into vertical mode and 1 time when going into horizontal mode
        };
        GameController.prototype.onResizeWindow = function () {
            if (this.common.isMobile) {
            }
            var windowWidth = window.innerWidth;
            var windowHeight = window.innerHeight;
            var canvas = this.stage.canvas;
            var maxWidth = windowWidth;
            var maxHeight = windowHeight;
            // keep aspect ratio
            var scale = Math.min(maxWidth / this.common.artWidth, maxHeight / this.common.artHeight);
            canvas.width = windowWidth;
            canvas.height = windowHeight;
            if (this.common.isMobile) {
                //scale *= 1.18;
                this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = scale;
                this.backgroundContainer.x = (maxWidth - this.common.artWidth * scale) / 2;
                this.backgroundContainer.y = (maxHeight - this.common.artHeight * scale) / 2;
                if (this.common.layouts != null && this.common.is_layout_load_compl) {
                    var backgroundScale = Math.max(windowWidth / this.backgroundContainer.getBounds().width, windowHeight / this.backgroundContainer.getBounds().height);
                    this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = backgroundScale;
                    this.backgroundContainer.x = this.backgroundContainer.y = 0;
                    scale = Math.min(windowHeight / this.common.artHeight, windowWidth / this.common.artWidth);
                    this.hudContainer.scaleX = this.hudContainer.scaleY = scale;
                    this.hudContainer.x = (maxWidth - this.common.artWidth * scale) / 2;
                    this.container.scaleX = this.container.scaleY = scale;
                    this.container.x = this.hudContainer.x + (((this.common.artWidth * scale) - (this.common.artWidth * this.container.scaleX)) / 2);
                    //this.container.y = this.hudContainer.y;
                    this.container.y = this.hudContainer.y = 0;
                    this.hudContainer.y = this.hudContainer.y + 86 * this.hudContainer.scaleY;
                    if (window.innerHeight > window.innerWidth) {
                        scale = maxWidth / this.common.artHeight;
                        this.hudContainer.x = this.settingContainer.x = this.stage.x;
                        this.hudContainer.y = this.stage.y;
                        this.hudContainer.scaleX = this.hudContainer.scaleY = scale;
                        this.container.y = (maxHeight - this.common.artHeight * scale) / 2 + 90 * this.hudContainer.scaleY;
                    }
                    else {
                        //this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = 2 * Math.max(windowWidth/this.backgroundContainer.getBounds().width, windowHeight/this.backgroundContainer.getBounds().height);
                        this.settingContainer.x = this.backgroundContainer.x = 0;
                    }
                    if (this.artLoaded) {
                        this.common.config.isRotated = true;
                        this.send(engine.NotificationList.CHANGE_DEVICE_ORIENTATION);
                        this.createReels();
                    }
                }
            }
            else {
                if (scale > 1 && !this.common.isMobile) {
                    scale = 1;
                }
                this.stage.scaleX = scale;
                this.stage.scaleY = scale;
                this.stage.x = (maxWidth - this.common.artWidth * scale) / 2;
                this.stage.y = (maxHeight - this.common.artHeight * scale) / 2;
                this.container.scaleX = this.container.scaleY = 1 / 1.2;
                this.container.x = ((this.common.artWidth) - (this.common.artWidth * this.container.scaleX)) / 2;
            }
            if (this.common.layouts != null && this.common.is_layout_load_compl) {
                this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = 2 * Math.max(windowWidth / this.backgroundContainer.getBounds().width, windowHeight / this.backgroundContainer.getBounds().height);
                this.backgroundContainer.x = this.backgroundContainer.y = 0;
            }
        };
        GameController.prototype.dispose = function () {
            this.send(engine.NotificationList.SERVER_DISCONNECT);
        };
        GameController.EVENT_BACK_TO_LOBBY = "back_to_lobby";
        GameController.EVENT_HIDE_PRELOADER = "hide_preloader";
        return GameController;
    })(engine.BaseController);
    engine.GameController = GameController;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.12.2014.
 */
var engine;
(function (engine) {
    var DrawLine = (function () {
        function DrawLine(container, common) {
            this.container = container;
            this.common = common;
            this.winLines = [];
        }
        DrawLine.prototype.create = function (id, alpha) {
            var width;
            var yc;
            var height;
            var params = this.common.symbolsRect[0][0];
            var x = params.x;
            var y = params.y;
            height = width = params.width;
            var cords = (this.common.jayaServer) ? this.common.config.allLines[id] : this.common.config.allLines[id - 1];
            var newLine = new createjs.Shape();
            var color = this.getRandomLineColor();
            yc = (this.common.jayaServer) ? cords[0] : cords[0] + 1;
            newLine.graphics.setStrokeStyle(3).beginStroke(color);
            if (this.common.jayaServer) {
                newLine.graphics.moveTo(x + (width / 2), y + ((yc * height) + height / 2));
            }
            else {
                newLine.graphics.moveTo(x + (width / 2), y + ((yc * height) - height / 2));
            }
            for (var c = 1; c < cords.length; c++) {
                if (this.common.jayaServer) {
                    yc = cords[c];
                    newLine.graphics.lineTo(x + (c * width) + (width / 2), y + ((yc * height) + height / 2));
                }
                else {
                    yc = cords[c] + 1;
                    newLine.graphics.lineTo(x + (c * width) + (width / 2), y + ((yc * height) - height / 2));
                }
            }
            newLine.graphics.endStroke();
            newLine.alpha = alpha;
            newLine.id = id;
            this.winLines.push(newLine);
            return newLine;
        };
        DrawLine.prototype.remove = function () {
            for (var i = this.container.children.length; i >= 0; i--) {
                var line = this.container.children[i];
                this.container.removeChild(line);
            }
        };
        DrawLine.prototype.getRandomLineColor = function () {
            return '#41E969';
        };
        DrawLine.prototype.reset = function () {
            this.index = -1;
        };
        DrawLine.prototype.showNextLine = function () {
            this.hideAll();
            this.leftTime = this.time;
            this.index++;
            this.index = this.index < this.common.server.arrLinesIds.length && this.index >= 0 ? this.index : 0;
            this.showLine();
        };
        DrawLine.prototype.hideAll = function () {
            for (var i = 0; i < this.winLines.length; i++) {
                this.winLines[i].alpha = 0;
            }
        };
        DrawLine.prototype.showLine = function () {
            var id = this.common.server.arrLinesIds[this.index];
            var winLine;
            for (var i = 0, len = this.winLines.length; i < len; i++) {
                if (this.winLines[i].id === id) {
                    winLine = this.winLines[i];
                }
            }
            if (!winLine || winLine == null)
                return;
            winLine.alpha = 1;
        };
        return DrawLine;
    })();
    engine.DrawLine = DrawLine;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var SetWheelController = (function (_super) {
        __extends(SetWheelController, _super);
        function SetWheelController(manager, view) {
            _super.call(this, manager);
            this.view = view;
        }
        SetWheelController.prototype.init = function () {
            var _this = this;
            _super.prototype.init.call(this);
            this.view.init();
            this.view.visible = false;
            var sendBtn = this.view.getSendBtn();
            sendBtn.onclick = function () {
                _this.send(engine.NotificationList.SERVER_SET_WHEEL, _this.view.getTextValue());
            };
        };
        SetWheelController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.KEYBOARD_CLICK);
            return notifications;
        };
        SetWheelController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.KEYBOARD_CLICK:
                    {
                        if (data == engine.Keyboard.BACK_QUOTE) {
                            this.view.setVisible(!this.view.getVisible());
                        }
                        break;
                    }
            }
        };
        return SetWheelController;
    })(engine.BaseController);
    engine.SetWheelController = SetWheelController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Shape = createjs.Shape;
    var ModalWindowController = (function (_super) {
        __extends(ModalWindowController, _super);
        function ModalWindowController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            this.isCreated = false;
        }
        ModalWindowController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.create();
        };
        ModalWindowController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_ERRORS);
            return notifications;
        };
        ModalWindowController.prototype.initModal = function (info) {
            this.view = this.common.layouts[this.modalWindowName] || this.common.layouts[engine.ConnectionView.LAYOUT_NAME];
            console.log("this.modalWindowName", this.modalWindowName);
            this.view.create();
            this.overlay = new Container();
            var maskView = new Container();
            this.overlay.addChild(maskView);
            this.overlay.addChild(this.view);
            this.container.addChild(this.overlay);
            var stage = this.container.getStage();
            var canvas = stage.canvas;
            var leftX = -(canvas.width - stage.x) / stage.scaleX;
            var top = -(canvas.height - stage.y) / stage.scaleY;
            var mask = new Shape();
            var bounds = this.view.getBounds();
            mask.graphics.beginFill("0").drawRect(leftX, top, bounds.width * 10, bounds.height * 5);
            mask.alpha = 0.7;
            maskView.addChild(mask);
            this.initHandlers();
            if (info)
                this.view.setText(info);
        };
        ModalWindowController.prototype.initHandlers = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
                this.view.changeButtonState(buttonsNames[i], true, true);
            }
        };
        ModalWindowController.prototype.disposeModal = function () {
            this.removeHandlers();
            this.view = null;
            this.overlay = null;
            this.container.removeAllChildren();
        };
        ModalWindowController.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        };
        ModalWindowController.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.ErrorView.OK_BTN:
                    {
                        //process error closing
                        this.send(engine.NotificationList.OK_BTN_ERROR_CLICKED);
                        break;
                    }
            }
            this.disposeModal();
        };
        ModalWindowController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_ERRORS:
                    {
                        this.modalWindowName = engine.ErrorView.LAYOUT_NAME;
                        this.initModal(data);
                        break;
                    }
            }
        };
        // create modal window
        ModalWindowController.prototype.create = function () {
            this.isCreated = true;
        };
        ModalWindowController.prototype.onGotResponse = function (data) {
        };
        return ModalWindowController;
    })(engine.BaseController);
    engine.ModalWindowController = ModalWindowController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BaseBonusController = (function (_super) {
        __extends(BaseBonusController, _super);
        function BaseBonusController(manager, common, container, startStep, isAutoRequest) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            if (this.common.server.bonus.data == null) {
                this.common.server.bonus.step = startStep;
            }
            this.isCreated = false;
            this.isAutoRequest = isAutoRequest;
        }
        BaseBonusController.prototype.init = function () {
            _super.prototype.init.call(this);
            var bonusData = this.common.server.bonus.data;
            if (bonusData == null && this.isAutoRequest) {
                this.sendRequest();
            }
            else {
                this.create();
                if (bonusData != null) {
                    this.onGotResponse(bonusData);
                }
            }
        };
        BaseBonusController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SERVER_GOT_BONUS);
            //notifications.push(NotificationList.START_BONUS);
            return notifications;
        };
        BaseBonusController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SERVER_GOT_BONUS:
                    {
                        if (!this.isCreated) {
                            this.create();
                        }
                        this.onGotResponse(data);
                        break;
                    }
            }
        };
        // create bonus game
        BaseBonusController.prototype.create = function () {
            this.isCreated = true;
        };
        BaseBonusController.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            //this.send(NotificationList.SERVER_SEND_BONUS, {"param": param, "step": ++this.common.server.bonus.step});
            this.send(engine.NotificationList.SERVER_SEND_BONUS);
        };
        BaseBonusController.prototype.onGotResponse = function (data) {
        };
        return BaseBonusController;
    })(engine.BaseController);
    engine.BaseBonusController = BaseBonusController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var GambleController = (function (_super) {
        __extends(GambleController, _super);
        function GambleController(manager, common, container) {
            // TODO: нужно чтобы на сервере был стандартный step=0
            _super.call(this, manager, common, container, -1, true);
            GambleController.SHOW_MESSAGE_TIME = engine.Utils.float2int(GambleController.SHOW_MESSAGE_TIME * Ticker.getFPS());
        }
        GambleController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.isFist = true;
        };
        GambleController.prototype.create = function () {
            _super.prototype.create.call(this);
            this.view = this.common.layouts[engine.GambleView.LAYOUT_NAME];
            this.view.create();
            this.view.alpha = 1;
            this.container.addChild(this.view);
            this.initHandlers();
        };
        GambleController.prototype.onGotResponse = function (data) {
            var _this = this;
            this.parseResponse(data);
            // show card
            if (!this.isFist) {
                this.view.showCard(this.history.pop());
            }
            else {
                this.view.hideCard();
            }
            this.view.showHistory(this.history);
            // set message
            if (this.isFist) {
                this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
                this.lockBonus(false);
            }
            else {
                if (this.bank == 0) {
                    this.view.setMessageText(GambleController.MESSAGE_LOSE);
                    // remove bonus
                    this.removeHandlers();
                    this.view.hideBonus(true, function () {
                        _this.send(engine.NotificationList.END_BONUS);
                    });
                }
                else {
                    this.view.setMessageText(GambleController.MESSAGE_WIN + this.bank);
                    Tween.get(this.view, { useTicks: true }).wait(GambleController.SHOW_MESSAGE_TIME).call(function () {
                        _this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
                        _this.lockBonus(false);
                    });
                }
            }
            this.view.setBankValue(this.bank);
            this.view.setDoubleValue(this.bank * 2);
            if (this.isFist) {
                this.isFist = false;
            }
        };
        GambleController.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            _super.prototype.sendRequest.call(this, param);
            this.lockBonus(true);
        };
        GambleController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.container.removeChild(this.view);
            this.view = null;
        };
        GambleController.prototype.parseResponse = function (xml) {
            this.bank = Math.abs(parseFloat(engine.XMLUtils.getElement(xml, "win").textContent));
            var data = engine.XMLUtils.getElement(xml, "data");
            this.history = [];
            if (data != null) {
                var wheels = engine.XMLUtils.getElement(data, "wheels");
                var items = wheels.children;
                var item;
                var suit;
                var cardId;
                var frameId;
                for (var i = items.length - 1; i >= 0; i--) {
                    item = items[i];
                    //TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
                    // on server suit from 1 to 4
                    suit = parseInt(item.attributes.suit.value) - 1;
                    // on server card id from 2 to 14
                    cardId = parseInt(item.textContent) - 2;
                    frameId = cardId + (GambleController.CARDS_COUNT * suit);
                    this.history.push(frameId);
                }
            }
        };
        GambleController.prototype.initHandlers = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        GambleController.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        };
        GambleController.prototype.onBtnClick = function (buttonName) {
            var _this = this;
            switch (buttonName) {
                case engine.GambleView.COLLECT_BTN:
                    {
                        // remove bonus
                        this.removeHandlers();
                        this.view.hideBonus(false, function () {
                            _this.send(engine.NotificationList.END_BONUS);
                        });
                        break;
                    }
                case engine.GambleView.RED_BTN:
                    {
                        this.sendRequest(1);
                        break;
                    }
                case engine.GambleView.BLACK_BTN:
                    {
                        this.sendRequest(2);
                        break;
                    }
            }
        };
        GambleController.prototype.lockBonus = function (value) {
            if (this.view != null) {
                var buttonsNames = this.view.buttonsNames;
                var buttonsCount = buttonsNames.length;
                for (var i = 0; i < buttonsCount; i++) {
                    this.view.getButton(buttonsNames[i]).setEnable(!value);
                }
            }
        };
        GambleController.SHOW_MESSAGE_TIME = 2;
        GambleController.CARDS_COUNT = 13;
        GambleController.MESSAGE_DEFAULT = "CHOOSE RED OR BLACK";
        GambleController.MESSAGE_WIN = "YOU WIN: ";
        GambleController.MESSAGE_LOSE = "DEALER WINS";
        return GambleController;
    })(engine.BaseBonusController);
    engine.GambleController = GambleController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var GambleController5 = (function (_super) {
        __extends(GambleController5, _super);
        function GambleController5(manager, common, container) {
            // TODO: нужно чтобы на сервере был стандартный step=0
            _super.call(this, manager, common, container, -1, true);
            GambleController5.SHOW_MESSAGE_TIME = engine.Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
        }
        GambleController5.prototype.init = function () {
            _super.prototype.init.call(this);
            this.isFist = true;
        };
        GambleController5.prototype.create = function () {
            _super.prototype.create.call(this);
            this.view = this.common.layouts[engine.GambleView5.LAYOUT_NAME];
            this.view.create();
            this.view.alpha = 1;
            this.totalbank = 0;
            this.container.addChild(this.view);
            this.initHandlers();
        };
        GambleController5.prototype.resetGame = function () {
            var _this = this;
            console.log('resetGame');
            this.view.setMessageText(GambleController5.MESSAGE_DEFAULT);
            this.lockBonus(false);
            this.stepBonus = 1;
            var cardName;
            GambleController5.SHOW_MESSAGE_TIME = engine.Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
            for (var n = 1; n < 6; n++) {
                cardName = 'card_' + n;
                this.view.getButton(cardName).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onCardCoverClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        GambleController5.prototype.onGotResponse = function (data) {
            this.parseResponse(data);
            // show card
            if (!this.isFist) {
                this.view.showCard(this.history.shift());
            }
            if (this.isFist) {
                this.resetGame();
            }
            else {
                if (this.stepBonus == 3) {
                    this.view.showHistory(this.history);
                    this.setBankMessage();
                }
            }
            this.view.setBankValue(this.totalbank);
            this.view.setDoubleValue(this.common.server.bet * 2);
            this.view.setHalfDoubleValue(this.common.server.bet * 1.5);
            if (this.isFist) {
                this.isFist = false;
            }
        };
        GambleController5.prototype.setBankMessage = function () {
            var _this = this;
            if (this.bank == 0) {
                this.view.setMessageText(GambleController5.MESSAGE_LOSE);
                // remove bonus
                this.removeHandlers();
                this.view.hideCard();
                this.view.hideBonus(true, function () {
                    _this.send(engine.NotificationList.END_BONUS);
                });
            }
            else {
                this.view.setMessageText(GambleController5.MESSAGE_WIN + this.totalbank);
                this.view.hideCard();
                this.common.server.bonus.step = 0;
                var self = this;
                Tween.get(this.view, { useTicks: true }).wait(GambleController5.SHOW_MESSAGE_TIME).call(function () {
                    self.resetGame();
                    //if one more
                    _this.view.setMessageText(GambleController5.MESSAGE_DEFAULT);
                });
            }
        };
        GambleController5.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            _super.prototype.sendRequest.call(this, param);
            this.lockBonus(true);
        };
        GambleController5.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.container.removeChild(this.view);
            this.view = null;
        };
        GambleController5.prototype.parseResponse = function (xml) {
            var data = engine.XMLUtils.getElement(xml, "data");
            this.history = [];
            this.bank = 0;
            if (data != null) {
                var wheels = engine.XMLUtils.getElement(data, "wheels");
                this.bank = engine.XMLUtils.getAttributeFloat(wheels, "win") || 0;
                this.totalbank = this.totalbank + this.bank;
                var items = wheels.children;
                var item;
                for (var i = items.length - 1; i >= 0; i--) {
                    item = items[i];
                    //TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
                    // on server suit from 1 to 4
                    console.log(item.attributes.suit.value, item.textContent);
                    var suit = parseInt(item.attributes.suit.value);
                    // on server card id from 2 to 14
                    var cardId = parseInt(item.textContent) - 1;
                    var frameId = cardId + (GambleController5.CARDS_COUNT * suit);
                    console.log(frameId);
                    this.history.unshift(frameId);
                }
            }
        };
        GambleController5.prototype.initHandlers = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        GambleController5.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        };
        GambleController5.prototype.onCardCoverClick = function (buttonName) {
            if (this.stepBonus == 2) {
                var name = buttonName.split('_');
                var indexCard = parseInt(name[1]);
                this.sendRequest(indexCard - 1);
                this.stepBonus++;
            }
        };
        GambleController5.prototype.onBtnClick = function (buttonName) {
            var _this = this;
            this.stepBonus++;
            switch (buttonName) {
                case engine.GambleView5.COLLECT_BTN:
                    {
                        // remove bonus
                        this.removeHandlers();
                        this.view.hideBonus(false, function () {
                            _this.send(engine.NotificationList.END_BONUS);
                        });
                        break;
                    }
                case engine.GambleView5.HALF_BTN:
                    {
                        this.sendRequest(1);
                        this.view.setMessageText(GambleController5.MESSAGE_DEFAULT_SECOND);
                        break;
                    }
                case engine.GambleView5.DOUBLE_BTN:
                    {
                        this.sendRequest(2);
                        this.view.setMessageText(GambleController5.MESSAGE_DEFAULT_SECOND);
                        break;
                    }
            }
        };
        GambleController5.prototype.lockBonus = function (value) {
            if (this.view != null) {
                var buttonsNames = this.view.buttonsNames;
                var buttonsCount = buttonsNames.length;
                for (var i = 0; i < buttonsCount; i++) {
                    this.view.getButton(buttonsNames[i]).setEnable(!value);
                }
            }
        };
        GambleController5.SHOW_MESSAGE_TIME = 2;
        GambleController5.SHOW_MESSAGE_TIME_DEFAULT = 2;
        GambleController5.CARDS_COUNT = 13;
        GambleController5.MESSAGE_DEFAULT = "CHOOSE DOUBLE OR DOUBLE HALF";
        GambleController5.MESSAGE_DEFAULT_SECOND = "PICK A HIGHER CARD";
        GambleController5.MESSAGE_WIN = "YOU WIN: ";
        GambleController5.MESSAGE_LOSE = "DEALER WINS";
        return GambleController5;
    })(engine.BaseBonusController);
    engine.GambleController5 = GambleController5;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Ticker = createjs.Ticker;
    var Sound = createjs.Sound;
    var Point = createjs.Point;
    var Shape = createjs.Shape;
    var FreeSpinsController = (function (_super) {
        __extends(FreeSpinsController, _super);
        function FreeSpinsController(manager, common, container) {
            _super.call(this, manager, common, container, 0, true);
            this.endBonusGameStarted = false;
            this.totalWin = 0;
            this.resetBonusFields();
        }
        FreeSpinsController.prototype.init = function () {
            _super.prototype.init.call(this);
            FreeSpinsController.UPDATE_WIN_TIME = engine.Utils.float2int(FreeSpinsController.UPDATE_WIN_TIME * Ticker.getFPS());
            this.resetBonusFields();
        };
        FreeSpinsController.prototype.create = function () {
            var _this = this;
            _super.prototype.create.call(this);
            this.resetBonusFields();
            this.send(engine.NotificationList.REMOVE_HEADER);
            this.view = this.common.layouts[engine.WeelView.LAYOUT_NAME];
            this.view.create();
            this.bonusContainer = new Container();
            this.modalContainer = new Container();
            var symbolsRect = this.common.symbolsRect;
            var rect = symbolsRect[0][0];
            var mask = new Shape();
            var x = this.view.x;
            var y = rect.y + 3;
            var h = y + (rect.height * 3);
            var w = x + (rect.width * 5) + 10;
            mask.graphics.beginFill("0").drawRect(x, y, w, h);
            this.view.mask = mask;
            //this.endBonusGame = false;
            //this.view.visible = false;
            this.isFist = true;
            this.view.addChild(this.bonusContainer);
            //this.container.addChild(this.view);
            this.view.on(engine.NotificationList.EVENT_All_BONUSES_SHOWED, function (eventObj) {
                _this.view.animationParser(_this.common.config.sectorsAnimation);
            });
            this.view.on(engine.NotificationList.SHOW_START_BONUS_MODAL, function (eventObj) {
                _this.showStartPop();
            });
            this.view.on(engine.NotificationList.START_WEEL_ROTATE, function (eventObj) {
                _this.send(engine.NotificationList.START_WEEL_ROTATE);
            });
            this.view.on(engine.NotificationList.STOP_WEEL_ROTATE, function (eventObj) {
                _this.send(engine.NotificationList.STOP_WEEL_ROTATE);
            });
            console.log("create freespinscontroller");
        };
        FreeSpinsController.prototype.removeBonusAnimation = function () {
            this.view.removeChild(this.bonusContainer);
        };
        FreeSpinsController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.START_BONUS);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            notifications.push(engine.NotificationList.EVENT_All_BONUSES_SHOWED);
            notifications.push(engine.NotificationList.END_FREE_SPINS);
            notifications.push(engine.NotificationList.WIN_POPUP_SHOWED);
            notifications.push(engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
            notifications.push(engine.NotificationList.FREESPINS_SHOW_LEFT_FIELDS);
            return notifications;
        };
        FreeSpinsController.prototype.handleNotification = function (message, data) {
            _super.prototype.handleNotification.call(this, message, data);
            switch (message) {
                case engine.NotificationList.EVENT_All_BONUSES_SHOWED:
                    {
                        this.removeBonusAnimation();
                    }
                case engine.NotificationList.START_BONUS:
                    {
                        this.isBonusStarted = true;
                        //this.view.setLeftSpins(this.leftSpins);
                        this.tryStartSpin();
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        this.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        this.send(engine.NotificationList.SHOW_WIN_TF);
                        break;
                    }
                case engine.NotificationList.END_FREE_SPINS:
                    {
                        this.resetBonus();
                        this.endBonusGame = true;
                        this.totalWin = data;
                        break;
                    }
                case engine.NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED:
                    {
                        console.log("this.isFist=" + this.isFist, "WIN_LINES_AND_WIN_POPUP_SHOWED");
                        this.send(engine.NotificationList.SHOW_WIN_TF, 0);
                        if (this.isFist) {
                            this.container.addChild(this.view);
                            var sectorNum = this.common.server.bonus.data['sector'];
                            this.view.setBonus(sectorNum);
                            this.showStartAnimation();
                            this.isFist = false;
                        }
                        else {
                            this.tryStartSpin();
                        }
                        break;
                    }
                case engine.NotificationList.FREESPINS_SHOW_LEFT_FIELDS:
                    {
                        this.send(engine.NotificationList.FREESPINS_SHOW_LEFT, this.common.server.bonus.step);
                        break;
                    }
            }
        };
        FreeSpinsController.prototype.tryStartSpin = function () {
            if (this.isBonusStarted) {
                console.log(this.common.server.bonus, "tryStartSpin");
                this.send(engine.NotificationList.FREESPINS_SHOW_LEFT, this.common.server.bonus.step);
                this.send(engine.NotificationList.REMOVE_WIN_LINES);
                this.send(engine.NotificationList.START_SPIN);
                this.sendRequest();
            }
            if (this.endBonusGame && !this.endBonusGameStarted) {
                this.showResultPopup(this.totalWin);
            }
        };
        FreeSpinsController.prototype.resetBonus = function () {
            this.isBonusStarted = false;
        };
        FreeSpinsController.prototype.resetBonusFields = function () {
            this.endBonusGame = false;
            this.isBonusStarted = false;
        };
        FreeSpinsController.prototype.onGotResponse = function (data) {
            this.parseResponse(data);
            if (this.isFist) {
            }
            else {
                this.send(engine.NotificationList.SERVER_GOT_SPIN);
            }
        };
        FreeSpinsController.prototype.onEnterFrame = function () {
            if (this.container == null)
                return;
            this.container.tickChildren = false;
            var speedControl = Ticker.getTicks() % 2;
            if (speedControl == 0) {
                this.container.tickChildren = true;
                if (this.view != null) {
                    this.view.onEnterFrame();
                }
            }
        };
        FreeSpinsController.prototype.onBtnClick = function (buttonName) {
            console.log(buttonName, "onBtnClick");
            switch (buttonName) {
                case engine.FreeSpinsStartView.START_BTN:
                    {
                        this.removeStartPopup();
                        break;
                    }
            }
            //HudController.playBtnSound(buttonName);
        };
        FreeSpinsController.prototype.showStartAnimation = function () {
            this.send(engine.NotificationList.SCATTER_WIN);
            var wheel = this.common.server.scatters;
            var symbolsRect = this.common.symbolsRect;
            var bonusCount = wheel.length;
            var allWinAniVOs = [];
            var winAniVOs = [];
            for (var i = 0; i < bonusCount; i++) {
                var bonusVO = wheel[i];
                var reel = bonusVO['reel'];
                var row = bonusVO['row'];
                var rect = symbolsRect[reel][row];
                var posIdx = new Point(reel, row);
                var winSymbolView = this.createBonusSymbol();
                var winAniVO = new engine.WinAniVO();
                winAniVO.winSymbolView = winSymbolView;
                winAniVO.rect = rect.clone();
                winAniVO.rect.x -= 10;
                winAniVO.rect.y -= 10;
                winAniVO.posIdx = posIdx;
                allWinAniVOs.push(winAniVO);
                winAniVOs.push(winAniVO);
            }
            this.view.addBonusAnimation(winAniVOs, this.common.isMobile);
        };
        FreeSpinsController.prototype.showStartPop = function () {
            var _this = this;
            var startPopup = this.common.layouts[engine.FreeSpinsStartView.LAYOUT_NAME];
            startPopup.create();
            startPopup.alpha = 1;
            var bonusNum = this.common.server.bonus.data['sector'];
            startPopup.showBonusTitle(bonusNum);
            this.container.addChild(startPopup);
            var buttonsNames = startPopup.buttonsNames;
            for (var i = 0; i < buttonsNames.length; i++) {
                var buttonName = buttonsNames[i];
                var button = startPopup.getBtn(buttonName);
                if (button == null) {
                    continue;
                }
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        button.removeAllEventListeners("click");
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        FreeSpinsController.prototype.removeStartPopup = function () {
            var _this = this;
            var startPopup = this.common.layouts[engine.FreeSpinsStartView.LAYOUT_NAME];
            startPopup.hide(function () {
                _this.container.removeChild(startPopup);
                _this.container.removeChild(_this.view);
                _this.send(engine.NotificationList.START_BONUS);
            });
        };
        FreeSpinsController.prototype.createBonusSymbol = function () {
            var regularAni = this.common.layouts['WinAnimation_2'];
            var winSymbolView = new engine.WinSymbolView();
            winSymbolView.create(regularAni, null);
            return winSymbolView;
        };
        FreeSpinsController.prototype.showResultPopup = function (win) {
            var _this = this;
            console.log("showResultPopup", win);
            this.endBonusGameStarted = true;
            var resultPopup = this.common.layouts[engine.FreeSpinsResultView.LAYOUT_NAME];
            resultPopup.create();
            resultPopup.alpha = 1;
            var collectBtn = resultPopup.getCollectBtn();
            collectBtn.on("click", function (eventObj) {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    console.log("collectBtn clicked");
                    collectBtn.removeAllEventListeners("click");
                    eventObj.removeAllEventListeners("click");
                    _this.hideResultPopup();
                }
            });
            this.container.addChild(resultPopup);
            if (win > 0) {
                resultPopup.setTotalWin(win);
            }
            else {
                //resultPopup.setTotalWin(win);
                resultPopup.setFreeSpinsMessage(FreeSpinsController._FS_TITLE_1 + " " + FreeSpinsController._FS_TITLE_2, ""); //show message freespins finished
            }
        };
        FreeSpinsController.prototype.hideResultPopup = function () {
            var _this = this;
            var resultView = this.common.layouts[engine.FreeSpinsResultView.LAYOUT_NAME];
            resultView.hide(function () {
                Sound.play(engine.SoundsList.COLLECT);
                _this.send(engine.NotificationList.END_BONUS);
                _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                //this.send(NotificationList.SHOW_WIN_TF);
                _this.container.removeChild(resultView);
            });
        };
        FreeSpinsController.prototype.parseResponse = function (xml) {
            //this.count = XMLUtils.getAttributeInt(xml, "all");
            //this.leftSpins = XMLUtils.getAttributeInt(xml, "counter");
            this.leftSpins = this.common.server.bonus.data.step;
            //this.multiplicator = XMLUtils.getAttributeInt(xml, "mp");
            //this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin");
            //this.isFist = this.leftSpins == this.count - 1;
            //var data:any = XMLUtils.getElement(xml, "data");
            //this.featureWin = XMLUtils.getAttributeFloat(data, "summ");
            //this.parseSpinData(data);
        };
        FreeSpinsController.prototype.parseSpinData = function (xml) {
            //var spinData:any = XMLUtils.getElement(xml, "spin");
            //var wheelsData:any = XMLUtils.getElement(spinData, "wheels").childNodes;
            //for (var i = 0; i < wheelsData.length; i++) {
            //	var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
            //	for (var j:number = 0; j < wheelData.length; j++) {
            //		this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
            //	}
            //}
            //var winLinesData:any = XMLUtils.getElement(spinData, "winposition").childNodes;
            //this.common.server.winLines = [];
            //for (var i:number = 0; i < winLinesData.length; i++) {
            //	var winLine:any = winLinesData[i];
            //	var winLineVO:WinLineVO = new WinLineVO();
            //	winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
            //	winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
            //	winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
            //	this.common.server.winLines.push(winLineVO);
            //}
            ////TODO: server return 0;
            ////this.common.server.setBalance(parseFloat(XMLUtils.getElement(spinData, "balance").textContent));
            //this.common.server.win = parseFloat(XMLUtils.getElement(spinData, "win").textContent);
        };
        FreeSpinsController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            //this.view.dispose();
            this.view.removeAllEventListeners(engine.NotificationList.EVENT_All_BONUSES_SHOWED);
            this.view.removeAllEventListeners(engine.NotificationList.SHOW_START_BONUS_MODAL);
            this.view.removeAllEventListeners(engine.NotificationList.START_WEEL_ROTATE);
            this.view.removeAllEventListeners(engine.NotificationList.STOP_WEEL_ROTATE);
        };
        FreeSpinsController.UPDATE_WIN_TIME = 0.5;
        FreeSpinsController._FS_TITLE_1 = "You have completed";
        FreeSpinsController._FS_TITLE_2 = "your Free Spins";
        return FreeSpinsController;
    })(engine.BaseBonusController);
    engine.FreeSpinsController = FreeSpinsController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var SelectItemController = (function (_super) {
        __extends(SelectItemController, _super);
        function SelectItemController(manager, common, container) {
            _super.call(this, manager, common, container, 0, false);
        }
        SelectItemController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        SelectItemController.prototype.create = function () {
            _super.prototype.create.call(this);
            this.view = this.common.layouts[engine.SelectItemView.LAYOUT_NAME];
            this.view.create();
            this.view.showBonus();
            this.view.hideAllWinAnimation();
            this.container.addChild(this.view);
            var itemCount = this.view.getItemCount();
            this.itemsVO = new Array(itemCount);
            for (var i = 0; i < itemCount; i++) {
                this.itemsVO[i] = new engine.ItemVO(i);
            }
            this.setEnableAllItem(true);
            this.initHandlers();
        };
        SelectItemController.prototype.initHandlers = function () {
            var _this = this;
            var items = this.view.getItems();
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                item.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        var currentItem = eventObj.currentTarget;
                        var itemId = engine.SelectItemView.getItemIdByName(currentItem.name);
                        _this.sendRequest(itemId);
                    }
                });
            }
        };
        SelectItemController.prototype.removeHandlers = function () {
            var items = this.view.getItems();
            for (var i = 0; i < items.length; i++) {
                items[i].removeAllEventListeners("click");
            }
            this.view.getCollectBtn().removeAllEventListeners("click");
        };
        SelectItemController.prototype.showWinAnimation = function (vo) {
            var _this = this;
            this.view.showWinAnimation(vo, function () {
                if (_this.isFinish) {
                    _this.view.setTotalWin(_this.calculateTotalWin());
                    _this.view.showWinPopup();
                    _this.view.getCollectBtn().on("click", function (eventObj) {
                        if (eventObj.nativeEvent instanceof MouseEvent) {
                            _this.view.getCollectBtn().removeAllEventListeners("click");
                            _this.view.hideWinPopup(function () {
                                _this.send(engine.NotificationList.END_BONUS);
                            });
                        }
                    });
                }
                else {
                    _this.setEnableAllItem(true);
                }
            });
        };
        SelectItemController.prototype.setEnableAllItem = function (value) {
            var items = this.view.getItems();
            for (var i = 0; i < this.itemsVO.length; i++) {
                if (!this.itemsVO[i].isSelected) {
                    items[i].setEnable(value);
                }
            }
        };
        SelectItemController.prototype.calculateTotalWin = function () {
            var result = 0;
            for (var i = 0; i < this.itemsVO.length; i++) {
                var itemVO = this.itemsVO[i];
                if (itemVO.isSelected) {
                    result += itemVO.winValue;
                }
            }
            return result;
        };
        SelectItemController.prototype.onGotResponse = function (xml) {
            var data = engine.XMLUtils.getElement(xml, "data");
            var wheels = engine.XMLUtils.getElement(data, "wheels");
            var items = engine.XMLUtils.getElements(wheels, "item");
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var id = engine.XMLUtils.getAttributeInt(item, "id") - 1;
                var vo = this.itemsVO[id];
                if (!vo.isSelected) {
                    vo.isSelected = engine.XMLUtils.getAttributeInt(item, "selected") == 1;
                    vo.winValue = parseFloat(item.textContent);
                    if (vo.isSelected) {
                        this.showWinAnimation(vo);
                    }
                }
            }
            this.isFinish = engine.XMLUtils.getAttributeInt(wheels, "finish") == 1;
        };
        SelectItemController.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            _super.prototype.sendRequest.call(this, param);
            this.setEnableAllItem(false);
        };
        SelectItemController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.container.removeChild(this.view);
            this.removeHandlers();
            this.view = null;
            this.itemsVO = null;
        };
        return SelectItemController;
    })(engine.BaseBonusController);
    engine.SelectItemController = SelectItemController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ErrorController = (function (_super) {
        __extends(ErrorController, _super);
        function ErrorController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        ErrorController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        ErrorController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_ERRORS);
            return notifications;
        };
        ErrorController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_ERRORS:
                    {
                        break;
                    }
            }
        };
        ErrorController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
            this.container = null;
        };
        ErrorController.parseXMLErrors = function (xml) {
            var errors = [];
            var xml_error = engine.XMLUtils.getElement(xml, "error");
            if (xml_error) {
                errors.push(xml_error.textContent);
            }
            return errors;
        };
        ErrorController.ERROR_NO_MONEY_STR = "NO MONEY. PLEASE, CHECK YOUR BALANCE.";
        return ErrorController;
    })(engine.BaseController);
    engine.ErrorController = ErrorController;
})(engine || (engine = {}));
///<reference path="../../dts/createjs.d.ts" />
///<reference path="../../dts/cryptojs.d.ts" />
///<reference path="../../dts/easeljs.d.ts" />
///<reference path="../../dts/layoutjs.d.ts" />
///<reference path="../../dts/preloadjs.d.ts" />
///<reference path="../../dts/soundjs.d.ts" />
///<reference path="../../dts/socket.d.ts" />
//# sourceMappingURL=slots-engine.js.map