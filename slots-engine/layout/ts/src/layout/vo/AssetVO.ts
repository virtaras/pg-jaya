module layout {
	export class AssetVO {
		public id:string;
		public src:string;
		public type:string;

		constructor(id:string, src:string, type:string = "") {
			this.id = id;
			this.src = src;
			this.type = type;
		}
	}
}
