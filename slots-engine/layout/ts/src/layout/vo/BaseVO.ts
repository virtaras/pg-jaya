module layout {

	export class BaseVO {
		public className:string;
		public name:string;
		public x:number;
		public y:number;

		constructor() {
		}

		public static parse(data:any):BaseVO {
			var vo:BaseVO;
			switch (data.className) {
				case ButtonVO.CLASS_NAME:
				{
					vo = new ButtonVO();
					break;
				}
				case ImageVO.CLASS_NAME:
				{
					vo = new ImageVO();
					break;
				}
				case SimpleMovieVO.CLASS_NAME:
				{
					vo = new SimpleMovieVO();
					break;
				}
				case SpriteVO.CLASS_NAME:
				{
					vo = new SpriteVO();
					break;
				}
				case TextVO.CLASS_NAME:
				{
					vo = new TextVO();
					break;
				}
			}
			vo.read(data);
			return vo;
		}

		public read(data:any):void {
			this.className = data.className;
			this.name = data.name;
			this.x = data.x;
			this.y = data.y;
		}
	}
}
