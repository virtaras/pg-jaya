module layout {

	export class BaseFilterVO {
		public className:string;

		constructor() {
		}

		public static parse(data:any):BaseFilterVO {
			var vo:BaseFilterVO;
			switch (data.className) {
				case BlurFilterVO.CLASS_NAME:
				{
					vo = new BlurFilterVO();
					break;
				}
				case GlowFilterVO.CLASS_NAME:
				{
					vo = new GlowFilterVO();
					break;
				}
				case DropShadowFilterVO.CLASS_NAME:
				{
					vo = new DropShadowFilterVO();
					break;
				}
				default:
				{
					throw new Error("ERROR: This type is not support: " + data.className);
				}
			}
			vo.read(data);
			return vo;
		}

		public read(data:any):void {
			this.className = data.className;
		}
	}
}
