module layout {

	export class SpriteVO extends BaseVO {
		public static CLASS_NAME:string = "s";

		public displays:Array<BaseVO>;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			this.readDisplays(data.displays);
		}

		private readDisplays(data:any):void {
			if (data != null) {
				this.displays = new Array(data.length);
				for (var i:number = 0; i < data.length; i++) {
					this.displays[i] = BaseVO.parse(data[i]);
				}
			}
		}
	}
}
