module layout {

	export class ButtonStateVO {
		public imageVO:ImageVO;
		public name:string;

		constructor() {
		}

		public read(data:any):void {
			this.name = data.name;
			this.imageVO = new ImageVO();
			this.imageVO.read(data.imageVO);
		}
	}
}
