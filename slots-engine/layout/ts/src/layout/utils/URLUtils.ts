module layout {
	export class URLUtils {
		public static getFolderUrl(url:string):string {
			var start:number = url.lastIndexOf("/");
			if (start != -1) {
				return url.substring(0, start);
			}
			return "";
		}
	}
}
