module layout {
	export class FileConst {
		public static PNG_EXTENSION:string = ".png";
		public static JPEG_EXTENSION:string = ".jpeg";
		public static JPEGXR_EXTENSION:string = ".jpeg";
		public static WEBP_EXTENSION:string = ".webp";
		public static XML_EXTENSION:string = ".xml";
		public static MAIN_RES_FOLDER:string = "assets/";
		public static PORT_RES_FOLDER:string = "p/";
	}
}
