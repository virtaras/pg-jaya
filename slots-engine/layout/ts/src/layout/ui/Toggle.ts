module layout {
	import Bitmap = createjs.Bitmap;

	export class Toggle extends Bitmap {
		private static STATE_ON:string = "on";
		private static STATE_OFF:string = "off";
		private static STATE_DISABLE:string = "dis";
		private static STATE_ON_OVER:string = "on_over";
		private static STATE_OFF_OVER:string = "off_over";

		private textures:any;
		private state:string;
		private isOn:boolean;

		constructor(textures:any) {
			this.state = Toggle.STATE_ON;
			this.isOn = true;
			this.textures = textures;
			var image:any = this.textures[this.state];
			super(image);

			this.on("click", (eventObj:any)=> {
				if (eventObj.nativeEvent instanceof MouseEvent) {
					this.isOn = !this.isOn;
					this.onChangeState(this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
				}
			});
			this.on("rollover", ()=> {
				this.onChangeState(this.isOn ? Toggle.STATE_ON_OVER : Toggle.STATE_OFF_OVER);
				document.body.style.cursor = "pointer";
			});
			this.on("rollout", ()=> {
				this.onChangeState(this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
				document.body.style.cursor = "default";
			});
		}

		private onChangeState(state:string):void {
			if (this.state != Toggle.STATE_DISABLE) {
				this.changeState(state);
			}
		}

		private changeState(state:string):void {
			if (this.state != state) {
				this.state = state;
				var image:any = this.textures[state];
				if (image != null) {
					this.image = image;
				}
			}
		}

		public getIsOn():boolean {
			return this.isOn;
		}

		public setIsOn(value:boolean):void {
			this.isOn = value;
			this.onChangeState(this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
		}

		public getEnable():boolean {
			return this.state != Toggle.STATE_DISABLE;
		}

		public setEnable(value:boolean):void {
			if (value) {
				this.changeState(this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
			}
			else {
				this.changeState(Toggle.STATE_DISABLE);
			}
			this.mouseEnabled = value;
		}
	}
}