/**
 * Created by Taras on 16.01.2015.
 */

declare module io {

    export function connect(url: string): Socket;

    interface Socket {
        on(event: string, callback: (data: any) => void );
        emit(event: string, data: any);
    }
}

