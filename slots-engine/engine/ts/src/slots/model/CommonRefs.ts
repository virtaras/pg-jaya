module engine {
	import Point = createjs.Point;
	import Rectangle = createjs.Rectangle;

	export class CommonRefs {
		public config:any;
		public layouts:Object;
		public server:ServerData;
		public symbolsRect:Array<Array<Rectangle>>;
		public gameName:string;
		public key:string;
		public login:string;
		public password:string;
		public room:string;
		public artWidth:number;
		public artHeight:number;
		public isDemo:boolean;
		public isDebug:boolean;
		public isMobile:boolean;
		public isSoundLoaded:boolean;
		public isSoundOn:boolean;
		public jayaServer:boolean;
		public rotateHudMode:string;
		public is_win_popup_showed:boolean = true;
		public is_win_lines_showed:boolean = true;
		public is_lazy_load_compl:boolean = false;
		public is_layout_load_compl:boolean = false;
		public is_server_init:boolean = false;
		constructor() {
		}
	}
}