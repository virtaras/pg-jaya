module engine {
    import Layout = layout.Layout;
    import Text = createjs.Text;
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;
    import Button = layout.Button;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;

    export class AutoPlayView extends Layout {
        public static LAYOUT_NAME:string = "AutoPlayView";
        public static YES_BTN:string = "yesBtn";
        public static NO_BTN:string = "noBtn";
        public static AUTO_COUNT:string = "counter";

        public buttonsNames:Array<string> = [
            AutoPlayView.YES_BTN,
            AutoPlayView.NO_BTN
        ];

        constructor() {
            super();
        }

        public onInit():void {
            var lines:Text = <Text>this.getChildByName(AutoPlayView.AUTO_COUNT);
            if(lines){
                lines.y = lines.y + lines.getBounds().height / 1.5;
            }
        }

        public setText(count:any):void{
            var lines:Text = <Text>this.getChildByName(AutoPlayView.AUTO_COUNT);
            if(lines){
                lines.text = count.toString();
            }
        }

        public getButton(name:string):Button {
            return <Button>this.getChildByName(name);
        }

        public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
            var button:Button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        }
    }
}