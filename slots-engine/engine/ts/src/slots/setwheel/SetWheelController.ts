module engine {
	import Container = createjs.Container;

	export class SetWheelController extends BaseController {
		private view:SetWheelView;

		constructor(manager:ControllerManager, view:SetWheelView) {
			super(manager);
			this.view = view;
		}

		public init():void {
			super.init();
			this.view.init();
			this.view.visible = false;

			var sendBtn:HTMLElement = this.view.getSendBtn();
			sendBtn.onclick = ()=> {
				this.send(NotificationList.SERVER_SET_WHEEL, this.view.getTextValue());
			};
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.KEYBOARD_CLICK);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.KEYBOARD_CLICK:
				{
					if (data == Keyboard.BACK_QUOTE) {
						this.view.setVisible(!this.view.getVisible());
					}
					break;
				}
			}
		}
	}
}