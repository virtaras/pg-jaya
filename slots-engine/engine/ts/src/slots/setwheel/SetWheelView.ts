module engine {
	import Ticker = createjs.Ticker;
	import DOMElement = createjs.DOMElement;
	import Container = createjs.Container;

	export class SetWheelView extends Container {
		private height:number;
		private width:number;
		private div:HTMLElement;
		private sendBtn:HTMLElement;
		private textFields:Array<Array<HTMLInputElement>>;

		constructor(width:number, height:number) {
			super();
			this.width = width;
			this.height = height;
		}

		public init():void {
			var canvasHolder:HTMLElement = document.getElementById("canvasHolder");

			this.div = document.createElement("div");
			this.div.style.background = "#666";
			canvasHolder.appendChild(this.div);

			var table:HTMLElement = document.createElement("table");
			this.div.appendChild(table);

			this.sendBtn = document.createElement("button");
			this.sendBtn.innerHTML = "SET";
			this.div.appendChild(this.sendBtn);

			var caption:HTMLElement = document.createElement("caption");
			caption.innerHTML = "SET WHEEL:";
			table.appendChild(caption);

			var tr:HTMLElement = document.createElement("tr");
			table.appendChild(tr);

			this.textFields = new Array(this.width);
			for (var i:number = 0; i < this.width; i++) {
				var td:HTMLElement = document.createElement("td");
				tr.appendChild(td);

				var column:Array<HTMLInputElement> = new Array(this.height);
				for (var j:number = 0; j < this.height; j++) {
					var input:HTMLInputElement = document.createElement("input");
					input.type = "text";
					input.value = "-1";
					input.style.width = "40px";
					td.appendChild(input);
					column[j] = input;

					var br:HTMLElement = document.createElement("br");
					td.appendChild(br);
				}
				this.textFields[i] = column;
			}

			var element:DOMElement = new DOMElement(this.div);
			this.addChild(element);
			this.setVisible(false);
		}

		public getSendBtn():HTMLElement {
			return this.sendBtn;
		}

		public setVisible(value:boolean):void{
			if (value){
				this.div.style.display = "";
			}
			else {
				this.div.style.display = "none";
			}
		}

		public getVisible():boolean{
			return this.div.style.display != "none"
		}

		public getTextValue():Array<Array<number>> {
			var result:Array<Array<number>> = new Array(this.textFields.length);
			for (var i:number = 0; i < this.textFields.length; i++) {
				var columnText:Array<HTMLInputElement> = this.textFields[i];
				var column:Array<number> = new Array(columnText.length);
				for (var j:number = 0; j < columnText.length; j++) {
					column[j] = parseInt(columnText[j].value);
					if (isNaN(column[j])) {
						column[j] = -1;
					}
				}
				result[i] = column;
			}
			return result;
		}
	}
}