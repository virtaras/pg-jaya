module engine {
	import Layout = layout.Layout;
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import DisplayObject = createjs.DisplayObject;
	import Container = createjs.Container;

	export class FreeSpinsStartView extends Layout {
		public static LAYOUT_NAME:string = "FreeSpinsStartView";
		public static START_BTN:string = "startFreeSpinBtn";
		private static MODAL_CONTAINER:string = "startFreeSpins";
		private static BONUS_TITLE_PREFIX:string = "bonus_";

		private static INVISIBLE_TIME:number = 0.5;

		constructor() {
			super();
		}

		public onInit():void {
			FreeSpinsStartView.INVISIBLE_TIME = Utils.float2int(FreeSpinsStartView.INVISIBLE_TIME * Ticker.getFPS());

		}

		public buttonsNames:Array<string> = [
			FreeSpinsStartView.START_BTN
		];

		public getBtn(btnName:string):Button {
			var cont:Container = <Container>this.getChildByName(FreeSpinsStartView.MODAL_CONTAINER);
			return <Button>cont.getChildByName(btnName);
		}

		public showBonusTitle(index:number):void {
			var cont:Container = <Container>this.getChildByName(FreeSpinsStartView.MODAL_CONTAINER);
			var title:DisplayObject;

			var i:number = 1;
			while ((title = cont.getChildByName(FreeSpinsStartView.BONUS_TITLE_PREFIX + i)) != null) {
				title.alpha = 0;
				i++;
			}
			title = cont.getChildByName(FreeSpinsStartView.BONUS_TITLE_PREFIX + (index + 1));
			title.alpha = 1;
		}


		public hide(callback:(...params:any[]) => any):void {
			var tween:Tween = Tween.get(this, {useTicks: true});
			tween.to({alpha: 0, override: true}, FreeSpinsStartView.INVISIBLE_TIME);
			tween.call(callback);
		}
	}
}