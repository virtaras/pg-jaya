module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Layout = layout.Layout;
	import Button = layout.Button;
	import Text = createjs.Text;
	import Shadow = createjs.Shadow;
	import DisplayObject = createjs.DisplayObject;
	import Container = createjs.Container;

	export class FreeSpinsResultView extends Layout {
		public static LAYOUT_NAME:string = "FreeSpinsResultView";
		private resultFreeSpinModal:Container;
		private static INVISIBLE_TIME:number = 0.5;
        //
		private static COLLECT_BTN:string = "collectBtn";
		private static GAME_WIN:string = "winTf";
		private static GAME_TITLE_1:string = "winTitle1";
		private static GAME_TITLE_2:string = "winTitle2";
		private static DEFAULT_GAME_TITLE_1:string = "Fantastic!";
		private static DEFAULT_GAME_TITLE_2:string = "You won";
		private static MODAL_CONTAINER:string = "resultFreeSpinModal";
		private textsPos:boolean;
		//private static FEATURE_WIN:string = "featureWinTf";
		//private static TOTAL_WIN:string = "totalWinTf";

		constructor() {
			super();
		}

		public onInit():void{
			FreeSpinsResultView.INVISIBLE_TIME = Utils.float2int(FreeSpinsResultView.INVISIBLE_TIME * Ticker.getFPS());
			this.resultFreeSpinModal = <Container>this.getChildByName(FreeSpinsResultView.MODAL_CONTAINER);
			this.setTextPositions();
		}

		public setTextPositions(){
			if(!this.textsPos){
				var title:Text = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_WIN);
				if(title != null){
					title.y += title.lineHeight;
					title.color = '#fff';
					title.shadow = new Shadow("#000", 0, 0, 10);
					title.maxWidth = 450;
				}
				title = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_1);
				if(title != null){
					title.y += title.lineHeight;
					title.shadow = new Shadow("#000", 0, 0, 10);
					title.maxWidth = 450;
				}
				title = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_2);
				if(title != null){
					title.y += title.lineHeight;
					title.shadow = new Shadow("#000", 0, 0, 10);
					title.maxWidth = 450;
				}
			}
			this.textsPos = true;
		}

		public getCollectBtn():Button {
			return <Button>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.COLLECT_BTN);
		}

		public setTotalWin(value:number):void {
			var tf:Text = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_WIN);
			if (tf != null) {
				tf.text = MoneyFormatter.format(value, true);
			}
			tf = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_1);
			if (tf != null) {
				tf.text = FreeSpinsResultView.DEFAULT_GAME_TITLE_1;
			}
			tf = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_2);
			if (tf != null) {
				tf.text = FreeSpinsResultView.DEFAULT_GAME_TITLE_2;
			}
		}

		public setFreeSpinsMessage(title1:string, title2:string){
			var tf:Text = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_WIN);
			if (tf != null) {
				tf.text = "";
			}
			tf = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_1);
			if (tf != null) {
				tf.text = title1;
			}
			tf = <Text>this.resultFreeSpinModal.getChildByName(FreeSpinsResultView.GAME_TITLE_2);
			if (tf != null) {
				tf.text = title2;
			}
			//var collectBtn:Button = this.getCollectBtn(); TODO: change collect button text to continue
		}

		public setGameWin(value:number):void {
			//var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.GAME_WIN);
			//if (tf != null) {
			//	tf.text = MoneyFormatter.format(value, true);
			//}
		}

		public setFeatureWin(value:number):void {
			//var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.FEATURE_WIN);
			//if (tf != null) {
			//	tf.text = MoneyFormatter.format(value, true);
			//}
		}

		public hide(callback:(...params:any[]) => any):void {
			var tween:Tween = Tween.get(this, {useTicks: true});
			tween.to({alpha: 0}, FreeSpinsResultView.INVISIBLE_TIME);
			tween.call(callback);
		}
	}
}