module engine {
	import Layout = layout.Layout;
	import Text = createjs.Text;

	export class FreeSpinsView extends Layout {
		public static LAYOUT_NAME:string = "FreeSpinsView";

		private static LEFT_SPINS_COUNT_TEXT:string = "leftSpinsTf";
		private static MULTIPLICATOR_TEXT:string = "multiplicatorTf";
		private static WIN_TEXT:string = "winTf";

		private win:AnimationTextField;

		constructor() {
			super();
		}

		public onInit():void {
			this.win = new AnimationTextField(<Text>this.getChildByName(FreeSpinsView.WIN_TEXT));
		}

		public setLeftSpins(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsView.LEFT_SPINS_COUNT_TEXT);
			if (tf != null) {
				tf.text = value.toString();
			}
		}

		public setMultiplicator(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsView.MULTIPLICATOR_TEXT);
			if (tf != null) {
				tf.text = "x" + value.toString();
			}
		}

		public setWin(value:number, updateTime:number = 0):void {
			this.win.setValue(value, updateTime)
		}

		public update():void{
			if (this.win != null){
				this.win.update();
			}
		}
	}
}