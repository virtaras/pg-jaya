module engine {
	import Container = createjs.Container;
	import Button = layout.Button;
	import Ticker = createjs.Ticker;
	import Sound = createjs.Sound;
	import Point = createjs.Point;
	import Shape = createjs.Shape;
	import Rectangle = createjs.Rectangle;
	import LayoutCreator = layout.LayoutCreator;

	export class FreeSpinsController extends BaseBonusController {
		private static UPDATE_WIN_TIME:number = 0.5;

		private view:WeelView;
		private bonusContainer:Container;
		private modalContainer:Container;
		private count:number;
		private leftSpins:number;
		private multiplicator:number;
		private gameWin:number;
		private featureWin:number;
		private isFist:boolean;
		private isBonusStarted:boolean;
		private endBonusGame:boolean;
		private endBonusGameStarted:boolean = false;
		private totalWin:number = 0;

		private static _FS_TITLE_1:string = "You have completed";
		private static _FS_TITLE_2:string = "your Free Spins";

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, true);
			this.resetBonusFields();
		}

		public init():void {
			super.init();
			FreeSpinsController.UPDATE_WIN_TIME = Utils.float2int(FreeSpinsController.UPDATE_WIN_TIME * Ticker.getFPS());
			this.resetBonusFields();
		}

		public create():void {
			super.create();
			this.resetBonusFields();
			this.send(NotificationList.REMOVE_HEADER);

			this.view = <WeelView>this.common.layouts[WeelView.LAYOUT_NAME];
			this.view.create();
			this.bonusContainer = new Container();
			this.modalContainer = new Container();
			var symbolsRect:Array<Array<Rectangle>> = this.common.symbolsRect;

			var rect:Rectangle = symbolsRect[0][0];
			var mask:Shape = new Shape();
			var x:number = this.view.x;
			var y:number = rect.y + 3;
			var h:number = y + (rect.height * 3);
			var w:number = x + (rect.width * 5) + 10;
			mask.graphics.beginFill("0").drawRect(x, y, w, h);
			this.view.mask = mask;
			//this.endBonusGame = false;
			//this.view.visible = false;
			this.isFist = true;
			this.view.addChild(this.bonusContainer);
			//this.container.addChild(this.view);

			this.view.on(NotificationList.EVENT_All_BONUSES_SHOWED, (eventObj:any)=> {
				this.view.animationParser(this.common.config.sectorsAnimation);
			});
			this.view.on(NotificationList.SHOW_START_BONUS_MODAL, (eventObj:any)=> {
				this.showStartPop();
			});
			this.view.on(NotificationList.START_WEEL_ROTATE, (eventObj:any)=> {
				this.send(NotificationList.START_WEEL_ROTATE);
			});
			this.view.on(NotificationList.STOP_WEEL_ROTATE, (eventObj:any)=> {
				this.send(NotificationList.STOP_WEEL_ROTATE);
			});

			console.log("create freespinscontroller");
		}

		public removeBonusAnimation():void {
			this.view.removeChild(this.bonusContainer);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.EVENT_All_BONUSES_SHOWED);
			notifications.push(NotificationList.END_FREE_SPINS);
			notifications.push(NotificationList.WIN_POPUP_SHOWED);
			notifications.push(NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
			notifications.push(NotificationList.FREESPINS_SHOW_LEFT_FIELDS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			super.handleNotification(message, data);
			switch (message) {
				case NotificationList.EVENT_All_BONUSES_SHOWED:
				{
					this.removeBonusAnimation();
				}
				case NotificationList.START_BONUS:
				{
					this.isBonusStarted = true;
					//this.view.setLeftSpins(this.leftSpins);
					this.tryStartSpin();
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					this.send(NotificationList.SHOW_WIN_LINES, true);
					this.send(NotificationList.SHOW_WIN_TF);
					break;
				}
				case NotificationList.END_FREE_SPINS:{
					this.resetBonus();
					this.endBonusGame = true;
					this.totalWin = data;
					//this.common.server.win = data;
					break;
				}
				case NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED:
				//case NotificationList.SERVER_GOT_BONUS:
				{
					console.log("this.isFist="+this.isFist, "WIN_LINES_AND_WIN_POPUP_SHOWED");
					this.send(NotificationList.SHOW_WIN_TF, 0);
					if (this.isFist) {
						this.container.addChild(this.view);
						var sectorNum:number = this.common.server.bonus.data['sector'];
						this.view.setBonus(sectorNum);
						this.showStartAnimation();
						this.isFist = false;
					}else{
						this.tryStartSpin();
					}
					break;
				}
				case NotificationList.FREESPINS_SHOW_LEFT_FIELDS:
				{
					this.send(NotificationList.FREESPINS_SHOW_LEFT, this.common.server.bonus.step);
					break;
				}

			}
		}

		public tryStartSpin():void{
			if(this.isBonusStarted) {
				console.log(this.common.server.bonus,"tryStartSpin");
				this.send(NotificationList.FREESPINS_SHOW_LEFT, this.common.server.bonus.step);
				this.send(NotificationList.REMOVE_WIN_LINES);
				this.send(NotificationList.START_SPIN);
				this.sendRequest();
			}
			if(this.endBonusGame && !this.endBonusGameStarted){
				this.showResultPopup(this.totalWin);
			}
		}

		private resetBonus():void {
			this.isBonusStarted = false;
		}

		private resetBonusFields():void{
			this.endBonusGame = false;
			this.isBonusStarted = false;
		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			if (this.isFist) {
				//console.log(this.common.server.scatters);
				//this.view.setLeftSpins(this.count);
				//this.view.setMultiplicator(this.multiplicator);
				//this.view.setWin(this.featureWin);

				//var sectorNum:number = this.common.server.bonus.data['sector'];
				//this.view.setBonus(sectorNum);
				//this.showStartAnimation();
				//this.isFist = false;
			}
			else {
				this.send(NotificationList.SERVER_GOT_SPIN);
			}
		}

		public onEnterFrame():void {
			if(this.container == null)return;
			this.container.tickChildren = false;
			var speedControl = Ticker.getTicks() % 2;
			if(speedControl == 0) {
				this.container.tickChildren = true;
				if (this.view != null) {
					this.view.onEnterFrame();
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			console.log(buttonName, "onBtnClick");
			switch (buttonName) {
				case FreeSpinsStartView.START_BTN:
				{
					this.removeStartPopup();
					break;
				}
			}
			//HudController.playBtnSound(buttonName);
		}

		private showStartAnimation():void {
			this.send(NotificationList.SCATTER_WIN);
			var wheel:Array<Object> = this.common.server.scatters;
			var symbolsRect:Array<Array<Rectangle>> = this.common.symbolsRect;
			var bonusCount:number = wheel.length;
			var allWinAniVOs:Array<WinAniVO> = [];
			var winAniVOs:Array<WinAniVO> = [];
			for (var i:number = 0; i < bonusCount; i++) {
				var bonusVO:Object = wheel[i];
				var reel:number = bonusVO['reel'];
				var row:number = bonusVO['row'];
				var rect:Rectangle = symbolsRect[reel][row];
				var posIdx:Point = new Point(reel, row);
				var winSymbolView:WinSymbolView = this.createBonusSymbol();
				var winAniVO:WinAniVO = new WinAniVO();
				winAniVO.winSymbolView = winSymbolView;
				winAniVO.rect = rect.clone();
				winAniVO.rect.x -= 10;
				winAniVO.rect.y -= 10;
				winAniVO.posIdx = posIdx;
				allWinAniVOs.push(winAniVO);
				winAniVOs.push(winAniVO);
			}
			this.view.addBonusAnimation(winAniVOs, this.common.isMobile);
		}


		public showStartPop():void {
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			startPopup.create();
			startPopup.alpha = 1;
			var bonusNum:number = this.common.server.bonus.data['sector'];
			startPopup.showBonusTitle(bonusNum);
			this.container.addChild(startPopup);

			var buttonsNames:Array<string> = startPopup.buttonsNames;
			for (var i:number = 0; i < buttonsNames.length; i++) {
				var buttonName:string = buttonsNames[i];
				var button:Button = startPopup.getBtn(buttonName);
				if (button == null) {
					continue;
				}
				button.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						button.removeAllEventListeners("click");
						this.onBtnClick(eventObj.currentTarget.name);
					}
				})
			}
		}

		private removeStartPopup():void {
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			startPopup.hide(()=> {
					this.container.removeChild(startPopup);
					this.container.removeChild(this.view);
					this.send(NotificationList.START_BONUS);
				}
			)
		}

		private createBonusSymbol():WinSymbolView {
			var regularAni:LayoutCreator = this.common.layouts['WinAnimation_2'];
			var winSymbolView:WinSymbolView = new WinSymbolView();
			winSymbolView.create(regularAni, null);
			return winSymbolView;
		}



		private showResultPopup(win:number):void {
			console.log("showResultPopup", win);
			this.endBonusGameStarted = true;
			var resultPopup:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultPopup.create();
			resultPopup.alpha = 1;
			var collectBtn:Button = resultPopup.getCollectBtn();
			collectBtn.on("click", (eventObj:any)=> {
				if (eventObj.nativeEvent instanceof MouseEvent) {
					console.log("collectBtn clicked");
					collectBtn.removeAllEventListeners("click");
					eventObj.removeAllEventListeners("click");
					this.hideResultPopup();
				}
			});
			this.container.addChild(resultPopup);
			if(win>0) {
				resultPopup.setTotalWin(win);
			}else{
				//resultPopup.setTotalWin(win);
				resultPopup.setFreeSpinsMessage(FreeSpinsController._FS_TITLE_1 + " " + FreeSpinsController._FS_TITLE_2, ""); //show message freespins finished
			}
		}

		private hideResultPopup():void{
			var resultView:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultView.hide(()=> {
					Sound.play(SoundsList.COLLECT);
					this.send(NotificationList.END_BONUS);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					//this.send(NotificationList.SHOW_WIN_TF);
					this.container.removeChild(resultView);
				}
			);
		}

		private parseResponse(xml:any):void {
			//this.count = XMLUtils.getAttributeInt(xml, "all");
			//this.leftSpins = XMLUtils.getAttributeInt(xml, "counter");
			this.leftSpins = this.common.server.bonus.data.step;
			//this.multiplicator = XMLUtils.getAttributeInt(xml, "mp");
			//this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin");
			//this.isFist = this.leftSpins == this.count - 1;
			//var data:any = XMLUtils.getElement(xml, "data");
			//this.featureWin = XMLUtils.getAttributeFloat(data, "summ");
			//this.parseSpinData(data);
		}

		private parseSpinData(xml:any):void {
			//var spinData:any = XMLUtils.getElement(xml, "spin");
			//var wheelsData:any = XMLUtils.getElement(spinData, "wheels").childNodes;
			//for (var i = 0; i < wheelsData.length; i++) {
			//	var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
			//	for (var j:number = 0; j < wheelData.length; j++) {
			//		this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
			//	}
			//}
			//var winLinesData:any = XMLUtils.getElement(spinData, "winposition").childNodes;
			//this.common.server.winLines = [];
			//for (var i:number = 0; i < winLinesData.length; i++) {
			//	var winLine:any = winLinesData[i];
			//	var winLineVO:WinLineVO = new WinLineVO();
			//	winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
			//	winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
			//	winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
			//	this.common.server.winLines.push(winLineVO);
			//}
			////TODO: server return 0;
			////this.common.server.setBalance(parseFloat(XMLUtils.getElement(spinData, "balance").textContent));
			//this.common.server.win = parseFloat(XMLUtils.getElement(spinData, "win").textContent);
		}

		public dispose():void {
			super.dispose();
			//this.view.dispose();
			this.view.removeAllEventListeners(NotificationList.EVENT_All_BONUSES_SHOWED);
			this.view.removeAllEventListeners(NotificationList.SHOW_START_BONUS_MODAL);
			this.view.removeAllEventListeners(NotificationList.START_WEEL_ROTATE);
			this.view.removeAllEventListeners(NotificationList.STOP_WEEL_ROTATE);
		}
	}
}