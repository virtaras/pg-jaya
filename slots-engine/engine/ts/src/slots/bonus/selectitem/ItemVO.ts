module engine {
	export class ItemVO {
		public id:number;
		public isSelected:Boolean;
		public winValue:number;

		constructor(id:number) {
			this.id = id;
			this.isSelected = false;
			this.winValue = -1;
		}
	}
}
