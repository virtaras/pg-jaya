module engine {
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Layout = layout.Layout;
	import Sprite = createjs.Sprite;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;

	export class SelectItemView extends Layout {
		public static LAYOUT_NAME:string = "SelectItemView";

		private static WIN_POPUP:string = "winPopup";
		private static BONUS:string = "bonus";

		private static WIN_ANIMATION:string = "winAnimation_";
		private static ITEM_TEXT:string = "itemTf_";
		private static ITEM_BTN:string = "itemBtn_";

		private static TOTAL_WIN_TEXT:string = "totalWin";
		private static COLLECT_BTN:string = "collectBtn";

		private static DELAY_FOR_NEXT_STEP:number = 2;
		private static HIDE_POPUP_TIME:number = 0.5;

		private bonus:Container;
		private winPopup:Container;
		private items:Array<Button>;
		private texts:Array<Text>;
		private animations:Array<Sprite>;
		private itemsCount:number;

		constructor() {
			super();
			SelectItemView.DELAY_FOR_NEXT_STEP = Utils.float2int(SelectItemView.DELAY_FOR_NEXT_STEP *Ticker.getFPS());
			SelectItemView.HIDE_POPUP_TIME = Utils.float2int(SelectItemView.HIDE_POPUP_TIME *Ticker.getFPS());
		}

		public onInit():void {
			this.bonus = <Container>this.getChildByName(SelectItemView.BONUS);
			this.winPopup = <Container>this.getChildByName(SelectItemView.WIN_POPUP);
			this.items = <Array<Button>>Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_BTN);
			this.itemsCount = this.items.length;
			this.texts = <Array<Text>>Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_TEXT);
			this.animations = <Array<Sprite>>Utils.fillDisplayObject(this.bonus, SelectItemView.WIN_ANIMATION);
		}

		public showWinAnimation(vo:ItemVO, callback:(...params:any[]) => any):void {
			this.items[vo.id].setEnable(false);

			var animation:Sprite = this.animations[vo.id];
			var frameCount:number = animation.spriteSheet.getNumFrames(null);
			animation.visible = true;
			animation.addEventListener("tick", (eventObj:any)=> {
				var currentAnimation:Sprite = eventObj.currentTarget;
				if (currentAnimation.currentFrame == frameCount - 1) {
					currentAnimation.stop()
				}
			});
			animation.gotoAndPlay(0);

			var itemText:Text = this.texts[vo.id];
			itemText.text = MoneyFormatter.format(vo.winValue, true);
			itemText.visible = true;
			itemText.alpha = 0;
			Tween.get(itemText, {useTicks: true})
				.to({alpha: 1}, frameCount / 2)
				.wait(SelectItemView.DELAY_FOR_NEXT_STEP)
				.call(callback);
		}

		public showWinPopup():void {
			this.winPopup.visible = true;
			this.winPopup.alpha = 1;
			this.bonus.visible = false;
		}

		public showBonus():void {
			this.bonus.visible = true;
			this.bonus.alpha = 1;
			this.winPopup.visible = false;
		}

		public hideAllWinAnimation():void {
			for (var i:number = 0; i < this.animations.length; i++) {
				this.animations[i].visible = false;
				this.texts[i].visible = false;
			}
		}

		public hideWinPopup(callback:(...params:any[]) => any):void {
			Tween.get(this.winPopup, {useTicks: true})
				.to({alpha: 0}, SelectItemView.HIDE_POPUP_TIME)
				.call(callback);
		}

		public getCollectBtn():Button {
			return <Button>this.winPopup.getChildByName(SelectItemView.COLLECT_BTN);
		}

		public getItems():Array<Button> {
			return this.items;
		}

		public getItemCount():number {
			return this.itemsCount;
		}

		public setTotalWin(value:number):void {
			var totalWin:Text = <Text>this.winPopup.getChildByName(SelectItemView.TOTAL_WIN_TEXT);
			totalWin.text = MoneyFormatter.format(value, true);
		}

		public static getItemIdByName(itemName:string):number {
			return parseInt(itemName.substr(SelectItemView.ITEM_TEXT.length + 1));
		}
	}
}