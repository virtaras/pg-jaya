module engine {
	import Container = createjs.Container;
	import Tween = createjs.Tween;
	import Button = layout.Button;

	export class SelectItemController extends BaseBonusController {
		private view:SelectItemView;
		private isFinish:boolean;
		private itemsVO:Array<ItemVO>;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, false);
		}

		public init():void {
			super.init();
		}

		public create():void {
			super.create();

			this.view = <SelectItemView>this.common.layouts[SelectItemView.LAYOUT_NAME];
			this.view.create();
			this.view.showBonus();
			this.view.hideAllWinAnimation();
			this.container.addChild(this.view);

			var itemCount:number = this.view.getItemCount();
			this.itemsVO = new Array(itemCount);
			for (var i:number = 0; i < itemCount; i++) {
				this.itemsVO[i] = new ItemVO(i);
			}

			this.setEnableAllItem(true);
			this.initHandlers();
		}

		private initHandlers():void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; i++) {
				var item:Button = items[i];
				item.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						var currentItem:Button = <Button>eventObj.currentTarget;
						var itemId = SelectItemView.getItemIdByName(currentItem.name);
						this.sendRequest(itemId);
					}
				});
			}
		}

		private removeHandlers():void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; i++) {
				items[i].removeAllEventListeners("click");
			}
			this.view.getCollectBtn().removeAllEventListeners("click")
		}

		public showWinAnimation(vo:ItemVO):void {
			this.view.showWinAnimation(vo, ()=> {
				if (this.isFinish) {
					this.view.setTotalWin(this.calculateTotalWin());
					this.view.showWinPopup();
					this.view.getCollectBtn().on("click", (eventObj:any)=> {
						if (eventObj.nativeEvent instanceof MouseEvent) {
							this.view.getCollectBtn().removeAllEventListeners("click");
							this.view.hideWinPopup(()=> {
								this.send(NotificationList.END_BONUS);
							})
						}
					});
				}
				else {
					this.setEnableAllItem(true);
				}
			});
		}

		public setEnableAllItem(value:boolean):void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < this.itemsVO.length; i++) {
				if (!this.itemsVO[i].isSelected) {
					items[i].setEnable(value);
				}
			}
		}

		public calculateTotalWin():number {
			var result:number = 0;
			for (var i:number = 0; i < this.itemsVO.length; i++) {
				var itemVO:ItemVO = this.itemsVO[i];
				if (itemVO.isSelected) {
					result += itemVO.winValue;
				}
			}
			return result;
		}

		public onGotResponse(xml:any):void {
			var data:any = XMLUtils.getElement(xml, "data");
			var wheels:any = XMLUtils.getElement(data, "wheels");
			var items:Array<any> = XMLUtils.getElements(wheels, "item");
			for (var i:number = 0; i < items.length; i++) {
				var item:any = items[i];
				var id:number = XMLUtils.getAttributeInt(item, "id") - 1;

				var vo:ItemVO = this.itemsVO[id];
				if (!vo.isSelected) {
					vo.isSelected = XMLUtils.getAttributeInt(item, "selected") == 1;
					vo.winValue = parseFloat(item.textContent);
					if (vo.isSelected) {
						this.showWinAnimation(vo);
					}
				}
			}
			this.isFinish = XMLUtils.getAttributeInt(wheels, "finish") == 1;
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
			this.setEnableAllItem(false);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.removeHandlers();
			this.view = null;
			this.itemsVO = null;
		}
	}
}