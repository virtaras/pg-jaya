module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Container = createjs.Container;

	export class GambleController5 extends BaseBonusController {
		private static SHOW_MESSAGE_TIME:number = 2;
		private static SHOW_MESSAGE_TIME_DEFAULT:number = 2;
		private static CARDS_COUNT:number = 13;

		private static MESSAGE_DEFAULT:string = "CHOOSE DOUBLE OR DOUBLE HALF";
		private static MESSAGE_DEFAULT_SECOND:string = "PICK A HIGHER CARD";
		private static MESSAGE_WIN:string = "YOU WIN: ";
		private static MESSAGE_LOSE:string = "DEALER WINS";

		private view:GambleView5;
		private bank:number;
		private totalbank:number;
		private history:Array<number>;
		private isFist:boolean;
		private stepBonus:number;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			// TODO: нужно чтобы на сервере был стандартный step=0
			super(manager, common, container, -1, true);
			GambleController5.SHOW_MESSAGE_TIME = Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
		}

		public init():void {
			super.init();
			this.isFist = true;
		}

		public create():void {
			super.create();
			this.view = <GambleView5>this.common.layouts[GambleView5.LAYOUT_NAME];
			this.view.create();
			this.view.alpha = 1;
			this.totalbank = 0;
			this.container.addChild(this.view);
			this.initHandlers();
		}

		private resetGame():void {
			console.log('resetGame');
			this.view.setMessageText(GambleController5.MESSAGE_DEFAULT);
			this.lockBonus(false);
			this.stepBonus = 1;
			var cardName:string;
			GambleController5.SHOW_MESSAGE_TIME = Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
			for (var n:number = 1; n < 6; n++) {
				cardName = 'card_' + n;
				this.view.getButton(cardName).on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onCardCoverClick(eventObj.currentTarget.name);
					}
				});
			}

		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			// show card
			if (!this.isFist) {
				this.view.showCard(this.history.shift());
			}
			if (this.isFist) {
				this.resetGame();
			}
			else {
				if(this.stepBonus == 3){
					this.view.showHistory(this.history);
					this.setBankMessage();
				}
			}
			this.view.setBankValue(this.totalbank);
			this.view.setDoubleValue(this.common.server.bet * 2);
			this.view.setHalfDoubleValue(this.common.server.bet * 1.5);

			if (this.isFist) {
				this.isFist = false;
			}
		}

		private setBankMessage():void{
			if (this.bank == 0){
				this.view.setMessageText(GambleController5.MESSAGE_LOSE);
				// remove bonus
				this.removeHandlers();
				this.view.hideCard();
				this.view.hideBonus(true, ()=> {
					this.send(NotificationList.END_BONUS);
				})
			}
			else {
				this.view.setMessageText(GambleController5.MESSAGE_WIN + this.totalbank);
				this.view.hideCard();
				this.common.server.bonus.step = 0;
				var self:any = this;
				Tween.get(this.view, {useTicks: true})
					.wait(GambleController5.SHOW_MESSAGE_TIME)
					.call(()=> {
						self.resetGame();
						//if one more
						this.view.setMessageText(GambleController5.MESSAGE_DEFAULT);
					});
			}
		}


		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
			this.lockBonus(true);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.view = null;
		}

		private parseResponse(xml:any):void {
			var data:any = XMLUtils.getElement(xml, "data");
			this.history = [];
			this.bank = 0;
			if(data != null){
				var wheels:any = XMLUtils.getElement(data, "wheels");
				this.bank = XMLUtils.getAttributeFloat(wheels, "win") || 0;
				this.totalbank = this.totalbank + this.bank;
				var items:any = wheels.children;
				var item:any;
				for (var i:number = items.length - 1; i >= 0; i--) {
					item = items[i];
					//TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
					// on server suit from 1 to 4
					console.log(item.attributes.suit.value, item.textContent);
					var suit:number = parseInt(item.attributes.suit.value);
					// on server card id from 2 to 14
					var cardId:number = parseInt(item.textContent) - 1;
					var frameId:number = cardId + (GambleController5.CARDS_COUNT * suit);
					console.log(frameId);
					this.history.unshift(frameId);
				}
			}
		}

		private initHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				this.view.getButton(buttonsNames[i]).on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onBtnClick(eventObj.currentTarget.name);
					}
				});
			}

		}

		private removeHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
			}
		}

		private onCardCoverClick(buttonName:string):void {
			if(this.stepBonus == 2){
				var name:Array<string> = buttonName.split('_');
				var indexCard:number = parseInt(name[1]);
				this.sendRequest(indexCard - 1);
				this.stepBonus++;
			}
		}

		private onBtnClick(buttonName:string):void {
			this.stepBonus++;
			switch (buttonName) {
				case GambleView5.COLLECT_BTN:
				{
					// remove bonus
					this.removeHandlers();
					this.view.hideBonus(false, ()=> {
						this.send(NotificationList.END_BONUS);
					});
					break;
				}
				case GambleView5.HALF_BTN:
				{
					this.sendRequest(1);
					this.view.setMessageText(GambleController5.MESSAGE_DEFAULT_SECOND);
					break;
				}
				case GambleView5.DOUBLE_BTN:
				{
					this.sendRequest(2);
					this.view.setMessageText(GambleController5.MESSAGE_DEFAULT_SECOND);
					break;
				}
			}
		}

		private lockBonus(value:boolean):void {
			if (this.view != null) {
				var buttonsNames:Array<string> = this.view.buttonsNames;
				var buttonsCount:number = buttonsNames.length;
				for (var i:number = 0; i < buttonsCount; i++) {
					this.view.getButton(buttonsNames[i]).setEnable(!value);
				}
			}
		}
	}
}