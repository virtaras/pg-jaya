module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Container = createjs.Container;

	export class GambleController extends BaseBonusController {
		private static SHOW_MESSAGE_TIME:number = 2;
		private static CARDS_COUNT:number = 13;

		private static MESSAGE_DEFAULT:string = "CHOOSE RED OR BLACK";
		private static MESSAGE_WIN:string = "YOU WIN: ";
		private static MESSAGE_LOSE:string = "DEALER WINS";

		private view:GambleView;
		private bank:number;
		private history:Array<number>;
		private isFist:boolean;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			// TODO: нужно чтобы на сервере был стандартный step=0
			super(manager, common, container, -1, true);
			GambleController.SHOW_MESSAGE_TIME = Utils.float2int(GambleController.SHOW_MESSAGE_TIME * Ticker.getFPS());
		}

		public init():void {
			super.init();
			this.isFist = true;
		}

		public create():void {
			super.create();
			this.view = <GambleView>this.common.layouts[GambleView.LAYOUT_NAME];
			this.view.create();
			this.view.alpha = 1;

			this.container.addChild(this.view);
			this.initHandlers();
		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			// show card
			if (!this.isFist) {
				this.view.showCard(this.history.pop());
			}
			else {
				this.view.hideCard();
			}
			this.view.showHistory(this.history);

			// set message
			if (this.isFist) {
				this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
				this.lockBonus(false);
			}
			else {
				if (this.bank == 0) {
					this.view.setMessageText(GambleController.MESSAGE_LOSE);
					// remove bonus
					this.removeHandlers();
					this.view.hideBonus(true, ()=> {
						this.send(NotificationList.END_BONUS);
					})
				}
				else {
					this.view.setMessageText(GambleController.MESSAGE_WIN + this.bank);
					Tween.get(this.view, {useTicks: true})
						.wait(GambleController.SHOW_MESSAGE_TIME)
						.call(()=> {
							this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
							this.lockBonus(false);
						});
				}
			}
			this.view.setBankValue(this.bank);
			this.view.setDoubleValue(this.bank * 2);

			if (this.isFist) {
				this.isFist = false;
			}
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
			this.lockBonus(true);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.view = null;
		}

		private parseResponse(xml:any):void {
			this.bank = Math.abs(parseFloat(XMLUtils.getElement(xml, "win").textContent));
			var data:any = XMLUtils.getElement(xml, "data");
			this.history = [];
			if(data != null){
				var wheels:any = XMLUtils.getElement(data, "wheels");
				var items:any = wheels.children;
				var item:any;
				var suit:number;
				var cardId:number;
				var frameId:number
				for (var i:number = items.length - 1; i >= 0; i--) {
					item = items[i];
					//TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
					// on server suit from 1 to 4
					suit = parseInt(item.attributes.suit.value) - 1;
					// on server card id from 2 to 14
					cardId = parseInt(item.textContent) - 2;
					frameId = cardId + (GambleController.CARDS_COUNT * suit);
					this.history.push(frameId);
				}
			}
		}

		private initHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				this.view.getButton(buttonsNames[i]).on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onBtnClick(eventObj.currentTarget.name);
					}
				});
			}
		}

		private removeHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
			}
		}

		private onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case GambleView.COLLECT_BTN:
				{
					// remove bonus
					this.removeHandlers();
					this.view.hideBonus(false, ()=> {
						this.send(NotificationList.END_BONUS);
					});
					break;
				}
				case GambleView.RED_BTN:
				{
					this.sendRequest(1);
					break;
				}
				case GambleView.BLACK_BTN:
				{
					this.sendRequest(2);
					break;
				}
			}
		}

		private lockBonus(value:boolean):void {
			if (this.view != null) {
				var buttonsNames:Array<string> = this.view.buttonsNames;
				var buttonsCount:number = buttonsNames.length;
				for (var i:number = 0; i < buttonsCount; i++) {
					this.view.getButton(buttonsNames[i]).setEnable(!value);
				}
			}
		}
	}
}