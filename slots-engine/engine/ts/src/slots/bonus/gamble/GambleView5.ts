module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Text = createjs.Text;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Sprite = createjs.Sprite;
	import Layout = layout.Layout;
	import Button = layout.Button;

	export class GambleView5 extends Layout {
		public static LAYOUT_NAME:string = "GambleView5";

		private static INVISIBLE_TIME:number = 0.5;
		private static DELAY_REMOVE:number = 3;

		private static MESSAGE_TEXT:string = "massageTf";
		private static DOUBLE_TEXT:string = "doubleTf";
		private static BANK_TEXT:string = "bankTf";
		private static HALF_TEXT:string = "halfTf";

		private static BACK:string = "back";
		private static CARD:string = "card";
		private static HISTORY_CARD_PREFIX:string = "history_";

		public static COLLECT_BTN:string = "collectBtn";
		//public static RED_BTN:string = "redBtn";
		//public static BLACK_BTN:string = "blackBtn";
		public static HALF_BTN:string = "halfBtn";
		public static DOUBLE_BTN:string = "doubleBtn";

		public buttonsNames:Array<string> = [
			GambleView5.COLLECT_BTN,
			GambleView5.HALF_BTN,
			GambleView5.DOUBLE_BTN
		];

		constructor() {
			super();
			GambleView5.INVISIBLE_TIME = Utils.float2int(GambleView5.INVISIBLE_TIME  * Ticker.getFPS());
			GambleView5.DELAY_REMOVE = Utils.float2int(GambleView5.DELAY_REMOVE  * Ticker.getFPS());
		}

		public getButton(name:string):Button {
			return <Button>this.getChildByName(name);
		}

		public setBankValue(value:number):void {
			console.log('setBankValue');
			var tf:Text = <Text>this.getChildByName(GambleView5.BANK_TEXT);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setHalfDoubleValue(value:number):void {
			console.log('setHalfValue');
			var tf:Text = <Text>this.getChildByName(GambleView5.HALF_TEXT);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setDoubleValue(value:number):void {
			console.log('setDoubleValue');
			var tf:Text = <Text>this.getChildByName(GambleView5.DOUBLE_TEXT);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setMessageText(text:string):void {
			var tf:Text = <Text>this.getChildByName(GambleView5.MESSAGE_TEXT);
			tf.text = text;
		}

		public showCard(cardId:number):void {
			console.log(cardId);
			var cardName:string = 'card_1';
			var back:DisplayObject = this.getChildByName(cardName);
			back.visible = false;
			var card:Sprite = <Sprite>this.getChildByName(cardName);
			card.gotoAndStop(cardId);
			card.visible = true;
		}

		public hideCard():void {
			console.log('hide all');
			for (var i:number = 1; i < 6; i++){
				var cardName:string = 'card_' + i;
				var card:Sprite = <Sprite>this.getChildByName(cardName);
				this.resetLot(card);
			}
		}

		private resetLot(card:Sprite):void {
			var tween:Tween = Tween.get(card, {useTicks: true});
			tween.wait(GambleView5.DELAY_REMOVE);
			tween.to({alpha: 0}, GambleView5.INVISIBLE_TIME);
			tween.call(function(){
				card.gotoAndStop(0);
				card.alpha = 1;
			});
		}

		public showHistory(historyData:Array<number>):void {
			for (var i:number = 2; i < 6; i++){
				var cardName:string = 'card_' + i;
				var back:DisplayObject = this.getChildByName(cardName);
				back.visible = false;
				var card:Sprite = <Sprite>this.getChildByName(cardName);
				card.gotoAndStop(historyData[i - 2]);
				card.visible = true;
			}
		}

		public hideBonus(isDelay:boolean, callback:(...params:any[]) => any):void {
			var tween:Tween = Tween.get(this, {useTicks: true});
			if (isDelay) {
				tween.wait(GambleView5.DELAY_REMOVE);
			}
			tween.to({alpha: 0}, GambleView5.INVISIBLE_TIME);
			tween.call(callback);
		}
	}
}