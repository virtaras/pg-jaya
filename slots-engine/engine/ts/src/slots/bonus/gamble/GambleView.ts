module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Text = createjs.Text;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Sprite = createjs.Sprite;
	import Layout = layout.Layout;
	import Button = layout.Button;

	export class GambleView extends Layout {
		public static LAYOUT_NAME:string = "GambleView";

		private static INVISIBLE_TIME:number = 0.5;
		private static DELAY_REMOVE:number = 3;

		private static MESSAGE_TEXT:string = "massageTf";
		private static DOUBLE_TEXT:string = "doubleTf";
		private static BANK_TEXT:string = "bankTf";

		private static BACK:string = "back";
		private static CARD:string = "card";
		private static HISTORY_CARD_PREFIX:string = "history_";

		public static COLLECT_BTN:string = "collectBtn";
		public static RED_BTN:string = "redBtn";
		public static BLACK_BTN:string = "blackBtn";

		public buttonsNames:Array<string> = [
			GambleView.COLLECT_BTN,
			GambleView.RED_BTN,
			GambleView.BLACK_BTN
		];

		constructor() {
			super();
			GambleView.INVISIBLE_TIME = Utils.float2int(GambleView.INVISIBLE_TIME  * Ticker.getFPS());
			GambleView.DELAY_REMOVE = Utils.float2int(GambleView.DELAY_REMOVE  * Ticker.getFPS());
		}

		public getButton(name:string):Button {
			return <Button>this.getChildByName(name);
		}

		public setBankValue(value:number):void {
			var tf:Text = <Text>this.getChildByName(GambleView.BANK_TEXT);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setDoubleValue(value:number):void {
			var tf:Text = <Text>this.getChildByName(GambleView.DOUBLE_TEXT);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setMessageText(text:string):void {
			var tf:Text = <Text>this.getChildByName(GambleView.MESSAGE_TEXT);
			tf.text = text;
		}

		public showCard(cardId:number):void {
			var back:DisplayObject = this.getChildByName(GambleView.BACK);
			back.visible = false;
			var card:Sprite = <Sprite>this.getChildByName(GambleView.CARD);
			card.gotoAndStop(cardId);
			card.visible = true;
		}

		public hideCard():void {
			var back:DisplayObject = this.getChildByName(GambleView.BACK);
			back.visible = true;
			var card:Sprite = <Sprite>this.getChildByName(GambleView.CARD);
			card.visible = false;
		}
 
		public showHistory(historyData:Array<number>):void {
			var i:number = 1;
			var cardContainer:Container;
			while ((cardContainer = <Container>this.getChildByName(GambleView.HISTORY_CARD_PREFIX + i)) != null) {
				var back = cardContainer.getChildByName(GambleView.BACK);
				var cards:Sprite = <Sprite>cardContainer.getChildByName(GambleView.CARD);
				if (i <= historyData.length) {
					cards.gotoAndStop(historyData[i - 1]);
					cards.visible = true;
					back.visible = false;
				}
				else {
					cards.visible = false;
					back.visible = true;
				}
				i++;
			}
		}

		public hideBonus(isDelay:boolean, callback:(...params:any[]) => any):void {
			var tween:Tween = Tween.get(this, {useTicks: true});
			if (isDelay) {
				tween.wait(GambleView.DELAY_REMOVE);
			}
			tween.to({alpha: 0}, GambleView.INVISIBLE_TIME);
			tween.call(callback);
		}
	}
}