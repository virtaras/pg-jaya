module engine {
	import Container = createjs.Container;
	import Text = createjs.Text;
	import Button = layout.Button;

	export class BaseBonusController extends BaseController {
		private isCreated:boolean;

		public common:CommonRefs;
		public container:Container;
		public isAutoRequest:Boolean;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container, startStep:number, isAutoRequest:boolean) {
			super(manager);
			this.common = common;
			this.container = container;

			if (this.common.server.bonus.data == null) {
				this.common.server.bonus.step = startStep;
			}
			this.isCreated = false;
			this.isAutoRequest = isAutoRequest;
		}

		public init():void {
			super.init();
			var bonusData:any = this.common.server.bonus.data;
			if (bonusData == null && this.isAutoRequest) {
				this.sendRequest();
			}
			else {
				this.create();
				if (bonusData != null) {
					this.onGotResponse(bonusData);
				}
			}
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SERVER_GOT_BONUS);
			//notifications.push(NotificationList.START_BONUS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {

				case NotificationList.SERVER_GOT_BONUS:
				{
					if (!this.isCreated) {
						this.create();
					}
					this.onGotResponse(data);
					break;
				}
			}
		}

		// create bonus game
		public create():void {
			this.isCreated = true;
		}

		public sendRequest(param:number = 0):void {
			//this.send(NotificationList.SERVER_SEND_BONUS, {"param": param, "step": ++this.common.server.bonus.step});
			this.send(NotificationList.SERVER_SEND_BONUS);
		}

		public onGotResponse(data:any):void {
		}
	}
}