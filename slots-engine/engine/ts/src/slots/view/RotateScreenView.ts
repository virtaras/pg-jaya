module engine {
	import Layout = layout.Layout;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Rectangle = createjs.Rectangle;
	import DisplayObject = createjs.DisplayObject;

	export class RotateScreenView extends Layout {
		public static LAYOUT_NAME:string = "RotateScreenView";
		private static ROTATION_TIME:number = 0.7;
		private static DELAY_TIME:number = 0.7;

		private art:DisplayObject;

		constructor() {
			super();
			RotateScreenView.ROTATION_TIME = Utils.float2int(RotateScreenView.ROTATION_TIME * Ticker.getFPS());
			RotateScreenView.DELAY_TIME = Utils.float2int(RotateScreenView.DELAY_TIME * Ticker.getFPS());
		}

		public onInit():void {
			this.art = this.getChildAt(0);
			var bounds:Rectangle = this.art.getBounds();
			this.art.regX = bounds.width / 2;
			this.art.regY = bounds.height / 2;
			this.art.x += this.art.regX;
			this.art.y += this.art.regY;
		}

		public play():void {
			if (this.art != undefined) {
				var _art = this.art;
				tween();
				function tween():void {
					_art.rotation = 0;
					Tween.get(_art, {useTicks: true})
						.to({rotation: -90}, RotateScreenView.ROTATION_TIME)
						.wait(RotateScreenView.DELAY_TIME)
						.call(tween);
				}
			}
		}

		public stop():void {
			if (this.art != undefined) {
				Tween.removeTweens(this.art);
			}
		}
	}
}