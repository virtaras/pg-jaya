module engine {
	import Container = createjs.Container;
	import Sprite = createjs.Sprite;
	import Rectangle = createjs.Rectangle;

	export class ReelView {
		private static SYMBOL_PREFIX:string = "symbol_";

		private view:Container;
		private reelId:number;
		private symbols:Array<Sprite>;

		constructor(view:Container, reelId:number) {
			this.view = view;
			this.reelId = reelId;
		}

		public init():void {
			this.symbols = [];
			var symbolId:number = 1;
			var symbol:Sprite;
			while ((symbol = <Sprite>this.view.getChildByName(ReelView.SYMBOL_PREFIX + symbolId)) != null) {
				this.symbols.push(symbol);
				symbolId++;
			}
		}

		public showAllSymbols():void {
			for (var i:number = 0; i < this.symbols.length; i++) {
				this.symbols[i].visible = true;
			}
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////
		public getReelId():number {
			return this.reelId;
		}

		public getSymbol(id:number):Sprite {
			return this.symbols[id];
		}

		public getSymbolFrame(id:number):number {
			return this.symbols[id].currentFrame;
		}

		public getSymbolCount():number {
			return this.symbols.length - 1;
		}

		public getX():number {
			return this.view.x;
		}

		public getY():number {
			return this.view.y;
		}

		/////////////////////////////////////////
		//	SETTERS
		/////////////////////////////////////////

		public setX(value:number):void {
			this.view.x = value;
		}

		public setY(value:number):void {
			this.view.y = value;
		}

		public setSymbolFrame(symbolId:number, frameId:number):void {
			return this.symbols[symbolId].gotoAndStop(frameId);
		}

		public getView():Container{
			return this.view;
		}
	}
}