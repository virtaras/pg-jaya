module engine {
	import Container = createjs.Container;
	import Shape = createjs.Shape;
	import Rectangle = createjs.Rectangle;
	import Point = createjs.Point;
	import Layout = layout.Layout;

	export class ReelsView extends Layout {
		public static LAYOUT_NAME:string = "ReelsView";
		public static PLAY_FIELD_MASK:Shape;

		private static REEL_PREFIX:string = "reel_";
		private reels:Array<ReelView>;

		private isMobile:boolean = false;

		constructor() {
			super();
		}

		public onInit():void {
			this.createReelsView();
			this.createReelsMask();
		}

		public setMobileTrue(isMobile:boolean):void {
			this.isMobile = isMobile;
			this.createReelsMask();
		}

		private createReelsView():void {
			this.reels = [];
			var view:Container;
			var reelId:number = 1;
			while ((view = <Container>this.getChildByName(ReelsView.REEL_PREFIX + reelId)) != null) {
				var reelView:ReelView = new ReelView(view, reelId - 1);
				reelView.init();
				this.reels.push(reelView);
				reelId++;
			}
		}

		private createReelsMask():void {
			var firstReel:ReelView = this.reels[0];
			var symbolBounds:Rectangle = firstReel.getSymbol(0).getBounds();
			var lastReel:ReelView = this.reels[this.reels.length - 1];

			var mask:Shape = new Shape();
			mask.graphics.beginFill("0").drawRect(firstReel.getX(), firstReel.getY(),lastReel.getX() + symbolBounds.width, this.getHeight() - symbolBounds.height);
			this.mask = mask;
			this.hitArea = mask;
			ReelsView.PLAY_FIELD_MASK = mask;
		}

		public showAllSymbols():void {
			for (var i:number = 0; i < this.reels.length; i++) {
				this.reels[i].showAllSymbols();
			}
		}

		public hideSymbols(positions:Array<Point>):void {
			for (var i:number = 0; i < positions.length; i++) {
				var pos:Point = positions[i];
				this.reels[pos.x].getSymbol(pos.y + 1).visible = false;
				//this.reels[pos.x].getSymbol(pos.y).visible = false;
			}
		}

		public dispose():void {
			this.parent.removeChild(this);
			this.reels = null;
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////

		public getReel(id:number):ReelView {
			return this.reels[id];
		}

		public getReelCount():number {
			return this.reels.length;
		}

		public getHeight():number {
			return this.getBounds().height;
		}
	}
}