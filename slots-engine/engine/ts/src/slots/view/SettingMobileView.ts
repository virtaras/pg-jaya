module engine {
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;
    import Text = createjs.Text;
    import Stage = createjs.Stage;
    import Container = createjs.Container;
    import DisplayObject = createjs.DisplayObject;
    import Sprite = createjs.Sprite;
    import Layout = layout.Layout;
    import Button = layout.Button;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;

    export class SettingMobileView extends Layout {
        public static LAYOUT_NAME:string = "SettingMobileView";
        private static BG_NAME:string = "setting_bg_hor";
        public static PAGES_INDICATORS:string = "indicators";
        public static INDICATOR_PREFIX:string = 'indicator';
        public static SOUND_SETTING_TEMPLATE:string = "SettingSoundView";


        public static CLOSE_BTN:string = "CloseBtn";
        public static PAY_BTN:string = "payBtn";
        public static OPTIONS_BTN:string = "settinBtn";
        public isPortrait:boolean = false;


        public buttonsNames:Array<string> = [
            SettingMobileView.CLOSE_BTN,
            SettingMobileView.PAY_BTN,
            SettingMobileView.OPTIONS_BTN
        ];

        public payViews:Array<string> = [
            "PayView_1",
            "PayView_2",
            "PayView_3",
            "PayView_4",
            "PayView_5"

        ];

        constructor() {
            super();
        }

        public indicatorClick(ind:number):void{
            var indicators:Container = <Container>this.getChildByName(SettingMobileView.PAGES_INDICATORS);
            var indicator:Sprite = <Sprite>indicators.getChildByName(SettingMobileView.INDICATOR_PREFIX + (ind + 1));
            indicator.gotoAndStop(1);
        }

        public updateMove(id:number):void {
            var indicator:Sprite;
            var indicators:Container = <Container>this.getChildByName(SettingMobileView.PAGES_INDICATORS);
            for(var a:number = 0; a < indicators.getNumChildren(); a++){
                 indicator = <Sprite>indicators.getChildByName(SettingMobileView.INDICATOR_PREFIX + (a + 1));
                 indicator.gotoAndStop(0);
            }
            indicator = <Sprite>indicators.getChildByName(SettingMobileView.INDICATOR_PREFIX + id);
            indicator.gotoAndStop(1);
        }

        public resizeForMobile():void {
            this.toggleIndicator(false);
        }

        public toggleIndicator(status:boolean):void {
            var indicators:DisplayObject = this.getChildByName(SettingMobileView.PAGES_INDICATORS);
            indicators.visible = status;
        }

        public getButton(name:string):Button {
            return <Button>this.getChildByName(name);
        }

        public getBtn(btnName:string):Button {
            return <Button>this.getChildByName(btnName);
        }

        public dispose():void {
            this.parent.removeChild(this);
        }
        public getSoundTempl():string {
            return SettingMobileView.SOUND_SETTING_TEMPLATE;
        }

        public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
            var button:Button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        }


    }
}