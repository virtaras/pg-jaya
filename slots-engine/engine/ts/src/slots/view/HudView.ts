module engine {
	import DisplayObject = createjs.DisplayObject;
	import Container = createjs.Container;
	import Shape = createjs.Shape;
	import Stage = createjs.Stage;
	import Layout = layout.Layout;
	import Button = layout.Button;
	import Text = createjs.Text;
	import Rectangle = createjs.Rectangle;
	import Ticker = createjs.Ticker;

	export class HudView extends Layout {
		public static LAYOUT_NAME:string = "HudView";
		//================================================================
		//------------------MENU------------------------------------------
		//================================================================

		//autospin table
		public static AUTO_SPIN_TABLE_NEW:string = "autoPlayPanel";
		public static AUTO_SPIN_TABLE_MASK:string = "autoSpinTableMask";
		public static OPEN_AUTO_SPIN_TABLE_BTN:string = "autoSpinStartBtn";

		public static TOTAL_BET_BTN:string = "totalBetBtn";
		public static SELECT_BET_TABLE:string = "TotalBetPanel";
		public static SELECT_BET_TABLE_MASK:string ="selectBetTableMask";

		public static SETTINGS_MENU:string = "settingsMenu";

		public static AUTO_SPIN_TEXT:string = "autoPlayText";
		public static TOTAL_BET_TITLE:string = "totalBet";
		public static WIN_TITLE:string = "winTitle";
		public static CASH_TEXT:string = "cashTitle";

		public static AUTO_SPIN_TEXT_START:string = "AUTOPLAY";
		public static AUTO_SPIN_TEXT_STOP:string = "STOP";
		public static AUTO_SPIN_TEXT_FREESPINS:string = "FREESPINS";

		public static TITLES_MARGIN:number = 10;

		private hudTitles:Array<string> = [
			HudView.AUTO_SPIN_TEXT,
			HudView.TOTAL_BET_TITLE,
			HudView.WIN_TITLE,
			HudView.CASH_TEXT
		];


		//================================================================
		//------------------BUTTONS NAMES---------------------------------
		//================================================================
		public static BACK_TO_LOBBY_BTN:string = "lobbyBtn";
		public static SETTINGS_BTN:string = "settingsBtn";
		public static FULL_SCREEN_BTN:string = "fullScreenBtn";

		//Запускает автоспины
		//public static START_AUTO_PLAY_BTN:string = "autoSpinStartBtn";
		//Начинает спин
		public static START_SPIN_BTN:string = "spinBtn";

		//Останавлевает автоспины
		public static STOP_AUTO_PLAY_BTN:string = "autoStopBtn";
		//Ускоряет остановку барабанов
		public static STOP_SPIN_BTN:string = "stopBtn";

		//Запускает рисковую
		public static GAMBLE_BTN:string = "gambleBtn";
		//Запускает бонус
		public static START_BONUS_BTN:string = "bonusStartBtn";
		//Открывает HELP
		public static PAY_TABLE_BTN:string = "payTableBtn";

		//Устанавлевает следующее значение фриспинов из конфига autoPlayCount
		public static INC_SPIN_COUNT_BTN:string = "incSpinsBtn";
		//Устанавлевает предыдущее значение фриспинов из конфига autoPlayCount
		public static DEC_SPIN_COUNT_BTN:string = "decSpinsBtn";

		//Увеличевает количество линий на 1 до максимального
		public static INC_LINE_COUNT_BTN:string = "incLinesBtn";
		//Уменьшает количество линий на 1 до 1
		public static DEC_LINE_COUNT_BTN:string = "decLinesBtn";
		//Меняет количество линий по кругу
		public static LINE_COUNT_BTN:string = "linesBtn";
		//Устанавлевает максимальное количество линий и начинает спин
		public static MAX_LINES_BTN:string = "maxLinesBtn";

		//Устанавлевает следующую ставку до максимальной
		public static NEXT_BET_BTN:string = "nextBetBtn";
		//Устанавлевает предыдущую ставку до первой
		public static PREV_BET_BTN:string = "prevBetBtn";
		//Устанавлевает следующую ставку по кругу
		public static BET_BTN:string = "betBtn";
		//Устанавлевает максимальный мультиплекатор и начинает спин
		public static MAX_BET_BTN:string = "maxBetBtn";
		//Устанавлевает следующий мультиплекатор по кругу
		public static MULTIPLY_BTN:string = "multiplyBtn";

		//================================================================
		//------------------TEXT FIELDS NAMES-----------------------------
		//================================================================
		public static WIN_TEXT:string = "winTf";
		public static BET_TEXT:string = "betTf";
		public static TOTAL_BET_TEXT:string = "totalBetTf";
		public static PREV_BET_PER_LINE_TEXT:string = "prevLineBetTf";
		public static BET_PER_LINE_TEXT:string = "lineBetTf";
		public static NEXT_BET_PER_LINE_TEXT:string = "nextLineBetTf";
		public static PREV_LINES_COUNT_TEXT:string = "prevLinesCountTf";
		public static LINES_COUNT_TEXT:string = "linesCountTf";
		public static NEXT_LINES_COUNT_TEXT:string = "nextLinesCountTf";
		public static PREV_AUTO_SPINS_COUNT_TEXT:string = "prevSpinCountTf";
		public static AUTO_SPINS_COUNT_TEXT:string = "spinCountTf";
		public static NEXT_AUTO_SPINS_COUNT_TEXT:string = "nextSpinCountTf";
		public static BALANCE_TEXT:string = "balanceTf";

		private textPositions:boolean;

		public buttonsNames:Array<string> = [
			HudView.SETTINGS_BTN,
			HudView.BACK_TO_LOBBY_BTN,
			//HudView.START_AUTO_PLAY_BTN,
			HudView.STOP_AUTO_PLAY_BTN,
			HudView.GAMBLE_BTN,
			HudView.INC_SPIN_COUNT_BTN,
			HudView.DEC_SPIN_COUNT_BTN,
			HudView.INC_LINE_COUNT_BTN,
			HudView.DEC_LINE_COUNT_BTN,
			HudView.NEXT_BET_BTN,
			HudView.PREV_BET_BTN,
			HudView.MAX_BET_BTN,
			HudView.MAX_LINES_BTN,
			HudView.START_SPIN_BTN,
			HudView.START_BONUS_BTN,
			HudView.STOP_SPIN_BTN,
			HudView.PAY_TABLE_BTN,
			HudView.BET_BTN,
			HudView.LINE_COUNT_BTN,
			HudView.MULTIPLY_BTN,
			HudView.OPEN_AUTO_SPIN_TABLE_BTN,
			HudView.TOTAL_BET_BTN
		];

		private spinButtons:Array<string> = [
			HudView.START_SPIN_BTN,
			HudView.STOP_SPIN_BTN
		];

		private autoSpinOrigin:number;
		private totalBetOrigin:number;


		public static HUD_BAR:string = "instance221";

		private panels:Array<string> = [
			HudView.AUTO_SPIN_TABLE_NEW,
			HudView.SELECT_BET_TABLE,
			HudView.AUTO_SPIN_TABLE_MASK,
			HudView.SELECT_BET_TABLE_MASK
		];

		public linesText:Array<Text>;

		public viewInitialised:boolean = false;

		constructor() {
			super();
			this.linesText = [];
		}

		public onInit():void {
			//var textField:Text;
			//var i:number = 1;
			//while ((textField = <Text>this.getChildByName(HudView.LINES_COUNT_TEXT + i)) != null) {
			//	this.linesText.push(textField);
			//	i++;
			//}
			this.initSetOrigin();

			this.scaleItems(0.58, this.panels);
			//this.scaleItems(0.7, this.hudTitles);
			//this.prepareHudTitles();
		}

		private prepareHudTitles():void{
			var fontSize:number = 18;
			this.setText(HudView.AUTO_SPINS_COUNT_TEXT, "");
			var item:Text = <Text>this.getChildByName(HudView.AUTO_SPIN_TEXT);
			if (item != null) {
				item.font = " "+fontSize+"px Bebas";
				//item.y += 25;
				item.y += 27;
			}
			item = <Text>this.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT);
			if (item != null) {
				item.font = " "+fontSize+"px Bebas";
				//item.y += fontSize;
				item.y += fontSize;
			}
			item = <Text>this.getChildByName(HudView.TOTAL_BET_TITLE);
			if (item != null) {
				item.font = " "+fontSize+"px Bebas";
				item.y += fontSize;
			}
			item = <Text>this.getChildByName(HudView.WIN_TITLE);
			if (item != null) {
				item.font = " "+fontSize+"px Bebas";
				item.y += fontSize;
			}
			item = <Text>this.getChildByName(HudView.CASH_TEXT);
			if (item != null) {
				item.font = " "+fontSize+"px Bebas";
				item.y += fontSize;
			}
			return;
		}

		private initSetOrigin():void {
			var autoSpinTableView:Container = <Container>this.getChildByName(HudView.AUTO_SPIN_TABLE_NEW);
			this.autoSpinOrigin = autoSpinTableView.y;

			var totalBetTableView:Container = <Container>this.getChildByName(HudView.SELECT_BET_TABLE);
			this.totalBetOrigin = totalBetTableView.y;
		}

		public setUpElements(common:CommonRefs){

			var stage:Stage = this.getStage();
			if (stage == null || this.textPositions) {
				return;
			}

			var winValue = <Text>this.getChildByName(HudView.WIN_TEXT);
			winValue.y = winValue.y + winValue.lineHeight;

			var totalBetValue = <Text>this.getChildByName(HudView.TOTAL_BET_TEXT);
			totalBetValue.y = totalBetValue.y + totalBetValue.getBounds().height;

			var balanceValue = <Text>this.getChildByName(HudView.BALANCE_TEXT);
			balanceValue.y = balanceValue.y + balanceValue.getBounds().height;

			var bar:DisplayObject = this.getChildByName(HudView.HUD_BAR);

			var arrowsA = <Button>this.getChildByName(HudView.OPEN_AUTO_SPIN_TABLE_BTN);
			var arrowsT = <Button>this.getChildByName(HudView.TOTAL_BET_BTN);
			this.setHitArea(arrowsA, bar);
			this.setHitArea(arrowsT, bar);

			this.textPositions = true;
		}

		private scaleItems(scale:number, arr:Array<any>):void {
			for (var i:number = 0; i < arr.length; i++) {
				var item:DisplayObject = <DisplayObject>this.getChildByName(arr[i]);
				if (item != null) {
					item.scaleX = item.scaleY = item.scaleX * scale;
				}
			}
		}

		public resizeForMobile(common:CommonRefs):void {
			var stage:Stage = this.getStage();
			if (stage == null || this.textPositions) {
				return;
			}

			//var winValue = <Text>this.getChildByName(HudView.WIN_TEXT);
			//winValue.y = winValue.y + winValue.lineHeight;
            //
			//var totalBetValue = <Text>this.getChildByName(HudView.TOTAL_BET_TEXT);
			//totalBetValue.y = totalBetValue.y + totalBetValue.getBounds().height;
            //
			//var balanceValue = <Text>this.getChildByName(HudView.BALANCE_TEXT);
			//balanceValue.y = balanceValue.y + balanceValue.getBounds().height;

			var bar:DisplayObject = this.getChildByName(HudView.HUD_BAR);

			var arrowsA = <Button>this.getChildByName(HudView.OPEN_AUTO_SPIN_TABLE_BTN);
			var arrowsT = <Button>this.getChildByName(HudView.TOTAL_BET_BTN);
			this.setHitArea(arrowsA, bar);
			this.setHitArea(arrowsT, bar);

			this.textPositions = true;
		}

		public setWin(winVal:number):void {
			var winValue = new AnimationTextField(this.getText(HudView.WIN_TEXT));
			winValue.setValue(winVal);
		}

		public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {

			var button:Button = this.getBtn(btnName);
			if (button) {
				button.visible = visible;
				button.setEnable(enable);
			}
		}

		public setLineCountText(value:string):void {
			for (var i:number = 0; i < this.linesText.length; i++) {
				this.linesText[i].text = value;
			}
		}

		private setHitArea(button:Button, topBar:DisplayObject):void {
			var bounds:Rectangle = button.getBounds();
			var hitArea:Shape = new Shape();
			hitArea.graphics.beginFill("0").drawRect( -2 * bounds.width, 0, bounds.width * 5, bounds.height * 5);
			button.hitArea = hitArea;
		}

		public setOrigin():void {
			var autoSpinTableView:Container = <Container>this.getChildByName(HudView.AUTO_SPIN_TABLE_NEW);
			autoSpinTableView.y = this.autoSpinOrigin;

			var totalBetTableView:Container = <Container>this.getChildByName(HudView.SELECT_BET_TABLE);
			totalBetTableView.y = this.totalBetOrigin;

		}

		public dispose():void {
			this.parent.removeChild(this);
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////

		public getBtn(btnName:string):Button {
			return <Button>this.getChildByName(btnName);
		}

		public getText(textFieldName:string):Text {
			return <Text>this.getChildByName(textFieldName);
		}

		public setText(field:string, value:string):void{
			var tf:Text = <Text>this.getChildByName(field);
			tf.text = value;
			return;
		}
	}
}