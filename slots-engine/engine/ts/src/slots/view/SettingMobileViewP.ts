module engine {
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;
    import Text = createjs.Text;
    import Container = createjs.Container;
    import DisplayObject = createjs.DisplayObject;
    import Sprite = createjs.Sprite;
    import Layout = layout.Layout;
    import Button = layout.Button;

    export class SettingMobileViewP extends Layout {
        public static LAYOUT_NAME:string = "SettingMobileViewP";
        public static SOUND_SETTING_TEMPLATE:string = "SettingSoundViewP";
        public static SLIDER_DRAG:string = "sliderDrug";

        public static CLOSE_BTN:string = "CloseBtn";
        public isPortrait:boolean = true;

        public buttonsNames:Array<string> = [
            SettingMobileViewP.CLOSE_BTN
        ];

        constructor() {
            super();
        }

        public payViews:Array<string> = [
            "PayView_1P",
            "PayView_2P",
            "PayView_3P",
            "PayView_4P",
            "PayView_5P"
        ];

        public updateMove(percent:number):void {
            var dragSlider:DisplayObject = this.getChildByName(SettingMobileViewP.SLIDER_DRAG);
            dragSlider.y = percent;
        }

        public resizeForMobile():void {

        }

        public getSoundTempl():string {
            return SettingMobileViewP.SOUND_SETTING_TEMPLATE;
        }

        public getButton(name:string):Button {
            return <Button>this.getChildByName(name);
        }

        public getBtn(btnName:string):Button {
            return <Button>this.getChildByName(btnName);
        }

        public dispose():void {
            this.parent.removeChild(this);
        }

        public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
            var button:Button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        }

    }
}