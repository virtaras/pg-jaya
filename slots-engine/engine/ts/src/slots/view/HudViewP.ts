module engine {
    import DisplayObject = createjs.DisplayObject;
    import Container = createjs.Container;
    import Rectangle = createjs.Rectangle;
    import Shape = createjs.Shape;
    import Stage = createjs.Stage;
    import Layout = layout.Layout;
    import Button = layout.Button;
    import Text = createjs.Text;
    import Ticker = createjs.Ticker;

    export class HudViewP extends HudView {
        public static LAYOUT_NAME:string = "HudView";
        public static WIN_TEXT:string = "winTf";
        public static WIN_TITLE:string = "winTitle";

        public linesText:Array<Text>;

        //buttons
        public static START_AUTO_PLAY_BTN:string = "autoSpinStartBtn";
        public static TOTAL_BET_BTNP:string = "totalBetBtn";
        public static AUTO_SPIN_BUTTONP:string = "autoSpinStartBtn";
        public static START_SPIN_BTN:string = "spinBtn";

        //autospin table
        public static AUTO_SPIN_TABLE_NEW:string = "autoPlayPanel";
        public static SELECT_BET_TABLE:string = "TotalBetPanel";
        public static AUTO_SPIN_TABLE_MASK:string = "autoSpinTableMask";
        public static SELECT_BET_TABLE_MASK:string = "selectBetTableMask";

        private autoSpinOrigin:number;
        private totalBetOrigin:number;

        //select bets table
        public static BALANCE_TEXT:string = "balanceTf";

        //bars
        public static HUD_TOP_BAR:string = "topBar";
        public static HUD_BOTTOM_BAR:string = "bottomBar";

        private hudTitles:Array<string> = [
            HudView.AUTO_SPIN_TEXT,
            HudView.TOTAL_BET_TITLE,
            HudView.WIN_TITLE,
            HudView.CASH_TEXT
        ];

        private textPositions:boolean;
        public viewInitialised:boolean = false;

        public buttonsNames:Array<string> = [
            HudViewP.TOTAL_BET_BTNP,
            HudView.SETTINGS_BTN,
            HudView.START_SPIN_BTN,
            HudView.STOP_SPIN_BTN,
            HudView.OPEN_AUTO_SPIN_TABLE_BTN
            //HudView.START_AUTO_PLAY_BTN
        ];

        private panels:Array<string> = [
            HudView.SELECT_BET_TABLE,
            HudView.SELECT_BET_TABLE_MASK
        ];

        private spinButtons:Array<string> = [
            HudView.START_SPIN_BTN,
            HudView.STOP_SPIN_BTN
        ];

        constructor() {
            super();
            this.linesText = [];
        }

        public onInit():void {
            var textField:Text;
            var i:number = 1;
            while ((textField = <Text>this.getChildByName(HudView.LINES_COUNT_TEXT + i)) != null) {
                this.linesText.push(textField);
                i++;
            }
            this.initSetOrigin();
        }

        private prepareHudTitles():void{
            this.setText(HudView.AUTO_SPINS_COUNT_TEXT, "");
            var item:DisplayObject = <DisplayObject>this.getChildByName(HudView.AUTO_SPIN_TEXT);
            var itemTf:Text = <Text>item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
                item.y = item.y + itemTf.lineHeight;
            }
            item = <DisplayObject>this.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT);
            itemTf = <Text>item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
                item.y = item.y + itemTf.lineHeight;
            }
            item = <DisplayObject>this.getChildByName(HudView.TOTAL_BET_TITLE);
            //itemTf = <Text>item;
            if (item != null) {
                //item.scaleY = item.scaleY * 0.5;
                //item.scaleX = item.scaleX * 0.7;
                item.y = item.y + itemTf.lineHeight;
                item.scaleX = item.scaleY = item.scaleX * 0.8;
            }
            item = <DisplayObject>this.getChildByName(HudView.WIN_TITLE);
            //itemTf = <Text>item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
                //item.y = item.y + itemTf.lineHeight;
            }
            item = <DisplayObject>this.getChildByName(HudView.CASH_TEXT);
            //itemTf = <Text>item;
            if (item != null) {
                item.scaleY = item.scaleY * 0.5;
                item.scaleX = item.scaleX * 0.68;
                //item.y = item.y + itemTf.lineHeight;
            }
        }

        private scaleItems(scale:number, arr:Array<any>):void {
            for (var i:number = 0; i < arr.length; i++) {
                var item:DisplayObject = <DisplayObject>this.getChildByName(arr[i]);
                if (item != null) {
                    item.scaleX = item.scaleY = item.scaleX * scale;
                }
            }
        }

        private initSetOrigin():void {
            var autoSpinTableView:Container = <Container>this.getChildByName(HudViewP.AUTO_SPIN_TABLE_NEW);
            this.autoSpinOrigin = autoSpinTableView.y;

            var totalBetTableView:Container = <Container>this.getChildByName(HudViewP.SELECT_BET_TABLE);
            this.totalBetOrigin = totalBetTableView.y;

            this.scaleItems(0.75,this.panels);
            //this.scaleItems(0.9,this.hudTitles);
            //this.prepareHudTitles();
        }

        public resizeForMobile(common:CommonRefs):void {
            console.log("resizeForMobile HudViewPPPPPPPPPPPPPP", "bottom="+bottom);
            var stage:Stage = this.getStage();
            if (stage == null) {
                return;
            }

            var canvas:HTMLCanvasElement = stage.canvas;

            var bottom = (canvas.height - stage.y) / this.parent.scaleY;

            var topBar:DisplayObject = this.getChildByName(HudViewP.HUD_TOP_BAR);
            var topBounds = topBar.getBounds();

            var bottomBar:DisplayObject = this.getChildByName(HudViewP.HUD_BOTTOM_BAR);
            var bottBounds = bottomBar.getBounds();
            bottomBar.y = bottom - bottBounds.height;

            //var totalBetValue = <Text>this.getChildByName(HudView.TOTAL_BET_TEXT);
            //totalBetValue.y = totalBetValue.lineHeight + HudView.TITLES_MARGIN;
            //totalBetValue.scaleX = totalBetValue.scaleY = 0.8;

            var winTitle = <Text>this.getChildByName(HudViewP.WIN_TITLE);
            winTitle.y = bottom - HudView.TITLES_MARGIN;

            var winValue = <Text>this.getChildByName(HudView.WIN_TEXT);
            winValue.y = winTitle.y - winTitle.lineHeight;

            var cashTitle = <Text>this.getChildByName(HudView.CASH_TEXT);
            var cashValue = <Text>this.getChildByName(HudView.BALANCE_TEXT);

            cashValue.y = winValue.y;
            cashTitle.y = winTitle.y;

            var arrowsA = <Button>this.getChildByName(HudViewP.START_AUTO_PLAY_BTN);
            var arrowsT = <Button>this.getChildByName(HudViewP.TOTAL_BET_BTNP);
            arrowsA.rotation = arrowsT.rotation = 180;
            this.setHitAreaP(arrowsA, topBar);
            this.setHitAreaP(arrowsT, topBar);

            var spin:Button;
            for (var i:number = 0; i < this.spinButtons.length; i++) {
                spin = this.getBtn(this.spinButtons[i]);
                spin.y = bottomBar.y - spin.getBounds().height;
            }

            this.textPositions = true;

        }

        public setWin(winVal:number):void {
            var winText = <Text>this.getChildByName(HudView.WIN_TEXT);
            var winValue = new AnimationTextField(winText);

            winValue.setValue(winVal);
        }

        private setHitAreaP(button:Button, topBar:DisplayObject):void {
            var bounds:Rectangle = button.getBounds();
            var hitArea:Shape = new Shape();
            hitArea.graphics.beginFill("0").drawRect(bounds.x - bounds.width * 2, topBar.y, bounds.width * 5, bounds.height * 5);
            button.hitArea = hitArea;
        }

        public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
            var button:Button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        }

        public setLineCountText(value:string):void {
            for (var i:number = 0; i < this.linesText.length; i++) {
                this.linesText[i].text = value;
            }
        }

        public setOrigin():void {
            var autoSpinTableView:Container = <Container>this.getChildByName(HudViewP.AUTO_SPIN_TABLE_NEW);
            autoSpinTableView.y = this.autoSpinOrigin;

            var totalBetTableView:Container = <Container>this.getChildByName(HudViewP.SELECT_BET_TABLE);
            totalBetTableView.y = this.totalBetOrigin;
        }

        public dispose():void {
            this.parent.removeChild(this);
        }

        public getBtn(btnName:string):Button {
            return <Button>this.getChildByName(btnName);
        }

        public getText(textFieldName:string):Text {
            return <Text>this.getChildByName(textFieldName);
        }

        public setText(field:string, value:string):void{
            var tf:Text = <Text>this.getChildByName(field);
            tf.text = value;
            return;
        }

    }
}