module engine {
    import Container = createjs.Container;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;
    import Point = createjs.Point;
    import Layout = layout.Layout;
    import DisplayObject = createjs.DisplayObject;
    import Tween = createjs.Tween;
    import Ease = createjs.Ease;
    import Button = layout.Button;
    import Sprite = createjs.Sprite;
    import Ticker = createjs.Ticker;

    export class WeelView extends Layout {
        public static LAYOUT_NAME:string = "WeelView";

        public static WEEL:string = "weel";
        public static ROTATOR:string = "rotator";

        public static BUTTON_START:string = "startWeel";
        public static BUTTON_BG:string = "bg";
        public bonusAnimationContainer:Container;

        private static START_ROTATION_TIME:number = 30;
        private static DELAY_TIME:number = 2;
        private initStart:boolean;

        private static WEEL_ROTATION_TIME:number = 3;
        private static WEEL_ROTATION_STOP_TIME:number = 10;
        public static ROTATION_TIMES:number = 1; //4
        private static DEGRESS_IN_A_FULL_ROTATION:number = 360;

        //sectorn name space
        public static SECTOR_CONT:string = "sector_container";
        public static SECTORS_TWO:string = "sector_2";
        public static SECTORS_PREFIX:string = "triangle_";
        public static FRAME_PREFIX:string = "f_";
        private static ANIMTAION_SECTOR_FRAMES:number = 90;
        private static SECOND_ANIMATION_DELAY:number = 22;
        private initSector:boolean;
        private initAnimFrame:number = 0;
        private triangles:Array<DisplayObject>;
        private rotateCenter:number;
        private winVOs:Array<WinAniVO>;

        private bonusGameElements:Array<string> = [
            WeelView.ROTATOR,
            WeelView.BUTTON_START,
            WeelView.BUTTON_BG,
            WeelView.SECTOR_CONT
        ];

        //rotator and origin coords
        private rotator:DisplayObject;
        private originY:number;
        private originX:number;
        private rotationLeft:number;
        private sectorCont:Container;

        private bonusSector:number;

        constructor() {
            super();
        }

        public setBonus(sector:number):void {
            this.bonusSector = sector;
        }


        public onInit():void {
            this.bonusAnimationContainer = new Container();
            this.addChild(this.bonusAnimationContainer);
            WeelView.WEEL_ROTATION_TIME = Utils.float2int(WeelView.WEEL_ROTATION_TIME * Ticker.getFPS());
            WeelView.WEEL_ROTATION_STOP_TIME = Utils.float2int(WeelView.WEEL_ROTATION_STOP_TIME * Ticker.getFPS());
        }

        private resetWeel():void {
            this.triangles = [];
            for(var a:number = 0; a < this.bonusGameElements.length; a++){
                var el:DisplayObject = this.getChildByName(this.bonusGameElements[a]);
                if(el != null)el.alpha = 0;
            }
            this.initStart = false;

            this.rotator = this.getChildByName(this.bonusGameElements[0]);
            this.originY = this.rotator.y;
            this.originX = this.rotator.x;
            this.rotator.rotation = -90;
            this.rotator.y = -this.rotator.getBounds().height / 2;
            this.rotator.x = -this.rotator.getBounds().width / 2;
            var ring = <Object>this.rotator['children'][0];
            ring['rotation'] = 0;
            ring['x']= ring['y'] = ring['regX'] = ring['regY'] = 0;
            if(this.sectorCont)this.sectorCont['x']= this.sectorCont['y'] = this.sectorCont['regX'] = this.sectorCont['regY'] = 0;
        }

        private showSectorAnimation():void {
            this.dispatchEvent(NotificationList.STOP_WEEL_ROTATE);
            var sectorTriangle:DisplayObject = this.sectorCont;
            var cont:Container = <Container>this.rotator;
            cont.addChild(sectorTriangle);

            var thisCont:Container = <Container>sectorTriangle;
            var sector:Container = <Container>thisCont.getChildByName(WeelView.SECTOR_CONT);
            var sectorTwo:Container = <Container>sector.getChildByName(WeelView.SECTORS_TWO);
            var triangle1:DisplayObject = sectorTwo.getChildByName(WeelView.SECTORS_PREFIX + '1');
            var triangle2:DisplayObject = sectorTwo.getChildByName(WeelView.SECTORS_PREFIX + '2');
            triangle2.alpha = triangle1.alpha = 0;
            this.triangles.push(triangle1);
            this.triangles.push(triangle2);
            this.dispatchEvent(NotificationList.EVENT_All_BONUSES_SHOWED);
        }

        public animationParser(keyframes:Object){
            this.initSector = true;
            var animation1:Object = keyframes['anim_1'];
            var animation2:Object = keyframes['anim_2'];
            this.createTwin(animation1, this.triangles[0]);
            var triangle:DisplayObject = this.triangles[1];
            var self:any = this;
            Tween.get(animation2, {useTicks: true}).to({alpha:0}).wait(WeelView.SECOND_ANIMATION_DELAY).call(function(){
                self.createTwin(animation2, triangle);
            });
        }

        public onEnterFrame():void {
            if(this.initSector){
                this.initAnimFrame++;
                if(this.initAnimFrame > WeelView.ANIMTAION_SECTOR_FRAMES / 2) {
                    this.initSector = false;
                    this.initAnimFrame = 0;
                    this.bonusAnimEnd();
                }
            }
        }

        private bonusAnimEnd():void {
            var tween:Tween = Tween.get(this.sectorCont, {useTicks: true});
            var self:any = this;
            tween.to({alpha:0}, WeelView.START_ROTATION_TIME).call(function(){
                self.dispatchEvent(NotificationList.SHOW_START_BONUS_MODAL);
            });
        }

        private createTwin(keyframes:Object, elem:DisplayObject){
            var tween:Tween = Tween.get(elem, {useTicks: true});
            var previos:number = 1;
            elem.x = elem.getBounds().width / 2;
            elem.regX = elem.getBounds().width / 2;
            //elem.y = elem.getBounds().height / 2;
            elem.regY = elem.getBounds().height / 2;

            for(var t:number = 1; t <= WeelView.ANIMTAION_SECTOR_FRAMES; t++){
                var name:string = WeelView.FRAME_PREFIX + t;
                if(keyframes[name]){
                    var params:Object = keyframes[name];
                    var duration:number = Math.floor(t - previos);
                    tween.to(params, duration);
                    previos = t;
               }
            }
        }

        private weelStartAnimation():void{
            if(this.initStart)return;
            this.initStart = true;

            var button:DisplayObject = this.getChildByName(this.bonusGameElements[1]);
            button.visible = false;

            for(var a:number = 0; a < this.bonusGameElements.length - 1; a++){
                var el:DisplayObject = this.getChildByName(this.bonusGameElements[a]);
                el.alpha = 1;
            }

            button.on("click", (eventObj:any)=> {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    button.removeAllEventListeners("click");
                    button.visible = false;
                    this.bonusAnimationContainer.removeAllChildren();
                    this.prepareRotation();
                }
            });


            Tween.get(this.rotator, {useTicks: true})
                .to({rotation: 0, y:this.originY, x:this.originX}, WeelView.START_ROTATION_TIME)
                .wait(WeelView.DELAY_TIME)
                .call(function(){
                    button.visible = true;
                    button.cursor = 'pointer';
            });
        }

        private prepareRotation():void {

            var ring = <Object>this.rotator['children'][0];

            var sectorCont:Container = new Container();
            sectorCont.rotation = 0;
            var sectorShape:Shape = new Shape();
            if(!this.rotateCenter)this.rotateCenter = ring['_rectangle']['width'];
            var width:number = this.rotateCenter;
            sectorShape.graphics.beginFill("0").drawRect(ring['x'],ring['y'],width, width);
            sectorShape.alpha = 0;

            sectorCont.addChild(sectorShape);
            var clone:any = this.getChildByName(WeelView.SECTOR_CONT);

            var sectorTriangle:any = clone.clone(true);
            sectorTriangle.y = width / 2 * 1.03;
            sectorTriangle.x = width / 2 - sectorTriangle.getBounds().width / 2;
            sectorCont.addChild(sectorTriangle);
            sectorCont.getChildByName(WeelView.SECTOR_CONT).alpha = 1;

            ring['x']= ring['y'] = ring['regX'] = ring['regY'] = width / 2;
            sectorCont['x']= sectorCont['y'] = sectorCont['regX'] = sectorCont['regY'] = width / 2;
            this.sectorCont = sectorCont;

            this.rotationLeft = 0;
            this.dispatchEvent(NotificationList.START_WEEL_ROTATE);
            this.launchWeel(<DisplayObject>ring);
        }

        private rotateWeel(ring:DisplayObject):void {
            ring.rotation = 0;
            var degrees:number = WeelView.DEGRESS_IN_A_FULL_ROTATION * WeelView.ROTATION_TIMES;
            Tween.get(ring, {useTicks: true}).to({"rotation": degrees}, WeelView.DELAY_TIME / 2, Ease.linear).call(()=> {
                this.stopRotate(ring);
            });
        }

        private stopRotate(ring:DisplayObject):void {
            ring.rotation = 0;
            var sectorDegr:number = WeelView.DEGRESS_IN_A_FULL_ROTATION / 5;
            var degrees:number = WeelView.DEGRESS_IN_A_FULL_ROTATION + (sectorDegr * this.bonusSector) + 360;

            Tween.get(ring, {useTicks: true}).to({"rotation": degrees}, WeelView.WEEL_ROTATION_STOP_TIME, Ease.getBackOut(1)).call(()=> {
                this.showSectorAnimation();
            });
        }


        private launchWeel(ring:DisplayObject):void {
            Tween.get(ring, {useTicks: true}).to({"rotation": WeelView.DEGRESS_IN_A_FULL_ROTATION}, WeelView.WEEL_ROTATION_TIME, Ease.getBackIn(1) ).call(()=> {
               this.rotateWeel(ring);
            });
        }

        public addBonusAnimation(winVOs:Array<WinAniVO>, isMobile:boolean):void{
            this.resetWeel();

            this.winVOs = winVOs;
            for (var i:number = 0; i < this.winVOs.length; i++) {
                var vo:WinAniVO = this.winVOs[i];
                var winSymbolView:WinSymbolView = vo.winSymbolView;
                var spriteContainer = <Container>winSymbolView.getChildAt(0);
                var sprite = <Sprite>spriteContainer.getChildAt(0);
                var frameCount:number = sprite.spriteSheet.getNumFrames(null);
                sprite.addEventListener("tick", (eventObj:any)=> {
                    var currentAnimation:Sprite = eventObj.currentTarget;
                    if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
                        currentAnimation.stop();
                        currentAnimation.removeAllEventListeners("tick");
                        if(isMobile){
                            var target:any = eventObj.currentTarget;
                            Tween.get(target, {useTicks: true}).to({"alpha": 0.9}, WeelView.WEEL_ROTATION_TIME / 2).call(()=> {
                                eventObj.currentTarget.alpha = 0;
                                this.weelStartAnimation();
                            });
                        } else {
                            eventObj.currentTarget.alpha = 0;
                            this.weelStartAnimation();
                        }
                    }
                });

                winSymbolView.x = vo.rect.x;
                winSymbolView.y = vo.rect.y;
                winSymbolView.play();
                this.bonusAnimationContainer.addChild(winSymbolView);
            }
        }

        public dispose():void {
            this.parent.removeChild(this);
        }

    }
}