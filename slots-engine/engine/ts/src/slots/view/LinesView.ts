module engine {
	import Layout = layout.Layout;
	import DisplayObject = createjs.DisplayObject;

	export class LinesView extends Layout {
		public static LAYOUT_NAME:string = "LinesView";
		private static LINE_PREFIX:string = "line_";

		private lines:Array<DisplayObject>;

		constructor() {
			super();
		}

		public onInit():void {
			var lineId = 1;
			var line:DisplayObject;
			this.lines = [];
			while ((line = this.getChildByName(LinesView.LINE_PREFIX + lineId)) != null) {
				this.lines.push(line);
				lineId++;
			}
		}

		public showLine(id:number):void {
			this.lines[id - 1].visible = true;
		}

		public hideLine(id:number):void {
			this.lines[id - 1].visible = false;
		}

		public showLinesFromTo(fromId:number, toId:number):void {
			for (var i:number = fromId - 1; i < toId; i++) {
				this.lines[i].visible = true;
			}
		}

		public showLines(linesIds:Array<number>):void {
			for (var i:number = 0; i < linesIds.length; i++) {
				this.lines[linesIds[i] - 1].visible = true;
			}
		}

		public hideAllLines():void {
			for (var i:number = 0; i < this.lines.length; i++) {
				this.lines[i].visible = false;
			}
		}
	}
}