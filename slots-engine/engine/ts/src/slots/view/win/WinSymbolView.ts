module engine {
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Sprite = createjs.Sprite;
	import LayoutCreator = layout.LayoutCreator;

	export class WinSymbolView extends Container {
		private winSymbol:DisplayObject;
		private highlight:Sprite;
		public _is_wild:boolean = false;

		constructor() {
			super();
		}

		public create(winSymbolCreator:LayoutCreator, highlightCreator:LayoutCreator):void {
			if (winSymbolCreator != null) {
				var winSymbolContainer:Container = new Container();
				winSymbolCreator.create(winSymbolContainer);
				this.winSymbol = winSymbolContainer.getChildAt(0);
				this.addChild(winSymbolContainer);
			}
			if (highlightCreator != null) {
				var highlightContainer:Container = new Container();
				highlightCreator.create(highlightContainer);
				this.highlight = <Sprite>highlightContainer.getChildAt(0);
				this.addChild(highlightContainer);
			}
		}

        public play():void {
			if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
				var sprite = (<Sprite>this.winSymbol);
				sprite.gotoAndPlay(0);
			}
			if (this.highlight != null) {
				this.highlight.gotoAndPlay(0)
			}
		}

		public setY(value:number):void {
			this.y = value;
		}
		public setX(value:number):void {
			this.x = value;
		}
	}
}