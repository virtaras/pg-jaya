/**
 * Created by Taras on 22.05.2015.
 */
module engine {
    import Container = createjs.Container;
    import DisplayObject = createjs.DisplayObject;
    import Sprite = createjs.Sprite;
    import LayoutCreator = layout.LayoutCreator;
    import Tween = createjs.Tween;

    export class WildSymbolView extends Container {
        private winSymbol:DisplayObject;
        private highlight:Sprite;
        private wildLoop:DisplayObject;
        public _is_wild_started:boolean = false;
        public _is_wild_to_show:boolean = false;

        constructor() {
            super();
        }

        public create(winSymbolCreator:LayoutCreator, highlightCreator:LayoutCreator, wildSymbolCreator:LayoutCreator, isMobile:boolean):void {
            if (winSymbolCreator != null) {
                var winSymbolContainer:Container = new Container();
                winSymbolCreator.create(winSymbolContainer);
                this.winSymbol = winSymbolContainer.getChildAt(0);
                this.addChild(winSymbolContainer);
            }

            if (highlightCreator != null) {
                var highlightContainer:Container = new Container();
                highlightCreator.create(highlightContainer);
                this.highlight = <Sprite>highlightContainer.getChildAt(0);
                if(isMobile){
                    this.highlight.scaleX *= 2;
                    this.highlight.scaleY *= 2;
                }
                this.addChild(highlightContainer);
            }

            var wildSymbolLoop:Container = new Container();
            wildSymbolCreator.create(wildSymbolLoop);
            this.wildLoop = wildSymbolLoop.getChildAt(0);
            this.wildLoop.visible = false;
            this.addChild(wildSymbolLoop);
        }

        public play():void {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = (<Sprite>this.winSymbol);
                this.wildLoop.visible = false;
                var wildLoop = <Sprite>this.wildLoop;
                var frameCount:number = sprite.spriteSheet.getNumFrames(null);
                sprite.addEventListener("tick", (eventObj:any)=> {
                    var currentAnimation:Sprite = eventObj.currentTarget;
                    currentAnimation.visible = true;
                    if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
                        currentAnimation.stop();
                        currentAnimation.visible = false;
                        currentAnimation.removeAllEventListeners("tick");
                        //sprite.removeAllEventListeners("tick");
                        wildLoop.visible = true;
                        wildLoop.gotoAndPlay(0);
                    }
                });
                if(!this._is_wild_started) {
                    this._is_wild_started = true;
                    sprite.gotoAndPlay(0);
                }
            }
        }

        public playFadeIn():void {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = <Sprite>this.winSymbol;
                sprite.visible = false;
                var wildLoop = <Sprite> this.wildLoop;
                wildLoop.visible = true;
                wildLoop.alpha = 0;
                wildLoop.gotoAndPlay(0);
                this._is_wild_started = true;
                Tween.get(wildLoop).to({alpha:1},900);
            }
        }

        public setY(value:number):void {
            this.y = value;
        }
        public setX(value:number):void {
            this.x = value;
        }
    }
}