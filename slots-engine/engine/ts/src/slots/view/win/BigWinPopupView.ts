module engine {
    import Layout = layout.Layout;
    import Text = createjs.Text;
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;
    import DisplayObject = createjs.DisplayObject;

    export class BigWinPopupView extends Layout {
        public static LAYOUT_NAME:string = "BigWinPopupView";

        public static FREE_SPINS_COUNT:string = "winTf";
        public static BG_COUNT:string = "bg";
        public static BG_COUNT_ALPHA:number = 0.7;
        private static INVISIBLE_TIME:number = 1.5;
        private static INVISIBLE_TIME_UP:number;

        private textsPos:boolean = false;
        public winTf:AnimationTextField;
        public winTfPlain:Text;
        public bg:DisplayObject;

        constructor() {
            super();
        }

        public onInit():void {
            BigWinPopupView.INVISIBLE_TIME_UP = Utils.float2int(BigWinPopupView.INVISIBLE_TIME * Ticker.getFPS());
            this.setTextPositions();
            this.winTfPlain = <Text>this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
            this.winTfPlain.shadow = new createjs.Shadow("#000000", 4, 4, 4);
            this.winTf = new AnimationTextField(this.winTfPlain);
            this.bg = <DisplayObject>this.getChildByName(BigWinPopupView.BG_COUNT);
            this.bg.alpha = BigWinPopupView.BG_COUNT_ALPHA;
            //this.alpha = 0;
        }

        public update():void{
            if (this.winTf != null){
                this.winTf.update();
            }
        }

        public setTextPositions(){
            if(!this.textsPos){
                var title:Text = <Text>this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
                if(title != null){
                    title.y += title.lineHeight;
                }
            }
            this.textsPos = true;
        }

        public setWinTf(value:number, updateTime:number = 0):void {
            //var tf:Text = <Text>this.getChildByName(BigWinPopupView.FREE_SPINS_COUNT);
            if (this.winTf != null) {
                this.winTf.setValue(value, updateTime);
            }
        }

        public show(callback:(...params:any[]) => any, time:number = 0):void {
            time = Utils.float2int(time * Ticker.getFPS());
            var tween:Tween = Tween.get(this, {useTicks:true, override:true});
            tween.to({alpha: 1}, time);
            tween.call(callback);
        }

        public hide(callback:(...params:any[]) => any, time:number = 0):void {
            time = Utils.float2int(time * Ticker.getFPS());
            var tween:Tween = Tween.get(this, {useTicks:true, override:true});
            tween.wait(time).to({alpha: 0}, BigWinPopupView.INVISIBLE_TIME_UP);
            tween.call(callback);
        }
    }
}