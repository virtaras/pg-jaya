/**
 * Created by Taras on 22.12.2014.
 */
module engine {
    import Layout = layout.Layout;

    export class DrawView extends Layout {

        public static LAYOUT_NAME:string = "DrawView";

        constructor() {
            super();
        }

    }
}
