module engine {
	export class BonusTypes {
		public static GAMBLE:string = "gamble";
		public static FREE_SPINS:string = "free_spins";
		public static MINI_GAME:string = "mini_game";
	}
}
