module engine {
	export class SoundsList {
		//-------------SOUND NAMES---------------------------------
		public static REGULAR_CLICK_BTN:string = "regular_click_btn";
		public static SPIN_CLICK_BTN:string = "spin_click_btn";

		public static REGULAR_SPIN_BG:string = "regular_spin_bg";
		public static AUTO_SPIN_BG:string = "auto_spin_bg";
		public static FREE_SPIN_BG:string = "free_spin_bg";
		public static PAY_TABLE_BG:string = "pay_table_bg";

		public static COLLECT:string = "collect";
		public static REEL_STOP:string = "reel_stop";
		public static FREE_SPIN_COLLECT:string = "free_spin_collect";

		public static SMALL_WIN_COUNTER:string = "small_win_counter";
		public static NORMAL_WIN_COUNTER:string = "normal_win_counter";
		public static MEDIUM_WIN_COUNTER:string = "medium_win_counter";
		public static BIG_WIN_COUNTER:string = "big_win_counter";
		public static COLOSSAL_WIN_COUNTER:string = "colossal_win_counter";
		public static AUTO_SPIN_WIN:string = "auto_spin_win";
		public static LINE_WIN_ENUM:string = "line_win_enum";

		public static WEEL_ROTATION:string = "weel_rotation";
		public static SECTOR_ANIMATION:string = "sector_animation";
		public static GLASS_ROTATE:string = "glass_sound";
		public static SCATER_START:string = "scatter_start";
	}
}