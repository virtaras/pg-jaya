module engine {
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;

	export class Utils {
		public static float2int(value:number):number {
			return value | 0;
		}

		public static toIntArray(value:Array<string>):Array<number> {
			var returnVal:Array<number> = new Array(value.length);
			for (var i:number = 0; i < value.length; i++) {
				returnVal[i] = parseInt(value[i]);
			}
			return returnVal;
		}

		public static getClassName(obj:any):string {
			var funcNameRegex = /function (.{1,})\(/;
			var results = (funcNameRegex).exec(obj["constructor"].toString());
			return (results && results.length > 1) ? results[1] : "";
		}

		public static fillDisplayObject(container:Container, prefix:string):Array<DisplayObject> {
			var i:number = 1;
			var item:DisplayObject;
			var result:Array<DisplayObject> = [];
			while ((item = container.getChildByName(prefix + i)) != null) {
				result.push(item);
				i++;
			}
			return result;
		}
	}
}