/**
 * Created by Taras on 24.03.2015.
 */

module engine {
    import Container = createjs.Container;
    import Sprite = createjs.Sprite;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;

    export class WeelController extends BaseController {
        //public static WIN_ANIMATION_PREFIX:string = "WinAnimation_";
        //public static WIN_ANIMATION_HIGHLIGHT_PREFIX:string = "SymbolHighlight";

        private common:CommonRefs;
        private view:WeelView;

        constructor(manager:ControllerManager, common:CommonRefs, view:WeelView) {
            super(manager);
            this.common = common;
            this.view = view;
        }

        public init():void {
            super.init();
            this.create();
        }

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            //notifications.push(NotificationList.SHOW_WIN_LINES);
            //notifications.push(NotificationList.REMOVE_WIN_LINES);
            //notifications.push(NotificationList.OPEN_PAY_TABLE);
            //notifications.push(NotificationList.CLOSE_PAY_TABLE);
            //notifications.push(NotificationList.HIDE_SYMBOLS);
            return notifications;
        }

        private create():void {

            this.view.create();
            //mask.alpha = 0.3;
            //this.view.addChild(mask);
        }


        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.SHOW_WIN_LINES:
                {
                    //this.create(data);
                    break;
                }
            }
        }

        public dispose():void {
            super.dispose();
            this.common = null;
        }


    }
}