module engine {
	import Sound = createjs.Sound;
	import SoundInstance = createjs.SoundInstance;

	export class SoundController extends BaseController {
		private common:CommonRefs;
		private regularSpinBg:SoundInstance = null;
		private autoSpinBg:SoundInstance = null;
		private freeSpinBg:SoundInstance = null;
		private rotateBg:SoundInstance = null;
		private rotateBgStart:SoundInstance = null;
		private payTable:SoundInstance = null;
		private winCounterSound:SoundInstance = null;
		private isShowAllLines:boolean;

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.TRY_START_AUTO_PLAY);
			notifications.push(NotificationList.END_AUTO_PLAY);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.SHOW_WIN_LINES);
			notifications.push(NotificationList.STOPPED_REEL_ID);
			notifications.push(NotificationList.STOPPED_REEL_ID_PREPARE);
			notifications.push(NotificationList.OPEN_OPTIONS_MENU);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.SHOW_LINES);
			notifications.push(NotificationList.SOUND_TOGGLE);
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.START_WEEL_ROTATE);
			notifications.push(NotificationList.STOP_WEEL_ROTATE);
			notifications.push(NotificationList.SCATTER_WIN);
			notifications.push(NotificationList.END_FREE_SPINS);
			notifications.push(NotificationList.PLAY_WIN_SOUND);
			notifications.push(NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SOUND_TOGGLE:
				{
					this.common.isSoundOn = !this.common.isSoundOn;
					Sound.setMute(!this.common.isSoundOn);
					break;
				}
				case NotificationList.TRY_START_AUTO_PLAY:
				{
					if(this.autoSpinBg == null){
						this.autoSpinBg = Sound.createInstance(SoundsList.AUTO_SPIN_BG);
						this.autoSpinBg.play({loop: -1});
					}
					break;
				}
				case NotificationList.END_AUTO_PLAY:
				{
					this.autoSpinBg.stop();
					this.autoSpinBg = null;
					break;
				}
				case NotificationList.START_SPIN:
				{
					//console.log();
					if (this.common.server.win > 0) {
						Sound.play(SoundsList.COLLECT);
					}
					if (this.autoSpinBg == null && this.freeSpinBg == null) {
						this.regularSpinBg = Sound.createInstance(SoundsList.REGULAR_SPIN_BG);
						this.regularSpinBg.play();
					}
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					if (this.autoSpinBg == null) {
						if(this.regularSpinBg != null) this.regularSpinBg.stop();
					}
					break;
				}
				case NotificationList.SHOW_WIN_LINES:
				{
					this.isShowAllLines = <boolean>data;
					var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
					if (winLinesVOs.length > 0) {
						if (this.autoSpinBg == null) {
							//Sound.play(StatsController.WIN_COUNTER_SOUND);
						}
						else {
							console.log('play free spin');
							Sound.play(SoundsList.AUTO_SPIN_WIN);
						}
					}
					break;
				}
				case NotificationList.PLAY_WIN_SOUND:
				{
					console.log("play_win_sound soundcontroller");
					if (this.winCounterSound != null) {
						this.winCounterSound.stop();
						this.winCounterSound = null;
					}
					this.winCounterSound = Sound.createInstance(StatsController.WIN_COUNTER_SOUND);
					this.winCounterSound.play();
					break;
				}
				case NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED:
				{
					if (this.winCounterSound != null) {
						this.winCounterSound.stop();
						this.winCounterSound = null;
					}
					break;
				}
				case NotificationList.STOPPED_REEL_ID_PREPARE:
				{
					if(!this.autoSpinBg){
						Sound.play(SoundsList.REEL_STOP);
					}
					break;
				}
				case NotificationList.OPEN_OPTIONS_MENU:
				{
					this.payTable = Sound.createInstance(SoundsList.PAY_TABLE_BG);
					this.payTable.play({loop: -1});
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.payTable.stop();
					break;
				}
				case NotificationList.SHOW_LINES:
				{
					if (!this.isShowAllLines) {
						Sound.play(SoundsList.LINE_WIN_ENUM);
					}
					break;
				}
				case NotificationList.START_BONUS:
				{
					this.freeSpinBg = Sound.createInstance(SoundsList.FREE_SPIN_BG);
					this.freeSpinBg.play({loop: -1});
					break;
				}
				case NotificationList.END_BONUS:
				{
					if (this.freeSpinBg != undefined && this.freeSpinBg != null) {
						this.freeSpinBg.stop();
						this.freeSpinBg = null;
					}
					if(this.common.server.bonus && this.common.server.bonus.className == "FreeSpinsController") {
						var bonusResult:SoundInstance = Sound.createInstance(SoundsList.FREE_SPIN_COLLECT);
						bonusResult.play();
					}
					break;
				}
				case NotificationList.START_WEEL_ROTATE:
				{
					if(this.rotateBgStart == null) {
						this.rotateBgStart = Sound.createInstance(SoundsList.GLASS_ROTATE);
						this.rotateBgStart.play({loop: -1});
					}
					break;
				}
				case NotificationList.SCATTER_WIN:
				{
					if(this.autoSpinBg != null){
						this.autoSpinBg.stop();
						this.autoSpinBg = null;
					}
					if (this.rotateBg == null) {
						this.rotateBg = Sound.createInstance(SoundsList.WEEL_ROTATION);
						this.rotateBg.play({loop: -1});
					}
					Sound.play(SoundsList.SCATER_START);
					break;
				}
				case NotificationList.STOP_WEEL_ROTATE:
				{
					if(this.rotateBgStart != null){
						this.rotateBgStart.stop();
						this.rotateBgStart = null;
					}

					if (this.rotateBg != null) {
						this.rotateBg.stop();
						this.rotateBg = null;
					}
					Sound.play(SoundsList.SECTOR_ANIMATION);
					//var sound:SoundInstance = Sound.createInstance(SoundsList.SECTOR_ANIMATION);
					//sound.play();
				}
			}
		}

		public dispose():void {
			super.dispose();
			this.common = null;
		}
	}
}