module engine {
    import Button = layout.Button;
    import Toggle = layout.Toggle;
    import Text = createjs.Text;
    import Sound = createjs.Sound;
    import Container = createjs.Container;
    import LayoutCreator = layout.LayoutCreator;
    import Point = createjs.Point;
    import Rectangle = createjs.Rectangle;
    import Shape = createjs.Shape;
    import Stage = createjs.Stage;
    import Layout = layout.Layout;
    import Sprite = createjs.Sprite;

    export class SettingMobileController extends BaseController {
        public static BG_INFO:string = "bg_info";
        public static BG_SIDEBAR:string = "bg_sidebar";

        private static BET_STRING_NAME:string = "bet";
        private static LINES_COUNT_STRING_NAME:string = "betLines";
        private static SOUND_TOGGLE:string = "soundSwitcher";

        public settingView:SettingMobileView;
        public settingViewHeight:number;
        private soundView:Container;
        private soundViewName:string;

        private payViews:Container;
        private viewsCount:number;
        private headerHeight:number;
        private totalHeight:number;
        private dragFactor:number = 5;
        private controlContainer:Layout;
        private hudContainer:Container;
        private stage:Stage;

        private allSettingsView:Container;
        private gameContainer:Container;

        public common:CommonRefs;
        private currentState:IState;
        private states:Object;
        private topScroll:number;
        private payViewsAdded:boolean = false;
        private bgScaleY:number = 1;

        constructor(manager:ControllerManager, common:CommonRefs, view:Container, name:string, hudContainer:Container, stage:Stage) {
            super(manager);
            this.common = common;
            this.settingView = this.common.layouts[name];
            this.gameContainer = view;
            this.topScroll = 0;
            this.viewsCount = this.common.config.viewsCount;
            this.hudContainer = hudContainer;
            this.stage = stage;
        }

        private payViewsAll:Array<Container> = [];

        public init() {
            super.init();
            this.allSettingsView = new Container();
            this.allSettingsView.visible = false;
            this.soundView = new Container();
            this.initContainers();
            this.settingView.create();
            this.soundViewName = this.settingView.getSoundTempl();
            this.gameContainer.addChild(this.allSettingsView);
            this.initButtons();
            this.settingViewHeight = this.settingViewHeight || this.settingView.getBounds().height;
        }

        private initContainers():void {
            for(var a:number = 0; a < this.viewsCount; a++){
                this.payViewsAll.push(new Container());
            }
            this.payViews = new Container();
        }

        private setupLandscapeMode():void {
            this.scaleSettings(false);
            this.payViews.y = 0;
            var topStart:number = 0;
            this.gameContainer.x = this.controlContainer.parent.x;

            this.payViews.visible = false;
            var screenSize:number = this.headerHeight * 1.8;

            var maskHeight:number = (this.common.isMobile) ? window.innerHeight : this.settingViewHeight / this.bgScaleY;
            var mask:Shape = new Shape();
            mask.graphics.beginFill("0").drawRect(0, 0, this.allSettingsView.getBounds().width * this.allSettingsView.scaleX, maskHeight);
            this.allSettingsView.mask = mask;
            //this.allSettingsView.hitArea = mask;

            var contain:SettingMobileController = this;
            contain.settingView.updateMove(1);
            var diff:number;

            var lastStageY = null;
            var bottomPoint:number = this.allSettingsView.getBounds().height;

            var doDrag = function(event):void{
                //console.log("contain.settingViewHeight= "+contain.settingViewHeight+" contain.settingView.scaleY="+contain.allSettingsView.scaleY+" contain.settingViewHeight="+contain.settingViewHeight+" contain.payViews.getBounds().height="+contain.payViews.getBounds().height+" this.settingView.getBounds().height="+contain.settingView.getBounds().height);
                if(contain.payViews.getBounds().height * contain.allSettingsView.scaleY > window.innerHeight) {
                    if (lastStageY == null) {
                        lastStageY = event.stageY;
                    } else {
                        diff = event.stageY - lastStageY;
                        //console.log("diff=" + diff + " contain.payViews.y=" + contain.payViews.y + " bottomPoint=" + bottomPoint + " contain.payViews.getBounds().height=" + contain.payViews.getBounds().height);
                        if ((contain.payViews.y + diff) <= (topStart) && (contain.payViews.y + diff + 100) >= (bottomPoint - contain.payViews.getBounds().height)) {
                            contain.payViews.y += diff;
                        } else {
                            if ((contain.payViews.y + diff) > (topStart)) {
                                contain.payViews.y = topStart;
                            } else {
                                if ((contain.payViews.y + diff) < (bottomPoint - contain.payViews.getBounds().height)) contain.payViews.y = bottomPoint - contain.payViews.getBounds().height - 100;
                            }
                        }
                        var indicator:number = Math.min(Math.abs(Math.floor(contain.payViews.y / screenSize)), 4);
                        //var indicator:number = Math.floor(contain.payViews.y / (contain.settingViewHeight - window.innerHeight) / 4) + 1;
                        contain.settingView.updateMove(indicator + 1);
                    }
                }

            };
            var onUp = function():void {
                lastStageY = null;
            };
            this.payViews.removeAllEventListeners("pressmove");
            this.payViews.removeAllEventListeners("pressup");
            this.payViews.on("pressmove", doDrag);
            this.payViews.on("pressup", onUp);

            for(var n:number = 1; n < 5; n++){
                this.payViewsAll[n].y = (n > 1) ? this.payViewsAll[n - 1].y + screenSize : screenSize;
            }

        }

        private setupPortraitMode():void {
            this.scaleSettings(true);
            var topStart:number = (this.headerHeight * 1.65);
            this.payViews.y = topStart;
            //var heightMask:number = this.allSettingsView.getBounds().height - topStart;
            var heightMask:number = window.innerHeight - topStart;
            var widthMask:number = this.allSettingsView.getBounds().width;

            var mask:Shape = new Shape();
            mask.graphics.beginFill("0").drawRect(0, topStart, widthMask, heightMask);
            this.payViews.mask = mask;
            this.payViews.hitArea = mask;

            var contain:SettingMobileController = this;
            var diff:number;
            var lastY:number;

            var lastStageY = null;
            //var bottomPoint:number = this.allSettingsView.getBounds().height - topStart;
            var bottomPoint:number = 900;
            var sliderPos:number = topStart;
            var payViewsToSliderHeight:number = 1/4;
            var newPayViewsPosY:number;

            var doDrag = function(event):void{
                if(lastStageY == null){
                    lastStageY = event.stageY;
                }else{
                    diff = event.stageY - lastStageY;
                    if((contain.payViews.y+diff) <= (topStart) && (contain.payViews.y+diff) >= (bottomPoint - contain.payViews.getBounds().height)) {
                        newPayViewsPosY = contain.payViews.y + diff;
                        sliderPos -= diff * payViewsToSliderHeight;
                    }else{
                        if((contain.payViews.y+diff) > (topStart)){
                            newPayViewsPosY = topStart;
                            sliderPos = topStart;
                        }
                        if((contain.payViews.y+diff) < (bottomPoint - contain.payViews.getBounds().height)){
                            newPayViewsPosY = bottomPoint - contain.payViews.getBounds().height;
                            sliderPos = bottomPoint+300;
                        }
                    }
                    contain.payViews.y = newPayViewsPosY;
                    contain.settingView.updateMove(sliderPos);
                }
            };
            var onUp = function():void {
                lastStageY = null;
            };
            this.payViews.removeAllEventListeners("pressmove");
            this.payViews.removeAllEventListeners("pressup");
            this.payViews.on("pressmove", doDrag);
            this.payViews.on("pressup", onUp);

            this.payViewsAll[1].y = this.headerHeight * 3;
            this.payViewsAll[2].y = this.payViewsAll[1].y + this.headerHeight * 4.3;
            this.payViewsAll[3].y = this.payViewsAll[2].y + this.headerHeight * 4;
            this.payViewsAll[4].y = this.payViewsAll[3].y + (this.headerHeight * 2.7);
        }

        private initSoundToggle():void {
            var soundToggle:Sprite = <Sprite>this.soundView.getChildByName(SettingMobileController.SOUND_TOGGLE);
            if (soundToggle != null) {
                if(!this.common.isSoundOn)soundToggle.gotoAndStop(1);
                else soundToggle.gotoAndStop(0);
                soundToggle.removeAllEventListeners("click");
                soundToggle.on("click", (eventObj:any)=> {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        if (!this.common.isSoundLoaded) {
                            this.send(NotificationList.LOAD_SOUNDS);
                        }
                        if(this.common.isSoundOn) soundToggle.gotoAndStop(1);
                        else soundToggle.gotoAndStop(0);
                        this.send(NotificationList.SOUND_TOGGLE);
                        Sound.play(SoundsList.REGULAR_CLICK_BTN);
                    }
                });
            }
        }

        private createPage():void {
            console.log("createPage");
            this.createPayViews();
            this.clearSoundCont();

            var settingView:LayoutCreator = this.common.layouts[this.soundViewName];
            settingView.create(this.soundView);
            this.headerHeight = this.soundView.getBounds().height;
            this.soundView.getChildByName(SettingMobileController.SOUND_TOGGLE).scaleY = 1.1; // special for iphone5 and iPod, that resize toggle if scale=1

            this.allSettingsView.addChild(this.settingView);
            this.settingView.resizeForMobile();
            this.allSettingsView.addChild(this.payViews);
            this.allSettingsView.addChild(this.soundView);
            this.soundView.visible = true;

            if(this.settingView.isPortrait)this.setupPortraitMode();
            else this.setupLandscapeMode();
            this.updateOptions();

            var hitArea:Shape = new Shape();
            hitArea.graphics.beginFill("0").drawRect(this.payViews.x, this.payViews.y, this.payViews.getBounds().width, this.payViews.getBounds().height);
            this.payViews.hitArea = hitArea;

            this.totalHeight = this.payViewsAll[this.payViewsAll.length - 1].y;
            this.initSoundToggle();
        }

        private scaleSettings(isPortait:boolean):void{
            //this.settingViewHeight = this.settingViewHeight || this.settingView.getBounds().height;
            //var scaleSettingContainer:number = this.hudContainer.getBounds().width * this.hudContainer.scaleX / (this.settingView.getBounds().width);
            var scaleSettingContainer:number;
            if(this.common.isMobile) {
                if(window.innerWidth > window.innerHeight) {
                    scaleSettingContainer = this.hudContainer.scaleX * Math.min(this.common.artWidth / this.settingView.getBounds().width, this.common.artHeight / this.settingViewHeight);
                }else{
                    scaleSettingContainer = Math.min(window.innerWidth / this.settingView.getBounds().width, window.innerHeight / this.settingViewHeight);
                }
                this.bgScaleY = window.innerHeight / this.settingViewHeight / scaleSettingContainer;
            }else{
                scaleSettingContainer = Math.min(this.hudContainer.getBounds().width / this.settingView.getBounds().width, this.hudContainer.getBounds().height / this.settingViewHeight);
                this.bgScaleY = this.hudContainer.getBounds().height / this.settingViewHeight / scaleSettingContainer;
            }
            this.allSettingsView.scaleX = this.allSettingsView.scaleY = scaleSettingContainer;

            console.log("window.innerHeight="+window.innerHeight+" this.settingView.getBounds().height="+this.settingView.getBounds().height+" this.allSettingsView.scaleY="+this.allSettingsView.scaleY+" this.bgScaleY="+this.bgScaleY);
            this.settingView.getChildByName(SettingMobileController.BG_INFO).scaleY = this.bgScaleY;
            this.settingView.getChildByName(SettingMobileController.BG_SIDEBAR).scaleY = this.bgScaleY;

            var options_btn:Button = this.settingView.getBtn(SettingMobileView.OPTIONS_BTN);
            if(options_btn) options_btn.y = 0.2 * this.settingViewHeight * this.settingView.scaleX;
            var pay_btn:Button = this.settingView.getBtn(SettingMobileView.PAY_BTN);
            if(pay_btn) pay_btn.y = 0.7 * this.settingViewHeight * this.settingView.scaleY;
        }

        private createPayViews():void {
            var payViewName:string;
            this.totalHeight = 0;
            if(!this.payViewsAdded) {
                for (var a:number = 0; a < this.payViewsAll.length; a++) {
                    var viewCont:Container = this.payViewsAll[a];
                    payViewName = this.settingView.payViews[a];
                    var payView:LayoutCreator = this.common.layouts[payViewName];
                    payView.create(viewCont);
                    this.payViews.addChild(viewCont);
                }
                this.payViewsAdded = true;
            }
        }

        private updateOptions():void {
            var bet:Text = <Text>this.soundView.getChildByName(SettingMobileController.BET_STRING_NAME);
            bet.text = MoneyFormatter.format(this.common.server.bet, true);
            var lines:Text = <Text>this.soundView.getChildByName(SettingMobileController.LINES_COUNT_STRING_NAME);
            lines.text = this.common.server.lineCount.toString()
        }
        private clearSoundCont():void {
            while (this.soundView.getNumChildren() > 0) {
                this.soundView.removeChildAt(0);
            }
        }

        private clearPageContainer():void {
            this.gameContainer.removeAllChildren();
        }

        private initOptState():void {
            this.states = {};
            this.states[OptionsState.NAME] = new OptionsState(this);
            this.changeState(OptionsState.NAME);
        }

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.OPEN_OPTIONS_MENU);
            notifications.push(NotificationList.CLOSE_PAY_TABLE);
            notifications.push(NotificationList.INIT_OPTIONS_STATE);
            notifications.push(NotificationList.TRY_START_SPIN);
            notifications.push(NotificationList.CHANGE_DEVICE_ORIENTATION);
            return notifications;
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.CLOSE_PAY_TABLE:
                {
                    this.tryClosePayTables();
                    break;
                }
                case NotificationList.OPEN_OPTIONS_MENU:
                {
                    this.controlContainer = data;
                    this.createPage();
                    this.allSettingsView.visible = true;
                    break;
                }
                case NotificationList.INIT_OPTIONS_STATE:
                {
                    this.initOptState();
                    break;
                }
                case NotificationList.TRY_START_SPIN:
                {
                    if(this.allSettingsView.visible){
                        this.send(NotificationList.CLOSE_PAY_TABLE);
                    }
                    break;
                }
                case NotificationList.CHANGE_DEVICE_ORIENTATION:
                {
                    this.tryClosePayTables();
                    break;
                }
            }
        }

        private removeHandlers():void {
            var buttonsNames:Array<string> = this.settingView.buttonsNames;
            var buttonsCount:number = buttonsNames.length;
            for (var i:number = 0; i < buttonsCount; i++) {
                var button:Button = this.settingView.getBtn(buttonsNames[i]);
                if(button && button != null)button.removeAllEventListeners("click");
            }
        }


        private initButtons():void {
            var buttonsNames:Array<string> = this.settingView.buttonsNames;
            for (var i:number = 0; i < buttonsNames.length; i++) {
                var buttonName:string = buttonsNames[i];
                var button:Button = this.settingView.getBtn(buttonName);
                if (button == null) {
                    //console.log("WARNING: Button with name = " + buttonName + " is null");
                    continue;
                }
                button.on("click", (eventObj:any)=> {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        this.onBtnClick(eventObj.currentTarget.name);
                    }
                })
            }
        }

        public onBtnClick(buttonName:string):void {
            console.log("onBtnClick "+buttonName);
            switch (buttonName) {
                case SettingMobileView.CLOSE_BTN:
                {
                    this.tryClosePayTables();
                    break;
                }
                case SettingMobileView.PAY_BTN:
                {
                    this.soundView.visible = false;
                    this.payViews.visible = true;
                    this.settingView.toggleIndicator(true);
                    break;
                }
                case SettingMobileView.OPTIONS_BTN:
                {
                    this.soundView.visible = true;
                    this.payViews.visible = false;
                    this.settingView.toggleIndicator(false);
                    break;
                }
            }
        }

        public changeState(name:string, data:any = null):void {
            console.log('init state ' + name);
            if (this.currentState != null) {
                this.currentState.end();
            }
            this.currentState = this.states[name];
            this.currentState.start(data);
        }

        public dispose():void {
            this.removeHandlers();
            super.dispose();
            this.common = null;
            this.clearPageContainer();
        }

        public tryClosePayTables():void{
            if(this.allSettingsView.visible) {
                this.allSettingsView.visible = false;
                this.send(NotificationList.CLOSE_PAY_TABLE);
            }
        }
    }
}