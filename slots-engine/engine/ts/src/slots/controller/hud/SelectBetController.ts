/**
 * Created by Taras on 10.02.2015.
 */

module engine {
    import Container = createjs.Container;
    import Rectangle = createjs.Rectangle;
    import Ticker = createjs.Ticker;
    import Tween = createjs.Tween;
    import Shape = createjs.Shape;
    import Text = createjs.Text;
    import Button = layout.Button;

    export class SelectBetController extends BaseController {
        private static ITEM_PREFIX:string = "Bet";
        private static ITEM_POSTFIX:string = "Btn";
        private static ITEM_TEXT_PREFIX:string = "item_value_";

        private static OPEN_TIME:number;
        private static CLOSE_TIME:number;

        private common:CommonRefs;
        private view:Container;
        private maskView:Container;
        private startY:number;
        private isOpen:boolean;
        private isNotPortr:boolean;

        private itemsBtn:Array<Button>;
        private itemsText:Array<Text>;

        constructor(manager:ControllerManager, common:CommonRefs, view:Container, maskView:Container) {
            super(manager);
            this.common = common;
            this.view = view;
            this.maskView = maskView;
            SelectBetController.OPEN_TIME = Utils.float2int(0.3 * Ticker.getFPS());
            SelectBetController.CLOSE_TIME = Utils.float2int(0.3 * Ticker.getFPS());
            this.view.alpha = 0;
        }

        public init():void {
            super.init();
            this.setupMask();
            this.initItems();
            this.isOpen = true;
            this.close(false);
        }

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.OPEN_SELECT_BET_TABLE);
            notifications.push(NotificationList.SET_BET);
            notifications.push(NotificationList.TRY_START_SPIN);
            notifications.push(NotificationList.OPEN_AUTO_SPIN_MENU);
            notifications.push(NotificationList.CHANGE_DEVICE_ORIENTATION);
            return notifications;
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.OPEN_SELECT_BET_TABLE:
                {
                    this.toggle();
                    break;
                }
                case NotificationList.OPEN_AUTO_SPIN_MENU:
                case NotificationList.TRY_START_SPIN:
                case NotificationList.SET_BET:
                case NotificationList.CHANGE_DEVICE_ORIENTATION:
                {
                    this.close(true);
                    break;
                }
                //case NotificationList.OPEN_PAY_TABLE:
                //{
                //    this.close(false);
                //    break;
                //}
            }
        }

        private toggle():void {
            if (this.isOpen) {
                this.close(true);
            }
            else {
                this.open(true);
            }
        }


        private initItems():void {
            var i:number;
            var selectBetCount:Array<number> = <Array<number>>this.common.config.selectBetCount;

            this.itemsBtn = new Array(selectBetCount.length);
            this.itemsText = new Array(selectBetCount.length);


            for (var i:number = 1; i <= this.itemsBtn.length; i++) {
                var button:Button = <Button>this.view.getChildByName(SelectBetController.ITEM_PREFIX + i + SelectBetController.ITEM_POSTFIX);
                var bounds:Rectangle = button.getBounds();
                var hitArea:Shape = new Shape();
                hitArea.graphics.beginFill("0").drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
                button.hitArea = hitArea;
                button.removeAllEventListeners("click");
                button.on("click", (eventObj:any)=>{
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        var target:Button = eventObj.currentTarget;
                        var id:number = parseInt(target.name.substring(SelectBetController.ITEM_PREFIX.length, target.name.length - SelectBetController.ITEM_POSTFIX.length));
                        var bet:number = parseFloat(this.itemsText[id - 1].text);
                        this.send(NotificationList.SET_BET, bet);
                    }
                });

                var itemValue:Text = <Text>this.view.getChildByName(SelectBetController.ITEM_TEXT_PREFIX + i);
                itemValue.text = selectBetCount[selectBetCount.length - i].toString();
                itemValue.y = button.y + bounds.height / 1.5;

                this.itemsBtn[i - 1] = button;
                this.itemsText[i - 1] = itemValue;
            }
        }

        private setupMask():void {
            this.maskView.visible = false;
            //this.maskView.parent.removeChild(this.maskView);
            var bounds:Rectangle = this.maskView.getBounds();
            var mode:string = this.common.rotateHudMode;
            this.isNotPortr = (mode == 'HudView' || !this.common.isMobile);
            var mask:Shape = new Shape();
            if(this.isNotPortr){
                mask.graphics.beginFill("0").drawRect(this.maskView.x + bounds.x, this.maskView.y, bounds.width * 1.5, bounds.height * 1.75);
            }else {
                mask.graphics.beginFill("0").drawRect(this.view.x, this.view.y + bounds.y, bounds.width * 2, bounds.height * 2);
            }
            this.view.mask = mask;
            this.startY = (this.isNotPortr) ? this.view.y * 1.20 : this.view.y;
        }

        private close(animation:boolean):void {
            var endPos:number = (this.isNotPortr) ? this.startY + this.view.getBounds().height : this.startY - this.view.getBounds().height ;
            if (animation) {
                var tween:Tween = Tween.get(this.view, {useTicks: true});
                tween.to({y: endPos}, SelectBetController.CLOSE_TIME).call(()=>{this.view.alpha = 0;});
            }
            else {
                this.view.y = endPos;
            }
            this.isOpen = false;
        }

        private open(animation:boolean):void {
            if (animation) {
                var tween:Tween = Tween.get(this.view, {useTicks: true});
                tween.to({y: this.startY}, SelectBetController.OPEN_TIME);
            }
            else {
                this.view.y = this.startY;
            }
            this.isOpen = true;
            this.view.alpha = 1;
        }

        public dispose():void {
            super.dispose();
        }
    }
}