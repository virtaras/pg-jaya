module engine {
	import Text = createjs.Text;
	import Ticker = createjs.Ticker;
	import Container = createjs.Container;

	export class StatsController extends BaseController {
		public static WIN_COUNTER_SOUND:string = SoundsList.SMALL_WIN_COUNTER;
		public static WIN_TO_BET_POPUP_RELATION:number = 0;

		private static BIG_WIN_POPUP_DELAY:number = 7.5;
		private static COLOSSAL_WIN_POPUP_DELAY:number = 9.5;
		private static SHOW_TIME:number = 1.5;
		private static HIDE_TIME:number = 0.3;

		private static SMALL_WIN_COUNTER_TIME:number = 1.5; // tr : 1.5
		private static NORMAL_WIN_COUNTER_TIME:number = 3; // tr : 3
		private static MEDIUM_WIN_COUNTER_TIME:number = 3; // tr : 3
		private static BIG_WIN_COUNTER_TIME:number = 6;
		private static COLOSSAL_WIN_COUNTER_TIME:number = 8;

		private static SMALL_WIN_COUNTER_FROM:number = 0;
		private static SMALL_WIN_COUNTER_TO:number = 1;
		private static NORMAL_WIN_COUNTER_FROM:number = 1;
		private static NORMAL_WIN_COUNTER_TO:number = 3;
		private static MEDIUM_WIN_COUNTER_FROM:number = 3;
		private static MEDIUM_WIN_COUNTER_TO:number = 6;
		private static BIG_WIN_COUNTER_FROM:number = 6;
		private static BIG_WIN_COUNTER_TO:number = 12;
		private static COLOSSAL_WIN_COUNTER_FROM:number = 12;

		public common:CommonRefs;
		public view:HudView;
		public bigWinPopupView:BigWinPopupView;
		public colossalWinPopupView:ColossalWinPopupView;
		public winTf:AnimationTextField;
		public balanceTf:AnimationTextField;
		public container:Container;

		private currentWin:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, view:HudView, container:Container) {
			super(manager);
			this.common = common;
			this.view = view;
			this.container = container;
			//StatsController.SHOW_TIME = Utils.float2int(StatsController.SHOW_TIME * Ticker.getFPS());
			StatsController.HIDE_TIME = Utils.float2int(StatsController.HIDE_TIME * Ticker.getFPS());

			var self = this;
			this.bigWinPopupView = <BigWinPopupView>this.common.layouts[BigWinPopupView.LAYOUT_NAME];
			this.bigWinPopupView.create();
			this.bigWinPopupView.winTf.setValue(0);
			self.bigWinPopupView.removeAllEventListeners("click");
			this.bigWinPopupView.on('click',function(eventObj:any){
				if (eventObj.nativeEvent instanceof MouseEvent) {
					console.log("bigWinPopupView clicked");
					self.bigWinPopupView.winTf.setValue(self.currentWin);
					self.bigWinPopupView.hide(()=> {
						self.hideBigWinPopupView(self.view.getBtn(HudView.START_SPIN_BTN).getEnable());
					}, 0.1);
				}
			});

			this.colossalWinPopupView = <ColossalWinPopupView>this.common.layouts[ColossalWinPopupView.LAYOUT_NAME];
			this.colossalWinPopupView.create();
			this.colossalWinPopupView.winTf.setValue(0);
			this.colossalWinPopupView.removeAllEventListeners("click");
			this.colossalWinPopupView.on('click',function(eventObj:any){
				if (eventObj.nativeEvent instanceof MouseEvent) {
					console.log("colossalWinPopupView clicked");
					self.colossalWinPopupView.winTf.setValue(self.currentWin);
					self.colossalWinPopupView.hide(()=> {
						self.hideColossalWinPopupView(self.view.getBtn(HudView.START_SPIN_BTN).getEnable());
					}, 1.5);
				}
			});
			this.bigWinPopupView.alpha = 0;
			this.colossalWinPopupView.alpha = 0;
			//this.container.addChild(this.bigWinPopupView);
			//this.container.addChild(this.colossalWinPopupView);

			console.log("StatsController constructor");
		}

		public init():void {
			super.init();
			this.resetTf();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WIN_TF);
			notifications.push(NotificationList.COLLECT_WIN_TF);
			notifications.push(NotificationList.UPDATE_BALANCE_TF);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.WIN_POPUP_SHOWED);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.FREESPINS_SHOW_LEFT);
			return notifications;
		}

		public onEnterFrame():void {
			this.winTf.update();
			this.balanceTf.update();

			this.bigWinPopupView.winTf.update();
			this.colossalWinPopupView.winTf.update();
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.FREESPINS_SHOW_LEFT:
				{
					this.view.setText(HudView.AUTO_SPIN_TEXT, HudView.AUTO_SPIN_TEXT_FREESPINS);
					this.view.setText(HudView.AUTO_SPINS_COUNT_TEXT, data);
					break;
				}
				case NotificationList.START_BONUS:
				{
					this.view.setText(HudView.AUTO_SPIN_TEXT, HudView.AUTO_SPIN_TEXT_FREESPINS);
					this.view.setText(HudView.AUTO_SPINS_COUNT_TEXT, "");
					//this.view.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT).alpha = 1;
					break;
				}
				case NotificationList.END_BONUS:
				{
					this.view.setText(HudView.AUTO_SPIN_TEXT, HudView.AUTO_SPIN_TEXT_START);
					//this.view.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT).alpha = 0;
					this.view.setText(HudView.AUTO_SPINS_COUNT_TEXT, "");
					break;
				}
				case NotificationList.UPDATE_BALANCE_TF:
				{
					this.updateBalanceText();
					break;
				}
				case NotificationList.SHOW_WIN_TF:
				{
					if(data != null) this.winTf.setValue(data);
					else this.updateWinText();
					break;
				}
				case NotificationList.COLLECT_WIN_TF:
				{
					this.collectWinText();
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				case NotificationList.START_SPIN:
				{
					this.common.is_win_popup_showed = false;
					this.common.is_win_lines_showed = false;
					break;
				}
				case NotificationList.WIN_POPUP_SHOWED:
				{
					console.log("WIN_POPUP_SHOWED");
					this.common.is_win_popup_showed = true;
					this.tryProcessWinLinesAndPopupShown();
					break;
				}
				case NotificationList.WIN_LINES_SHOWED:
				{
					this.common.is_win_lines_showed = true;
					this.tryProcessWinLinesAndPopupShown();
					break;
				}

			}
		}

		public tryProcessWinLinesAndPopupShown():void{
			console.log(this.common.is_win_lines_showed+" "+this.common.is_win_popup_showed, "tryProcessWinLinesAndPopupShown");
			if(this.common.is_win_lines_showed && this.common.is_win_popup_showed){
				this.send(NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED);
				//this.common.is_win_lines_showed = false;
				//this.common.is_win_popup_showed = false;
			}
		}

		public collectWinText():void {
			this.winTf.setValue(0, StatsController.HIDE_TIME);
		}

		public updateBalanceText():void {
			this.balanceTf.setValue(this.common.server.getBalance(), Utils.float2int(StatsController.SHOW_TIME * Ticker.getFPS()));
		}

		public updateWinText():void {
			//this.common.is_win_popup_showed = true;
			this.bigWinPopupView.winTf.setValue(0);
			this.colossalWinPopupView.winTf.setValue(0);
			var win_to_bet:number = Math.abs(this.common.server.win / this.common.server.getTotalBet());
			console.log("bet="+this.common.server.getTotalBet()+"; value="+this.common.server.win+" time="+StatsController.SHOW_TIME+"; win_to_bet="+win_to_bet,"setValue");

			//this.showBigWinPopupView(this.common.server.win, StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS());
			//return;

			switch(true){
				case (win_to_bet>StatsController.SMALL_WIN_COUNTER_FROM && win_to_bet<StatsController.SMALL_WIN_COUNTER_TO):
				{
					StatsController.WIN_COUNTER_SOUND = SoundsList.SMALL_WIN_COUNTER;
					this.winTf.setValue(this.common.server.win, Utils.float2int(StatsController.SMALL_WIN_COUNTER_TIME * Ticker.getFPS()));
					this.send(NotificationList.WIN_POPUP_SHOWED);
					break;
				}
				case (win_to_bet>=StatsController.NORMAL_WIN_COUNTER_FROM && win_to_bet<StatsController.NORMAL_WIN_COUNTER_TO):
				{
					StatsController.WIN_COUNTER_SOUND = SoundsList.NORMAL_WIN_COUNTER;
					this.winTf.setValue(this.common.server.win, Utils.float2int(StatsController.NORMAL_WIN_COUNTER_TIME * Ticker.getFPS()));
					var self = this;
					setTimeout(function(){self.send(NotificationList.WIN_POPUP_SHOWED);}, (StatsController.NORMAL_WIN_COUNTER_TIME+1)*1000);
					break;
				}
				case (win_to_bet>=StatsController.MEDIUM_WIN_COUNTER_FROM && win_to_bet<StatsController.MEDIUM_WIN_COUNTER_TO):
				{
					StatsController.WIN_COUNTER_SOUND = SoundsList.MEDIUM_WIN_COUNTER;
					this.winTf.setValue(this.common.server.win, Utils.float2int(StatsController.MEDIUM_WIN_COUNTER_TIME * Ticker.getFPS()));
					var self = this;
					setTimeout(function(){self.send(NotificationList.WIN_POPUP_SHOWED);}, (StatsController.MEDIUM_WIN_COUNTER_TIME+1)*1000);
					break;
				}
				case (win_to_bet>=StatsController.BIG_WIN_COUNTER_FROM && win_to_bet<StatsController.BIG_WIN_COUNTER_TO):
				{
					StatsController.WIN_COUNTER_SOUND = SoundsList.BIG_WIN_COUNTER;
					//this.winTf.setValue(0);
					this.winTf.setValue(this.common.server.win, Utils.float2int(StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS()));
					this.showBigWinPopupView(this.common.server.win, StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS());
					break;
				}
				case (win_to_bet>=StatsController.COLOSSAL_WIN_COUNTER_FROM):
				{
					StatsController.WIN_COUNTER_SOUND = SoundsList.COLOSSAL_WIN_COUNTER;
					//this.winTf.setValue(0);
					this.winTf.setValue(this.common.server.win, Utils.float2int(StatsController.COLOSSAL_WIN_COUNTER_TIME * Ticker.getFPS()));
					this.showColossalWinPopupView(this.common.server.win, StatsController.COLOSSAL_WIN_COUNTER_TIME * Ticker.getFPS());
					break;
				}
				default:
				{
					StatsController.WIN_COUNTER_SOUND = SoundsList.SMALL_WIN_COUNTER;
					this.send(NotificationList.WIN_POPUP_SHOWED);
					break;
				}
			}
			if(this.common.server.win) this.send(NotificationList.PLAY_WIN_SOUND);
			//this.showColossalWinPopupView(7777, StatsController.COLOSSAL_WIN_COUNTER_TIME * Ticker.getFPS());
			//this.showBigWinPopupView(999, StatsController.BIG_WIN_COUNTER_TIME * Ticker.getFPS());
			//this.winTf.setValue(0);
			//this.winTf.setValue(this.common.server.win, StatsController.SHOW_TIME);
		}

		public hideBigWinPopupView(enableSpinBtn:boolean = true){
			enableSpinBtn = true;
			this.container.removeChild(this.bigWinPopupView);
			this.bigWinPopupView.alpha = 0;
			this.view.changeButtonState(HudView.START_SPIN_BTN, this.view.getBtn(HudView.START_SPIN_BTN).visible, enableSpinBtn);
			this.send(NotificationList.WIN_POPUP_SHOWED);
		}
		public hideColossalWinPopupView(enableSpinBtn:boolean = true){
			enableSpinBtn = true;
			this.container.removeChild(this.colossalWinPopupView);
			this.colossalWinPopupView.alpha = 0;
			this.view.changeButtonState(HudView.START_SPIN_BTN, this.view.getBtn(HudView.START_SPIN_BTN).visible, enableSpinBtn);
			this.send(NotificationList.WIN_POPUP_SHOWED);
		}

		public showBigWinPopupView(value:number, updateTime:number = 0):void{
			this.currentWin = value;
			var self = this;
			this.bigWinPopupView.alpha = 1;
			this.bigWinPopupView.winTf.setValue(value, updateTime);
			this.container.addChild(this.bigWinPopupView);
			this.bigWinPopupView.alpha = 1;
			//console.log("this.bigWinPopupView.alpha",this.bigWinPopupView.alpha);
			this.bigWinPopupView.hide(()=> {this.hideBigWinPopupView(this.view.getBtn(HudView.START_SPIN_BTN).getEnable());}, StatsController.BIG_WIN_POPUP_DELAY);
			this.view.changeButtonState(HudView.START_SPIN_BTN, this.view.getBtn(HudView.START_SPIN_BTN).visible, false);
		}
		public showColossalWinPopupView(value:number, updateTime:number = 0):void{
			this.currentWin = value;
			var self = this;
			this.colossalWinPopupView.alpha = 1;
			this.colossalWinPopupView.winTf.setValue(value, updateTime);
			this.container.addChild(this.colossalWinPopupView);
			this.colossalWinPopupView.alpha = 1;
			this.colossalWinPopupView.hide(()=> {this.hideColossalWinPopupView(this.view.getBtn(HudView.START_SPIN_BTN).getEnable());}, StatsController.COLOSSAL_WIN_POPUP_DELAY);
			this.view.changeButtonState(HudView.START_SPIN_BTN, this.view.getBtn(HudView.START_SPIN_BTN).visible, false);
		}

		public setView(view:HudView):void{
			this.view = view;
			this.resetTf();
		}

		public resetTf():void{
			this.winTf = new AnimationTextField(this.view.getText(HudViewP.WIN_TEXT));
			this.winTf.setValue(0);

			this.balanceTf = new AnimationTextField(this.view.getText(HudViewP.BALANCE_TEXT));
			this.balanceTf.setValue(this.common.server.getBalance());
		}
	}
}