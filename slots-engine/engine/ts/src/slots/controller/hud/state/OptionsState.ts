module engine {
    export class OptionsState implements IState {

        public static NAME:string = "OptionsState";

        private controller:SettingMobileController;
        private common:CommonRefs;
        private view:SettingMobileView;

        constructor(controller:SettingMobileController) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.settingView;
        }

        public start(data:any):void {
            // update buttons
            this.view.changeButtonState(SettingMobileView.CLOSE_BTN, true, true);
            this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.CLOSE_PAY_TABLE:
                {
                    //this.controller.changeState(DefaultState.NAME);
                    break;
                }
                case NotificationList.OPEN_OPTIONS_MENU:
                {
                    //this.view.visible = true;
                    break;
                }

            }
        }

        public onBtnClick(buttonName:string):void {
            switch (buttonName) {
                case SettingMobileView.CLOSE_BTN:
                {
                    this.controller.settingView.visible = false;
                    console.log('close BTN opt');
                    break;
                }
            }
        }

        public end():void {

        }

        public setView(view:HudView):void{
        }

        public afterRotate():void{
        }
    }
}