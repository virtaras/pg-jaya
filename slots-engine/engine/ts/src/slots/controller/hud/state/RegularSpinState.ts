module engine {
	export class RegularSpinState implements IState {
		public static NAME:string = "RegularSpinState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.setView(controller.view);
		}

		public start(data:any):void {
			this.afterRotate();

			this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
			this.controller.send(NotificationList.COLLECT_WIN_TF);
			this.controller.send(NotificationList.REMOVE_WIN_LINES);
			this.controller.send(NotificationList.START_SPIN);
			this.controller.send(NotificationList.SERVER_SEND_SPIN);
		}

		public afterRotate():void{
			this.view.changeButtonState(HudView.SETTINGS_BTN, true, false);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
			//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);
			this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, true, true);
			this.view.changeButtonState(HudView.BACK_TO_LOBBY_BTN, true, false);

			console.log("afterRotate", "RegularSpinState");
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.KEYBOARD_CLICK:
				{
					if (data == Keyboard.SPACE) {
						this.stopSpin();
					}
					break;
				}
				case NotificationList.ON_SCREEN_CLICK:
				{
					this.stopSpin();
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{

					var bonus:BonusVO = this.common.server.bonus;

					if (bonus == null || bonus.type == BonusTypes.GAMBLE) {
						this.controller.changeState(DefaultState.NAME);
						this.controller.send(NotificationList.SHOW_WIN_LINES,false);
					}
					else {
						this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
						this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
						this.controller.send(NotificationList.SHOW_WIN_LINES, true);
					}
					this.controller.send(NotificationList.SHOW_WIN_TF);
					this.controller.send(NotificationList.UPDATE_BALANCE_TF);
					break;
				}
				case NotificationList.WIN_LINES_SHOWED:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus != null && bonus.type != BonusTypes.GAMBLE) {
						this.controller.changeState(BonusState.NAME);
					}
					this.view.setWin(this.common.server.win);
					break;
				}
				case NotificationList.EXPRESS_STOP:
				{
					this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.STOP_SPIN_BTN:
				{
					this.stopSpin();
					break;
				}
			}
		}

		private stopSpin():void {
			this.controller.send(NotificationList.EXPRESS_STOP);
		}

		public end():void {
		}

		public setView(view:HudView):void{
			this.view = view;
		}
	}
}