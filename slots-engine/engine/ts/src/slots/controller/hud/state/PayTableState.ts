module engine {
	export class PayTableState implements IState {
		public static NAME:string = "PayTableState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.setView(controller.view);
		}

		public start(data:any):void {
			// update buttons
			//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, !this.common.isMobile, false);
			this.afterRotate();

			if (this.common.config.showAllTimeSpinCount) {
				this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT).visible = false;
			}
			this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
			this.controller.send(NotificationList.OPEN_PAY_TABLE);
		}

		public afterRotate():void{
			this.view.changeButtonState(HudView.GAMBLE_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.START_SPIN_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.SETTINGS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.controller.changeState(DefaultState.NAME);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
		}

		public end():void {
			if (this.common.config.showAllTimeSpinCount) {
				this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT).visible = true;
			}
		}

		public setView(view:HudView):void{
			this.view = view;
		}
	}
}