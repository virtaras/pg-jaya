module engine {
	import Text = createjs.Text;
	import Button = layout.Button;

	export class AutoSpinState implements IState {
		public static NAME:string = "AutoSpinState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;
		private autoSpinCount:number;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.setView(controller.view);
		}

		public start(data:any):void {
			this.autoSpinCount = data;

			//var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
			//autoPlayText.visible = true;

			this.afterRotate();

			this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
			this.controller.send(NotificationList.REMOVE_WIN_LINES);
			this.controller.tryStartSpin();

			this.view.setText(HudView.AUTO_SPIN_TEXT, HudView.AUTO_SPIN_TEXT_STOP);
			this.view.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT).alpha = 1;
		}

		public afterRotate():void{
			this.view.changeButtonState(HudView.SETTINGS_BTN, true, false);
			this.view.changeButtonState(HudView.TOTAL_BET_BTN, false, false);
			//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, true, true);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);

			console.log("afterRotate", "AutoSpinState");
			this.view.getText(HudView.AUTO_SPIN_TEXT).text = HudView.AUTO_SPIN_TEXT_STOP;
			this.updateAutoSpinCountText();
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.WIN_POPUP_SHOWED:
				{
					//this.controller.tryStartSpin();
					break;
				}
				case NotificationList.TRY_START_SPIN:
				{
					if(this.common.is_win_popup_showed === true) {
						this.controller.send(NotificationList.REMOVE_WIN_LINES);
						this.autoSpinCount--;
						this.updateAutoSpinCountText();
						this.controller.send(NotificationList.COLLECT_WIN_TF);
						this.controller.send(NotificationList.START_SPIN);
						this.controller.send(NotificationList.SERVER_SEND_SPIN);
						break;
					}
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus == null || bonus.type == BonusTypes.GAMBLE) {
						this.controller.send(NotificationList.SHOW_WIN_LINES, true);
					}
					else {
						this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
						//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
						this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
						this.controller.send(NotificationList.SHOW_WIN_LINES, true);
					}
					this.controller.send(NotificationList.SHOW_WIN_TF);
					this.controller.send(NotificationList.UPDATE_BALANCE_TF);
					break;
				}
				case NotificationList.HIDE_SYMBOLS:
				{
					this.controller.send(NotificationList.HIDE_SYMBOLS, data);
					break;
				}
				case NotificationList.WIN_LINES_AND_WIN_POPUP_SHOWED:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus != null && bonus.type != BonusTypes.GAMBLE) {
						this.controller.changeState(BonusState.NAME);
					}
					else if (this.autoSpinCount == 0) {
						this.controller.changeState(DefaultState.NAME);
						this.controller.send(NotificationList.END_AUTO_PLAY);
						return;
					}
					else {
						if(this.common.is_win_popup_showed === true) {
							this.controller.tryStartSpin();
						}
					}
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				//case HudView.START_AUTO_PLAY_BTN:
				case HudView.OPEN_AUTO_SPIN_TABLE_BTN:
				case HudViewP.AUTO_SPIN_BUTTONP:
				{
					this.autoSpinCount = 0;
					//if(!this.common.isMobile){
					//	var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
					//	autoPlayText.visible = this.common.config.showAllTimeSpinCount;
					//}
					this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
					//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
					this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
					this.view.changeButtonState(HudView.TOTAL_BET_BTN, true, false);
					break;
				}
			}
		}

		private updateAutoSpinCountText():void {
			var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
			if (autoPlayText != null) {
				console.log("updateAutoSpinCountText");
				autoPlayText.text = this.autoSpinCount.toString();
			}
		}

		public end():void {
			this.view.setText(HudView.AUTO_SPIN_TEXT, HudView.AUTO_SPIN_TEXT_START);
			//this.view.getChildByName(HudView.AUTO_SPINS_COUNT_TEXT).alpha = 0;
			this.view.setText(HudView.AUTO_SPINS_COUNT_TEXT, "");
		}

		public setView(view:HudView):void{
			this.view = view;
		}
	}
}