module engine {
	import Text = createjs.Text;
	import Button = layout.Button;

	export class DefaultState implements IState {
		public static NAME:string = "DefaultState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.setView(controller.view);
		}

		public start(data:any):void {
			console.log("start DefaultState "+this.view);
			this.controller.send(NotificationList.SHOW_HEADER);

			this.view.changeButtonState(HudView.SETTINGS_BTN, true, true);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, true);
			//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, true);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, true);
			this.view.changeButtonState(HudView.START_SPIN_BTN, true, true);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, true);
			this.view.changeButtonState(HudView.BET_BTN, true, true);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, true);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, true);
			this.view.changeButtonState(HudView.TOTAL_BET_BTN, true, true);
			this.view.changeButtonState(HudView.BACK_TO_LOBBY_BTN, true, true);

			this.controller.updateAutoPlayBtn();
			this.updateGambleBtn();
			this.controller.updateAutoSpinCountText();
			this.updateBetBtn();
			this.updateLineBtn();
		}

		public afterRotate():void{
			this.view.changeButtonState(HudView.SETTINGS_BTN, true, true);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, true);
			//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, true);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, true);
			this.view.changeButtonState(HudView.START_SPIN_BTN, true, true);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, true);
			this.view.changeButtonState(HudView.BET_BTN, true, true);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, true);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, true);
			this.view.changeButtonState(HudView.TOTAL_BET_BTN, true, true);
			this.view.changeButtonState(HudView.BACK_TO_LOBBY_BTN, true, true);

			this.controller.updateAutoPlayBtn();
			this.updateGambleBtn();
			this.controller.updateAutoSpinCountText();
			this.updateBetBtn();
			this.updateLineBtn();

			console.log("afterRotate", "DefaultState");
			this.view.setText(HudView.AUTO_SPIN_TEXT, HudView.AUTO_SPIN_TEXT_START);
			this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT).text = "";
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.KEYBOARD_CLICK:
				{
					if (data == Keyboard.SPACE) {
						if(this.common.is_win_popup_showed) {
							this.controller.tryStartSpin();
							this.controller.send(NotificationList.COLLECT_WIN_TF);
							this.common.config.statechanged = false;
						}
					}
					break;
				}
				case NotificationList.ON_SCREEN_CLICK:
				{
					this.controller.tryStartSpin();
					break;
				}
				case NotificationList.TRY_START_SPIN:
				{
					console.log('try TRY_START_SPIN',"DefaultState");
					if(!this.common.config.statechanged){
						this.controller.changeState(RegularSpinState.NAME);
						this.common.config.statechanged = true;
					}
					break;
				}
				case NotificationList.TRY_START_AUTO_PLAY:
				{
					var balanceAfterSpin:number = this.common.server.getBalance() - this.common.server.getTotalBet();
					if (balanceAfterSpin >= 0) {
						this.controller.changeState(AutoSpinState.NAME, data);
					}else {
						this.controller.send(NotificationList.SHOW_ERRORS, [ErrorController.ERROR_NO_MONEY_STR]);
					}
					break;
				}
				case NotificationList.SET_BET:{
					this.common.server.setBet(data);
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					this.controller.updateBetOnServ();
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, true);
					this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
					this.view.changeButtonState(HudView.TOTAL_BET_BTN, true, true);
					this.view.changeButtonState(HudView.SETTINGS_BTN, true, true);
					this.view.changeButtonState(HudView.BACK_TO_LOBBY_BTN, true, true);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				//case HudViewP.TOTAL_BET_BTNP:
				case HudView.TOTAL_BET_BTN:
				{
					this.controller.send(NotificationList.OPEN_SELECT_BET_TABLE);
					break;
				}
				case HudView.START_SPIN_BTN: case HudViewP.START_SPIN_BTN:
				{
					this.controller.tryStartSpin();
					break;
				}
				case HudView.LINE_COUNT_BTN:
				{
					this.common.server.setNextLineCount(true);
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.updateLinesOnServ();
					break;
				}
				case HudView.INC_LINE_COUNT_BTN:
				{
					this.common.server.setNextLineCount(false);
					this.updateLineBtn();
					this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, true);
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.updateLinesOnServ();
					break;
				}
				case HudView.DEC_LINE_COUNT_BTN:
				{
					this.common.server.setPrevLineCount(false);
					this.updateLineBtn();
					this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, true);
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.updateLinesOnServ();
					break;
				}
				case HudView.MULTIPLY_BTN:
				{
					this.common.server.setNextMultiply();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					break;
				}
				case HudView.BET_BTN:
				{
					this.common.server.setNextBet(true);
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					this.controller.updateBetOnServ();
					break;
				}
				case HudView.NEXT_BET_BTN:
				{
					this.common.server.setNextBet(false);
					this.updateBetBtn();
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					this.controller.updateBetOnServ();
					break;
				}
				case HudView.PREV_BET_BTN:
				{
					this.common.server.setPrevBet(false);
					this.updateBetBtn();
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					this.controller.updateBetOnServ();
					break;
				}
				case HudView.MAX_BET_BTN:
				{
					this.common.server.setMaxMultiply();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					this.controller.tryStartSpin();
					this.controller.updateBetOnServ();
					break;
				}
				case HudView.MAX_LINES_BTN:
				{
					this.common.server.setMaxLines();
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.tryStartSpin();
					break;
				}
				case HudView.SETTINGS_BTN:
				{
					this.controller.send(NotificationList.OPEN_OPTIONS_MENU, this.view);
					this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
					this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, false, false);
					this.view.changeButtonState(HudView.TOTAL_BET_BTN, false, false);
					this.view.changeButtonState(HudView.SETTINGS_BTN, false, false);
					this.view.changeButtonState(HudView.BACK_TO_LOBBY_BTN, false, false);
					break;
				}
				case HudView.OPEN_AUTO_SPIN_TABLE_BTN:
				//case HudViewP.AUTO_SPIN_BUTTONP:
				{
					console.log(this.common.is_win_lines_showed+" "+this.common.is_win_popup_showed, "OPEN_AUTO_SPIN_TABLE_BTN");
					if(this.common.is_win_popup_showed) {
						this.controller.send(NotificationList.OPEN_AUTO_SPIN_MENU);
					}
					break;
				}
				case HudView.INC_SPIN_COUNT_BTN:
				{
					this.controller.autoPlayCountIdx++;
					this.controller.updateAutoPlayBtn();
					this.controller.updateAutoSpinCountText();
					break;
				}
				case HudView.DEC_SPIN_COUNT_BTN:
				{
					this.controller.autoPlayCountIdx--;
					this.controller.updateAutoPlayBtn();
					this.controller.updateAutoSpinCountText();
					break;
				}
			}
		}

		private updateGambleBtn():void {
			//console.log("Update gamble btn");
			var bonusVO:BonusVO = this.common.server.bonus;
			if (bonusVO != null && bonusVO.type == BonusTypes.GAMBLE) {
				this.view.changeButtonState(HudView.GAMBLE_BTN, true, true);
			}
		}

		public updateBetBtn():void {
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, this.common.server.isHasNextBet());
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, this.common.server.isHasPrevBet());
		}

		public updateLineBtn():void {
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, this.common.server.isHasNextLine());
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, this.common.server.isHasPrevLine());
		}

		public end():void {
		}

		public setView(view:HudView):void{
			console.log("DefaultState setView",view);
			this.view = view;
		}
	}
}