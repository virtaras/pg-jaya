module engine {
	import Button = layout.Button;

	export class BonusState implements IState {
		public static NAME:string = "BonusState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.setView(controller.view);
		}

		public start(data:any):void {
			this.afterRotate();

			this.controller.send(NotificationList.COLLECT_WIN_TF);
			this.controller.send(NotificationList.REMOVE_WIN_LINES);
			this.controller.send(NotificationList.CREATE_BONUS);
		}

		public afterRotate():void{
			this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.SETTINGS_BTN, true, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			//this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);
			this.view.changeButtonState(HudView.TOTAL_BET_BTN, false, false);

			console.log("afterRotate", "BonusState");
			this.controller.send(NotificationList.FREESPINS_SHOW_LEFT_FIELDS);
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.END_BONUS:
				{
					// remove bonus VO
					this.common.server.bonus = null;
					this.controller.changeState(DefaultState.NAME);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.START_SPIN_BTN:
				case HudView.START_BONUS_BTN:
				{
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
					this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
					//this.controller.send(NotificationList.START_BONUS);
					break;
				}
				case NotificationList.WIN_LINES_SHOWED:
				{
					console.log('WIN_LINES_SHOWED');
					this.controller.send(NotificationList.SHOW_WIN_TF);
					this.controller.send(NotificationList.UPDATE_BALANCE_TF);
					break;
				}
			}
		}

		public end():void {
		}

		public setView(view:HudView):void{
			this.view = view;
		}
	}
}