module engine {
	import Button = layout.Button;
	import Toggle = layout.Toggle;
	import Text = createjs.Text;
	import Sound = createjs.Sound;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Rectangle = createjs.Rectangle;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;

	export class SettingsController extends BaseController {
		private static PRELOADER_ROTATION_TIME:number = 1;

		private static BET_MENU:string = "betMenu";
		private static SOUND_TOGGLE:string = "soundToggle";
		private static SOUND_PRELOADER:string = "soundPreloader";
		private static LINES_MENU:string = "linesMenu";
		private static SPINS_COUNT_MENU:string = "spinsCountMenu";

		private common:CommonRefs;
		private hud:HudController;
		private view:Container;
		private autoPlayCountIdx:number;

		constructor(manager:ControllerManager, common:CommonRefs, hudController:HudController, view:Container) {
			super(manager);
			this.common = common;
			this.hud = hudController;
			this.view = view;
			this.view.visible = false;
			this.autoPlayCountIdx = 0;

			SettingsController.PRELOADER_ROTATION_TIME = Utils.float2int(SettingsController.PRELOADER_ROTATION_TIME * Ticker.getFPS());
		}

		public init():void {
			super.init();

			this.initLinesMenu();
			this.initBetMenu();
			this.initSpinsCountMenu();
			this.initSoundToggle();
			this.initPreloader();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.TOGGLE_SETTINGS_MENU);
			notifications.push(NotificationList.CLOSE_SETTINGS_MENU);
			notifications.push(NotificationList.LOAD_SOUNDS);
			notifications.push(NotificationList.SOUNDS_LOADED);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.TOGGLE_SETTINGS_MENU:
				{
					this.view.visible = !this.view.visible;
					break;
				}
				case NotificationList.CLOSE_SETTINGS_MENU:
				{
					this.view.visible = false;
					break;
				}
				case NotificationList.LOAD_SOUNDS:
				{
					this.showPreloader();
					break;
				}
				case NotificationList.SOUNDS_LOADED:
				{
					this.removePreloader();
					break;
				}
			}
		}

		private initPreloader():void {
			if (this.common.isSoundLoaded) {
				this.removePreloader();
			}
			else {
				var soundPreloader:DisplayObject = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
				var bounds:Rectangle = soundPreloader.getBounds();
				soundPreloader.regX = bounds.width / 2;
				soundPreloader.regY = bounds.height / 2;
				soundPreloader.x += bounds.width / 2;
				soundPreloader.y += bounds.height / 2;
				soundPreloader.visible = false;
			}
		}

		private showPreloader():void {
			var soundPreloader:DisplayObject = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
			this.rotatePreloader();
			soundPreloader.visible = true;
		}

		private rotatePreloader() {
			var soundPreloader:DisplayObject = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
			soundPreloader.rotation = 0;
			Tween.get(soundPreloader, {useTicks: true}).to({"rotation": 360}, SettingsController.PRELOADER_ROTATION_TIME).call(()=> {
				this.rotatePreloader();
			});
		}

		private removePreloader():void {
			var soundPreloader:DisplayObject = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
			Tween.removeAllTweens();
			this.view.removeChild(soundPreloader);
		}

		private initSoundToggle():void {
			//var soundToggle:Toggle = <Toggle>this.view.getChildByName(SettingsController.SOUND_TOGGLE);
			//if (soundToggle != null) {
			//	soundToggle.setIsOn(this.common.isSoundLoaded);
			//	soundToggle.on("click", (eventObj:any)=> {
			//		if (eventObj.nativeEvent instanceof MouseEvent) {
			//			if (!this.common.isSoundLoaded) {
			//				this.send(NotificationList.LOAD_SOUNDS);
			//			}
			//			this.send(NotificationList.SOUND_TOGGLE);
			//			Sound.play(SoundsList.REGULAR_CLICK_BTN);
			//		}
			//	});
			//}
		}

		private initLinesMenu():void {
			var values:Array<number> = [];
			for (var i:number = 1; i <= this.common.server.maxLineCount; i++) {
				values.push(i);
			}
			var menu:TouchMenu = new TouchMenu(<Container>this.view.getChildByName(SettingsController.LINES_MENU));
			menu.init(this.common.server.lineCount - 1, values, (value:number)=> {
				this.common.server.lineCount = value + 1;
				this.hud.updateLineCountText();
			});
		}

		private initBetMenu():void {
			var availableBets:Array<number> = this.common.server.availableBets;
			var idx:number = availableBets.indexOf(this.common.server.bet);
			var menu:TouchMenu = new TouchMenu(<Container>this.view.getChildByName(SettingsController.BET_MENU));
			menu.init(idx, availableBets, (value:number)=> {
				this.common.server.bet = availableBets[value];
				this.hud.updateBetText();
				this.hud.updateBetPerLineText();
				this.hud.updateTotalBetText();
			});
		}

		private initSpinsCountMenu():void {
			var menu:TouchMenu = new TouchMenu(<Container>this.view.getChildByName(SettingsController.SPINS_COUNT_MENU));
			menu.init(0, this.common.config.autoPlayCount, (value:number)=> {
				this.hud.autoPlayCountIdx = value;
				this.hud.updateAutoPlayBtn();
				this.hud.updateAutoSpinCountText();
			});
		}

		public dispose():void {
			super.dispose();
			this.view = null;
			this.common = null;
		}
	}
}