module engine {
	import EventDispatcher = createjs.EventDispatcher;
	import Ticker = createjs.Ticker;

	export class ReelMover extends EventDispatcher {
		// events
		public static EVENT_REEL_STOPPED:string = "event_reel_stopped";
		public static EVENT_REEL_PREPARE_STOPPED:string = "event_reel_prepare_stopped";
		// reel strips type
		public static TYPE_REGULAR:string = "regular";
		public static TYPE_FREE_SPIN:string = "freeSpins";
		// mover states
		private static STATE_NORMAL:string = "normal";
		private static STATE_UP:string = "up";
		private static STATE_MOVING:string = "moving";
		private static STATE_DOWN:string = "end";

		private static UP_MOVE_SYMBOLS:number = 1;
		private static DOWN_MOVE_SYMBOLS:number = 1;
		private static MOVE_SPEED:number = 2;
		private static DELAY_BETWEEN_START_REEL:number = 7;
		private static REGULAR_STOP_SYMBOLS:number = 4;
		private static HARD_STOP_SYMBOLS:number = 4;

		private static EPSILON:number = 0.001;
		private static PREPARE_REEL_SOUND_TIME:number = 0.09;

		private reelStrips:Object;
		private reelView:ReelView;
		// состояние вращения барабана (STATE_NORMAL, STATE_UP, STATE_MOVING, STATE_END)
		private state:string;
		// высота которая приходится на один видимый символ
		private dh:number;
		// позиция барабана при инициализации
		private startReelPos:number;
		// осталось до старта вращения барабана
		private delayStartReel:number;
		private isStarted:boolean;
		private isCanStop:boolean;
		// тип ленты
		private moverType:string;
		// лента для текущего барабана
		private reelSequence:Array<number>;
		// длина лента для текущего барабана
		private sequenceLength:number;
		private upEasing:BezierEasing;
		private moveEasing:LinearEasing;
		private downEasing:BezierEasing;
		// текущее смещение слота в координатах
		private pos:number;
		// смещение на предыдущем шаге слота в координатах
		private posPrevState:number;
		// текущий индекс в ленте
		private currentIdx:number;
		// индекс старта вращения в ленте
		private startIdx:number;
		// индекс окончания вращения в ленте
		private targetIdx:number;
		private isCutSequence:boolean;
		private isHardStop:boolean;

		private isRotated:boolean;
		private common:CommonRefs;
		constructor(reelStrips:Object, reelView:ReelView) {
			super();
			this.reelStrips = reelStrips;
			this.reelView = reelView;
		}

		public init(wheel:Array<number>, sequenceType:string, common:CommonRefs):void {
			this.state = ReelMover.STATE_NORMAL;
			this.dh = this.reelView.getSymbol(0).getBounds().height;
			this.startReelPos = this.reelView.getY();
			this.common = common;
			this.changeSequenceType(sequenceType);
			this.resetData();
			this.setServerResponse(wheel);
		}

		public onEnterFrame():void {
			if (this.state == ReelMover.STATE_NORMAL) {
				return;
			}
			if (this.delayStartReel > 0 && this.state == ReelMover.STATE_UP) {
				this.delayStartReel -= 1;
				return;
			}
			if (this.isStarted) {
				this.isStarted = false;
			}
			if (this.state == ReelMover.STATE_UP) {
				this.up();
			}
			if (this.state == ReelMover.STATE_MOVING) {
				this.move();
			}
			if (this.state == ReelMover.STATE_DOWN) {
				this.down();
			}
			this.updatePos(false);
		}

		public start():void {
			this.isCanStop = false;
			this.state = ReelMover.STATE_UP;
		}

		public expressStop():void {
			this.isCutSequence = false;
			this.isHardStop = true;
		}

		public onGetSpin(wheel:Array<number>):void {
			if (!this.isCanStop) {
				this.updatePos(false);
				this.isCanStop = true;
				this.setServerResponse(wheel);
			}
		}

		private up():void {
			if (this.upEasing.getTime() < 1) {
				this.pos = this.upEasing.getPosition();
			}
			else {
				this.posPrevState = this.pos;
				this.state = ReelMover.STATE_MOVING;
			}
		}

		private move():void {
			if (this.moveEasing.getTime() >= 1) {
				if (this.isCanStop) {
					this.cutSequence();
					if (this.currentIdx == this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS)) {
						this.posPrevState = this.pos;
						this.state = ReelMover.STATE_DOWN;
						return;
					}
				}
				this.moveEasing.reset();
				this.posPrevState = this.pos;
			}
			this.pos = this.posPrevState + this.moveEasing.getPosition();
		}

		private down():void {
			if (this.downEasing.getTime() < 1) {
				this.pos = this.posPrevState + this.downEasing.getPosition();
				if(this.downEasing.getTime()<ReelMover.PREPARE_REEL_SOUND_TIME){
					this.dispatchEvent(ReelMover.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
				}
			}
			else {
				this.stop();
			}
		}

		public changeSequenceType(moverType:string):void {
			if (this.moverType != moverType) {
				this.moverType = moverType;

				var reelStrips:Array<Array<number>> = this.reelStrips[moverType];
				var reelId:number = this.reelView.getReelId();
				this.reelSequence = reelStrips[reelId].reverse();
				this.sequenceLength = this.reelSequence.length;

				this.upEasing = new BezierEasing(0, 0, 4, -3, 0);
				this.upEasing.setParameters(this.dh * ReelMover.UP_MOVE_SYMBOLS, 10);

				this.moveEasing = new LinearEasing();
				this.moveEasing.setParameters(this.dh, ReelMover.MOVE_SPEED);

				this.downEasing = new BezierEasing(0, -2, 10, -15, 8);
				this.downEasing.setParameters(this.dh * ReelMover.DOWN_MOVE_SYMBOLS, 15);

				this.resetForChangeSequence();
			}
		}

		private stop():void {
			this.state = ReelMover.STATE_NORMAL;
			this.resetData();
			this.dispatchEvent(ReelMover.EVENT_REEL_STOPPED, this.reelView.getReelId());
		}

		private setServerResponse(wheel:Array<number>):void {
			var symbolCount:number = this.reelView.getSymbolCount();
			var targetWheels:Array<number> = new Array(symbolCount);

			if(this.common.isMobile){
				var id:number = this.reelView.getReelId();
				//console.log('reelId:',id);
				//var targetWheelsTemp:Array<number> = this.common.config.tempReels[id];
				//if(targetWheelsTemp && this.isRotated){
				//	console.log('is rotated');
				//	targetWheels = targetWheelsTemp;
				//	this.isRotated = false;
				//}
				//else {
					for (var i:number = 0; i < symbolCount; i++) {
						targetWheels[i] = wheel[symbolCount - i - 1 + symbolCount * this.reelView.getReelId()];
						//console.log((symbolCount - i - 1 + symbolCount * this.reelView.getReelId()),targetWheels[i]);
					}
					this.common.config.tempReels[id] = targetWheels;
				//}
			}
			else {
				for (var i:number = 0; i < symbolCount; i++) {
					targetWheels[i] = wheel[symbolCount - i - 1 + symbolCount * this.reelView.getReelId()];
				}
			}

			var idx:number = this.getSequenceIdx(targetWheels);
			//console.log(idx);
			if (idx == -1) {
				var reverseReelSequence:Array<number> = this.reelSequence.slice(0).reverse();
				var reverseTargetWheels:Array<number> = targetWheels.slice(0).reverse();
				//console.log("[ERROR]: reel id: " + this.reelView.getReelId() + " sequence type: " + this.moverType + " target wheels: " + reverseTargetWheels + " not found in sequence: " + reverseReelSequence);
				this.reelSequence = this.reelSequence.concat(targetWheels);
				this.sequenceLength = this.reelSequence.length;
				idx = this.getSequenceIdx(targetWheels);
			}
			//console.log(idx, 'setServerResponse idx');
			if (this.state == ReelMover.STATE_NORMAL) {
				this.startIdx = idx;
				this.updatePos(true);
			}
			else {
				this.targetIdx = idx;
			}

		}

		public resetStartPosition():void {
			this.isRotated = true;
			this.reelView.setY(this.startReelPos);
			var id:number = this.reelView.getReelId();
			var targetWheelsTemp:Array<number> = this.common.config.tempReels[id];
			if(targetWheelsTemp){
				var idx:number = this.getSequenceIdx(targetWheelsTemp);
				if(idx == -1){
					this.reelSequence = this.reelSequence.concat(targetWheelsTemp);
					idx = this.getSequenceIdx(targetWheelsTemp);
				}
			}
			var symbolCount:number = this.reelView.getSymbolCount();
			for (var i:number = 0; i < symbolCount; i++) {
				var symbolId:number = symbolCount - i - 1;
				var symbolIdx:number = this.correctIdx(idx + i);
				var frame:number = this.reelSequence[symbolIdx] - 1;
				this.reelView.setSymbolFrame(symbolId, frame);
			}
			this.currentIdx = idx;
		}

		private updatePos(isFist:boolean):void {
			var idx:number = this.getCurrentIdx();
			var dh:number = this.pos % this.dh - this.dh;
			this.reelView.setY(this.startReelPos + dh);
			//if (this.currentIdx != idx || isFist) {
				var symbolCount:number = this.reelView.getSymbolCount() + 1;
				for (var i:number = 0; i < symbolCount; i++) {
					var symbolId:number = symbolCount - i - 1;
					var symbolIdx:number = this.correctIdx(idx + i);
					//var frame:number = (symbolId == 0 || isFist) ? this.reelSequence[symbolIdx] - 1 : this.reelView.getSymbolFrame(symbolId - 1);
					var frame:number = this.reelSequence[symbolIdx] - 1;
					this.reelView.setSymbolFrame(symbolId, frame);
					//console.log("symbolId="+symbolId+" symbolIdx="+symbolIdx+" id="+this.reelView.getReelId(), "resetStartPosition");
				}
				this.currentIdx = idx;
			//}
			//update WildPos
			//if(WinController.winSymbolsWild[this.reelView.getReelId()]!=null) {
			//	WinController.winSymbolsWild[this.reelView.getReelId()].setY(this.pos+25); //TODO: calculate 25
			//}
			if(typeof(ReelWildController.wildSymbolsViews[this.reelView.getReelId()])!="undefined" && (ReelWildController.wildSymbolsViews[this.reelView.getReelId()]._is_wild_started && ReelWildController.wildSymbolsViews[this.reelView.getReelId()].y < (this.pos+25) || ReelWildController.wildSymbolsViews[this.reelView.getReelId()]._is_wild_started && ReelWildController.wildSymbolsViews[this.reelView.getReelId()].y - this.pos < 200)) {
				ReelWildController.wildSymbolsViews[this.reelView.getReelId()].setY(this.pos+25); //TODO: calculate 25
			}
		}

		private getCurrentIdx():number {
			return Utils.float2int(this.correctIdx(this.startIdx + Math.abs(this.pos + ReelMover.EPSILON) / this.dh));
		}

		private getSequenceIdx(value:Array<number>):number {
			var i:number = 0;
			var valueLength:number = value.length;
			var j:number = 0;
			while (j < valueLength) {
				if (this.reelSequence.length == i) {
					// for set wheel
					return -1;
				}
				if (this.reelSequence[i] == value[j]) {
					j++;
					if (i == this.sequenceLength - 1) {
						i = -1;
					}
				}
				else if (j > 0) {
					i = this.correctIdx(i - j);
					j = 0;
				}
				i++;
			}
			var idx:number = i - valueLength;
			if (idx < 0) {
				idx = this.sequenceLength + idx;
			}
			//console.log(idx);
			//console.log(this.reelSequence, "getSequenceIdx idx="+idx);
			return idx;
		}

		private correctIdx(value:number):number {
			if (value >= 0 && value < this.sequenceLength) {
				return value;
			}
			return (value + this.sequenceLength) % this.sequenceLength;
		}

		private cutSequence():boolean {
			if (this.isCutSequence) {
				return false;
			}
			var maxDiv:number = this.isHardStop ? ReelMover.HARD_STOP_SYMBOLS : ReelMover.REGULAR_STOP_SYMBOLS;
			this.isHardStop = false;
			var realDivIdx:number = this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS - this.currentIdx);
			if (realDivIdx <= maxDiv) {
				return;
			}
			var symbolId:number = this.reelSequence[this.correctIdx(this.currentIdx + maxDiv)];
			if (this.isGroupSymbol(symbolId)) {
				return;
			}
			var div:number = realDivIdx - maxDiv;
			while (this.isGroupSymbol(this.reelSequence[this.correctIdx(this.currentIdx + div + maxDiv)])) {
				div--;
			}
			if (div <= 0) {
				return;
			}
			this.pos += div * this.dh;
			this.currentIdx = this.getCurrentIdx();
			//console.log(this.currentIdx);
			this.isCutSequence = true;
		}

		// TODO: need implemented
		private isGroupSymbol(symbolId:number):boolean {
			return false;
		}

		// Сброс данных после остановки барабана
		private resetData():void {
			this.delayStartReel = ReelMover.DELAY_BETWEEN_START_REEL * this.reelView.getReelId();
			this.startIdx = this.targetIdx;
			this.targetIdx = 0;
			this.isCutSequence = false;
			this.pos = 0;
			this.upEasing.reset();
			this.downEasing.reset();
			this.moveEasing.reset();
		}

		// Сброс данных после изменения ленты
		private resetForChangeSequence():void {
			this.currentIdx = 0;
			this.startIdx = 0;
		}

		public getReelView():ReelView{
			return this.reelView;
		}
	}
}