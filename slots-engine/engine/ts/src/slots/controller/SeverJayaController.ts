//new controller

module engine {

    import Socket = io;
    export class ServerController extends BaseController {


        private demoAuthorizationUrl:string;
        private authorizationUrl:string;
        private authorizationRoom:string;
        private closeSessionUrl:string;
        private getBalanceUrl:string;
        private getBetsUrl:string;
        private spinUrl:string;
        private bonusUrl:string;
        private setWheelUrl:string;
        private socketIo:any;
        private startGame:boolean;
        private startingBonus:boolean = false;

        private common:CommonRefs;

        constructor(manager:ControllerManager, common:CommonRefs) {
            super(manager);
            this.common = common;
        }

        private actions:Object = {
            'test':{'action':'play',id:GameConst.ID_JAYA},
            'bonus':{'action':'play',id:GameConst.ID_JAYA,'context':[5,14,4,17,1]}
        };

        public init():void {
            super.init();
            this.startGame = false;
            this.common.jayaServer = true;
        }

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.RES_LOADED);
            notifications.push(NotificationList.SERVER_SEND_SPIN);
            notifications.push(NotificationList.SERVER_SEND_BONUS);
            notifications.push(NotificationList.SERVER_SET_WHEEL);
            notifications.push(NotificationList.SERVER_DISCONNECT);
            notifications.push(NotificationList.CHANGED_LINE_COUNT);
            notifications.push(NotificationList.CHANGE_BET);
            return notifications;
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.RES_LOADED:
                {
                    this.common.server = new ServerData();
                    this.sendJayaRequest();

                }break;
                case NotificationList.SERVER_SEND_SPIN:
                {
                    this.send(NotificationList.SHOW_ALL_SYMBOLS);
                    this.playAction();

                }break;
                case NotificationList.SERVER_SEND_BONUS:
                {
                    this.send(NotificationList.SHOW_ALL_SYMBOLS);
                    this.playAction();
                }break;
                case NotificationList.SERVER_SET_WHEEL:
                {
                    //this.sendSetWheel(data);

                }break;
                case NotificationList.SERVER_DISCONNECT:
                {
                    //this.sendDisconnectRequest();

                }break;
                case NotificationList.CHANGE_BET: case NotificationList.CHANGED_LINE_COUNT:
                {
                    this.updateBets();

                }break;
            }
        }

        private updateBets():void {
            this.socketIo.emit('action', {'action': 'bet', 'context': [this.common.server.lineCount, this.common.server.bet], id: GameConst.ID_JAYA});
        }

        private sendJayaRequest():void{
            var self = this;
            this.socketIo = Socket.connect(GameConst.SERVER_URL_JAYA);
            this.socketIo.emit('join', {'id':GameConst.ID_JAYA});
            this.socketIo.on('join', function (data:any) {
                self.sendConfig();
            });
        }

        private sendConfig():void{
            this.common.server.bonus = null;
            var self = this;
            this.socketIo.emit('action', {'action': 'config', id: GameConst.ID_JAYA});
            this.socketIo.on('action',function(data:any){
                switch (data.action){
                    case 'bet':
                        if(!this.startGame){
                            this.startGame = true;

                            self.onAuthorized();
                        }
                        break;
                    case 'config':
                        self.setActions(data.events[0]);
                        break;
                    case 'play':
                        self.parseServResult(data);
                        break;
                }
            });
            this.socketIo.on('balance',function(data){
                self.common.server.setBalance(parseFloat(data));
            })
        }
        private playAction():void {
            var params:Object = this.actions[this.common.login];
            if(this.common.login == 'bonus')this.common.login = 'test';
            console.log(params, "params");
            this.socketIo.emit('action', params);
        }

        private parseServResult(data:any):void {
            console.log(data, "parseServResult");
            this.common.server.win = 0;
            this.common.server.winLines = [];
            this.common.server.arrLinesIds = [];
            this.startingBonus = false;

            var events:Array<any> = data.events;
            var winlines:Array<any> = [];
            var win:boolean = false;
            for (var n = 0; n < events.length; n++){
                if(events[n].event == 'spinWin'){
                    win = true;
                    var context:Object = events[n].context;
                    winlines.push(context);
                    var mode:string = context['mode'];
                    if(mode == 'scatter' && context['what']=="bonus") {
                        this.common.server.scatters = context["context"];
                        this.startingBonus = true;
                        //this.send(NotificationList.SERVER_GOT_BONUS);
                    }
                }
                if(events[n].event == 'playedSpin'){
                    this.setResultWeels(events[n].context);
                }
                if(events[n].event == 'enterBonus'){
                    this.setBonus(events[n].context);
                }
                if(events[n].event == 'playedBonusSpin'){
                    this.processBonusStep(events[n].context);
                }
                if(events[n].event == 'gameEnd'){
                    var winValue:number = events[n].context['win'];
                    //this.common.server.win = winValue;
                    this.send(NotificationList.END_FREE_SPINS, winValue);
                }
            }
            if(win && !this.startingBonus){
                for(var i:number = 0; i < winlines.length; i++){
                    var winContext = winlines[i].context;
                    this.parseLine(winContext, winlines[i].occurs, winlines[i].pay, winlines[i]);
                }
            }
            //console.log(this.common.server.winLines,"this.common.server.winLines parseServResult");
            this.send(NotificationList.SERVER_GOT_SPIN);
        }

        private setResultWeels(reels:Array<Array<string>>):void{
            var resultArray:Array<number> = [];
            for(var i:number = 0; i < reels.length; i++){
                var reel:Array<string> = reels[i];
                for(var s:number = 0; s < reel.length; s++){
                    var symbolIndex:number = this.parseSymbol(reel[s]);
                    resultArray.push(symbolIndex + 1);
                    if((symbolIndex + 1)==this.common.config.wildSymbolId && typeof(ReelWildController.wildSymbolsViews[i])!=="undefined") ReelWildController.wildSymbolsViews[i]._is_wild_to_show = true;
                }
            }
            this.common.server.wheel = resultArray;
        }

        private parseSymbol(symbol:string):number {
            var targetNames:Array<string> = ['wild','bonus','h1','h2','h3','h4','h5','l1','l2','l3','l4'];
            return targetNames.indexOf(symbol);
        }

        private getBonusIndex(name:string):number {
            var bonusNames:Array<string> = [
                'freespins_high_icon',
                'freespins_reel_3_wild',
                'freespins_all_scatter',
                'freespins_reel_2_4_wild',
                'freespins_reel_1_3_5_wild'
            ];
            return bonusNames.indexOf(name);
        }

        private processBonusStep(context){
            this.common.server.bonus.step = context['left'];
            return;
        }

        private setBonus(context:Object):void{

            var triggering:any = context['triggering'];
            var bonusVO:BonusVO = new BonusVO();
            bonusVO.id = 2;
            bonusVO.key = 'key';
            bonusVO.step = context['left'];
            bonusVO.type = triggering.bonus;
            console.log(bonusVO.type,'BONUS TYPE');
            bonusVO.className = "FreeSpinsController";
            bonusVO.data = {'sector':this.getBonusIndex(triggering.bonus)};

            this.common.server.bonus = bonusVO;
        }

        private parseLine(context:Object, count:number, pay:number, context_full:any):void {
            var lineId:number = context['paylineId'];
            if(lineId>=0 && lineId != null && context_full['mode']!='scatter'){
                var winLineVO:WinLineVO = new WinLineVO();
                winLineVO.lineId = lineId;
                winLineVO.win = pay;
                winLineVO.winPos = [];
                var line:string = '';
                var positions:Array<number> = context['payline'];
                for(var n:number = 0; n < positions.length; n++){
                    var current = positions[n];
                    line += current;
                    if(n < count)winLineVO.winPos.push(current);
                }
                this.common.server.winLines.push(winLineVO);
                this.common.server.arrLinesIds.push(winLineVO.lineId);
            }else if(context_full['what']!="bonus" && context_full['mode']=='scatter'){
                var winLineVO:WinLineVO = new WinLineVO();
                winLineVO.win = pay;
                winLineVO.winPos = [];
                for(var i:number = 0; i < ReelWildController.reelsCnt; ++i){
                    winLineVO.winPos[i] = -1;
                }
                for(var i:number = 0; i<context.length; ++i){
                    winLineVO.winPos[context[i]['reel']] = context[i]['row'];
                }
                this.common.server.winLines.push(winLineVO);
            }
        }

        private setLines(lines:Object):void{
            this.common.config.allLines = lines['availablePayLines'];
        }

        private setRegularSpins(reels:any):void {
            var lines:Array<Array<string>> = reels['reels'];
            var initWeelArray:Array<number> = [];
            for(var a:number = 0; a < lines.length; a++){
                var lineReel:Array<string> = lines[a];

                var newLineArray:Array<number> = [];
                for(var n:number = 0; n < lineReel.length; n++){
                    var symbolName:string = lineReel[n];
                    var index:number = this.parseSymbol(symbolName);
                    //console.log(symbolName, index);
                    index++;
                    newLineArray.push(index);
                    if(n < 3)initWeelArray.push(index);
                }

                this.common.config.reelStrips.regular[a] = newLineArray;
            }
            this.common.server.wheel = initWeelArray;
        }


        private setActions(config:Object):void {
            //console.log(config, "setActions");
            var context:Object = config['context'];
            this.setLines(context);
            this.setRegularSpins(context);

            this.common.server.jackpot = parseFloat("0.00");

            this.common.server.multiply = 1;
            if(config['context']['availablePayLines']) {
                this.common.server.maxLineCount = config['context']['availablePayLines'].length;
                this.common.server.lineCount = this.common.server.maxLineCount;
            }
            this.common.server.availableMultiples = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            //this.common.server.availableBets = [0.02, 0.05, 0.1, 0.2, 0.25];
            this.common.server.availableBets = this.common.config.selectBetCount;
            this.common.server.bet = parseFloat(this.common.server.availableBets[0].toString());
            this.common.server.bonus = null;
            this.socketIo.emit('action', {'action': 'bet', 'context': [this.common.server.lineCount,this.common.server.bet], id: GameConst.ID_JAYA});
        }

        private onAuthorized():void {
            this.send(NotificationList.SERVER_INIT);
        }

        public dispose():void {
            super.dispose();
            this.common = null;
        }

    }
}