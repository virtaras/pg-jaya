module engine {
	import Container = createjs.Container;
	import LayoutCreator = layout.LayoutCreator;

	export class BackgroundController extends BaseController {
		public static REGULAR_LAYOUT_NAME:string = "BackgroundView";
		public static FREE_SPINS_LAYOUT_NAME:string = "FreeSpinsBackgroundView";

		private common:CommonRefs;
		private container:Container;
		private view:Container;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
		}

		public init():void {
			super.init();
			this.changeType();
		}

		private createView(type:string):void {
			if(window.innerHeight > window.innerWidth  && this.common.isMobile) type += 'P';
			var creator:LayoutCreator = this.common.layouts[type];
			if (creator != null) {
				if (this.view != null) {
					this.container.removeChild(this.view);
				}
				this.view = new Container();
				creator.create(this.view);
				this.view.y -= 150;
				this.container.addChild(this.view);
			}
		}

		private changeType():void {
			var bonus:BonusVO = this.common.server.bonus;
			if (bonus != null && bonus.type == BonusTypes.FREE_SPINS) {
				//this.createView(BackgroundController.FREE_SPINS_LAYOUT_NAME);
				this.createView(BackgroundController.REGULAR_LAYOUT_NAME);
			}
			else{
				this.createView(BackgroundController.REGULAR_LAYOUT_NAME);
			}
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.END_BONUS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.CREATE_BONUS:
				case NotificationList.END_BONUS:
				{
					this.changeType();
					break;
				}
			}
		}

		public dispose():void {
			super.dispose();
		}
	}
}