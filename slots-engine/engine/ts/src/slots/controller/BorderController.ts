/**
 * Created by Taras on 02.02.2015.
 */
module engine {
//	import Tween = createjs.Tween;
//	import Point = createjs.Point;
//	import Shape = createjs.Shape;
//	import Ticker = createjs.Ticker;
//	import Graphics = createjs.Graphics;
//	import Rectangle = createjs.Rectangle;

    export class BorderController extends BaseController {
//		private static DELAY_REMOVE:number = 2;
//		private static INVISIBLE_TIME:number = 1;
        public common:CommonRefs;
        public view:BorderView;

        constructor(manager:ControllerManager, common:CommonRefs, view:BorderView) {
            super(manager);
            this.common = common;
            this.view = view;
            //this.view.visible = false;
//			LinesController.DELAY_REMOVE = Utils.float2int(LinesController.DELAY_REMOVE * Ticker.getFPS());
//			LinesController.INVISIBLE_TIME = Utils.float2int(LinesController.INVISIBLE_TIME * Ticker.getFPS());
        }

        public init():void {
            super.init();
            this.view.create();
//			this.view.hideAllLines();
        }
    }
}