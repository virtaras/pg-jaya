module engine {
	export class WinLines extends BaseWinLines {
		private index:number;

		constructor(winLines:Array<WinLine>, time:number) {
			super(winLines, time);
		}

		public create():void {
			this.index = -1;
			this.showNextLine();
		}

		public onEndTime():void {
			if (!this.isAllLineShowed && this.index == this.winLines.length - 1) {
				this.dispatchEvent(BaseWinLines.EVENT_All_LINES_SHOWED);
				this.isAllLineShowed = true;
			}
			if (this.winLines == null || this.winLines.length == 1) {
				return;
			}
			this.winLines[this.index].remove();
			this.showNextLine();
		}

		private showNextLine():void {
			this.leftTime = this.time;
			this.index = (this.index + 1) % this.winLines.length;
			this.showLine();
			this.dispatchEvent(BaseWinLines.EVENT_LINE_SHOWED, this.index);
		}

		private showLine():void {
			var winLine:WinLine = this.winLines[this.index];
			var winLinesIds:Array<number> = [];
			var lineId:number = winLine.getLineId();
			if (lineId > 0) {
				winLinesIds.push(lineId);
			}
			this.dispatchEvent(BaseWinLines.EVENT_CREATE_LINE, [winLine.getPositions(), winLinesIds]);
			winLine.create();
		}
	}
}