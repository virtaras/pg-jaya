module engine {
	import Container = createjs.Container;
	import Point = createjs.Point;
	import EventDispatcher = createjs.EventDispatcher;

	export class WinLine extends EventDispatcher {
		private container:Container;
		private lineId:number;
		private winVOs:Array<WinAniVO>;
		private wildLineId:number = -1;

		constructor(container:Container, lineId:number, winVOs:Array<WinAniVO>) {
			this.container = container;
			this.lineId = lineId;
			this.winVOs = winVOs;
		}

		public create():void {
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var winSymbolView:WinSymbolView = vo.winSymbolView;
				if (winSymbolView.parent != null) {
					winSymbolView.parent.removeChild(winSymbolView);
				}
				winSymbolView.x = vo.rect.x;
				winSymbolView.y = vo.rect.y;
				if(!winSymbolView._is_wild && !ReelWildController.wildSymbolsViews[i]._is_wild_to_show) {
					if(!ReelWildController.isWildReelStarted(i)) {
						winSymbolView.play();
						this.container.addChild(winSymbolView);
					}
				}else{
					this.wildLineId = i;
					this.dispatchEvent(BaseWinLines.SHOW_WILD_REEL);
				}
			}
		}

		public remove():void {
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var winSymbolView:WinSymbolView = vo.winSymbolView;
				this.container.removeChild(winSymbolView);
			}
		}

		public getPositions():Array<Point> {
			var positions:Array<Point> = new Array(this.winVOs.length);
			for (var i:number = 0; i < this.winVOs.length; i++) {
				positions[i] = this.winVOs[i].posIdx.clone();
			}
			return positions;
		}

		public getLineId():number {
			return this.lineId;
		}

		public getWinVOs():Array<WinAniVO>{
			return this.winVOs;
		}

		public getWinSymbolViewByInd(ind:number):WinSymbolView{
			return this.winVOs[ind].winSymbolView;
		}
	}
}