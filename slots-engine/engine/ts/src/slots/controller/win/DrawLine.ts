/**
 * Created by Taras on 22.12.2014.
 */
module engine {
    import Container = createjs.Container;
    import Point = createjs.Point;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;

    export class DrawLine {
        private container:Container;
        private common:CommonRefs;
        private index:number;
        private leftTime:number;
        private time:number;
        public winLines:Array<Shape>;

        constructor(container:Container, common:CommonRefs){
            this.container = container;
            this.common = common;
            this.winLines = [];
        }

        public create(id:number, alpha:number):Shape {
            var width:number;
            var yc:number;
            var height:number;
            var params:Rectangle = this.common.symbolsRect[0][0];
            var x:number = params.x;
            var y:number = params.y;
            height = width = params.width;
            var cords:Array<number> = (this.common.jayaServer) ? this.common.config.allLines[id] : this.common.config.allLines[id - 1];
            var newLine = new createjs.Shape();
            var color:string = this.getRandomLineColor();
            yc = (this.common.jayaServer) ? cords[0] : cords[0] + 1;
            newLine.graphics.setStrokeStyle(3).beginStroke(color)
            if(this.common.jayaServer){
                newLine.graphics.moveTo(x + (width/2), y + ((yc * height) + height/2));
            }
            else {
                newLine.graphics.moveTo(x + (width/2), y + ((yc * height) - height/2));
            }
            for (var c:number = 1; c < cords.length; c++) {
                if(this.common.jayaServer){
                    yc = cords[c];
                    newLine.graphics.lineTo(x  + (c * width) + (width/2), y + ((yc * height) + height/2));
                }
                else {
                    yc = cords[c] + 1;
                    newLine.graphics.lineTo(x  + (c * width) + (width/2), y + ((yc * height) - height/2));
                }
            }
            newLine.graphics.endStroke();
            newLine.alpha = alpha;
            newLine.id = id;
            this.winLines.push(newLine);
            return newLine;
        }

        public remove():void {
            for (var i:number = this.container.children.length; i >= 0; i--) {
                var line = this.container.children[i];
                this.container.removeChild(line);
            }
        }

        private getRandomLineColor():string{
            return '#41E969';
        }

        public reset():void {
            this.index = -1;
        }

        public showNextLine():void {
            this.hideAll();
            this.leftTime = this.time;
            this.index++;
            this.index = this.index < this.common.server.arrLinesIds.length && this.index >= 0 ? this.index : 0;
            this.showLine();
        }

        public hideAll():void {
            for(var i:number = 0; i < this.winLines.length; i++){
                this.winLines[i].alpha = 0;
            }
        }

        private showLine():void {
            var id:number = this.common.server.arrLinesIds[this.index];
            var winLine:Shape;
            for (var i = 0, len = this.winLines.length; i < len; i++) {
                if (this.winLines[i].id === id) {
                    winLine = this.winLines[i];
                }
            }
            if(!winLine || winLine == null)return;
            winLine.alpha = 1;

        }

    }
}