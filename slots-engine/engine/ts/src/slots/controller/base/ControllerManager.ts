module engine {
	export class ControllerManager {
		private observerMap:Object;
		private controllers:Array<BaseController>;

		constructor() {
			this.observerMap = {};
			this.controllers = [];
		}

		public register(controller:BaseController):void {
			var notificationsNames:Array<string> = controller.listNotification();
			for (var i:number = 0; i < notificationsNames.length; i++) {
				this.registerObserver(notificationsNames[i], controller);
			}
			this.controllers.push(controller);
		}

		public remove(controller:BaseController):void {
			var notificationsNames:Array<string> = controller.listNotification();
			for (var i:number = 0; i < notificationsNames.length; i++) {
				this.removeObserver(notificationsNames[i], controller);
			}
			var index:number = this.controllers.indexOf(controller);
			this.controllers.splice(index, 1);
		}

		public registerObserver(notificationName:string, controller:BaseController):void {
			if (this.observerMap[notificationName] != null) {
				this.observerMap[notificationName].push(controller);
			} else {
				this.observerMap[notificationName] = [controller];
			}
		}

		public removeObserver(notificationName:string, controller:BaseController):void {
			var controllers:Array<BaseController> = this.observerMap[notificationName];
			for (var i:number = 0; i < controllers.length; i++) {
				if (controllers[i] == controller) {
					controllers.splice(i, 1);
				}
			}
		}

		public send(notificationName:string, data:any):void {
			console.log(notificationName, data, "send");
			var controllers:Array<BaseController> = this.observerMap[notificationName];
			if (controllers != null) {
				for (var i:number = 0; i < controllers.length; i++) {
					controllers[i].handleNotification(notificationName, data);
				}
			}
		}

		public onEnterFrame():void {
			for (var i:number = 0; i < this.controllers.length; i++) {
				this.controllers[i].onEnterFrame();
			}
		}
	}
}