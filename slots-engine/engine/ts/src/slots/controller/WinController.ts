module engine {
	import Container = createjs.Container;
	import Sprite = createjs.Sprite;
	import Point = createjs.Point;
	import Ticker = createjs.Ticker;
	import Rectangle = createjs.Rectangle;
	import LayoutCreator = layout.LayoutCreator;
	import Shape = createjs.Shape;

	export class WinController extends BaseController {
		public static winSymbolsWild:Array<WinSymbolView> = new Array<WinSymbolView>();

		public static WIN_ANIMATION_PREFIX:string = "WinAnimation_";
		public static WIN_ANIMATION_HIGHLIGHT_PREFIX:string = "SymbolHighlight";

		private common:CommonRefs;
		private container:Container;
		private winLines:BaseWinLines;
		private symbolsToHide:Array<Array<Point>>;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
			this.createWinMask();
		}

		public init():void {
			super.init();
			this.createWinMask();
		}

		private createWinMask():void {
			this.container.mask = ReelsView.PLAY_FIELD_MASK;
			this.container.hitArea = ReelsView.PLAY_FIELD_MASK;
			var mask:Shape = new Shape();
			if(this.common.isMobile){
				mask.graphics.beginFill("0").drawRect(0, 25,800, 460);
				this.container.mask = mask;
				this.container.hitArea = mask;
			}
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WIN_LINES);
			notifications.push(NotificationList.REMOVE_WIN_LINES);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.HIDE_SYMBOLS);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.STOPPED_REEL_ID);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SHOW_WIN_LINES:
				{
					this.create(data);
					break;
				}
				case NotificationList.REMOVE_WIN_LINES:
				{
					this.remove();
					break;
				}
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.container.visible = false;
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.container.visible = true;
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					//this.removeWinSymbolsWild();
					break;
				}
				case NotificationList.STOPPED_REEL_ID:
				{
					this.removeWinSymbolWild(data);
					break;
				}
			}
		}

		public onEnterFrame():void {
			this.container.tickChildren = false;
			var k:number = (this.common.isMobile) ? 5 : 2;
			var speedControl = Ticker.getTicks() % 2;
			if(speedControl == 0) {
				this.container.tickChildren = true;
				if (this.winLines != null) {
					this.winLines.onEnterFrame();
				}
			}
		}

		public dispose():void {
			super.dispose();
			this.remove();
			this.common = null;
			this.container = null;
		}

		private create(isTogether:Boolean):void {
			var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
			if (winLinesVOs.length == 0) {
				this.send(NotificationList.WIN_LINES_SHOWED);
				return;
			}
			if (isTogether) {
				this.winLines = new WinLinesTogether(this.createWinLines(), this.common.config.winAnimationTime);
			}
			else {
				this.winLines = new WinLines(this.createWinLines(), this.common.config.winAnimationTime);
                this.send(NotificationList.GET_ALL_IDS);
			}
			this.winLines.on(BaseWinLines.EVENT_CREATE_LINE, (eventObj:any)=> {
				var hydeSymbols:Array<Point> = eventObj.target[0];
				this.send(NotificationList.HIDE_SYMBOLS, hydeSymbols);
				this.send(NotificationList.HIDE_LINES);
				this.send(NotificationList.SHOW_LINES, eventObj.target);
			});
			this.winLines.on(BaseWinLines.EVENT_All_LINES_SHOWED, ()=> {
				//if(this.common.is_win_popup_showed) {
					this.send(NotificationList.WIN_LINES_SHOWED);
				//}
			});
			this.winLines.on(BaseWinLines.EVENT_LINE_SHOWED, (eventObj:any)=> {
				this.send(NotificationList.SHOW_ALL_SYMBOLS);
				var index:number = eventObj.currentTarget.index;
				var hydeSymbols:Array<Point> = this.symbolsToHide[index];
				this.send(NotificationList.HIDE_SYMBOLS, hydeSymbols);
			});
			for(var i=0; i<this.winLines.winLines.length; ++i) {
				this.winLines.winLines[i].on(BaseWinLines.SHOW_WILD_REEL, (data)=> {
					this.send(NotificationList.SHOW_WILD_REEL, data.currentTarget.wildLineId);
				});
			}
			this.winLines.create();
		}

		private createWinLines():Array<WinLine> {
			var wheel:Array<number> = this.common.server.wheel;
			var symbolsRect:Array<Array<Rectangle>> = this.common.symbolsRect;
			var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
			var symbolCount:number = symbolsRect[0].length;
			var allWinAniVOs:Array<WinAniVO> = [];
			var winLines:Array<WinLine> = [];
			var winValue:number = 0;
			this.symbolsToHide = [];
			for (var i:number = 0; i < winLinesVOs.length; i++) {
				var maskedSymbols:Array<Point> = [];
				var winLinesVO:WinLineVO = winLinesVOs[i];
				winValue += winLinesVO.win;
				var winPos:Array<number> = winLinesVO.winPos;
				var winAniVOs:Array<WinAniVO> = [];
				for (var reelId:number = 0; reelId < winPos.length; reelId++) {
					var symbolIdx:number = (this.common.jayaServer) ? winPos[reelId] : winPos[reelId] - 1;
                    if (symbolIdx >= 0) {
						var rect:Rectangle = (wheel[symbolIdx + symbolCount * reelId] == 1) ? symbolsRect[reelId][0] : symbolsRect[reelId][symbolIdx];
						var posIdx:Point = new Point(reelId, symbolIdx);
						var winAniVO:WinAniVO = WinController.findWinVO(allWinAniVOs, posIdx);
						var winSymbolView:WinSymbolView;
						if (winAniVO == null) {
							winSymbolView = this.createWinSymbol(wheel[symbolIdx + symbolCount * reelId]);
							//if(winSymbolView._is_wild) ReelWildController.wildSymbolsViews[reelId]._is_wild_to_show = true;
						}
						else {
							winSymbolView = winAniVO.winSymbolView;
						}
						winAniVO = new WinAniVO();
						winAniVO.winSymbolView = winSymbolView;
						winAniVO.rect = rect.clone();
						winAniVO.rect.x -= 10;
						winAniVO.rect.y -= 10;
						winAniVO.posIdx = posIdx;
						var hideSymbol:Point = posIdx;
						allWinAniVOs.push(winAniVO);
						winAniVOs.push(winAniVO);
						maskedSymbols.push(hideSymbol);
					}
				}
				this.symbolsToHide.push(maskedSymbols);
				var winLine:WinLine = new WinLine(this.container, winLinesVO.lineId + 1, winAniVOs);
				winLines.push(winLine);
			}
			this.common.server.win = winValue;
			return winLines;
		}

		private createWinSymbol(symbolId:number):WinSymbolView {
			var regularAni:LayoutCreator = this.common.layouts[WinController.WIN_ANIMATION_PREFIX + symbolId];
			var highlightAni:LayoutCreator = this.common.layouts[WinController.WIN_ANIMATION_HIGHLIGHT_PREFIX];
			var winSymbolView:WinSymbolView = new WinSymbolView();
			winSymbolView.create(regularAni, highlightAni);
			if(typeof(this.common.config.wildSymbolId)!="undefined" && symbolId == this.common.config.wildSymbolId){
				winSymbolView._is_wild = true;
			}
			return winSymbolView;
		}

		private static findWinVO(winVOs:Array<WinAniVO>, posIdx:Point):WinAniVO {
			for (var i:number = 0; i < winVOs.length; i++) {
				var vo:WinAniVO = winVOs[i];
				if (vo.posIdx.x == posIdx.x && vo.posIdx.y == posIdx.y) {
					return vo;
				}
			}
			return null;
		}

		private remove():void {
			if (this.winLines != null) {
				this.winLines.dispose();
				this.winLines = null;
			}
		}

		public removeWinSymbolsWild():void{
			if(WinController.winSymbolsWild!=null) {
				for (var i:number = 0; i < WinController.winSymbolsWild.length; ++i) {
					this.removeWinSymbolWild(i);
				}
			}
		}
		public removeWinSymbolWild(symbolid):void{
			//console.log(symbolid, "removeWinSymbolWild");
			if(WinController.winSymbolsWild[symbolid]!=null) {
				this.container.removeChild(WinController.winSymbolsWild[symbolid]);
				WinController.winSymbolsWild[symbolid] = null;
			}
		}
	}
}