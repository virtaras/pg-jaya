module engine {
	import LoadQueue = createjs.LoadQueue;
	import Sound = createjs.Sound;
	import WebAudioPlugin = createjs.WebAudioPlugin;
	import HTMLAudioPlugin = createjs.HTMLAudioPlugin;
	import LayoutCreator = layout.LayoutCreator;
	import Layout = layout.Layout;
	import Button = layout.Button;

	export class LoaderController extends BaseController {
		private static SOUND_PERCENTAGE:number = 0.4;
		private static ASSETS_PERCENTAGE:number = 1 - LoaderController.SOUND_PERCENTAGE;

		private common:CommonRefs;
		private view:LoaderView;
		private isGameStarted:boolean;
        private breakPoint:number;
		private firstUp:number;
		private maxLeyers:number;
		private tempLayout:Object[];
		private isSoundLoadStarted:boolean = false;

		constructor(manager:ControllerManager, common:CommonRefs, view:LoaderView) {
			super(manager);
			this.common = common;
			this.view = view;
			this.isGameStarted = false;
		}

		public init():void {
			super.init();
			this.common.isSoundLoaded = false;
			this.view.on(LayoutCreator.EVENT_LOADED, ()=> {
				this.send(NotificationList.HIDE_PRELOADER);
				this.initLoader();
			});
			this.firstUp = 0;
			this.tempLayout = [];
			this.view.load(GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.LAYOUT_FOLDER + LoaderView.LAYOUT_NAME + FileConst.LAYOUT_EXTENSION, true);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SERVER_INIT);
			notifications.push(NotificationList.LOAD_SOUNDS);
            notifications.push(NotificationList.LAZY_LOAD);
            notifications.push(NotificationList.LAZY_LOAD_COMP);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SERVER_INIT:
				{
					break;
				}
				case NotificationList.LOAD_SOUNDS:
				{
					this.loadSounds();
					break;
				}
			}
		}

		private initLoader():void {
			for (var i:number = 0; i < this.view.buttonsNames.length; i++) {
				var buttonName:string = this.view.buttonsNames[i];
				var button:Button = <Button>this.view.getChildByName(buttonName);
				button.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onButtonClick(<Button>eventObj.currentTarget);
					}
				});
			}
		}

		private onButtonClick(button:Button):void {
			switch (button.name) {
				case LoaderView.BUTTON_YES:
				{
					this.common.isSoundOn = true;
					break;
				}
				case LoaderView.BUTTON_NO:
				{
					this.common.isSoundOn = false;
					break;
				}
			}
			if (this.common.isMobile) {
				FullScreen.fullScreen();
			}
			this.loadConfig();
			this.view.showPreloaderBg(0, 1);
			this.view.hideButtons();
			this.view.showProgressBar();
		}

		private loadSounds():void {
			if(!this.isSoundLoadStarted) {
				this.isSoundLoadStarted = true;
				var totalFiles:number = this.common.config.sounds.length;
				var leftLoad:number = totalFiles;
				if (leftLoad > 0) {
					Sound.alternateExtensions = ["mp3"];
					Sound.registerPlugins([WebAudioPlugin, HTMLAudioPlugin]);
					Sound.addEventListener("fileload", (event:any)=> {
						console.log("loaded sound id = " + event.id + " url = " + event.src);
						leftLoad--;
						if (this.view != null) {
							var progress = LoaderController.ASSETS_PERCENTAGE + (totalFiles - leftLoad) / totalFiles * LoaderController.SOUND_PERCENTAGE;
							this.view.setProgress(progress);
						}
						if (leftLoad == 0) {
							this.onSoundsLoaded();
						}
					});
					Sound.registerManifest(this.common.config.sounds, GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.SOUNDS_FOLDER);
				}
				else if (leftLoad == 0) {
					this.onSoundsLoaded();
				}
			}
		}

		private onSoundsLoaded():void {
			this.common.isSoundLoaded = true;
			this.onLoaded();
		}

		private loadConfig():void {
			var configUrl:string = GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.GAME_CONFIG_NAME + FileConst.JSON_EXTENSION + "?" + this.common.key;
			var assetVO:AssetVO = new AssetVO(GameConst.GAME_CONFIG_NAME, configUrl, LoadQueue.JSON);
            createjs.LoadQueue.loadTimeout = 1000;
			var configLoader:LoadQueue = new LoadQueue(false);
			configLoader.on("complete", () => {
				this.common.config = configLoader.getResult(GameConst.GAME_CONFIG_NAME);
				this.loadFonts();
			});
            configLoader.setMaxConnections(10);
            configLoader.loadFile(assetVO);
		}

		private loadFonts():void {
			var fonts:Array<any> = this.common.config.fonts;
			if (fonts != null && fonts.length > 0) {
				var style:HTMLElement = document.createElement('style');
				var manifest:Array<any> = [];

				for (var i:number = 0; i < fonts.length; i++) {
					var fontName:string = fonts[i].name;
					var fontURL:string = GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.FONTS_FOLDER + fonts[i].fileName + FileConst.WOFF_EXTENSION;
					style.appendChild(document.createTextNode("@font-face {font-family: '" + fontName + "'; src: url('" + fontURL + "');}"));
					document.head.appendChild(style);
					manifest.push({"id": fontName, "src": fontURL});
				}

				var configLoader:LoadQueue = new LoadQueue(false);
				configLoader.on("complete", () => {
					this.getForFirst();
				});
				configLoader.loadManifest(manifest);
			}
			else {
				this.getForFirst();
			}
		}

		private getUrlDownl():string {
			return GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.LAYOUT_FOLDER;
		}

		private loadLayouts(from:number, to:number, onComplete:Function = null, eachLoaded:Function = null):void {
			if(to > this.maxLeyers)return;
			var layoutsName:Array<string> = this.common.config.layouts;
			var layouts:Object = this.tempLayout;
			for (var i:number = from; i < to; i++) {
				var layoutName:string = layoutsName[i];
				var LayoutClass:any = engine[layoutName];
				layouts[layoutName] = LayoutClass ? new LayoutClass() : new LayoutCreator();
			}
			var needLoad:number = to - from;
			var leftLoad:number = needLoad;
			var baseLayoutUrl:string = this.getUrlDownl();
			for (var i:number = from; i < to; i++) {
				//console.log(i);
				var layoutName:string = layoutsName[i];
				var layoutObj:Layout = layouts[layoutName];
				layoutObj.on(LayoutCreator.EVENT_LOADED, ()=> {
					eachLoaded != null && eachLoaded();
					leftLoad -= 1;
					if (leftLoad == 0) {
						onComplete != null && onComplete();
					}
				});
				var url:string = baseLayoutUrl + layoutName + FileConst.LAYOUT_EXTENSION + "?" + this.common.key;
				layoutObj.load(url);
			}
            this.common.layouts = layouts;
		}

		private initGame():void {
			if (this.view != null) {
				this.view.dispose();
				this.view = null;
			}
			this.isGameStarted = true;
			this.loadLayouts(this.firstUp, this.common.config.layouts.length, ()=>{
				this.common.is_layout_load_compl = true;
				this.send(NotificationList.RES_LOADED);
				if (this.common.isSoundOn) {
					this.send(NotificationList.LOAD_SOUNDS);
				}
				else {
					this.onLoaded();
				}
			});
		}

		private getForFirst():void {
			this.breakPoint = this.common.config.priorityLevel;
			this.maxLeyers = this.common.config.layouts.length;
			this.loadLayouts(this.firstUp, this.breakPoint, ()=>{
				this.firstUp = this.breakPoint;
				this.initGame();
			}, ()=>{++this.firstUp; this.updateLoader();});
		}

		private updateLoader():void {
			var leftLoad = this.breakPoint - this.firstUp;
			var progress:number = (this.breakPoint - leftLoad) / this.breakPoint;
			if (this.common.isSoundOn) {
				progress *= LoaderController.ASSETS_PERCENTAGE;
			}
			this.view.setProgress(progress);
		}

		private onLoaded():void {

			this.send(NotificationList.LAZY_LOAD_COMP);
			this.dispose();
		}

		public dispose():void {
			if (this.common.isSoundLoaded) {
				super.dispose();
				this.common = null;
			}

		}
	}
}