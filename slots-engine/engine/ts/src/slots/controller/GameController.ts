module engine {
	import Stage = createjs.Stage;
	import Ticker = createjs.Ticker;
	import Touch = createjs.Touch;
	import Container = createjs.Container;
	import LayoutCreator = layout.LayoutCreator;
	import Layout = layout.Layout;

	export class GameController extends BaseController {
		public static EVENT_BACK_TO_LOBBY:string = "back_to_lobby";
		public static EVENT_HIDE_PRELOADER:string = "hide_preloader";

		private stage:Stage;
		private common:CommonRefs;
		private container:Container;
		private backgroundContainer:Container;
		private hudContainer:Container;
		private settingContainer:Container;
		private orientationView:HudView;
		private artLoaded:boolean;
		private tempLayout:Array<BaseController>;

		private statsController:StatsController = null;
		private hudController:HudController = null;
		private settingMobileController:SettingMobileController = null;

		constructor() {
			super(new ControllerManager());
		}

		/*public start(gameName:string, key:string, container:Container):void {
			super.init();

			this.container = container;
			this.common = new CommonRefs();
			this.common.gameName = gameName;
			this.common.key = key;
			this.common.isMobile = Device.isMobile();

			this.initBaseControllers();
		}*/

		public startSingle(stage:Stage, artWidth:number, artHeight:number, gameName:string, mobile:boolean, isDemo:boolean, login:string, password:string, isDebug:boolean, room:string):void {
			super.init();
			this.stage = stage;

			this.common = new CommonRefs();
			this.common.isMobile = mobile;
			this.common.gameName = this.common.isMobile ? gameName + "_mobile" : gameName;
			this.common.login = login;
			this.common.password = password;
			this.common.room = room;
			this.common.isDemo = isDemo;
			this.common.isDebug = isDebug;
			this.common.artWidth = artWidth;
			this.common.artHeight = artHeight;
			this.tempLayout = [];

			Ticker.setFPS(GameConst.GAME_FPS);

			this.container = new Container();
			this.backgroundContainer = new Container();
			this.hudContainer = new Container();
			this.settingContainer = new Container();

			if (!this.common.isMobile) {
				this.stage.enableMouseOver(Ticker.getFPS());
			}
			this.stage.mouseMoveOutside = true;
			if (Touch.isSupported()) {
				Touch.enable(this.stage, true, true);
			}
			this.stage.addChild(this.backgroundContainer);
			this.stage.addChild(this.container);
			this.stage.addChild(this.hudContainer);

			this.stage.addChild(this.settingContainer);

			Ticker.addEventListener("tick", ()=> {
				this.gameUpdate();
			});

			this.onResize();
			FullScreen.init();
			this.initBaseControllers();
		}

		public gameUpdate():void {
			this.manager.onEnterFrame();
		}

		public onEnterFrame():void {
			if (this.stage != null) {
				this.stage.update();
			}
		}

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.SERVER_INIT);
            notifications.push(NotificationList.BACK_TO_LOBBY);
            notifications.push(NotificationList.HIDE_PRELOADER);
            notifications.push(NotificationList.LAZY_LOAD_COMP);
            return notifications;
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.SERVER_INIT:
                {
                    this.initGameControllers();
					this.initKeyboardHandler();
                    break;
                }
                case NotificationList.BACK_TO_LOBBY:
                {
                    this.container.dispatchEvent(GameController.EVENT_BACK_TO_LOBBY);
                    break;
                }
                case NotificationList.HIDE_PRELOADER:
                {
                    this.stage.dispatchEvent(GameController.EVENT_HIDE_PRELOADER);
                    break;
                }
                case NotificationList.LAZY_LOAD_COMP:
                {
					this.common.is_lazy_load_compl = true;
                    break;
                }
            }
        }

		public tryInitGame():void{
			if(this.common.is_server_init && this.common.is_layout_load_compl){
				this.initGameControllers();
				this.initKeyboardHandler();
			}
		}

		private initBaseControllers():void {
			var baseController:BaseController;

			baseController = new ServerController(this.manager, this.common);
			baseController.init();

			var loaderView:LoaderView = new LoaderView();
			baseController = new LoaderController(this.manager, this.common, loaderView);
			baseController.init();

			this.backgroundContainer.addChild(loaderView);
		}

		private refreshSomeView():void {
			var baseController:BaseController;
			var hudView:HudView = this.getHud();

			console.log("refreshSomeView",hudView);
			this.statsController.setView(hudView);

			this.hudController.setView(hudView);
			this.hudController.refreshHud();
			this.hudController.setOrigin();

			this.createHudPanels(hudView);
			hudView.resizeForMobile(this.common);
		}

		private initGameControllers():void {
			var baseController:BaseController;

			this.crateHuds();
			var hudView:HudView = this.getHud();

			// create background
			var backgroundContainer:Container = new Container();
			baseController = new BackgroundController(this.manager, this.common, backgroundContainer);
			baseController.init();
			this.tempLayout.push(baseController);

			// create header
			var headerContainer:Container = new Container();
			baseController = new HeaderController(this.manager, this.common, headerContainer);
			baseController.init();

			var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
			baseController = new ReelController(this.manager, this.common, reelsView);
			baseController.init();

			// create win animation
			var winView:Container = new Container();
			baseController = new WinController(this.manager, this.common, winView);
			baseController.init();

			//create Wild Views
			baseController = new ReelWildController(this.manager, this.common, reelsView, winView);
			baseController.init();

			// create lines
			var linesView:LinesView = this.common.layouts[LinesView.LAYOUT_NAME];
			baseController = new LinesController(this.manager, this.common, linesView);
			baseController.init();

			// create bonus
			var bonusContainer:Container = new Container();
			baseController = new BonusHolderController(this.manager, this.common, bonusContainer);
			baseController.init();

			var modalContainer:Container = new Container();
			baseController = new ModalWindowController(this.manager, this.common, modalContainer);
			baseController.init();

			// error controller
			baseController = new ErrorController(this.manager, this.common, modalContainer);
			baseController.init();

			this.statsController = new StatsController(this.manager, this.common, hudView, this.container);
			this.statsController.init();

			this.hudController = new HudController(this.manager, this.common, hudView);
			this.hudController.init();

			var borderView:BorderView = this.common.layouts[BorderView.LAYOUT_NAME];
			baseController = new BorderController(this.manager, this.common, borderView);
			baseController.init();

			var nameTmpl:string = SettingMobileView.LAYOUT_NAME;
			this.settingMobileController = new SettingMobileController(this.manager, this.common, this.settingContainer, nameTmpl, this.hudContainer, this.stage);
			this.settingMobileController.init();
			this.createHudPanels(hudView);

			baseController = new SoundController(this.manager, this.common);
			baseController.init();

			var fon:GameFonView = <GameFonView>this.common.layouts[GameFonView.LAYOUT_NAME];
			fon.create();

			this.backgroundContainer.addChild(backgroundContainer);
			this.container.addChild(fon);
			this.container.addChild(reelsView);
			this.container.addChild(borderView);
			this.container.addChild(linesView);
			this.container.addChild(winView);
			//fon.scaleX = fon.scaleY = reelsView.scaleX = reelsView.scaleY = borderView.scaleX = borderView.scaleY = linesView.scaleX = linesView.scaleY = winView.scaleX = winView.scaleY = hudView.getBounds().width / borderView.getBounds().width;

			this.hudContainer.addChild(hudView);
			this.artLoaded = true;
			if (this.common.isMobile) {
				//this.onResize();
				//hudView.resizeForMobile(this.common);
			}
			else {
				hudView.setUpElements(this.common);
			}
			this.onResize();
			this.container.addChild(bonusContainer);
			this.container.addChild(headerContainer);
			this.container.addChild(modalContainer);
		}

		private createHudPanels(hudView:HudView):void {
			var baseController:BaseController;
			var autoSpinTableView:Container = <Container>hudView.getChildByName(HudView.AUTO_SPIN_TABLE_NEW);
			var autoSpinTableMask:Container = <Container>hudView.getChildByName(HudView.AUTO_SPIN_TABLE_MASK);
			if (autoSpinTableView != null) {
				baseController = new AutoSpinTableController(this.manager, this.common, autoSpinTableView, autoSpinTableMask);
				baseController.init();
				//this.tempLayout.push(baseController);
			}
			var selectBetTableView:Container = <Container>hudView.getChildByName(HudView.SELECT_BET_TABLE);
			var selectBetTableMask:Container  = <Container>hudView.getChildByName(HudView.SELECT_BET_TABLE_MASK);
			if (selectBetTableView != null) {
				baseController = new SelectBetController(this.manager, this.common, selectBetTableView, selectBetTableMask);
				baseController.init();
				//this.tempLayout.push(baseController);
			}
		}

		private createReels():void {
			//var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
			//var index:number = this.container.getChildIndex(reelsView);
			//this.container.removeChild(reelsView);
			//reelsView.onInit();
			//this.container.addChildAt(reelsView, index);

			//var weelView:WeelView = this.common.layouts[WeelView.LAYOUT_NAME];
			//var index:number = this.container.getChildIndex(weelView);
			//this.container.removeChild(weelView);
			//weelView.onInit();
			//this.container.addChildAt(weelView, index);

			this.refreshSomeView();
		}

		private initKeyboardHandler():void {
			document.onkeydown = (event)=> {
				this.send(NotificationList.KEYBOARD_CLICK, event.keyCode);
			};
		}

		private crateHuds():void {
			if (this.common.layouts != null) {
				var hudView:HudView = this.common.layouts['HudView'];
				hudView.create();
				if(this.common.isMobile){
					var hudView2:HudViewP = this.common.layouts['HudViewP'];
					hudView2.create();
				}
			}
		}

		private getHud():HudView {
			var hudView:HudView;
			if (this.orientationView != undefined){
				while (this.hudContainer.getNumChildren() > 0) {
					this.hudContainer.removeChildAt(0);
				}
			}
			if (this.common.layouts != null) {
				var name:string;
				name = (window.innerHeight > window.innerWidth && this.common.isMobile)? 'HudViewP' : 'HudView';
				hudView = this.common.layouts[name];
				this.orientationView = hudView;
				this.hudContainer.addChild(this.orientationView);
				this.common.rotateHudMode = name;
			}
			return hudView;
		}


		public onResize() {
			console.log("onResize");
			this.onResizeWindow(); //TODO: fix that function is called 2 times when going into vertical mode and 1 time when going into horizontal mode
		}

		public onResizeWindow():void{
			if (this.common.isMobile) {
				//window.scrollTo(0, 1);
				//FullScreen.init();
			}
			var windowWidth = window.innerWidth;
			var windowHeight = window.innerHeight;
			var canvas:HTMLCanvasElement = this.stage.canvas;
			var maxWidth:number = windowWidth;
			var maxHeight:number = windowHeight;
			// keep aspect ratio
			var scale:number = Math.min(maxWidth / this.common.artWidth, maxHeight / this.common.artHeight);

			canvas.width = windowWidth;
			canvas.height = windowHeight;

			if(this.common.isMobile){
				//scale *= 1.18;
				this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = scale;
				this.backgroundContainer.x = (maxWidth - this.common.artWidth * scale) / 2;
				this.backgroundContainer.y = (maxHeight - this.common.artHeight * scale) / 2;

				if (this.common.layouts != null && this.common.is_layout_load_compl) {
					var backgroundScale:number = Math.max(windowWidth / this.backgroundContainer.getBounds().width, windowHeight / this.backgroundContainer.getBounds().height);
					this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = backgroundScale;
					this.backgroundContainer.x = this.backgroundContainer.y = 0;
					scale = Math.min(windowHeight/this.common.artHeight, windowWidth/this.common.artWidth);

					this.hudContainer.scaleX = this.hudContainer.scaleY = scale;
					this.hudContainer.x = (maxWidth - this.common.artWidth * scale) / 2;

					this.container.scaleX = this.container.scaleY = scale;
					this.container.x = this.hudContainer.x + (((this.common.artWidth * scale) - (this.common.artWidth * this.container.scaleX)) / 2);

					//this.container.y = this.hudContainer.y;
					this.container.y = this.hudContainer.y = 0;
					this.hudContainer.y = this.hudContainer.y + 86*this.hudContainer.scaleY;

					if(window.innerHeight > window.innerWidth){
						scale = maxWidth / this.common.artHeight;
						this.hudContainer.x = this.settingContainer.x = this.stage.x;
						this.hudContainer.y = this.stage.y;
						this.hudContainer.scaleX = this.hudContainer.scaleY = scale;
						this.container.y = (maxHeight - this.common.artHeight * scale) / 2 + 90*this.hudContainer.scaleY;
						//this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = scale;
						//this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = 2 * Math.max(windowWidth/this.backgroundContainer.getBounds().width, windowHeight/this.backgroundContainer.getBounds().height);
					}else{
						//this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = 2 * Math.max(windowWidth/this.backgroundContainer.getBounds().width, windowHeight/this.backgroundContainer.getBounds().height);
						this.settingContainer.x = this.backgroundContainer.x = 0;
					}

					if(this.artLoaded){
						this.common.config.isRotated = true;
						this.send(NotificationList.CHANGE_DEVICE_ORIENTATION);
						this.createReels();
					}
				}
			}
			else {
				if (scale > 1 && !this.common.isMobile) {
					scale = 1;
				}
				this.stage.scaleX = scale;
				this.stage.scaleY = scale;
				this.stage.x = (maxWidth - this.common.artWidth * scale) / 2;
				this.stage.y = (maxHeight - this.common.artHeight * scale) / 2;

				this.container.scaleX = this.container.scaleY = 1 / 1.2;
				this.container.x = ((this.common.artWidth) - (this.common.artWidth * this.container.scaleX)) / 2;
			}

			if(this.common.layouts != null && this.common.is_layout_load_compl){
				this.backgroundContainer.scaleX = this.backgroundContainer.scaleY = 2 * Math.max(windowWidth / this.backgroundContainer.getBounds().width, windowHeight / this.backgroundContainer.getBounds().height);
				this.backgroundContainer.x = this.backgroundContainer.y = 0;
			}
		}

		public dispose():void {
			this.send(NotificationList.SERVER_DISCONNECT);
		}
	}
}