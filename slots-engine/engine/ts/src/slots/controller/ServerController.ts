module engine {

	export class ServerController extends BaseController {
		private demoAuthorizationUrl:string;
		private authorizationUrl:string;
		private authorizationRoom:string;
		private closeSessionUrl:string;
		private getBalanceUrl:string;
		private getBetsUrl:string;
		private spinUrl:string;
		private bonusUrl:string;
		private setWheelUrl:string;

		private common:CommonRefs;

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.RES_LOADED);
			notifications.push(NotificationList.SERVER_SEND_SPIN);
			notifications.push(NotificationList.SERVER_SEND_BONUS);
			notifications.push(NotificationList.SERVER_SET_WHEEL);
			notifications.push(NotificationList.SERVER_DISCONNECT);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.RES_LOADED:
				{
					var gameName:string = this.common.config.serverGameName;
					var baseServerUrl:string = this.common.config.serverUrl;

					this.demoAuthorizationUrl = (baseServerUrl + GameConst.DEMO_AUTHORIZATION_URL).toLowerCase();
					this.authorizationUrl = (baseServerUrl + GameConst.AUTHORIZATION_URL).toLowerCase();
					this.authorizationRoom = (baseServerUrl + GameConst.AUTHORIZATION_ROOM);
					this.getBalanceUrl = (baseServerUrl + GameConst.GET_BALANCE_URL).toLowerCase();
					this.closeSessionUrl = (baseServerUrl + GameConst.CLOSE_SESSION_URL).toLowerCase();
					this.getBetsUrl = (baseServerUrl + GameConst.GET_BETS_URL.replace("%s", gameName)).toLowerCase();
					this.spinUrl = (baseServerUrl + GameConst.SPIN_URL.replace("%s", gameName)).toLowerCase();
					this.bonusUrl = (baseServerUrl + GameConst.BONUS_URL.replace("%s", gameName)).toLowerCase();
					this.setWheelUrl = (baseServerUrl + GameConst.SET_WHEEL_URL.replace("%s", gameName)).toLowerCase();

					this.common.server = new ServerData();
					if (this.common.key == null) {
						if (this.common.isDemo) {
							this.sendDemoLoginRequest(()=> {
								this.onAuthorized();
							});
						}
						else {
							this.sendLoginRequest(this.common.login, this.common.password, this.common.room, ()=> {
								this.onAuthorized();
							});
						}
					}
					else {
						this.onAuthorized();
					}
					this.setLines();
					break;
				}
				case NotificationList.SERVER_SEND_SPIN:
				{
					this.sendSpinRequest(()=> {
						this.send(NotificationList.SERVER_GOT_SPIN);
					});
					break;
				}
				case NotificationList.SERVER_SEND_BONUS:
				{
					this.sendBonusRequest(data, ()=> {
						this.send(NotificationList.SERVER_GOT_BONUS);
					});
					break;
				}
				case NotificationList.SERVER_SET_WHEEL:
				{
					this.sendSetWheel(data);
					break;
				}
				case NotificationList.SERVER_DISCONNECT:
				{
					this.sendDisconnectRequest();
					break;
				}
			}
		}


		private sendSpinRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.spinUrl, true, {
				"multiply": this.common.server.multiply,
				"key": this.common.key,
				"bet": this.common.server.bet,
				"lineBet": this.common.server.lineCount
			});
			request.get((responseData)=> {
				var spinData:any = XMLUtils.getElement(responseData, "spin");
				this.parseSpinData(spinData);
				this.parseBonusData(spinData);
				onComplete != null && onComplete();
			});
		}

		private sendBonusRequest(params:any, onComplete:Function):void {
			params.key = this.common.key;
			params.bonus = this.common.server.bonus.key;
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.bonusUrl, true, params);
			request.get((responseData)=> {
				var error:any = XMLUtils.getElement(responseData, "error");
				if (error == null) {
					console.log(responseData);
					this.common.server.bonus.data = XMLUtils.getElement(responseData, "bonus");
					onComplete != null && onComplete();
				}
				else {
					throw new Error("ERROR: " + error.textContent);
				}
			});
		}

		private sendSetWheel(params:Array<Array<number>>):void {
			var XML:any = document.createElement("data");
			for (var i:number = 0; i < params.length; i++) {
				var column:any = document.createElement("i");
				column.setAttribute("id", i.toString());
				column.innerHTML = params[i].join(",");
				XML.appendChild(column);
			}
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.setWheelUrl, true, {"key": this.common.key, "wheel": XML.innerHTML});
			request.get();
		}

		private sendDemoLoginRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.JSON, this.demoAuthorizationUrl, true, null);
			request.get((responseData)=> {
				this.common.key = responseData.key;
				console.log("session key: " + this.common.key);
				onComplete();
			});
		}

		private sendLoginRequest(login:string, password:string, room:string, onComplete:Function):void {
			var passwordMD5:string = CryptoJS.MD5(password).toString();
			var roomUrl = (room && room != null);
			if(roomUrl){
				if(room != '-1'){
					passwordMD5 = '*' + CryptoJS.SHA1(CryptoJS.SHA1(password));
				}
				var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.authorizationRoom, true, {"login": login, "password": passwordMD5, "room":room});
			}
			else {
				var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.authorizationUrl, true, {"login": login, "password": passwordMD5});
			}

			request.get((responseData)=> {
				var result:any = responseData.documentElement.textContent;
				switch (result) {
					case "1001":
					{
						alert("User already authorized! Please relogin!");
						throw new Error("ERROR: User already authorized!");
					}
				}
				if(roomUrl){
					result = XMLUtils.getSessionRoom(responseData, "key");
				}
				if (result == null || result.length == 0) {
					alert("Login or password is incorrect! Please relogin!");
					throw new Error("ERROR: Login or password is incorrect");
				}
				this.common.key = result;
				console.log("session key: " + this.common.key);
				onComplete();
			});
		}

		private sendGetBalanceRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.getBalanceUrl, true, {key: this.common.key});
			request.get((responseData)=> {
				this.common.server.setBalance(parseFloat(responseData.documentElement.textContent));
				onComplete != null && onComplete();
			});
		}

		private sendGetBetsRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.getBetsUrl, true, {key: this.common.key});
			request.get((responseData)=> {
				var bets:any = XMLUtils.getElement(responseData, "bets");
				this.common.server.jackpot = XMLUtils.getAttributeFloat(bets, "jackpot");
				this.common.server.bet = XMLUtils.getAttributeFloat(bets, "bet");
				this.common.server.multiply = XMLUtils.getAttributeInt(bets, "mp");
				this.common.server.lineCount = XMLUtils.getAttributeInt(bets, "lines");
				this.common.server.maxLineCount = XMLUtils.getAttributeInt(bets, "maxlines");
				this.common.server.wheel = Utils.toIntArray(XMLUtils.getAttribute(bets, "wheel").split(","));
				this.common.server.availableMultiples = Utils.toIntArray(XMLUtils.getAttribute(bets, "multiply").split(","));
				this.common.server.availableBets = [];
				var betsData:any = XMLUtils.getElements(bets, "item");
				for (var i:number = 0; i < betsData.length; i++) {
					var bet:any = betsData[i].childNodes[0].data;
					this.common.server.availableBets.push(parseFloat(bet));
				}
				this.parseBonusData(bets);
				onComplete != null && onComplete();
			});
		}

		private sendDisconnectRequest():void {
			if (this.common.key != null) {
				var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.closeSessionUrl, false, {key: this.common.key});
				request.get();
			}
		}

		private onAuthorized():void {
			this.sendGetBalanceRequest(() => {
				this.sendGetBetsRequest(()=> {
					this.send(NotificationList.SERVER_INIT);
				});
			});
		}

		private findBonusConfig(bonusId:number):void {
			var bonuses:Array<any> = <Array<any>>this.common.config.bonuses;
			for (var i:number = 0; i < bonuses.length; i++) {
				var bonus:any = bonuses[i];
				if (bonus.id == bonusId) {
					return bonus;
				}
			}
			console.log("ERROR: No bonus in config id = " + bonusId);
		}

		private parseSpinData(data:any):void {
			console.log(data, "parseSpinData");
			var wheelsData:any = XMLUtils.getElement(data, "wheels").childNodes;
			for (var i:number = 0; i < wheelsData.length; i++) {
				var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
				for (var j:number = 0; j < wheelData.length; j++) {
					this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
				}
			}
			var winLinesData:any = XMLUtils.getElement(data, "winposition").childNodes;
			this.common.server.winLines = [];
			this.common.server.arrLinesIds = [];
			for (var i:number = 0; i < winLinesData.length; i++) {
				var winLine:any = winLinesData[i];
				var winLineVO:WinLineVO = new WinLineVO();
				winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
				winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
				winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
				this.common.server.winLines.push(winLineVO);
				this.common.server.arrLinesIds.push(winLineVO.lineId);
			}
			this.common.server.setBalance(parseFloat(XMLUtils.getElement(data, "balance").textContent));
			this.common.server.win = parseFloat(XMLUtils.getElement(data, "win").textContent);
		}

		private parseBonusData(data:any):void {
			this.common.server.bonus = null;
			var bonusData:any = XMLUtils.getElement(data, "bonus");
			if (bonusData != null && bonusData.attributes.length > 0) {
				var step:number, restoreBonus:any;
				var bonusId:number = XMLUtils.getAttributeInt(bonusData, "id");
				var bonusConfig:any = this.findBonusConfig(bonusId);
				if (bonusConfig == null) {
					return;
				}

				var keyData:string = XMLUtils.getAttribute(bonusData, "key");
				var last:any = XMLUtils.getElement(bonusData, "last");
				if (last != null) {
					restoreBonus = XMLUtils.getElement(last, "bonus");
					step = XMLUtils.getAttributeInt(bonusData, "step") - 1;
				}
				var bonusVO:BonusVO = new BonusVO();
				bonusVO.id = bonusId;
				bonusVO.key = keyData != null ? keyData : bonusData.textContent;
				bonusVO.step = step;
				bonusVO.type = bonusConfig.type;
				bonusVO.className = bonusConfig.className;
				bonusVO.data = restoreBonus != null ? restoreBonus : null;

				this.common.server.bonus = bonusVO;
			}
		}

		public dispose():void {
			super.dispose();
			this.common = null;
		}

		private setLines():void {
			this.common.config.allLines = [[1, 1, 1, 1, 1],[0, 0, 0, 0, 0],[2, 2, 2, 2, 2],[0, 1, 2, 1, 0],[2, 1, 0, 1, 2],[1, 0, 0, 0, 1],[1, 2, 2, 2, 1],[0, 0, 1, 2, 2],[2, 2, 1, 0, 0],[1, 2, 1, 0, 1],
				[1, 0, 1, 2, 1],[0, 1, 1, 1, 0],[2, 1, 1, 1, 2],[0, 1, 0, 1, 0],[2, 1, 2, 1, 2],[1, 1, 0, 1, 1],[1, 1, 2, 1, 1],[0, 0, 2, 0, 0],[2, 2, 0, 2, 2],[0, 2, 2, 2, 0],
				[2, 0, 0, 0, 2],[1, 2, 0, 2, 1],[1, 0, 2, 0, 1],[0, 2, 0, 2, 0],[2, 0, 2, 0, 2],[0, 2, 1, 0, 2],[2, 0, 1, 2, 0],[1, 0, 2, 1, 2],[0, 2, 1, 2, 0],[2, 1, 0, 0, 1],
				[0, 1, 2, 2, 1],[1, 0, 1, 0, 1],[1, 2, 1, 2, 1],[0, 1, 0, 1, 2],[2, 1, 2, 0, 0],[2, 0, 0, 1, 2],[1, 2, 2, 0, 0],[0, 0, 1, 1, 2],[2, 2, 0, 1, 0],[0, 0, 2, 2, 2],
				[2, 2, 0, 0, 0],[1, 2, 0, 1, 2],[1, 0, 2, 1, 0],[0, 1, 2, 1, 1],[2, 1, 0, 2, 1],[1, 2, 1, 0, 0],[1, 0, 1, 2, 2],[2, 2, 1, 2, 2],[0, 0, 1, 0, 0],[2, 1, 2, 0, 1]];
		}
	}
}